package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.ComponenteDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class ComponenteDAOImpl extends GenericDAOImpl<Componente> implements ComponenteDAOLocal {

	private static final Logger logger = Logger.getLogger(ComponenteDAOImpl.class);
	
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;
	
	@Override
	public ReturnObject insertar(Componente prmT) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public Boolean actualizar(Componente prmT) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public Boolean eliminar(Componente prmT) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Componente> listar(Componente prmT)
			throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public Componente buscarXId(Componente prmT) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Componente> listarBandejaPorTipoConsultaExt(final String tipoConsExt) throws PersistenciaException {
		final List<Componente> lstComponente = new ArrayList<Componente>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_BANDJ_X_TIP_CONS_EXT( ?, ? ) }");
						
						cstm.setString("P_TIPO_CONSULTA_EXTERNA", tipoConsExt);

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Componente componente = new Componente();
							componente.setId(rs.getLong("COMPONENTE_ID"));
							componente.setNombreInterno(rs.getString("NOMBRE_INTERNO"));
							componente.setNombreExterno(rs.getString("NOMBRE_EXTERNO"));
							
							lstComponente.add(componente);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:PerfilDAOImpl:listarBandejaPorTipoConsultaExt " + e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PerfilDAOImpl:listarBandejaPorTipoConsultaExt " + e);
		}
		return lstComponente;
	}

}

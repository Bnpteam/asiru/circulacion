package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto;

import javax.ejb.Remote;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;

@Remote
public interface SolicitudServiceRemote extends GenericService<Solicitud> {
	
	//public ReturnObject actualizar2(Solicitud prmT) throws ServicioException;

}

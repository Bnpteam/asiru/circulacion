package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteVerRespuestas;
import pe.gob.servir.sistemas.transparencia.model.negocio.SolicitudMovimiento;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Remote
public interface MovimientoSolicitudServiceRemote extends GenericService<SolicitudMovimiento>{
	
	public ReturnObject insertarMultiDesdeJefe(SolicitudMovimiento prmObj) throws ServicioException;
	
	public ReturnObject validarCodigoFormulario(String codigo) throws ServicioException;
	
	public List<SolicitudMovimiento> lstLinTiempoXAreas1(SolicitudMovimiento tmpObj, String mostrarEstados) throws ServicioException;
	
	public List<SolicitudMovimiento> lstLinTiempoXAreas2(SolicitudMovimiento tmpObj, String mostrarEstados) throws ServicioException;
	
	public List<ReporteVerRespuestas> lstVerRespuestas(SolicitudMovimiento tmpObj) throws ServicioException;

}

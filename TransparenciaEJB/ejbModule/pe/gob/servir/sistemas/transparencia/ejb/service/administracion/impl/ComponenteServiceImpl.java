package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.ComponenteDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local.ComponenteServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.ComponenteServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Componente;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Stateless
public class ComponenteServiceImpl implements ComponenteServiceRemote, ComponenteServiceLocal {

	@EJB
	private ComponenteDAOLocal componenteDAOLocal;
	
	@Override
	public ReturnObject insertar(Componente prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Boolean actualizar(Componente prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Boolean eliminar(Componente prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<Componente> listar(Componente prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Componente buscarXId(Componente prmT) throws ServicioException {
		// 
		return null;
	}

	public ComponenteDAOLocal getComponenteDAOLocal() {
		return componenteDAOLocal;
	}

	public void setComponenteDAOLocal(ComponenteDAOLocal componenteDAOLocal) {
		this.componenteDAOLocal = componenteDAOLocal;
	}

	@Override
	public List<Componente> listarBandejaPorTipoConsultaExt(String tipoConsExt) throws ServicioException {
		try {
			return this.getComponenteDAOLocal().listarBandejaPorTipoConsultaExt(tipoConsExt);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	

}

package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.UsuarioDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class UsuarioDAOImpl extends GenericDAOImpl<Usuario> implements UsuarioDAOLocal {

	private static final Logger logger = Logger.getLogger(UsuarioDAOImpl.class);
	
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;
	
	@Override
	public ReturnObject insertar(final Usuario prmObj) throws PersistenciaException {
		
		ReturnObject result = new ReturnObject();
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_INSERTAR_TBL_USUARIO"
								+ " ( ?, ?, ?, ?, ?, ?, ?) }");
						
						cstm.registerOutParameter("P_USUARIO_ID", OracleTypes.NUMBER);	
						cstm.setString("P_USUARIO", prmObj.getUsuario());
						cstm.setString("P_NOMBRE_COMPLETO", prmObj.getNombreCompleto());
						cstm.setLong("P_PERFIL_ID", prmObj.getPerfil().getId());
						cstm.setString("P_T01_UNIDAD_ORGANICA", prmObj.getIdUnidadOrganica());
						cstm.setLong("P_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());
						cstm.setString("P_CORREO", prmObj.getCorreo());
						//cstm.setString("P_DNI", prmObj.getNroDNI());
						cstm.execute();
						
						BigDecimal decId= new BigDecimal(0);
						Object object=cstm.getObject("P_USUARIO_ID");
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Integer id = decId.intValue();
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						logger.error("SQLException:UsuarioDAOImpl:insert "+e.getMessage());
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:UsuarioDAOImpl:insert "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setSw(prmObj.getResultTransaction());	
		return result;	
										
	}

	@Override
	public Boolean actualizar(final Usuario prmObj) throws PersistenciaException {
		
		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_ACTUALIZAR_TBL_USUARIO"
								+ "( ?, ?, ?, ?, ?, ?, ?, ? ) }");
						
						cstm.setLong("P_USUARIO_ID", prmObj.getId());
						cstm.setString("P_USUARIO", prmObj.getUsuario());
						cstm.setString("P_NOMBRE_COMPLETO", prmObj.getNombreCompleto());

						cstm.setLong("P_PERFIL_ID", prmObj.getPerfil().getId());
						cstm.setString("P_T01_UNIDAD_ORGANICA", prmObj.getIdUnidadOrganica());
										
						cstm.setLong("P_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());
						
						cstm.setString("P_CORREO", prmObj.getCorreo());

						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);
																									
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	
								
					} catch (SQLException e) {
						cn.rollback();
						e.printStackTrace();
						logger.error("SQLException:UsuarioDAOImpl:actualizar "+e.getMessage());
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:UsuarioDAOImpl:actualizar "+e.getMessage());
		}
		
		sw=prmObj.getResultTransaction();
		return sw;

	}

	@Override
	public Boolean eliminar(final Usuario prmObj) throws PersistenciaException {

		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_ELIMINAR_TBL_USUARIO"
								+ "(?, ?, ?) }");
						
						
						cstm.setLong("P_USUARIO_ID", prmObj.getId());
						
						cstm.setLong("P_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());

						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);
																									
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {

							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	

												
					} catch (SQLException e) {
						e.printStackTrace();
						cn.rollback();
						logger.error("SQLException:UsuarioDAOImpl:eliminar "+e.getMessage());
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:UsuarioDAOImpl:eliminar "+e.getMessage());
		}
		
		sw=prmObj.getResultTransaction();
		
		return sw;
	
	}

	@Override
	public List<Usuario> listar(final Usuario prmObj) throws PersistenciaException {
		
		final List<Usuario> lstUsuario = new ArrayList<Usuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCARXCRIMUL_TBL_USUARIO"
								+ "( ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setString("P_USUARIO", prmObj.getUsuario());
						cstm.setLong("P_PERFIL_ID", prmObj.getPerfil().getId());
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {							
							
							Usuario usuario= new Usuario();

							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));
							//usuario.setClave(rs.getString("CLAVE"));
							
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							Perfil perfil=new Perfil();
							perfil.setNombre(rs.getString("PERFIL_NOMBRE"));
							usuario.setPerfil(perfil);

							lstUsuario.add(usuario);
						}
					} catch (SQLException e) {
						logger.error("SQLException:UsuarioDAOImpl:listar "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:listar "+e.getMessage());
		}

		return lstUsuario;
		
	}
	
	@Override
	public List<Usuario> listarUsuarioXPerfilXArea(final Usuario prmObj) throws PersistenciaException {
		
		final List<Usuario> lstUsuario = new ArrayList<Usuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCAR_XRESPUO_XUNDORG"
								+ "( ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String undOrg = prmObj.getIdUnidadOrganica();
						cstm.setString("P_T01_UNIDAD_ORGANICA", undOrg); //Hace referencia al perfil Id de los Responsables UO.
						//cstm.setLong("P_PERFIL_ID", prmObj.getPerfil().getId()); //Es el perfil del usuario actual.
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {							
							
							Usuario usuario= new Usuario();

							usuario.setId(rs.getLong("R_USUARIO_ID"));
							usuario.setUsuario(rs.getString("R_USUARIO"));
							//usuario.setClave(rs.getString("CLAVE"));
							
							usuario.setNombreCompleto(rs.getString("R_NOMBRE_COMPLETO"));
							Perfil perfil=new Perfil();
							perfil.setId(rs.getLong("R_PERFIL_ID"));
							perfil.setNombre(rs.getString("R_PERFIL_NOMBRE"));
							usuario.setPerfil(perfil);

							lstUsuario.add(usuario);
						}
					} catch (SQLException e) {
						logger.error("SQLException:UsuarioDAOImpl:listarUsuarioXPerfilXArea "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:listarUsuarioXPerfilXArea "+e.getMessage());
		}

		return lstUsuario;
		
	}

	@Override
	public Usuario buscarXId(final Usuario prmObj) throws PersistenciaException {
		final Usuario usuario = new Usuario();
		
		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCARXID_TBL_USUARIO( ?, ?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("P_USUARIO_ID", prmObj.getId());
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {							

							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));							
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							Perfil perfil=new Perfil();
							perfil.setId(rs.getLong("PERFIL_ID"));
							usuario.setPerfil(perfil);
							
							//usuario.setSituacion(rs.getString("SITUACION"));
							usuario.setIdUnidadOrganica(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							usuario.setCorreo(rs.getString("CORREO"));
							//usuario.setNroDNI(rs.getString("DNI"));
							usuario.setSwDisponible(rs.getString("DISPONIBLE").equals(String.valueOf('1')) ? true : false);
						}
					} catch (SQLException e) {
						e.printStackTrace();
						logger.error("SQLException:UsuarioDAOImpl:buscarXId "+e.getMessage());
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("SQLException:UsuarioDAOImpl:buscarXId "+e.getMessage());
		}

		return usuario;
	}

	/*Valida si usuario existe en la tabla TBL_USUARIOS del Sistema de Consultas Externas posterior a su validaci�n en el LDAP*/
	@Override
	public Usuario validarAcceso(final Usuario prmObj) throws PersistenciaException {
		
		final Usuario usuario = new Usuario();
		/*
		if(prmObj.getUsuario().equals("ADMIN") && prmObj.getClave().equals("QY594xutGq4=")) {
			usuario.setId(1L);
			usuario.setUsuario("ADMIN");
			usuario.setClave("QY594xutGq4=");			// 1
			usuario.setNombreCompleto("Luis Herrera");
			usuario.setPerfil("Administrador");
		}
		*/
				
		try {
			
			Session hbSession = em.unwrap(Session.class); 
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					
					ResultSet rs = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{call PKG_ADMINISTRACIN_MANTENIMIENT.SP_VALIDAR_ACCESO_TBL_USUARIO"
								+ " (?, ?) }");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_USUARIO", prmObj.getUsuario());
						//cstm.setString("P_CLAVE", prmObj.getClave());
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							usuario.setCorreo(rs.getString("CORREO"));
							usuario.setIdUnidadOrganica(rs.getString("R_T01_UNIDAD_ORGANICA"));
							usuario.setPerfil(new Perfil());
							usuario.getPerfil().setId(rs.getLong("PERFIL_ID"));
							usuario.getPerfil().setNombre(rs.getString("PERFIL_NOMBRE"));
							
							/*AGREGAR MAS CAMPOS*/
						}	
					} catch (SQLException e) {
						e.printStackTrace();
						logger.error("SQLException:UsuarioDAOImpl:validarAcceso "+e.getMessage());
					}
					finally {
						Conexion.closeSqlConnections(rs, cstm, cn);
					}
				}
			});
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:validarAcceso "+e.getMessage());
		}
		
		return usuario;
	}

	@Override
	public Boolean cambiarClave(final Usuario prmObj) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public ReturnObject validarUsuario(final Usuario prmUsuario) throws PersistenciaException {
		
		final ReturnObject returnObject = new ReturnObject();
		
		try {
			
			Session hbSession = em.unwrap(Session.class); 
			
			hbSession.doWork(new Work() {

				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					
					ResultSet rs = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{call PKG_ADMINISTRACIN_MANTENIMIENT.SP_VALIDARUSUARIO_TBL_USUARIO"
								+ " ( ?, ?) }");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_USUARIO", prmUsuario.getUsuario());
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						returnObject.setMsg("0");
						while (rs.next()) {
							returnObject.setMsg(rs.getString("TIPO_USUARIO"));
						}
						
					} catch (SQLException e) {
						e.printStackTrace();
						logger.error("SQLException:UsuarioDAOImpl:validarUsuario "+e.getMessage());
					}
					finally {
						Conexion.closeSqlConnections(rs, cstm, cn);
					}
				}
			});
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:validarUsuario "+e.getMessage());
		}
		
		return returnObject;

	}

	@Override
	public Usuario obtenerUsuarioAsignado(final String prmBandeja) throws PersistenciaException {
		final Usuario usuario = new Usuario();
			
		try {
			
			Session hbSession = em.unwrap(Session.class); 
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					
					ResultSet rs = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{call PKG_ADMINISTRACIN_MANTENIMIENT.SP_OBTENER_USER_ASIGN_USUARIO"
								+ " (?, ?) }");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_BANDEJA", prmBandeja);
						//cstm.setString("P_CLAVE", prmObj.getClave());
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							usuario.setSwDisponible(rs.getString("DISPONIBLE").equals(String.valueOf('1')) ? true : false);
							
							usuario.setPerfil(new Perfil());
							usuario.getPerfil().setId(rs.getLong("PERFIL_ID"));
							usuario.getPerfil().setNombre(rs.getString("PERFIL_NOMBRE"));
							
							/*AGREGAR MAS CAMPOS*/
							
						}	
					} catch (SQLException e) {
						e.printStackTrace();
						logger.error("SQLException:UsuarioDAOImpl:obtenerUsuarioAsignado "+e.getMessage());
					}
					finally {
						Conexion.closeSqlConnections(rs, cstm, cn);
					}
				}
			});
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:obtenerUsuarioAsignado "+e.getMessage());
		}
		
		return usuario;
	}

	@Override
	public List<Usuario> listarUsuariosPorPerfil(final Perfil prmPerfil) throws PersistenciaException {
		final List<Usuario> lstUsuario = new ArrayList<Usuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_USRS_X_PERFIL_USUARS( ?, ? ) }");
						
						//cstm.setLong("P_PERFIL_ID", prmUsuario.getPerfil().getId());
						
						cstm.setLong("P_PERFIL_ID", prmPerfil.getId());

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Usuario usuario = new Usuario();
							
							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							usuario.setSwDisponible(rs.getString("DISPONIBLE").equals(String.valueOf('1')) ? true : false);

							if(!usuario.getSwDisponible()){
								usuario.setNombreCompleto("(*) " + usuario.getNombreCompleto());
							}
							
							usuario.setPerfil(new Perfil());
							usuario.getPerfil().setId(rs.getLong("PERFIL_ID"));
							usuario.getPerfil().setNombre(rs.getString("PERFIL_NOMBRE"));
							
							
							lstUsuario.add(usuario);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:UsuarioDAOImpl:listarUsuariosPorPerfil " + e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:listarUsuariosPorPerfil " + e);
		}
		return lstUsuario;
	}

	

	@Override
	public List<Usuario> listarUsuariosDispPorPerfil(final Perfil prmPerfil) throws PersistenciaException {
		
		final List<Usuario> lstUsuario = new ArrayList<Usuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_USRSDIS_X_PERFL_USRS( ?, ? ) }");
						
						//cstm.setLong("P_PERFIL_ID", prmUsuario.getPerfil().getId());
						
						cstm.setLong("P_PERFIL_ID", prmPerfil.getId());

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Usuario usuario = new Usuario();
							
							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							usuario.setSwDisponible(rs.getString("DISPONIBLE").equals(String.valueOf('1')) ? true : false);
							
							usuario.setPerfil(new Perfil());
							usuario.getPerfil().setId(rs.getLong("PERFIL_ID"));
							usuario.getPerfil().setNombre(rs.getString("PERFIL_NOMBRE"));
							
							
							lstUsuario.add(usuario);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:UsuarioDAOImpl:listarUsuariosPorPerfil " + e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:listarUsuariosPorPerfil " + e);
		}
		
		return lstUsuario;
		
	}

	@Override
	public Boolean consultarExistencia(final Usuario prmUsuario) throws PersistenciaException {
		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_CONSULTAR_EXISTENC_USUARIO"
								+ " ( ?, ? ) }");
					
						cstm.setString("P_USUARIO", prmUsuario.getUsuario());
						cstm.registerOutParameter("P_RETORNO", OracleTypes.CHAR);

						cstm.execute();
						
						Object object = cstm.getObject("P_RETORNO");
						

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret > 0)) {
								prmUsuario.setResultTransaction(true);
								//cn.commit();
							}else{			
								//cn.rollback();
								prmUsuario.setResultTransaction(false);
							}							
							
						}else{
							//cn.rollback();
							prmUsuario.setResultTransaction(false);
						}	
					
												
					} catch (SQLException e) {
						logger.error("SQLException:UsuarioDAOImpl:consultarExistencia "+e.getMessage());
						e.printStackTrace();
						cn.rollback();
						prmUsuario.setResultTransaction(false);
						prmUsuario.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmUsuario.setResultTransaction(false);
			prmUsuario.setStackException(e,true);
			throw new PersistenciaException("Exception:UsuarioDAOImpl:consultarExistencia "+e);
		}		
		
		result = prmUsuario.getResultTransaction();	
		
		return result;
	}

	@Override
	public List<Usuario> listarTodos() throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Usuario> listarUsuariosPorBandeja(final String bandeja) throws PersistenciaException {
		final List<Usuario> lstUsuario = new ArrayList<Usuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_USRS_X_BANDEJ_USUARS( ?, ? ) }");
						
						cstm.setString("P_BANDEJA", bandeja);

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Usuario usuario = new Usuario();
							
							usuario.setId(rs.getLong("USUARIO_ID"));
							usuario.setUsuario(rs.getString("USUARIO"));
							usuario.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							usuario.setSwDisponible(rs.getString("DISPONIBLE").equals(String.valueOf('1')) ? true : false);
							
							if(!usuario.getSwDisponible()){
								usuario.setNombreCompleto("(*) " + usuario.getNombreCompleto());
							}
							
							usuario.setPerfil(new Perfil());
							usuario.getPerfil().setId(rs.getLong("PERFIL_ID"));
							usuario.getPerfil().setNombre(rs.getString("PERFIL_NOMBRE"));
							
							lstUsuario.add(usuario);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:UsuarioDAOImpl:listarUsuariosPorBandeja " + e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("SQLException:UsuarioDAOImpl:listarUsuariosPorBandeja " + e);
		}
		return lstUsuario;
	}

	@Override
	public List<Usuario> listarUsuarioXPerfilXAreaAlternativo(final Usuario prmObj)
			throws PersistenciaException {

		
		final List<Usuario> lstUsuario = new ArrayList<Usuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_BUSCAR_XRESPUO_XUNDORG_ALTR"
								+ "( ?, ? , ?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String undOrg = prmObj.getIdUnidadOrganica();
						cstm.setString("P_T01_UNIDAD_ORGANICA", undOrg); //Hace referencia al perfil Id de los Responsables UO.
						cstm.setLong("P_PERFIL_ID", prmObj.getPerfil().getId()); //Es el perfil del usuario actual.
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {							
							
							Usuario usuario= new Usuario();

							usuario.setId(rs.getLong("R_USUARIO_ID"));
							usuario.setUsuario(rs.getString("R_USUARIO"));
							//usuario.setClave(rs.getString("CLAVE"));
							
							usuario.setNombreCompleto(rs.getString("R_NOMBRE_COMPLETO"));
							Perfil perfil=new Perfil();
							perfil.setId(rs.getLong("R_PERFIL_ID"));
							perfil.setNombre(rs.getString("R_PERFIL_NOMBRE"));
							usuario.setPerfil(perfil);

							lstUsuario.add(usuario);
						}
					} catch (SQLException e) {
						logger.error("SQLException:UsuarioDAOImpl:listarUsuarioXPerfilXAreaAlternativo "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:UsuarioDAOImpl:listarUsuarioXPerfilXAreaAlternativo "+e.getMessage());
		}

		return lstUsuario;		
	
	}

}
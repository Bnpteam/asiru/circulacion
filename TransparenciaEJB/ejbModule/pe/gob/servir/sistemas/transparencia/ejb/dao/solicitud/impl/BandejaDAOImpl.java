package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.BandejaDAOLocal;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class BandejaDAOImpl  extends GenericDAOImpl<ReporteBandeja> implements BandejaDAOLocal{

	private static final Logger logger = Logger.getLogger(BandejaDAOImpl.class);	
	@PersistenceContext(unitName = "PUCirculacion")
	
	private EntityManager em;
	@Override
	public ReturnObject insertar(ReporteBandeja prmT)
			throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public Boolean actualizar(ReporteBandeja prmT) throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public Boolean eliminar(ReporteBandeja prmT) throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public List<ReporteBandeja> listar(ReporteBandeja prmT)
			throws PersistenciaException {
		// 
		return null;
	}
	@Override
	public ReporteBandeja buscarXId(ReporteBandeja prmT)
			throws PersistenciaException {
		// 
		return null;
	}

	//SECRETARIA OAJ
	@Override
	public List<ReporteBandeja> lstReporteBandejaSecretariaOAJ(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_SECR_OAJ" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						Long varEstSolId = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", varEstSolId);
						Long usuarioResp = tmpRptBandeja.getUsuario_id_responsable();
						cstm.setLong("P_USUARIO_ID_RECEPTOR", usuarioResp);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
												
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+e.getMessage());
		}

		return listarpt;
	}
	
	// ASISTENTE OAJ
	@Override
	public List<ReporteBandeja> lstReporteBandejaAsistenteOAJ(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_ASIST_OAJ" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						Long varEstSolId = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", varEstSolId);
						Long usuarioResp = tmpRptBandeja.getUsuario_id_responsable();
						cstm.setLong("P_USUARIO_ID_RECEPTOR", usuarioResp);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setVersion_solicitud(rs.getInt("R_VERSION_SOLICITUD"));
							
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setFechaIngreso(rs.getDate("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setCorreo(rs.getString("R_CORREO"));
							rpt.setAsunto(rs.getString("R_ASUNTO"));
							rpt.setDetalle_respuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalle_asignacion(rs.getString("R_DETALLE_ASIGNACION"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							rpt.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
							
								
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+e.getMessage());
		}

		return listarpt;
	}
	
	// JEFE OAJ
	@Override
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_JEFE_OAJ" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						//Long varSitSolId = tmpRptBandeja.getSituacion_solicitud_id();
						//cstm.setLong("P_SITUACION_SOLICITUD_ID", varSitSolId);
						Long varEstSolId = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", varEstSolId);
						Long usuarioResp = tmpRptBandeja.getUsuario_id_responsable();
						cstm.setLong("P_USUARIO_ID_RECEPTOR", usuarioResp);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setVersion_solicitud(rs.getInt("R_VERSION_SOLICITUD"));
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setCorreo(rs.getString("R_CORREO"));
							rpt.setAsunto(rs.getString("R_ASUNTO"));
							rpt.setDetalle_respuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalle_asignacion(rs.getString("R_DETALLE_ASIGNACION"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							rpt.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
											
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandeja "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandeja "+e.getMessage());
		}

		return listarpt;
	}
	// Historial de solicitudes derivadas �reas.
	@Override
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ2(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_JEFE_OAJ2" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						//Long varSitSolId = tmpRptBandeja.getSituacion_solicitud_id();
						//cstm.setLong("P_SITUACION_SOLICITUD_ID", varSitSolId);
						Long varEstSolId = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", varEstSolId);
						Long usuarioResp = tmpRptBandeja.getUsuario_id_responsable();
						cstm.setLong("P_USUARIO_ID_RECEPTOR", usuarioResp);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setVersion_solicitud(rs.getInt("R_VERSION_SOLICITUD"));
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setCorreo(rs.getString("R_CORREO"));
							rpt.setAsunto(rs.getString("R_ASUNTO"));
							rpt.setDetalle_respuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalle_asignacion(rs.getString("R_DETALLE_ASIGNACION"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							rpt.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
											
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandeja "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandeja "+e.getMessage());
		}

		return listarpt;
	}
	
	// SECREATARIA UO
	@Override
	public List<ReporteBandeja> lstReporteBandejaSecretariaUO(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_SECRET_UO" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						/*
						Long varSitSolId = tmpRptBandeja.getSituacion_solicitud_id();
						cstm.setLong("P_SITUACION_SOLICITUD_ID", varSitSolId);*/
						Long varEstSolId = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", varEstSolId);
						Long usuarioResp = tmpRptBandeja.getUsuario_id_responsable();
						cstm.setLong("P_USUARIO_ID_RECEPTOR", usuarioResp);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setCorreo(rs.getString("R_CORREO"));
							rpt.setAsunto(rs.getString("R_ASUNTO"));
							rpt.setDetalle_respuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalle_asignacion(rs.getString("R_DETALLE_ASIGNACION"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							rpt.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
							
												
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandejaSecretariaUO "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaSecretariaUO "+e.getMessage());
		}

		return listarpt;
	}
	// RESPONSABLE UO
	@Override
	public List<ReporteBandeja> lstReporteBandejaResponsableUO(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_RESPNS_UO" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						//Long varSitSolId = tmpRptBandeja.getSituacion_solicitud_id();
						//cstm.setLong("P_SITUACION_SOLICITUD_ID", varSitSolId);
						Long varEstSolId = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", varEstSolId);
						Long usuarioResp = tmpRptBandeja.getUsuario_id_responsable();
						cstm.setLong("P_USUARIO_ID_RECEPTOR", usuarioResp);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setCorreo(rs.getString("R_CORREO"));
							rpt.setAsunto(rs.getString("R_ASUNTO"));
							rpt.setDetalle_respuesta(rs.getString("R_DETALLE_RESPUESTA"));
							rpt.setDetalle_asignacion(rs.getString("R_DETALLE_ASIGNACION"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							rpt.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
							
										
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandejaSecretariaUO "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaSecretariaUO "+e.getMessage());
		}

		return listarpt;
	}
	// LISTAR ULTIMO ENVIO DE MSJ
	@Override
	public ReporteBandeja lstUltimoEnvioMsj(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final ReporteBandeja prmReporteBanj = new ReporteBandeja();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_ULTM_RPT_ENV_MSJ" + "( ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						Long solTransId = tmpRptBandeja.getSolicitud_transparencia_id();
						cstm.setLong("P_SOLICITUD_TRANSPARENCIA_ID", solTransId);
						Long estSol = tmpRptBandeja.getEstado_solicitud_id();
						cstm.setLong("P_ESTADO_SOLICITUD_ID", estSol);

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							prmReporteBanj.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							prmReporteBanj.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							prmReporteBanj.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							prmReporteBanj.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							prmReporteBanj.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							prmReporteBanj.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							prmReporteBanj.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							prmReporteBanj.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							prmReporteBanj.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							prmReporteBanj.setNombres(rs.getString("R_NOMBRES"));
							prmReporteBanj.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							prmReporteBanj.setCoddep(rs.getString("R_CODDEP"));
							prmReporteBanj.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							prmReporteBanj.setCodpro(rs.getString("R_CODPRO"));
							prmReporteBanj.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							prmReporteBanj.setCoddis(rs.getString("R_CODDIS"));
							prmReporteBanj.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							prmReporteBanj.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							prmReporteBanj.setCelular(rs.getString("R_CELULAR"));
							prmReporteBanj.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							prmReporteBanj.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							prmReporteBanj.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							prmReporteBanj.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							prmReporteBanj.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							prmReporteBanj.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							prmReporteBanj.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							prmReporteBanj.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							prmReporteBanj.setObservacion(rs.getString("R_OBSERVACION"));
							prmReporteBanj.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							prmReporteBanj.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							prmReporteBanj.setEstado(rs.getString("R_ESTADO"));
							prmReporteBanj.setSituacion(rs.getString("R_SITUACION"));
							prmReporteBanj.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							prmReporteBanj.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							prmReporteBanj.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							prmReporteBanj.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							prmReporteBanj.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							prmReporteBanj.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							prmReporteBanj.setCodigo(rs.getString("R_CODIGO"));
							prmReporteBanj.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							prmReporteBanj.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							prmReporteBanj.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							prmReporteBanj.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							prmReporteBanj.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							prmReporteBanj.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							prmReporteBanj.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							prmReporteBanj.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							prmReporteBanj.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							prmReporteBanj.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							prmReporteBanj.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							prmReporteBanj.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							prmReporteBanj.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							prmReporteBanj.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							prmReporteBanj.setCorreo(rs.getString("R_CORREO"));
							prmReporteBanj.setAsunto(rs.getString("R_ASUNTO"));
							prmReporteBanj.setDetalle_asignacion(rs.getString("R_DETALLE_SOLICITUD_MOVIMIENTO"));
							prmReporteBanj.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							prmReporteBanj.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
												
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstUltimoEnvioMsj "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstUltimoEnvioMsj "+e.getMessage());
		}

		return prmReporteBanj;
	}
	
	//REPORTE SEGUIMIENTO
	@Override
	public List<ReporteBandeja> lstReporteBandejaSeguimiento(final ReporteBandeja tmpRptBandeja)
			throws PersistenciaException {
		final List<ReporteBandeja> listarpt = new ArrayList<ReporteBandeja>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_REPORTE.SP_BUSC_RPT_SOLIC_SEGUIMIENTO" + "( ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						String anio = tmpRptBandeja.getAnio_solicitud();
						cstm.setString("P_ANIO_SOLICITUD", anio);
						String numDoc = tmpRptBandeja.getNumero_documento_identidad();
						cstm.setString("P_NUMERO_DOCUMENTO_IDENTIDAD", numDoc);
						String apePat = tmpRptBandeja.getApellido_paterno();
						cstm.setString("P_APELLIDO_PATERNO", apePat);
						String apeMat = tmpRptBandeja.getApellido_materno(); 
						cstm.setString("P_APELLIDO_MATERNO", apeMat);
						String nombres = tmpRptBandeja.getNombres();
						cstm.setString("P_NOMBRES", nombres);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {							
							
							ReporteBandeja rpt= new ReporteBandeja();
							rpt.setSolicitud_transparencia_id(rs.getInt("R_SOLICITUD_TRANSPARENCIA_ID"));
							rpt.setFuncionario_resp_entrginfo("Jefe de la Ofina de Asesoria Juridica".toUpperCase());
							rpt.setCorrelativo_solicitud(rs.getString("R_CORRELATIVO_SOLICITUD"));
							rpt.setAnio_solicitud(rs.getString("R_ANIO_SOLICITUD"));
							//rpt.setTipo_documento_id
							rpt.setTipo_documento_identidad_id(rs.getInt("R_TIPO_DOCUMENTO_IDENTIDAD_ID"));
							rpt.setTipo_doc_identidad_detalle(rs.getString("R_TIPO_DOC_IDENTIDAD_DETALLE"));
							rpt.setNumero_documento_identidad(rs.getString("R_NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setApellido_paterno(rs.getString("R_APELLIDO_PATERNO"));
							rpt.setApellido_materno(rs.getString("R_APELLIDO_MATERNO"));
							rpt.setNombres(rs.getString("R_NOMBRES"));
							rpt.setDireccion_domicilio(rs.getString("R_DIRECCION_DOMICILIO"));
							rpt.setCoddep(rs.getString("R_CODDEP"));
							rpt.setCoddep_detalle(rs.getString("R_CODDEP_DETALLE"));
							rpt.setCodpro(rs.getString("R_CODPRO"));
							rpt.setCodpro_detalle(rs.getString("R_CODPRO_DETALLE"));
							rpt.setCoddis(rs.getString("R_CODDIS"));
							rpt.setCoddis_detalle(rs.getString("R_CODDIS_DETALLE"));
							rpt.setTelefono(rs.getString("R_TELEFONO"));
							//rpt.setFax(rs.getString("R_FAX"));
							rpt.setCelular(rs.getString("R_CELULAR"));
							rpt.setCorreo_electronico(rs.getString("R_CORREO_ELECTRONICO"));
							
							rpt.setDetalle_solicitud(rs.getString("R_DETALLE_SOLICITUD"));
							
							rpt.setDependencia_de_informacion(rs.getString("R_DEPENDENCIA_DE_INFORMACION"));
							rpt.setDependencia_nformacion_det(rs.getString("R_DEPENDENCIA_NFORMACION_DET"));
							rpt.setForma_de_entrega(rs.getString("R_FORMA_DE_ENTREGA"));
							rpt.setFlag_autoriza_recojo(rs.getString("R_FLAG_AUTORIZA_RECOJO"));
							rpt.setPers_auto_nro_documento(rs.getString("R_PERS_AUTO_NRO_DOCUMENTO"));
							rpt.setPers_auto_nombre_apellidos(rs.getString("R_PERS_AUTO_NOMBRE_APELLIDOS"));
							rpt.setObservacion(rs.getString("R_OBSERVACION"));
							rpt.setFlag_declaracion_jurada(rs.getString("R_FLAG_DECLARACION_JURADA"));
							rpt.setFlag_notif_electronica(rs.getString("R_FLAG_NOTIF_ELECTRONICA"));
							rpt.setEstado(rs.getString("R_ESTADO"));
							rpt.setSituacion(rs.getString("R_SITUACION"));
							rpt.setUsuario_id_registro(rs.getInt("R_USUARIO_ID_REGISTRO"));
							rpt.setFecha_registro(rs.getString("R_FECHA_REGISTRO"));
							rpt.setUsuario_id_modificacion(rs.getString("R_USUARIO_ID_MODIFICACION"));
							rpt.setFecha_modificacion(rs.getString("R_FECHA_MODIFICACION"));
							rpt.setFecha_ingreso_std(rs.getString("R_FECHA_INGRESO_STD"));
							rpt.setAsunto_std(rs.getString("R_ASUNTO_STD"));
							rpt.setCodigo(rs.getString("R_CODIGO"));
							rpt.setFecha_ingreso_oaj(rs.getString("R_FECHA_INGRESO_OAJ"));
							rpt.setRuta_documento_acceso(rs.getString("R_RUTA_DOCUMENTO_ACCESO"));
							rpt.setFecha_entrega_courier(rs.getString("R_FECHA_ENTREGA_COURIER"));
							rpt.setFecha_cargo_courier(rs.getString("R_FECHA_CARGO_COURIER"));
							rpt.setFecha_cargo_ciudadano(rs.getString("R_FECHA_CARGO_CIUDADANO"));
							rpt.setSumilla_cargo_ciudadano(rs.getString("R_SUMILLA_CARGO_CIUDADANO"));
							rpt.setUsuario_id_responsable(rs.getLong("R_USUARIO_ID_RESPONSABLE"));
							rpt.setUsuario_nombre_responsable(rs.getString("R_USUARIO_NOMBRE_RESPONSABLE"));
							/*** SMO_ SOLICITUD MOVIMIENTO ***/
							rpt.setSolicitud_movimiento_id(rs.getInt("R_SOLICITUD_MOVIMIENTO_ID"));
							rpt.setT01_und_org_receptor_id(rs.getString("R_T01_UND_ORG_RECEPTOR"));
							rpt.setEstado_solicitud_id(rs.getInt("R_ESTADO_SOLICITUD_ID"));
							rpt.setEstado_solicitud_detall(rs.getString("R_ESTADO_SOLICITUD_DETA"));
							//rpt.setEstado_codigo_registro_id(rs.getInt("R_ESTADO_CODIGO_REGISTRO_ID"));
							rpt.setSituacion_solicitud_id(rs.getInt("R_SITUACION_SOLICITUD_ID"));
							rpt.setSituacion_solicitud_detall(rs.getString("R_SITUACION_SOLICITUD_DETA"));
							rpt.setCorreo(rs.getString("R_CORREO"));
							rpt.setAsunto(rs.getString("R_ASUNTO"));
							rpt.setDetalle_asignacion(rs.getString("R_DETALLE_SOLICITUD_MOVIMIENTO"));
							rpt.setFecha_asignacion(rs.getString("R_FECHA_ASIGNACION"));
							rpt.setListaUndOrgDeriv(rs.getString("R_LISTA_PROYECCION_UND_ORG"));
												
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:lstReporteBandejaSeguimiento "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaSeguimiento "+e.getMessage());
		}

		return listarpt;
	}
}

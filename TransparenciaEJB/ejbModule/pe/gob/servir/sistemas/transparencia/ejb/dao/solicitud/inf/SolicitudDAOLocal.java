package pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;

@Local
public interface SolicitudDAOLocal extends GenericDAO<Solicitud> {
	//public ReturnObject actualizar2(Solicitud prmT) throws PersistenciaException;
}

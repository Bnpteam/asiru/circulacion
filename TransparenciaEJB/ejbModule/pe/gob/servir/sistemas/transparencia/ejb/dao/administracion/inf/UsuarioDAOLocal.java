package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Local
public interface UsuarioDAOLocal extends GenericDAO<Usuario> {

	public Usuario validarAcceso(Usuario usuario)throws PersistenciaException;
	
	public Boolean cambiarClave(Usuario usuario)throws PersistenciaException;
	
	public ReturnObject validarUsuario(Usuario prmUsuario) throws PersistenciaException;
	
	public Usuario obtenerUsuarioAsignado(String prmBandeja) throws PersistenciaException;
	
	public List<Usuario> listarUsuariosPorPerfil(Perfil prmPerfil) throws PersistenciaException;
	
	public List<Usuario> listarUsuarioXPerfilXArea(Usuario prmPerfil) throws PersistenciaException;
	
	public List<Usuario> listarUsuariosDispPorPerfil(Perfil prmPerfil) throws PersistenciaException;
	
	public List<Usuario> listarUsuariosPorBandeja(String bandeja) throws PersistenciaException;
	
	public List<Usuario> listarTodos() throws PersistenciaException;
	
	public Boolean consultarExistencia(Usuario usuario) throws PersistenciaException;
	
	public List<Usuario> listarUsuarioXPerfilXAreaAlternativo(Usuario prmObj) throws PersistenciaException;
	
}

package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.UsuarioDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.local.UsuarioServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Stateless
public class UsuarioServiceImpl implements UsuarioServiceRemote, UsuarioServiceLocal {

	@EJB
	private UsuarioDAOLocal usuarioDAOLocal; 
	
	@Override
	public ReturnObject insertar(Usuario prmUsuario) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().insertar(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizar(Usuario prmUsuario) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().actualizar(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean eliminar(Usuario prmUsuario) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().eliminar(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Usuario> listar(Usuario prmUsuario) throws ServicioException {
		try {
			return getUsuarioDAOLocal().listar(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<Usuario> listarUsuarioXPerfilXArea(Usuario prmUsuario) throws ServicioException {
		try {
			return getUsuarioDAOLocal().listarUsuarioXPerfilXArea(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Usuario buscarXId(Usuario prmUsuario) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().buscarXId(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Usuario validarAcceso(Usuario prmUsuario) throws ServicioException {
		
		Usuario tmpUsuario = new Usuario();
		
		try {
			
			tmpUsuario = getUsuarioDAOLocal().validarAcceso(prmUsuario);
			
		} catch (PersistenciaException e) {
			// 
			e.printStackTrace();
		}	
		
		return tmpUsuario;
		
	}

	@Override
	public Boolean cambiarClave(Usuario prmUsuario) throws ServicioException {
		// 
		return null;
	}

	@Override
	public ReturnObject validarUsuario(Usuario prmUsuario)
			throws ServicioException {
		// 
		return null;
	}

	@Override
	public Usuario obtenerUsuarioAsignado(String prmBandeja) throws ServicioException {
		Usuario usuarioAsignado = new Usuario();
		
		try {
			
			usuarioAsignado = getUsuarioDAOLocal().obtenerUsuarioAsignado(prmBandeja);
			
		} catch (PersistenciaException e) {
			// 
			e.printStackTrace();
		}	
		
		return usuarioAsignado;
	}

	@Override
	public List<Usuario> listarUsuariosPorPerfil(Perfil prmPerfil) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().listarUsuariosPorPerfil(prmPerfil);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	


	public UsuarioDAOLocal getUsuarioDAOLocal() {
		return usuarioDAOLocal;
	}

	public void setUsuarioDAOLocal(UsuarioDAOLocal usuarioDAOLocal) {
		this.usuarioDAOLocal = usuarioDAOLocal;
	}

	@Override
	public List<Usuario> listarTodos() throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().listarTodos();
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Usuario> listarUsuariosDispPorPerfil(Perfil prmPerfil) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().listarUsuariosDispPorPerfil(prmPerfil);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean consultarExistencia(Usuario prmUsuario) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().consultarExistencia(prmUsuario);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Usuario> listarUsuariosPorBandeja(String bandeja) throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().listarUsuariosPorBandeja(bandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<Usuario> listarUsuarioXPerfilXAreaAlternativo(Usuario prmObj)
			throws ServicioException {
		try {
			return this.getUsuarioDAOLocal().listarUsuarioXPerfilXAreaAlternativo(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
}

package pe.gob.servir.sistemas.transparencia.ejb.service.smtp;

import java.util.ArrayList;

import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;

public interface SmtpSendService 
{
	// 1. Env�o de correo SIN adjuntos.
	public boolean envioCorreoUsuario(String titulo, String mensaje, String paraEmail);
	// 2. Env�o de correo CON adjuntos.
	public boolean envioCorreoUsuario(String titulo, String mensaje, String paraEmail, ArrayList<FileAlfresco> prmLstFileAlfresco);
	//3.- Para mostrar QR Code en cuerpo del mensaje
	public boolean envioCorreoUsuarioSinTemplate(CorreoModel c);

}

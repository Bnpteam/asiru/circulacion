package pe.gob.servir.sistemas.transparencia.ejb.service.smtp;

import java.io.IOException;

import javax.mail.MessagingException;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.properties.Property;
import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;
import pe.gob.servir.systems.util.smtp.ContentIdGenerator;

public class EmailUsuario {
	private CorreoModel correoModel;
	private SmtpSendService 	mailSender;	
	
	public EmailUsuario() {
		super();
		this.correoModel = new CorreoModel();
		this.mailSender = new SmtpSendServiceImpl();
	}

	public void  enviarEmailCreacionUsuario(String emailDestino, String tipoUsuario, String nombreArchivoQR, String nombreUsuario, String sexo) throws MessagingException, IOException{
	  String cid = ContentIdGenerator.getContentId();
	  String nombreDestinatario = sexo.equals(Constants.PERSONA_GENERO_FEMENINO_CODIGO)?"Estimada "+nombreUsuario+ ": ":"Estimado "+nombreUsuario+ ": ";
	  String textoMensaje = obtenerTextoMensajeCreacionUsuario(tipoUsuario,nombreDestinatario,cid);  
	  String asunto = ""; 
	  if (tipoUsuario.equals(Constants.USER_TYPE_GENERAL)){
		  asunto = Property.getValueKey("ASUNTO_CORREO_USUARIO_GENERAL_CREACION");
	  }
	  if (tipoUsuario.equals(Constants.USER_TYPE_RESEARCHER)){
		  asunto = Property.getValueKey("ASUNTO_CORREO_USUARIO_INVESTIGADOR_CREACION");
	  }
	  this.enviarEmail(emailDestino, textoMensaje,nombreArchivoQR,asunto,cid);
	}		
	
	public void  reenviarCodigoQR(String emailDestino, String tipoUsuario, String nombreArchivoQR, String nombreUsuario, String sexo) throws MessagingException, IOException{
		String cid = ContentIdGenerator.getContentId();
		String nombreDestinatario = sexo.equals(Constants.PERSONA_GENERO_FEMENINO_CODIGO)?"Estimada "+nombreUsuario+ ": ":"Estimado "+nombreUsuario+ ": ";
		String textoMensaje = obtenerTextoMensajeReenvioCodigoQR(nombreDestinatario,cid);  
		String asunto = Property.getValueKey("ASUNTO_CORREO_USUARIO_REENVIO_CODIGO_QR");
		this.enviarEmail(emailDestino, textoMensaje,nombreArchivoQR,asunto,cid);
	}
	
	  private String obtenerTextoMensajeReenvioCodigoQR(String nombreDestinatario, String cidQRCode ){
		  String resultado = "";
			  resultado = Property.getValueKey("PLANTILLA_CORREO_USUARIO_REENVIO_CODIGO_QR_PARTE1")+
					  	  nombreDestinatario+
					  	  Property.getValueKey("PLANTILLA_CORREO_USUARIO_REENVIO_CODIGO_QR_PARTE2")+ 
					  	  cidQRCode + 
					  	  Property.getValueKey("PLANTILLA_CORREO_USUARIO_REENVIO_CODIGO_QR_PARTE3");
		  return resultado;
		  
	  }		
	  
	public boolean enviarEmail(String emailDestino, String textoMensaje, String nombreArchivoQR, String asunto, String cidQRImage){
		boolean resultadoEnvio = false;
		this.setearPropiedadesCorreo();
		correoModel.setTemplateTexto(textoMensaje);
		correoModel.addPara(emailDestino);
		correoModel.setAsunto(asunto);

	    String rutaQRCode = Property.getValueKey(Constants.PROPERTY_QRCODE_REPOSITORY_PATH) ; 
	    String nombreQRCode = nombreArchivoQR + Constants.FILE_CONTENT_TYPE_IMAGE_EXTENSION_DEFAULT;	
		
		correoModel.getSmtpObject().setImageInlinePath(rutaQRCode);
		correoModel.getSmtpObject().setImageInlineName(nombreQRCode);
		correoModel.getSmtpObject().setImageCID(cidQRImage);
	    
		resultadoEnvio = this.mailSender.envioCorreoUsuarioSinTemplate(correoModel);
		return resultadoEnvio;
	}	
	
	  private String obtenerTextoMensajeCreacionUsuario(String tipoUsuario, String nombreDestinatario, String cidQRCode ){
		  String resultado = "";
		  if (tipoUsuario.equals(Constants.USER_TYPE_GENERAL)){
			  resultado = Property.getValueKey("PLANTILLA_CORREO_USUARIO_GENERAL_CREACION_PARTE1")+
					  	  nombreDestinatario+
					  	  Property.getValueKey("PLANTILLA_CORREO_USUARIO_GENERAL_CREACION_PARTE2")+ 
					  	  cidQRCode + 
					  	  Property.getValueKey("PLANTILLA_CORREO_USUARIO_GENERAL_CREACION_PARTE3");
		  }
		  if (tipoUsuario.equals(Constants.USER_TYPE_RESEARCHER)){
			  resultado = Property.getValueKey("PLANTILLA_CORREO_USUARIO_INVESTIGADOR_CREACION_PARTE1")+
				  	  nombreDestinatario+
				  	  Property.getValueKey("PLANTILLA_CORREO_USUARIO_INVESTIGADOR_CREACION_PARTE2")+ 
				  	  cidQRCode + 
				  	  Property.getValueKey("PLANTILLA_CORREO_USUARIO_INVESTIGADOR_CREACION_PARTE3");
		  }
		  return resultado;
		  
	  }	
	  
		private void setearPropiedadesCorreo(){
			correoModel.getSmtpObject().setAccountMail(Property.getValueKey("SMTP_ACCOUNT_MAIL"));
			correoModel.getSmtpObject().setAccountPass(Property.getValueKey("SMTP_ACCOUNT_PASS"));
			correoModel.getSmtpObject().setPersonalName(Property.getValueKey("SMTP_PERSONAL_NAME"));
			correoModel.getSmtpObject().setHost(Property.getValueKey("SMTP_HOST"));
			correoModel.getSmtpObject().setPort(Property.getValueKey("SMTP_PORT"));
			correoModel.getSmtpObject().setAuth(Property.getValueKey("SMTP_AUTH"));
			correoModel.getSmtpObject().setEnable(Property.getValueKey("SMTP_ENABLE"));
		}		  
}

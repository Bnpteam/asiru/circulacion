package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;

@Remote
public interface MovimientoDetalleServiceRemote extends GenericService<DetalleMovimiento>{

	public List<DetalleMovimiento> lstDocumentosIngresados(Long prmIdSolicitudMov) throws ServicioException;
	
	public List<DetalleMovimiento> lstDocsDeRespuesta(String prmIdSolisMovs) throws ServicioException;
}

package pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;

@Remote
public interface PerfilServiceRemote extends GenericService<Perfil> {

	public List<Perfil> listarTodos() throws ServicioException;
	
	public List<Perfil> listarPerfilesPorBandeja(String bandeja) throws ServicioException;
	
	public List<Perfil> listar() throws ServicioException;
	
	public List<Perfil> listarPerfilesTodasBandejas() throws ServicioException;
	
}

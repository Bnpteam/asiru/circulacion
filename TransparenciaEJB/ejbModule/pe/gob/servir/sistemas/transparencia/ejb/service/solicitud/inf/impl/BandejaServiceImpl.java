package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.BandejaDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.local.BandejaServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.systems.util.retorno.ReturnObject;


@Stateless
public class BandejaServiceImpl implements BandejaServiceRemote,
BandejaServiceLocal {

	@EJB
	private BandejaDAOLocal bandejaDAOLocal;

	@Override
	public ReturnObject insertar(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Boolean actualizar(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Boolean eliminar(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<Solicitud> listar(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Solicitud buscarXId(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<ReporteBandeja> lstReporteBandejaSecretariaOAJ(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaSecretariaOAJ(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public List<ReporteBandeja> lstReporteBandejaAsistenteOAJ(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaAsistenteOAJ(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaJefeOAJ(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ2(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaJefeOAJ2(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<ReporteBandeja> lstReporteBandejaSecretariaUO(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaSecretariaUO(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<ReporteBandeja> lstReporteBandejaResponsableUO(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaResponsableUO(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public ReporteBandeja lstUltimoEnvioMsj(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstUltimoEnvioMsj(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<ReporteBandeja> lstReporteBandejaSeguimiento(ReporteBandeja tmpRptBandeja)
			throws ServicioException {
		try {
			return this.getBandejaDAOLocal().lstReporteBandejaSeguimiento(tmpRptBandeja);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	public BandejaDAOLocal getBandejaDAOLocal() {
		return bandejaDAOLocal;
	}

	public void setBandejaDAOLocal(BandejaDAOLocal bandejaDAOLocal) {
		this.bandejaDAOLocal = bandejaDAOLocal;
	}
	
}

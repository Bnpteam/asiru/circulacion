package pe.gob.servir.sistemas.transparencia.ejb.dao.mantenimiento.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.ejb.dao.mantenimiento.inf.T01MaestroDAOLocal;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.systems.util.code.CodeUtil;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;
import pe.gob.servir.systems.util.validator.VO;

@Stateless
public class T01MaestroDAOImpl extends GenericDAOImpl<T01Maestro> implements T01MaestroDAOLocal {

	private static final Logger logger = Logger.getLogger(T01MaestroDAOImpl.class);

	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;
	
	@Override
	public ReturnObject insertar(final T01Maestro prmObj) throws PersistenciaException{		
		
		ReturnObject result = new ReturnObject();
		//Boolean result = false;

		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_INSERTAR_TBL_T01MASTR"
								+ " ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
						cstm.registerOutParameter("P_T01MAESTRO_ID", OracleTypes.NUMBER);
						cstm.setString("P_CODIGO_TABLA", prmObj.getCodigoTabla());
						
						cstm.setString("P_NOMBRE_CORTO",prmObj.getNombreCorto());
						cstm.setString("P_NOMBRE_LARGO", CodeUtil.parseEncode(prmObj.getNombreLargo()));
						cstm.setString("P_T01_SITUACION_MAESTRO",prmObj.getT01SituacionMaestro());
						cstm.setInt("P_ORDEN",prmObj.getOrden());
						
						cstm.setString("P_VALOR1",prmObj.getValor1());
						cstm.setString("P_VALOR2",prmObj.getValor2());
						cstm.setString("P_VALOR3",prmObj.getValor3());
						cstm.setString("P_VALOR4",prmObj.getValor4());
						cstm.setString("P_VALOR5",prmObj.getValor5());
																		
						cstm.setLong("P_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());
						cstm.setString("P_IP_REGISTRO", prmObj.getIpRegistro());
						
						cstm.execute();
						
						BigDecimal decId= new BigDecimal(0);
						
						Object object=cstm.getObject("P_T01MAESTRO_ID");
						
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
												
						Integer id = decId.intValue();
						
						
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						logger.error("SQLException:T01MaestroDAOImpl:insert "+e.getMessage());
						cn.rollback();
						e.printStackTrace();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:insert "+e);
		}		
		result.setSw(prmObj.getResultTransaction());
		result.setId(prmObj.getId());
		return result;		

	}
	
	@Override
	public T01Maestro insertarDirecto(final T01Maestro prmObj) throws PersistenciaException{		
		
		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_NOTIFICACION.SP_INSERTARDIR_TBL_T01MASTR"
								+ " ( ?, ?, ?, ?, ?) }");
						
						cstm.registerOutParameter("P_CODIGO_REGISTRO", OracleTypes.NUMBER);
						cstm.setString("P_CODIGO_TABLA", prmObj.getCodigoTabla());
						
						cstm.setString("P_NOMBRE_CORTO",prmObj.getNombreCorto().toUpperCase());
																		
						cstm.setLong("P_USUARIO_REGISTRO_ID", prmObj.getUsuarioIdRegistro());
						cstm.setString("P_IP_REGISTRO", prmObj.getIpRegistro());

						cstm.execute();
						
						BigDecimal decId= new BigDecimal(0);
						
						Object object=cstm.getObject("P_CODIGO_REGISTRO");
						
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
												
						Integer id = decId.intValue();
						
						
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setCodigoRegistro(id.toString());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						logger.error("SQLException:T01MaestroDAOImpl:insertarDirecto "+e.getMessage());
						cn.rollback();
						e.printStackTrace();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:insertarDirecto "+e);
		}		
		result=prmObj.getResultTransaction();
		if (result) {
			return prmObj;
		}
		return null;		

	}
	
	@Override
	public Boolean actualizar(final T01Maestro prmObj) throws PersistenciaException{

		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_ACTUALIZAR_TBL_T01MASTR"
								+ " ( ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?) }");
						
						cstm.setLong("P_T01MAESTRO_ID", prmObj.getId());
						
						cstm.setString("P_NOMBRE_CORTO",prmObj.getNombreCorto());
						cstm.setString("P_NOMBRE_LARGO",CodeUtil.parseEncode(prmObj.getNombreLargo()));
						cstm.setString("P_T01_SITUACION_MAESTRO",prmObj.getT01SituacionMaestro());
						cstm.setInt("P_ORDEN",prmObj.getOrden());
						
						cstm.setString("P_VALOR1",prmObj.getValor1());
						cstm.setString("P_VALOR2",prmObj.getValor2());
						cstm.setString("P_VALOR3",prmObj.getValor3());
						cstm.setString("P_VALOR4",prmObj.getValor4());
						cstm.setString("P_VALOR5",prmObj.getValor5());
																								
						cstm.setLong("P_USUARIO_ID_MODIFICACION",prmObj.getUsuarioIdModificacion());
						cstm.setString("P_IP_MODIFICACION", prmObj.getIpModificacion());
						cstm.setString("P_OBS_MODIFICACION", prmObj.getObsModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.CHAR);

						cstm.execute();
												
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	
												
					} catch (SQLException e) {
						logger.error("SQLException:T01MaestroDAOImpl:actualizar "+e.getMessage());
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:actualizar "+e);
		}		
		result=prmObj.getResultTransaction();	
		return result;		

	}

	@Override
	public Boolean eliminar(final T01Maestro prmObj) throws PersistenciaException{

		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_ELIMINAR_TBL_T01MASTR"
								+ " ( ?, ?, ?, ?, ?) }");
						
						cstm.setLong("P_T01MAESTRO_ID", prmObj.getId());
																														
						cstm.setLong("P_USUARIO_ID_MODIFICACION",prmObj.getUsuarioIdModificacion());
						cstm.setString("P_IP_MODIFICACION", prmObj.getIpModificacion());
						cstm.setString("P_OBS_MODIFICACION", prmObj.getObsModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.CHAR);

						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
								
							}else{
								cn.rollback();
								
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	
												
					} catch (SQLException e) {
						logger.error("SQLException:T01MaestroDAOImpl:eliminar "+e.getMessage());
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:eliminar "+e);
		}		
		result=prmObj.getResultTransaction();	
		return result;		
	}

	@Override
	public List<T01Maestro> listar(final T01Maestro prmObj) throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARXNOMCOR_TBL_T01MASTR( ?, ?, ?, ?)}");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA",prmObj.getCodigoTabla());
						String pmrNombreCorto=prmObj.getNombreCorto();
						pmrNombreCorto=(pmrNombreCorto==null || pmrNombreCorto.trim().length()==0)?"%":pmrNombreCorto;
						cstm.setString("P_NOMBRE_CORTO",pmrNombreCorto);
						cstm.setString("P_SITUACION_MAESTRO",prmObj.getSituacion());
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							T01Maestro t01Maestro= new T01Maestro();
		 					
							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(CodeUtil.parseEncode(rs.getString("NOMBRE_LARGO")));
							t01Maestro.setT01SituacionMaestro(rs.getString("T01_SITUACION_MAESTRO"));
							t01Maestro.setT01SituacionMaestroNc(rs.getString("T01_SITUACION_MAESTRO_NC"));
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) {
						logger.error("SQLException:T01MaestroDAOImpl:listar "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:listar "+e);
		}

		return lstT01Maestro;
	}

	@Override
	public T01Maestro buscarXId(final T01Maestro prmT01maestro) throws PersistenciaException{

		final T01Maestro t01Maestro = new T01Maestro();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARXID_TBL_T01MASTR( ?, ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("P_T01MAESTRO_ID", prmT01maestro.getId());

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {

							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(rs.getString("NOMBRE_LARGO"));			
							t01Maestro.setT01SituacionMaestro(rs.getString("T01_SITUACION_MAESTRO"));
							t01Maestro.setOrden(rs.getInt("ORDEN"));
							
							t01Maestro.setValor1(rs.getString("VALOR1"));			
							t01Maestro.setValor2(rs.getString("VALOR2"));			
							t01Maestro.setValor3(rs.getString("VALOR3"));			
							t01Maestro.setValor4(rs.getString("VALOR4"));			
							t01Maestro.setValor5(rs.getString("VALOR5"));			
						
						}
					} catch (Exception e) {
						logger.error("SQLException:T01MaestroDAOImpl:buscarXId "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:buscarXId "+e);
		}

		return t01Maestro;
	}

	@Override
	public List<T01Maestro> listarTodos() throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARTODOS_TBL_T01MASTR( ?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							T01Maestro t01Maestro= new T01Maestro();
		 					
							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
						
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) {
						logger.error("SQLException:T01MaestroDAOImpl:listarTodos "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:listarTodos "+e);
		}

		return lstT01Maestro;

	}

	@Override
	public List<T01Maestro> listarSeries() throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARSERIES_TBL_T01MASTR( ?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							T01Maestro t01Maestro= new T01Maestro();
		 					
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
						
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) {
						logger.error("SQLException:T01MaestroDAOImpl:listarSeries "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:listarSeries "+e);
		}

		return lstT01Maestro;

	}

	@Override
	public List<T01Maestro> listarSubSeries(final String serie) throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARSUBSERIE_TBL_T01MASTR( ?,?) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setString("P_NOMBRE_CORTO", serie);

						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							T01Maestro t01Maestro= new T01Maestro();
		 					
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
						
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) {
						logger.error("SQLException:T01MaestroDAOImpl:listarSubSeries "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:listarSubSeries "+e);
		}

		return lstT01Maestro;

	}
	
	@Override
	public List<T01Maestro> listarXCodigoTabla(final String codigoTabla) throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		 
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try 
					{
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARXCODGTBL_TBL_T01MASTR( ?, ? )}");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA", codigoTabla);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							T01Maestro t01Maestro = new T01Maestro();
							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(rs.getString("NOMBRE_LARGO"));
							t01Maestro.setValor3(rs.getString("VALOR3"));
							boolean isDefault = (!VO.isEmpty(rs.getString("SWDEFAULT"))&&(rs.getString("SWDEFAULT").equals("1"))) ? true : false;
							t01Maestro.setSwDefault(isDefault);
							lstT01Maestro.add(t01Maestro);
						}
					} catch (Exception e) 
					{
						logger.error("SQLException:T01MaestroDAOImpl:listarXCodigoTabla "+e.getMessage());
						e.printStackTrace();
					}
					finally 
					{
						Conexion.closeSqlConnections(rs, cstm);
					}
					
				}
			});
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new  PersistenciaException("Exception:T01MaestroDAOImpl:listarXCodigoTabla "+e);
		}finally{
						
		}
		return lstT01Maestro;
	}
	
	
	@Override
	public List<T01Maestro> listarXCodigoTablaXValor1(final String codigoTabla,final String valor1) throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		 
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try 
					{
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTARXVALOR1_TBL_T01MASTR( ?, ?,? )}");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA", codigoTabla);
						cstm.setString("P_VALOR1", valor1);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							T01Maestro t01Maestro = new T01Maestro();

							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							boolean isDefault = (!VO.isEmpty(rs.getString("SWDEFAULT"))&&(rs.getString("SWDEFAULT").equals("1"))) ? true : false;
							t01Maestro.setSwDefault(isDefault);
							
							//Maestras
							t01Maestro.setNombreLargo(CodeUtil.parseEncode(rs.getString("NOMBRE_LARGO")));
							t01Maestro.setT01SituacionMaestro(rs.getString("T01_SITUACION_MAESTRO"));
							t01Maestro.setT01SituacionMaestroNc(rs.getString("T01_SITUACION_MAESTRO_NC"));
							t01Maestro.setValor1(rs.getString("VALOR1"));
							
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) 
					{
						logger.error("SQLException:T01MaestroDAOImpl:listarXCodigoTablaxValor1 "+e.getMessage());
						e.printStackTrace();
					}
					finally 
					{
						Conexion.closeSqlConnections(rs, cstm);
					}
					
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new  PersistenciaException("Exception:T01MaestroDAOImpl:listarXCodigoTablaxValor1 "+e);
		}finally{
						
		}
		return lstT01Maestro;
	}

	@Override
	public boolean validarNombreCorto(final  T01Maestro prmObj) throws PersistenciaException{
		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_VALDUPNOMCOR_TBL_T01MASTR"
								+ " ( ?, ?, ?, ?) }");
						
						if (VO.isEmpty(prmObj.getCodigoRegistro())) {
							prmObj.setCodigoRegistro("-");
						}
						cstm.setString("P_CODIGO_TABLA", prmObj.getCodigoTabla());
						cstm.setString("P_CODIGO_REGISTRO", prmObj.getCodigoRegistro());
						cstm.setString("P_NOMBRE_CORTO",prmObj.getNombreCorto());
						cstm.registerOutParameter("P_RETORNO", OracleTypes.CHAR);

						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	
												
					} catch (SQLException e) {
						logger.error("SQLException:T01MaestroDAOImpl:validarNombreCorto "+e.getMessage());
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:validarNombreCorto "+e);
		}		
		result=prmObj.getResultTransaction();	
		return result;		
		
	}
	
	@Override
	public boolean validarNombreCortoValor1(final  T01Maestro prmObj) throws PersistenciaException{
		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_VALDUPNOMVAL1_TBL_T01MASTR"
								+ " ( ?, ?, ?, ?,?) }");
						
						if (VO.isEmpty(prmObj.getCodigoRegistro())) {
							prmObj.setCodigoRegistro("-");
						}
						cstm.setString("P_CODIGO_TABLA", prmObj.getCodigoTabla());
						cstm.setString("P_CODIGO_REGISTRO", prmObj.getCodigoRegistro());
						cstm.setString("P_NOMBRE_CORTO",prmObj.getNombreCorto());
						cstm.setString("P_VALOR1",prmObj.getValor1());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.CHAR);

						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	
												
					} catch (SQLException e) {
						logger.error("SQLException:T01MaestroDAOImpl:validarNombreCortoValor1 "+e.getMessage());
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:validarNombreCortoValor1 "+e);
		}		
		result=prmObj.getResultTransaction();	
		return result;		
		
	}

	
	@Override
	public List<T01Maestro> listarXCodigoTablaNL(final String codigoTabla) throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		 
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try 
					{
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTXCODGTBLNL_TBL_T01MASTR( ?, ? )}");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA", codigoTabla);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							T01Maestro t01Maestro = new T01Maestro();

							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(rs.getString("NOMBRE_LARGO"));
							boolean isDefault = (!VO.isEmpty(rs.getString("SWDEFAULT"))&&(rs.getString("SWDEFAULT").equals("1"))) ? true : false;
							t01Maestro.setSwDefault(isDefault);
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) 
					{
						logger.error("SQLException:T01MaestroDAOImpl:listarXCodigoTablaNL "+e.getMessage());
						e.printStackTrace();
					}
					finally 
					{
						Conexion.closeSqlConnections(rs, cstm);
					}
					
				}
			});
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new  PersistenciaException("Exception:T01MaestroDAOImpl:listarXCodigoTablaNL "+e);
		}finally{
						
		}
		return lstT01Maestro;
	}
	
	@Override
	public List<T01Maestro> listarXUndOrgCnSecretarias(final int prmCodPerfilUser) throws PersistenciaException{
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		 
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try 
					{
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LIST_UND_ORG_CNSECREATARIAS( ?, ? )}");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setLong("P_PERFIL_ID", prmCodPerfilUser);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							T01Maestro t01Maestro = new T01Maestro();

							t01Maestro.setId(rs.getLong("R_T01MAESTRO_ID"));
							t01Maestro.setCodigoTabla(rs.getString("R_CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("R_CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("R_NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(rs.getString("R_NOMBRE_LARGO"));
							boolean isDefault = (!VO.isEmpty(rs.getString("R_SWDEFAULT"))&&(rs.getString("SWDEFAULT").equals("1"))) ? true : false;
							t01Maestro.setSwDefault(isDefault);
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) 
					{
						logger.error("SQLException:T01MaestroDAOImpl:listarXCodigoTablaNL "+e.getMessage());
						e.printStackTrace();
					}
					finally 
					{
						Conexion.closeSqlConnections(rs, cstm);
					}
					
				}
			});
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new  PersistenciaException("Exception:T01MaestroDAOImpl:listarXCodigoTablaNL "+e);
		}finally{
						
		}
		return lstT01Maestro;
	}

	@Override
	public List<T01Maestro> listarItemXValor1(final T01Maestro prmObj)throws PersistenciaException {
		
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_ITEM_TBL_T01MASTR( ?, ?, ?, ?, ?)}");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA",prmObj.getCodigoTabla());
						String pmrNombreCorto=prmObj.getNombreCorto();
						pmrNombreCorto=(pmrNombreCorto==null || pmrNombreCorto.trim().length()==0)?"%":pmrNombreCorto;
						cstm.setString("P_NOMBRE_CORTO",pmrNombreCorto);
						cstm.setString("P_VALOR1",prmObj.getValor1());
						cstm.setString("P_SITUACION_MAESTRO",prmObj.getSituacion());
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							T01Maestro t01Maestro= new T01Maestro();
		 					
							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(CodeUtil.parseEncode(rs.getString("NOMBRE_LARGO")));
							t01Maestro.setT01SituacionMaestro(rs.getString("T01_SITUACION_MAESTRO"));
							t01Maestro.setT01SituacionMaestroNc(rs.getString("T01_SITUACION_MAESTRO_NC"));
							
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) {
						logger.error("SQLException:T01MaestroDAOImpl:listarItemXValor1 "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaException("Exception:T01MaestroDAOImpl:listarItemXValor1 "+e);
		}

		return lstT01Maestro;
		
	}
	
	@Override
	public Boolean actualizarPlantilla(final T01Maestro prmObj) throws PersistenciaException {
		
		Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_MAESTROS_MANTENIMIENTO.SP_ACTUALIZAR_PLANTILLA_CORREO"
								+ " ( ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?) }");
						
						cstm.setLong("P_T01MAESTRO_ID", prmObj.getId());
						
						cstm.setString("P_NOMBRE_CORTO",prmObj.getNombreCorto());
						cstm.setString("P_NOMBRE_LARGO",prmObj.getNombreLargo());
						cstm.setString("P_T01_SITUACION_MAESTRO",prmObj.getT01SituacionMaestro());
						cstm.setInt("P_ORDEN",prmObj.getOrden());
						
						cstm.setString("P_VALOR1",prmObj.getValor1());
						cstm.setString("P_VALOR2",prmObj.getValor2());
						cstm.setString("P_VALOR3",prmObj.getValor3());
						cstm.setString("P_VALOR4",prmObj.getValor4());
						cstm.setString("P_VALOR5",prmObj.getValor5());
																								
						cstm.setLong("P_USUARIO_ID_MODIFICACION",prmObj.getUsuarioIdModificacion());
						cstm.setString("P_IP_MODIFICACION", prmObj.getIpModificacion());
						cstm.setString("P_OBS_MODIFICACION", prmObj.getObsModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.CHAR);

						cstm.execute();
												
						Object object=cstm.getObject("P_RETORNO");

						if (object!=null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret>0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	
												
					} catch (SQLException e) {
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException(e);
		}		
		result=prmObj.getResultTransaction();	
		return result;
	}

	@Override
	public List<T01Maestro> listarDiasxValor(final String codigoTabla)
			throws PersistenciaException {
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		 
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try 
					{
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_DIAS_POR_INGRESO( ?, ? )}");
						
						cstm.setString("P_NOMBRE_CORTO", codigoTabla);
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							T01Maestro t01Maestro = new T01Maestro();

							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(CodeUtil.parseEncode(rs.getString("NOMBRE_LARGO")));
							boolean isDefault = (!VO.isEmpty(rs.getString("SWDEFAULT"))&&(rs.getString("SWDEFAULT").equals("1"))) ? true : false;
							t01Maestro.setSwDefault(isDefault);
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) 
					{
						logger.error("SQLException:T01MaestroDAOImpl:listarDiasxValor "+e.getMessage());
						e.printStackTrace();
					}
					finally 
					{
						Conexion.closeSqlConnections(rs, cstm);
					}
					
				}
			});
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new  PersistenciaException("Exception:T01MaestroDAOImpl:listarDiasxValor "+e);
		}finally{
						
		}
		return lstT01Maestro;
	}

	@Override
	public List<T01Maestro> listarXCodigoTablaResp(final String codigoTabla)
			throws PersistenciaException {
		final List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		 
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					
					CallableStatement cstm = null;
					ResultSet rs = null;
					
					try 
					{
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{Call PKG_MAESTROS_MANTENIMIENTO.SP_LISTAR_ORGANO_RESPONSABLES( ?, ? )}");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setString("P_CODIGO_TABLA", codigoTabla);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) {
							T01Maestro t01Maestro = new T01Maestro();

							t01Maestro.setId(rs.getLong("T01MAESTRO_ID"));
							t01Maestro.setCodigoTabla(rs.getString("CODIGO_TABLA"));
							t01Maestro.setCodigoRegistro(rs.getString("CODIGO_REGISTRO"));
							t01Maestro.setNombreCorto(rs.getString("NOMBRE_CORTO"));
							t01Maestro.setNombreLargo(CodeUtil.parseEncode(rs.getString("NOMBRE_LARGO")));
							boolean isDefault = (!VO.isEmpty(rs.getString("SWDEFAULT"))&&(rs.getString("SWDEFAULT").equals("1"))) ? true : false;
							t01Maestro.setSwDefault(isDefault);
							lstT01Maestro.add(t01Maestro);
						}
						
					} catch (Exception e) 
					{
						logger.error("SQLException:T01MaestroDAOImpl:listarXCodigoTablaNL "+e.getMessage());
						e.printStackTrace();
					}
					finally 
					{
						Conexion.closeSqlConnections(rs, cstm);
					}
					
				}
			});
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new  PersistenciaException("Exception:T01MaestroDAOImpl:listarXCodigoTablaNL "+e);
		}finally{
						
		}
		return lstT01Maestro;	
	}
			
}

package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.MovimientoSolicitudDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.local.MovimientoSolicitudServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteVerRespuestas;
import pe.gob.servir.sistemas.transparencia.model.negocio.SolicitudMovimiento;
import pe.gob.servir.systems.util.retorno.ReturnObject;


@Stateless
public class MovimientoSolicitudServiceImpl implements MovimientoSolicitudServiceRemote,
	MovimientoSolicitudServiceLocal {

	@EJB
	private MovimientoSolicitudDAOLocal movimientoSolicitudDAOLocal;

	@Override
	public ReturnObject insertar(SolicitudMovimiento prmT) throws ServicioException {
		try {
			return this.getMovimientoSolicitudDAOLocal().insertar(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public ReturnObject insertarMultiDesdeJefe(SolicitudMovimiento prmT) throws ServicioException {
		try {
			return this.getMovimientoSolicitudDAOLocal().insertarMultiDesdeJefe(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public ReturnObject validarCodigoFormulario(String prmCodigo) throws ServicioException {
		try {
			return this.getMovimientoSolicitudDAOLocal().validarCodigoFormulario(prmCodigo);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<SolicitudMovimiento> lstLinTiempoXAreas1(SolicitudMovimiento prmObj, String mostrarEstados) throws ServicioException {
		try {
			return this.getMovimientoSolicitudDAOLocal().lstLinTiempoXAreas1(prmObj, mostrarEstados);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<SolicitudMovimiento> lstLinTiempoXAreas2(SolicitudMovimiento prmObj, String mostrarEstados) throws ServicioException {
		try {
			return this.getMovimientoSolicitudDAOLocal().lstLinTiempoXAreas2(prmObj, mostrarEstados);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	
	@Override
	public List<ReporteVerRespuestas> lstVerRespuestas(SolicitudMovimiento prmObj) throws ServicioException {
		try {
			return this.getMovimientoSolicitudDAOLocal().lstVerRespuestas(prmObj);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizar(SolicitudMovimiento prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Boolean eliminar(SolicitudMovimiento prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<SolicitudMovimiento> listar(SolicitudMovimiento prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public SolicitudMovimiento buscarXId(SolicitudMovimiento prmT) throws ServicioException {
		// 
		return null;
	}

	public MovimientoSolicitudDAOLocal getMovimientoSolicitudDAOLocal() {
		return movimientoSolicitudDAOLocal;
	}

	public void setMovimientoSolicitudDAOLocal(
			MovimientoSolicitudDAOLocal movimientoSolicitudDAOLocal) {
		this.movimientoSolicitudDAOLocal = movimientoSolicitudDAOLocal;
	}

	
	
	
}

package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.servir.sistemas.transparencia.ejb.dao.solicitud.inf.SolicitudDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.local.SolicitudServiceLocal;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.systems.util.retorno.ReturnObject;


@Stateless
public class SolicitudServiceImpl implements SolicitudServiceRemote,
SolicitudServiceLocal {

	@EJB
	private SolicitudDAOLocal solicitudDAOLocal;

	@Override
	public ReturnObject insertar(Solicitud prmT) throws ServicioException {
		try {
			return this.getSolicitudDAOLocal().insertar(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	@Override
	public Boolean actualizar(Solicitud prmT) throws ServicioException {
		try {
			return this.getSolicitudDAOLocal().actualizar(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}
	/*
	@Override
	public ReturnObject actualizar2(Solicitud prmT) throws ServicioException {
		try {
			return this.getSolicitudDAOLocal().actualizar2(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}*/

	@Override
	public Boolean eliminar(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public List<Solicitud> listar(Solicitud prmT) throws ServicioException {
		// 
		return null;
	}

	@Override
	public Solicitud buscarXId(Solicitud prmT) throws ServicioException {
		try {
			return this.getSolicitudDAOLocal().buscarXId(prmT);
		} catch (Exception e) {
			throw new ServicioException(e);
		}
	}

	public SolicitudDAOLocal getSolicitudDAOLocal() {
		return solicitudDAOLocal;
	}

	public void setSolicitudDAOLocal(SolicitudDAOLocal solicitudDAOLocal) {
		this.solicitudDAOLocal = solicitudDAOLocal;
	}
}

package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.dao.base.inf.GenericDAO;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;

@Local
public interface PerfilDAOLocal extends GenericDAO<Perfil> {

	public List<Perfil> listarTodos() throws PersistenciaException;
	
	public List<Perfil> listarPerfilesPorBandeja(String bandeja) throws PersistenciaException;
	
	public List<Perfil> listar() throws PersistenciaException;
	
	public List<Perfil> listarPerfilesTodasBandejas() throws PersistenciaException;
	
}

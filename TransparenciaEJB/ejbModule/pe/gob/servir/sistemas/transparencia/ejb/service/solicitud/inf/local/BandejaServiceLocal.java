package pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;

@Local
public interface BandejaServiceLocal  extends GenericService<Solicitud> {
	
	public List<ReporteBandeja> lstReporteBandejaSecretariaOAJ(ReporteBandeja tmpRptBandeja) throws ServicioException;
	
	public List<ReporteBandeja> lstReporteBandejaAsistenteOAJ(ReporteBandeja tmpRptBandeja) throws ServicioException;

	public List<ReporteBandeja> lstReporteBandejaJefeOAJ(ReporteBandeja tmpRptBandeja) throws ServicioException;
	
	public List<ReporteBandeja> lstReporteBandejaJefeOAJ2(ReporteBandeja tmpRptBandeja) throws ServicioException;
	
	public List<ReporteBandeja> lstReporteBandejaSecretariaUO(ReporteBandeja tmpRptBandeja) throws ServicioException;

	public List<ReporteBandeja> lstReporteBandejaResponsableUO(ReporteBandeja tmpRptBandeja) throws ServicioException;

	public ReporteBandeja lstUltimoEnvioMsj(ReporteBandeja tmpRptBandeja) throws ServicioException;
	
	public List<ReporteBandeja> lstReporteBandejaSeguimiento(ReporteBandeja tmpRptBandeja) throws ServicioException;

}

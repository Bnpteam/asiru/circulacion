package pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.servir.sistemas.transparencia.ejb.dao.administracion.inf.PerfilDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class PerfilDAOImpl extends GenericDAOImpl<Perfil> implements PerfilDAOLocal {

	private static final Logger logger = Logger.getLogger(PerfilDAOImpl.class);
	
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;
	
	@Override
	public ReturnObject insertar(final Perfil prmObj) throws PersistenciaException {
		ReturnObject result = new ReturnObject();
		//Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_INSERTAR_TBL_PERFIL( ?, ?, ?) }");
						
						cstm.registerOutParameter("P_PERFIL_ID", OracleTypes.NUMBER);
						cstm.setString("P_NOMBRE", prmObj.getNombre());
						
						cstm.setLong("P_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());

						cstm.execute();
						
						BigDecimal decId= new BigDecimal(0);
						
						Object object=cstm.getObject("P_PERFIL_ID");

						if (object!=null) {
							decId = (BigDecimal)object;	
						}
												
						Integer id = decId.intValue();
						
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}
												
					} catch (SQLException e) {
						logger.error("SQLException:PerfilDAOImpl:insert "+e.getMessage());
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:PerfilDAOImpl:insert "+e);
		}		
		result.setSw(prmObj.getResultTransaction());
		result.setId(prmObj.getId());	
		return result;	
	}

	@Override
	public Boolean actualizar(final Perfil prmObj) throws PersistenciaException {

		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_BANDEJ_MANTENIMIENT_VIRTUL.SP_ACTUALIZAR_TBL_PERFIL"
								+ "( ?, ?, ?, ?, ?) }");
						
						
						cstm.setLong("P_PERFIL_ID", prmObj.getId());
						cstm.setString("P_NOMBRE",prmObj.getNombre());
						cstm.setString("P_ESTADO",prmObj.getEstado().trim());
		                cstm.setLong("P_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);		
						
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");
						
						if (object != null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret > 0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	

												
					} catch (SQLException e) {
						logger.error("SQLException:PerfilDAOImpl:actualizar "+ e.getMessage());
						cn.rollback();
						e.printStackTrace();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:PerfilDAOImpl:actualizar " + e);
		}
		
		sw=prmObj.getResultTransaction();
		return sw; 
	
	}

	@Override
	public Boolean eliminar(final Perfil prmObj) throws PersistenciaException {


		boolean sw=false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						
						cstm = cn.prepareCall("{ Call PKG_BANDEJ_MANTENIMIENT_VIRTUL.SP_ELIMINAR_PERFIL"
								+ "( ?, ?, ?) }");
						
						
						cstm.setLong("P_PERFIL_ID", prmObj.getId());	                
		                cstm.setLong("P_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());
						
						cstm.registerOutParameter("P_RETORNO", OracleTypes.VARCHAR);		
						
						cstm.execute();
						
						Object object=cstm.getObject("P_RETORNO");
						
						if (object != null) {
							Integer ret = Integer.valueOf(object.toString());
							if ((ret > 0)) {
								prmObj.setResultTransaction(true);
								cn.commit();
							}else{
								cn.rollback();
								prmObj.setResultTransaction(false);
							}							
							
						}else{
							cn.rollback();
							prmObj.setResultTransaction(false);
						}	

												
					} catch (SQLException e) {
						logger.error("SQLException:PerfilDAOImpl:eliminar "+ e.getMessage());
						cn.rollback();
						e.printStackTrace();
						prmObj.setResultTransaction(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultTransaction(false);
			prmObj.setStackException(e,true);
			throw new PersistenciaException("Exception:PerfilDAOImpl:eliminar " + e);
		}
		
		sw=prmObj.getResultTransaction();
		return sw; 
	
	
	}

	@Override
	public List<Perfil> listar(final Perfil prmObj) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public Perfil buscarXId(final Perfil prmObj) throws PersistenciaException {
		// 
		return null;
	}

	@Override
	public List<Perfil> listarTodos() throws PersistenciaException {
		

		final List<Perfil> lstPerfil = new ArrayList<Perfil>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_TODOS_TBL_PERFIL( ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Perfil perfil= new Perfil();
							perfil.setId(rs.getLong("PERFIL_ID"));
							perfil.setNombre(rs.getString("NOMBRE"));
							lstPerfil.add(perfil);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:PerfilDAOImpl:listarTodos "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PerfilDAOImpl:listarTodos"+e);
		}
		return lstPerfil;
		
	}

	@Override
	public List<Perfil> listarPerfilesPorBandeja(final String bandeja) throws PersistenciaException {
		final List<Perfil> lstPerfil = new ArrayList<Perfil>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_PERFLS_X_BAND_PERFIL( ?, ? ) }");
						
						//cstm.setLong("P_PERFIL_ID", prmUsuario.getPerfil().getId());
						
						cstm.setString("P_BANDEJA", bandeja);

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Perfil perfil= new Perfil();
							perfil.setId(rs.getLong("PERFIL_ID"));
							perfil.setNombre(rs.getString("NOMBRE"));
							lstPerfil.add(perfil);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:PerfilDAOImpl:listarPerfilesPorBandeja " + e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PerfilDAOImpl:listarPerfilesPorBandeja " + e);
		}
		return lstPerfil;
	}

	@Override
	public List<Perfil> listar() throws PersistenciaException {

		final List<Perfil> lstPerfil = new ArrayList<Perfil>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_BANDEJ_MANTENIMIENT_VIRTUL.SP_LISTAR_TBL_PERFIL( ? ) }");

						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Perfil perfil= new Perfil();
							perfil.setId(rs.getLong("PERFIL_ID"));
							//perfil.setCodigo(rs.getString("CODIGO"));
							perfil.setNombre(rs.getString("NOMBRE"));
							perfil.setEstado(rs.getString("ESTADO").trim());
							lstPerfil.add(perfil);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:PerfilDAOImpl:listar "+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PerfilDAOImpl:listar"+e);
		}
		return lstPerfil;
		
	
	}

	@Override
	public List<Perfil> listarPerfilesTodasBandejas() throws PersistenciaException {
		final List<Perfil> lstPerfil = new ArrayList<Perfil>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ADMINISTRACIN_MANTENIMIENT.SP_LISTAR_PERF_ALL_BANDJS_PERF( ? ) }");
						
						cstm.registerOutParameter("P_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("P_C_CURSOR");

						while (rs.next()) {
							
							Perfil perfil= new Perfil();
							perfil.setId(rs.getLong("PERFIL_ID"));
							perfil.setNombre(rs.getString("NOMBRE"));
							lstPerfil.add(perfil);
						
						}
					} catch (Exception e) {
						logger.error("SQLException:PerfilDAOImpl:listarPerfilesTodasBandejas " + e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PerfilDAOImpl:listarPerfilesTodasBandejas " + e);
		}
		return lstPerfil;
	}

}

package pe.gob.servir.sistemas.maestros.ejb.dao.maestras.impl;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import oracle.jdbc.OracleTypes;
import pe.gob.servir.sistemas.maestros.ejb.dao.maestras.inf.PersonaDAOLocal;
import pe.gob.servir.sistemas.transparencia.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.servir.sistemas.transparencia.ejb.dao.exception.PersistenciaException;
import pe.gob.servir.sistemas.transparencia.model.maestra.Persona;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.systems.util.converter.Convert;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.sql.Conexion;

@Stateless
public class PersonaDAOImpl  extends GenericDAOImpl<Persona> implements PersonaDAOLocal{
	
	private static final Logger logger = Logger.getLogger(PersonaDAOImpl.class);
	
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;

	@Override
	public ReturnObject insertar(final Persona prmObj) {
		ReturnObject result = new ReturnObject();
		//Boolean result = false;
		
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					// 
					
					CallableStatement cstm = null;
					if (cn!=null) {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_ESQUEM_MAESTRS_MANTENIMINT.SP_INSERTAR_DATOS_PERSONA"
								+ "( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) }");
						
						cstm.setString("E_TIPO_DOCUMENTO", prmObj.getTipoDocumentoNom());
						cstm.setString("E_NRO_DOCUMENTO", prmObj.getNroDocumento());
						cstm.setString("E_AP_PATERNO", prmObj.getApPaterno());
						cstm.setString("E_AP_MATERNO", prmObj.getApMaterno());
						cstm.setString("E_AP_CASADA", prmObj.getApCasada());
						cstm.setString("E_NOMBRES", prmObj.getNombres());
						cstm.setString("E_DIRECCION", prmObj.getDireccion());
						cstm.setString("E_UBG_DEPARTAMENTO", prmObj.getUbgDepartamento());
						cstm.setString("E_UBG_PROVINCIA", prmObj.getUbgProvincia());
						cstm.setString("E_UBG_DISTRITO", prmObj.getUbgDistrito());
						cstm.setString("E_NOM_DEPARTAMENTO", prmObj.getNomDepartamento());
						cstm.setString("E_NOM_PROVINCIA", prmObj.getNomProvincia());
						cstm.setString("E_NOM_DISTRITO", prmObj.getNomDistrito());
						cstm.setString("E_SEXO", prmObj.getSexo());
						cstm.setString("E_ESTADO_CIVIL", prmObj.getEstadoCivil());
						cstm.setString("E_FECHA_NACIMIENTO", Convert.toString(prmObj.getFechaNacimiento()));
						cstm.setString("E_NOM_PADRE", prmObj.getNomPadre());
						cstm.setString("E_NOM_MADRE", prmObj.getNomMadre());
						cstm.setString("E_FECHA_FALLECIMIENTO", Convert.toString(prmObj.getFechaFallecimiento()));
						cstm.setString("E_FECHA_CADUCIDAD", Convert.toString(prmObj.getFechaCaducidad()));
						cstm.registerOutParameter("S_ID_PERSONA", OracleTypes.NUMBER);
						
						cstm.execute();
						
						BigDecimal decId = (BigDecimal)cstm.getObject("S_ID_PERSONA");
						Integer id = decId.intValue();
						
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setPersonaId(id.longValue());
							prmObj.setResultTransaction(true);
						}else {
							cn.rollback();
							prmObj.setResultTransaction(false);
						}					
					}
				}
			});
			result.setSw(prmObj.isResultTransaction());
			result.setId(prmObj.getPersonaId());
		} catch (Exception e) {
			logger.error("SQLException:PersonaDAOImpl:insert "+e.getMessage());
			e.printStackTrace();
			result.setSw(false);
		}

		return result;
	}

	@Override
	public Boolean actualizar(Persona prmObj) {
		
		return null;
	}

	@Override
	public Boolean eliminar(Persona prmObj) {
		return null;
	}

	@Override
	public List<Persona> listar(Persona prmObj) {
		return null;
	}

	@Override
	public Persona buscarXId(Persona prmObj) {
		return null;
	}

	@Override
	public Persona buscarXDni(final String prmDni) 
	{
		final Persona persona = new Persona();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				
				@Override
				public void execute(Connection cn) throws SQLException {
					// 
					CallableStatement cstm = null;
					ResultSet rs = null;
					if (cn!=null) {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_ESQUEM_MAESTRS_MANTENIMINT.SP_BUSCARXTIPNRDOC_TBL_PERSONA( ?, ?, ? ) }");
						
						cstm.registerOutParameter("P_C_CURSOR", OracleTypes.CURSOR);
						cstm.setLong("P_TIPO_DOCUMENTO_ID", 1);
						cstm.setString("P_NRO_DOCUMENTO", prmDni);
						
						cstm.execute();
						
						rs = (ResultSet)cstm.getObject("P_C_CURSOR");
						
						while (rs.next()) 
						{
							persona.setPersonaId(rs.getLong("PERSONA_ID"));
							persona.setTipoDocumentoId(rs.getLong("TIPO_DOCUMENTO_ID"));
							persona.setNroDocumento(rs.getString("NRO_DOCUMENTO"));
							persona.setApPaterno(rs.getString("AP_PATERNO"));
							persona.setApMaterno(rs.getString("AP_MATERNO"));
							persona.setNombres(rs.getString("NOMBRES"));
							persona.setNombreCompleto(persona.getApPaterno()+" "+persona.getApMaterno()+", "+persona.getNombres());
							persona.setDireccion(rs.getString("DIRECCION"));
							persona.setSexo(rs.getString("SEXO"));
							persona.setSexoNom(persona.getSexo().equals("F") ? "FEMENINO" : "MASCULINO");
						}

					}	
				}
			});
			
		} catch (Exception e) {
			logger.error("SQLException:PersonaDAOImpl:buscarXDni "+e.getMessage());
			e.printStackTrace();
			//persona = null;
		}
		return persona;
	}

	@Override
	public List<UbigeoReniec> listUbigeo(final UbigeoReniec prmObj) throws PersistenciaException {
		
		final List<UbigeoReniec> lstubigeo = new ArrayList<UbigeoReniec>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ESQUEM_MAESTRS_MANTENIMINT.SP_OBTENER_DATOS_UBIGEO"
								+ "( ?, ?, ?, ? ) }");
						
						String dep = prmObj.getCoddep();
						cstm.setString("E_CODDEP", dep);
						String pro = prmObj.getCodpro();
						cstm.setString("E_CODPRO", pro);
						String dis = prmObj.getCoddis();
						cstm.setString("E_CODDIS", dis);						
						cstm.registerOutParameter("S_UBIGEO",OracleTypes.CURSOR);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_UBIGEO");

						while (rs.next()) {							
							
							UbigeoReniec tmpubigeo= new UbigeoReniec();

							tmpubigeo.setCoddep(rs.getString("CODDEP"));
							tmpubigeo.setCodpro(rs.getString("CODPRO"));
							tmpubigeo.setCoddis(rs.getString("CODDIS"));
							tmpubigeo.setDescripcion(rs.getString("DESCRIPCION"));
							
							lstubigeo.add(tmpubigeo);
						}
					} catch (SQLException e) {
						logger.error("SQLException:PersonaDAOImpl:buscarUbigeo"+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PersonaDAOImpl:buscarUbigeo"+e.getMessage());
		}

		return lstubigeo;
	}

	@Override
	public UbigeoReniec buscarxubigeo(UbigeoReniec prmubigeo) {
		// 
		return null;
	}

	@Override
	public List<UbigeoReniec> listUbigeoXDescripcion(final UbigeoReniec prmObj) throws PersistenciaException {
		
		final List<UbigeoReniec> lstubigeo = new ArrayList<UbigeoReniec>();

		try {

			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ESQUEM_MAESTRS_MANTENIMINT.SP_OBTENER_DATOS_UBIGE_X_CODIG"
								+ "( ?, ?, ?, ? ,?) }");
						
						
						cstm.setString("E_DESCRIPCION", prmObj.getDescripcion());
						String dep = prmObj.getCoddep();
						cstm.setString("E_CODDEP", dep);
						String pro = prmObj.getCodpro();
						cstm.setString("E_CODPRO", pro);
						String dis = prmObj.getCoddis();
						cstm.setString("E_CODDIS", dis);						
						cstm.registerOutParameter("S_UBIGEO",OracleTypes.CURSOR);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_UBIGEO");

						while (rs.next()) {							
							
							UbigeoReniec tmpubigeo= new UbigeoReniec();

							tmpubigeo.setCoddep(rs.getString("CODDEP"));
							tmpubigeo.setCodpro(rs.getString("CODPRO"));
							tmpubigeo.setCoddis(rs.getString("CODDIS"));
							tmpubigeo.setDescripcion(rs.getString("DESCRIPCION"));
							
							lstubigeo.add(tmpubigeo);
						}
					} catch (SQLException e) {
						logger.error("SQLException:PersonaDAOImpl:buscarUbigeo"+e.getMessage());
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenciaException("Exception:PersonaDAOImpl:buscarUbigeo"+e.getMessage());
		}

		return lstubigeo;
	}

}

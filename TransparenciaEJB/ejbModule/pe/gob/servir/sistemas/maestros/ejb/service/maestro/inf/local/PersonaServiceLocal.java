package pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.model.maestra.Persona;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;

@Local
public interface PersonaServiceLocal extends GenericService<Persona> {
	
	public Persona buscarXDni(String prmDni);
	
	public Persona buscarPersonaXDni(String prmDni);
	
	public List<UbigeoReniec> listUbigeo(UbigeoReniec prmubigeo) throws ServicioException;
	
	public UbigeoReniec buscarxubigeo(UbigeoReniec prmubigeo);
	
	public List<UbigeoReniec> listUbigeoXDescripcion(UbigeoReniec prmubigeo) throws ServicioException;
}

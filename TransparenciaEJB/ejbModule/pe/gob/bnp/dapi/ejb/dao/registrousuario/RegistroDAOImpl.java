package pe.gob.bnp.dapi.ejb.dao.registrousuario;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.domain.entity.Preference;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.domain.entity.RegistroVisitaASala;
import pe.gob.bnp.dapi.domain.entity.VerificacionIdentidad;
import pe.gob.bnp.dapi.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.dao.repository.RegistroDAOLocal;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.servir.sistemas.transparencia.model.dto.DocumentoIdentidadDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroUsuarioDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroVisitaDto;
import pe.gob.servir.sistemas.transparencia.model.dto.HistorialRegistroDto;
import pe.gob.servir.systems.util.sql.Conexion;
import pe.gob.servir.systems.util.validator.VO;

@Stateless
public class RegistroDAOImpl extends GenericDAOImpl<RegistroUsuario> implements RegistroDAOLocal{
	 	  
	private static final Logger logger = Logger.getLogger(RegistroDAOImpl.class);
	
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;	
	
	@Override
	public ResponseObject insert(final RegistroUsuario prmObj)throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO"
								+ " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
						cstm.setString("E_TIPO_USUARIO_BIBLIOTECA", prmObj.getUser().getTipoUsuarioBiblioteca());
						cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD", prmObj.getUser().getTipoDocumentoIdentidadId());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", VO.getStringFormatted(prmObj.getUser().getNumeroDocumentoIdentidad()));
						cstm.setString("E_NOMBRES", VO.getStringFormatted(prmObj.getUser().getNombre()));
						cstm.setString("E_APELLIDO_PATERNO", VO.getStringFormatted(prmObj.getUser().getApellidoPaterno()));
						cstm.setString("E_APELLIDO_MATERNO", VO.getStringFormatted(prmObj.getUser().getApellidoMaterno()));
						cstm.setDate("E_FECHA_NACIMIENTO", VO.getSQLDate(prmObj.getUser().getFechaNacimiento()));
						cstm.setString("E_SEXO", VO.getStringFormatted(prmObj.getUser().getSexo()));
						cstm.setString("E_DIRECCION_DOMICILIO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getDireccion()));
						cstm.setString("E_CODDEP", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodDep()));
						cstm.setString("E_CODPRO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodPro()));
						cstm.setString("E_CODDIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodDis()));
						cstm.setString("E_CODPAIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodPais()));
						cstm.setString("E_TELEFONO_FIJO", VO.getStringFormatted(prmObj.getUser().getTelefonoFijo()));					
						cstm.setString("E_TELEFONO_CELULAR", VO.getStringFormatted(prmObj.getUser().getTelefonoMovil()));
						cstm.setString("E_CORREO_ELECTRONICO", VO.getStringFormatted(prmObj.getUser().getCorreoElectronico()));
						cstm.setString("E_FLAG_AUTORIZO_AVISOS", VO.getStringFormatted(prmObj.getUser().getFlagAutorizoAvisos()));
						cstm.setString("E_FLAG_LABORA_ACTUALMENTE",VO.getStringFormatted(prmObj.getUser().getFlagLaboroActualmente()));;
						cstm.setString("E_NOMBRE_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getCentroLaboralNombre() ));
						cstm.setString("E_TIPO_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getTipo() ));
						cstm.setString("E_CODPAIS_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getDireccion().getCodPais()));
						cstm.setString("E_CIUDAD_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getDireccion().getNombreCiudad()));
						cstm.setString("E_TELEFONO_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getTelefono()));
						cstm.setString("E_DIRECCION_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getDireccion().getDireccion()));
						cstm.setString("E_FLAG_ESTUDIO_ACTUALMENTE", VO.getStringFormatted( prmObj.getUser().getFlagEstudioActualmente()));

						cstm.setString("E_NOMBRE_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getCentroEstudioNombre()));
						cstm.setString("E_TIPO_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getTipo()));
						cstm.setString("E_CODPAIS_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getDireccion().getCodPais()));
						cstm.setString("E_CIUDAD_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getDireccion().getNombreCiudad()));
						cstm.setString("E_TELEFONO_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getTelefono()));
						cstm.setString("E_DIRECCION_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getDireccion().getDireccion()));
						cstm.setString("E_FLAG_ACEPTO_ACTA_COMPROMISO", VO.getStringFormatted( prmObj.getUser().getFlagAceptoCompromiso()));
						cstm.setBinaryStream("E_FOTO_RENIEC", VO.getInputStream(prmObj.getUser().getFotoReniec().getRawFile()));
						cstm.setBinaryStream("E_FOTO_ADJUNTADA", VO.getInputStream(prmObj.getUser().getFotoAdjuntada().getRawFile()));
						cstm.setBinaryStream("E_ARCHIVO_CARTA_PRESENTACION", VO.getInputStream(prmObj.getUser().getCartaPresentacion().getRawFile()));
						cstm.setString("E_NOMBRE_CARTA_PRESENTACION", VO.getString(prmObj.getUser().getCartaPresentacion().getFileName()));
						cstm.setString("E_NOMBRE_FOTO_ADJUNTADA", VO.getString(prmObj.getUser().getFotoAdjuntada().getFileName()));
						cstm.setString("E_TIPO_INSTITUCION_AVALA", VO.getString(prmObj.getUser().getTipoInstitucionAvala()));
						cstm.setString("E_TEMA_INVESTIGACION", VO.getStringFormatted(prmObj.getUser().getTemaInvestigacion()));
						cstm.setString("E_ESTADO", VO.getString(prmObj.getEstadoRegistro()));
						cstm.setString("E_EXTENSION_FOTO_ADJUNTADA", VO.getString(prmObj.getUser().getFotoAdjuntada().getExtension()));
						cstm.setString("E_EXTENSION_CARTA_ADJUNTADA", VO.getString(prmObj.getUser().getCartaPresentacion().getExtension()));

						cstm.setString("E_CONCEPTO", VO.getString(prmObj.getConcepto()));
						cstm.setString("E_VIGENCIA", VO.getString(prmObj.getVigencia()));
						cstm.setDate("E_FECHA_INICIO", VO.getSQLDate(prmObj.getFechaInicio()));
						cstm.setDate("E_FECHA_FIN", VO.getSQLDate(prmObj.getFechaFin()));
						cstm.setDouble("E_COSTO", prmObj.getCosto());
						cstm.setString("E_MONEDA", VO.getString(prmObj.getMoneda()));
						cstm.setString("E_RECIBO_PAGO", VO.getStringFormatted(VO.getString(prmObj.getReciboPago())));
						cstm.setDate("E_FECHA_PAGO", VO.getSQLDate(prmObj.getFechaPago()));
						
						cstm.setString("E_PREFERENCIAS", prmObj.getUser().getCadenaPreferencias());
						cstm.setString("E_PERIODO_SUSPENSION", VO.getString(prmObj.getPeriodoSuspension()));
						cstm.setDate("E_FECHA_INICIO_SUSPENSION", VO.getSQLDate(prmObj.getFechaInicioSuspension()));
						cstm.setDate("E_FECHA_FIN_SUSPENSION", VO.getSQLDate(prmObj.getFechaFinSuspension()));
						cstm.setString("E_PREFERENCIA_ESPECIFICA", VO.getStringFormatted(VO.getString(prmObj.getUser().getPreferenciaEspecifica())));
						cstm.setLong("E_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdRegistro());
						
						cstm.setBinaryStream("E_FOTO_CARNET_FRENTE", VO.getInputStream(prmObj.getFotoCarnetFrente().getRawFile()));
						cstm.setBinaryStream("E_FOTO_CARNET_REVERSO", VO.getInputStream(prmObj.getFotoCarnetReverso().getRawFile()));
						
						cstm.setInt("E_ESCALA_MANUAL_FOTO_CARNET", VO.getInteger(prmObj.getEscalaManualFotoCarnet()));
						cstm.setInt("E_POSX_MANUAL_FOTO_CARNET", VO.getInteger(prmObj.getPosicionXFotoCarnet()));
						cstm.setInt("E_POSY_MANUAL_FOTO_CARNET", VO.getInteger(prmObj.getPosicionYFotoCarnet()));
						
						cstm.setString("E_NUMERO_CARNET_BIBLIOTECA", VO.getString(prmObj.getNumeroCarnetBiblioteca()));
						cstm.setString("E_CODIGO_QR_VALOR", VO.getString(prmObj.getCodigoQRValor()));
						cstm.setString("E_CODPAIS_PASAPORTE", VO.getString(prmObj.getUser().getCodPaisPasaporte()));
						cstm.setString("E_COD_BARRAS_LECTOR", VO.getString(prmObj.getCodBarrasAbsysnet()));

						cstm.setString("E_GRADO_INSTRUCCION_ID", VO.getString(prmObj.getUser().getIdGradoInstruccion()));
						cstm.setString("E_GRADO_INSTRUCCION", VO.getStringFormatted(prmObj.getUser().getNombreGradoInstruccion()));
						cstm.setString("E_NACIONALIDAD_ID", VO.getString(prmObj.getUser().getIdNacionalidad()));
						cstm.setString("E_NACIONALIDAD", VO.getStringFormatted(prmObj.getUser().getNombreNacionalidad()));
						cstm.setString("E_ORIGEN_REGISTRO", VO.getString(prmObj.getOrigenRegistro()));
						cstm.setString("E_NOMBREPAIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombrePais()));
						cstm.setString("E_NOMBREDEP", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombreDep()));
						cstm.setString("E_NOMBREPRO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombrePro()));
						cstm.setString("E_NOMBREDIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombreDis()));
						cstm.setString("E_FLAG_MAYOR_EDAD", VO.getString(prmObj.getUser().getFlagMayorDeEdad()));
						cstm.registerOutParameter("S_REGISTRO_USUARIO_ID", OracleTypes.NUMBER);
						cstm.registerOutParameter("S_USUARIO_BIBLIOTECA_ID", OracleTypes.NUMBER);	
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);

						cstm.execute();
						Object object=cstm.getObject("S_REGISTRO_USUARIO_ID");
						BigDecimal decId= new BigDecimal(0);						
												
						if (object!=null) {
							decId = (BigDecimal)object;	
						}
						Integer id = decId.intValue();
						if (id!=null&&id>0) {
							cn.commit();
							prmObj.setId(id.longValue());
							prmObj.setResultadoTransaccion(true);
						}else {
							cn.rollback();
							prmObj.setResultadoTransaccion(false);
						}
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:SolicitudDAOImpl:insertar:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO "+ex);
						System.out.println("SQLException:SolicitudDAOImpl:insertar:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultadoTransaccion(false);
						prmObj.setStackException(e,true);
					/*} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						logger.error("SQLException:SolicitudDAOImpl:insert "+ e.getMessage());*/
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultadoTransaccion(false);
			prmObj.setStackException(e,true);
			throw new PersistenceException("SQLException:SolicitudDAOImpl:insertar:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO"+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setResultado(prmObj.getResultadoTransaccion());	
		return result;	
	}

	@Override
	public Boolean update(RegistroUsuario entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(RegistroUsuario entityT) throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RegistroUsuario> list(RegistroUsuario entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegistroUsuario findById(RegistroUsuario entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseObject validarDocumentoIdentidad(final DocumentoIdentidadDto documentoIdentidadDto)throws PersistenceException {
		ResponseObject result = new ResponseObject();
		Session hbSession = em.unwrap(Session.class);
		//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
		hbSession.doWork(new Work() {
			@Override
			public void execute(Connection cn) throws SQLException {
				CallableStatement cstm = null;
				try {
					cn.setAutoCommit(false);
					cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_VALIDAR_DOCUMENTO_IDENTIDAD( ?,?,?,?) }");
					cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD", documentoIdentidadDto.getTipoDocumento());
					cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", documentoIdentidadDto.getNumeroDocumento());
					cstm.setString("E_CODPAIS_PASAPORTE", documentoIdentidadDto.getCodPaisEmision());
					cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);
					cstm.execute();
					Object object=cstm.getObject("S_RESULTADO");
					if (object!=null) { documentoIdentidadDto.setResultado((String)object);}
				} catch (SQLException e) {
					String ex = e.getMessage();
					logger.error("PKG_GESTION_CIRCULACION.SP_VALIDAR_DOCUMENTO_IDENTIDAD "+ex);
					System.out.println("PKG_GESTION_CIRCULACION.SP_VALIDAR_DOCUMENTO_IDENTIDADO "+ex);
					e.printStackTrace();
				}
				finally{
					Conexion.closeCallableStatement(cstm);
				}
			}
		});
		result.setMensaje(documentoIdentidadDto.getResultado());
		result.setResultado(true);
		return result;		
	}

	@Override
	public List<RegistroUsuario> listarBandejaRegistrosUsuario(
			final FiltroRegistroUsuarioDto filtro) throws PersistenceException {
		final List<RegistroUsuario> listarpt = new ArrayList<RegistroUsuario>();

		try {

			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_LIST_BAND_REGI_USU" + "( ?, ?, ?, ?, ?, ?, ?, ? ) }");

						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.setString("E_ESTADO", filtro.getEstadoRegistro());
						cstm.setString("E_TIPO_USUARIO_BIBLIOTECA", filtro.getTipoUsuarioBiblioteca());
						cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD", filtro.getTipoDocumentoId());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", filtro.getNumeroDocumento());
						cstm.setString("E_NOMBRES", filtro.getNombre());
						cstm.setString("E_APELLIDO_PATERNO", filtro.getApellidoPaterno());
						cstm.setString("E_APELLIDO_MATERNO", filtro.getApellidoMaterno());
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							
							RegistroUsuario rpt= new RegistroUsuario();
							rpt.setCodBarrasAbsysnet(rs.getString("COD_BARRAS_LECTOR"));
							rpt.setId(rs.getLong("REGISTRO_USUARIO_ID"));
							rpt.setEstadoRegistro(rs.getString("ESTADO"));
							rpt.setFechaSolicitud(rs.getString("FECHA_REGISTRO_SOLICITUD"));
							rpt.getUser().setTipoUsuarioBiblioteca(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.getUser().setTipoDocumentoIdentidadId(rs.getString("TIPO_DOCUMENTO_IDENTIDAD"));
							rpt.getUser().setNumeroDocumentoIdentidad(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.getUser().setNombre(rs.getString("NOMBRES"));
							rpt.getUser().setApellidoPaterno(rs.getString("APELLIDO_PATERNO"));
							rpt.getUser().setApellidoMaterno(rs.getString("APELLIDO_MATERNO"));
							rpt.getUser().setearNombreTipoDocumentoIdentidad();
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_BAND_REGI_USU "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+e.getMessage());
		}

		return listarpt;
	}

	@Override
	public RegistroUsuario obtenerRegistrosUsuario(final Long idRegistroUsuario) throws PersistenceException {
		final RegistroUsuario rpt = new RegistroUsuario();

		try {

			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {

				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_OBTENER_REGISTRO_USUARIO" + "( ?, ?) }");

						cstm.setLong("E_REGISTRO_USUARIO_ID", idRegistroUsuario);
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							rpt.setCodBarrasAbsysnet(rs.getString("COD_BARRAS_LECTOR"));
							rpt.setId(rs.getLong("REGISTRO_USUARIO_ID"));
							rpt.getUser().setUsuarioBibliotecaId(rs.getLong("USUARIO_BIBLIOTECA_ID"));
							rpt.setEstadoRegistro(rs.getString("ESTADO"));
							rpt.setFechaRegistro(VO.getUtilDate(rs.getDate("FECHA_REGISTRO_SOLICITUD")));
							rpt.setConcepto(VO.getString(rs.getString("CONCEPTO")));
							rpt.setCosto(VO.getDouble(rs.getDouble("COSTO")));
							rpt.setVigencia(VO.getString(rs.getString("VIGENCIA")));
							rpt.setFechaInicio(VO.getUtilDate(rs.getDate("FECHA_INICIO")));
							rpt.setFechaFin(VO.getUtilDate(rs.getDate("FECHA_FIN")));
							rpt.setMoneda(VO.getString(rs.getString("MONEDA")));
							rpt.setReciboPago(VO.getString(rs.getString("RECIBO_PAGO")));
							rpt.setFechaPago(VO.getUtilDate(rs.getDate("FECHA_PAGO")));
							rpt.getUser().setTipoUsuarioBiblioteca(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.getUser().setTipoDocumentoIdentidadId(rs.getString("TIPO_DOCUMENTO_IDENTIDAD"));
							rpt.getUser().setNumeroDocumentoIdentidad(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.getUser().setNombre(rs.getString("NOMBRES"));
							rpt.getUser().setApellidoPaterno(rs.getString("APELLIDO_PATERNO"));
							rpt.getUser().setApellidoMaterno(rs.getString("APELLIDO_MATERNO"));
							rpt.getUser().setearNombreTipoDocumentoIdentidad();
							rpt.getUser().setFechaNacimiento(VO.getUtilDate(rs.getDate("FECHA_NACIMIENTO")));
							rpt.getUser().setSexo(rs.getString("SEXO"));
							rpt.getUser().getDireccion().getDireccionResidencia().setDireccion(VO.getString(rs.getString("DIRECCION_DOMICILIO")));
							rpt.getUser().getDireccion().getDireccionResidencia().setCodDep(VO.getString(rs.getString("CODDEP")));
							rpt.getUser().getDireccion().getDireccionResidencia().setCodPro(VO.getString(rs.getString("CODPRO")));
							rpt.getUser().getDireccion().getDireccionResidencia().setCodDis(VO.getString(rs.getString("CODDIS")));
							rpt.getUser().getDireccion().getDireccionResidencia().setCodPais((VO.getString(rs.getString("CODPAIS"))));
							rpt.getUser().setTelefonoFijo(VO.getString(rs.getString("TELEFONO_FIJO")));
							rpt.getUser().setTelefonoMovil(VO.getString(rs.getString("TELEFONO_CELULAR")));
							rpt.getUser().setCorreoElectronico(VO.getString(rs.getString("CORREO_ELECTRONICO")));
							rpt.getUser().setFlagAutorizoAvisos(VO.getString(rs.getString("FLAG_AUTORIZO_AVISOS")));
							rpt.getUser().setFlagLaboroActualmente(VO.getString(rs.getString("FLAG_LABORA_ACTUALMENTE")));
							rpt.getUser().getCentroLaboral().setCentroLaboralNombre(VO.getString((rs.getString("NOMBRE_CENTRO_LABORAL"))));
							rpt.getUser().getCentroLaboral().setTipo(VO.getString(rs.getString("TIPO_CENTRO_LABORAL")));
							rpt.getUser().getCentroLaboral().getDireccion().setCodPais(VO.getString(rs.getString("CODPAIS_CENTRO_LABORAL")));
							rpt.getUser().getCentroLaboral().getDireccion().setNombreCiudad(VO.getString(rs.getString("CIUDAD_CENTRO_LABORAL")));
							rpt.getUser().getCentroLaboral().setTelefono(VO.getString(rs.getString("TELEFONO_CENTRO_LABORAL")));
							rpt.getUser().getCentroLaboral().getDireccion().setDireccion(VO.getString(rs.getString("DIRECCION_CENTRO_LABORAL")));
							rpt.getUser().setFlagEstudioActualmente(VO.getString(rs.getString("FLAG_ESTUDIO_ACTUALMENTE")));
							rpt.getUser().getCentroEstudio().setCentroEstudioNombre(VO.getString(rs.getString("NOMBRE_CENTRO_ESTUDIO")));
							rpt.getUser().getCentroEstudio().setTipo(VO.getString(rs.getString("TIPO_CENTRO_ESTUDIO")));
							rpt.getUser().getCentroEstudio().getDireccion().setCodPais(VO.getString(rs.getString("CODPAIS_CENTRO_ESTUDIO")));
							rpt.getUser().getCentroEstudio().getDireccion().setNombreCiudad(VO.getString(rs.getString("CIUDAD_CENTRO_ESTUDIO")));
							rpt.getUser().getCentroEstudio().setTelefono(VO.getString(rs.getString("TELEFONO_CENTRO_ESTUDIO")));
							rpt.getUser().getCentroEstudio().getDireccion().setDireccion(VO.getString(rs.getString("DIRECCION_CENTRO_ESTUDIO")));
							rpt.getUser().setFlagAceptoCompromiso(VO.getString(rs.getString("FLAG_ACEPTO_ACTA_COMPROMISO")));
							rpt.getUser().setTipoUsuarioBiblioteca(VO.getString(rs.getString("TIPO_USUARIO_BIBLIOTECA")));
							rpt.getUser().getFotoReniec().setRawFile((VO.getBytes(rs.getBinaryStream("FOTO_RENIEC"))));
							rpt.getUser().getFotoAdjuntada().setRawFile(VO.getBytes(rs.getBinaryStream("FOTO_ADJUNTADA")));
							rpt.getUser().getCartaPresentacion().setRawFile(VO.getBytes(rs.getBinaryStream("ARCHIVO_CARTA_PRESENTACION")));
							rpt.getUser().getCartaPresentacion().setFileName(VO.getString(rs.getString("NOMBRE_CARTA_PRESENTACION")));
							rpt.getUser().getFotoAdjuntada().setFileName(VO.getString(rs.getString("NOMBRE_FOTO_ADJUNTADA")));
							rpt.getUser().setTipoInstitucionAvala(VO.getString(rs.getString("TIPO_INSTITUCION_AVALA")));
							rpt.getUser().setTemaInvestigacion(VO.getString(rs.getString("TEMA_INVESTIGACION")));
							rpt.getUser().getFotoAdjuntada().setExtension((VO.getString(rs.getString("EXTENSION_FOTO_ADJUNTADA"))));
							rpt.getUser().getCartaPresentacion().setExtension((VO.getString(rs.getString("EXTENSION_CARTA_ADJUNTADA"))));
							rpt.setPeriodoSuspension(VO.getString(rs.getString("PERIODO_SUSPENSION")));
							rpt.setFechaInicioSuspension(VO.getUtilDate(rs.getDate("FECHA_INICIO_SUSPENSION")));
							rpt.setFechaFinSuspension(VO.getUtilDate(rs.getDate("FECHA_FIN_SUSPENSION")));
							rpt.getUser().setPreferenciaEspecifica(VO.getString(rs.getString("PREFERENCIA_ESPECIFICA")));
							rpt.getFotoCarnetFrente().setRawFile(VO.getBytes(rs.getBinaryStream("FOTO_CARNET_FRENTE")));
							rpt.getFotoCarnetReverso().setRawFile(VO.getBytes(rs.getBinaryStream("FOTO_CARNET_REVERSO")));
							rpt.setEscalaManualFotoCarnet(VO.getInteger(rs.getInt("ESCALA_MANUAL_FOTO_CARNET")));
							rpt.setPosicionXFotoCarnet(VO.getInteger(rs.getInt("POSX_MANUAL_FOTO_CARNET")));
							rpt.setPosicionYFotoCarnet(VO.getInteger(rs.getInt("POSY_MANUAL_FOTO_CARNET")));
							
							rpt.setNumeroCarnetBiblioteca(VO.getString(rs.getString("NUMERO_CARNET_BIBLIOTECA")));
							rpt.setCodigoQRValor(VO.getString(rs.getString("CODIGO_QR_VALOR")));
							rpt.getUser().setCodPaisPasaporte(VO.getString(rs.getString("CODPAIS_PASAPORTE")));
							rpt.getUser().setIdNacionalidad(VO.getString(rs.getString("NACIONALIDAD_ID")));
							rpt.getUser().setNombreNacionalidad(VO.getString(rs.getString("NACIONALIDAD")));
							rpt.getUser().setIdGradoInstruccion(VO.getString(rs.getString("GRADO_INSTRUCCION_ID")));
							rpt.getUser().setNombreGradoInstruccion(VO.getString(rs.getString("GRADO_INSTRUCCION")));
							rpt.getUser().getDireccion().getDireccionResidencia().setNombrePais(VO.getString(rs.getString("NOMBREPAIS")));
							rpt.getUser().getDireccion().getDireccionResidencia().setNombreDep(VO.getString(rs.getString("NOMBREDEP")));
							rpt.getUser().getDireccion().getDireccionResidencia().setNombrePro(VO.getString(rs.getString("NOMBREPRO")));
							rpt.getUser().getDireccion().getDireccionResidencia().setNombreDis(VO.getString(rs.getString("NOMBREDIS")));
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_BAND_REGI_USU "+ ex);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+e.getMessage());
		}

		return rpt;
	}

	@Override
	public ResponseObject actualizarRegistroUsuario(final RegistroUsuario prmObj) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_UPDATE_REGISTRO_USUARIO"
								+ " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
						cstm.setLong("E_REGISTRO_USUARIO_ID", prmObj.getId());
						cstm.setLong("E_USUARIO_BIBLIOTECA_ID", prmObj.getUser().getUsuarioBibliotecaId());

						cstm.setString("E_TIPO_USUARIO_BIBLIOTECA", prmObj.getUser().getTipoUsuarioBiblioteca());
						cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD", prmObj.getUser().getTipoDocumentoIdentidadId());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", VO.getStringFormatted(prmObj.getUser().getNumeroDocumentoIdentidad()));
						cstm.setString("E_NOMBRES", VO.getStringFormatted(prmObj.getUser().getNombre()));
						cstm.setString("E_APELLIDO_PATERNO", VO.getStringFormatted(prmObj.getUser().getApellidoPaterno()));
						cstm.setString("E_APELLIDO_MATERNO", VO.getStringFormatted(prmObj.getUser().getApellidoMaterno()));
						cstm.setDate("E_FECHA_NACIMIENTO", VO.getSQLDate(prmObj.getUser().getFechaNacimiento()));
						cstm.setString("E_SEXO", VO.getStringFormatted(prmObj.getUser().getSexo()));
						cstm.setString("E_DIRECCION_DOMICILIO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getDireccion()));
						cstm.setString("E_CODDEP", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodDep()));
						cstm.setString("E_CODPRO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodPro()));
						cstm.setString("E_CODDIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodDis()));
						cstm.setString("E_CODPAIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getCodPais()));
						cstm.setString("E_TELEFONO_FIJO", VO.getStringFormatted(prmObj.getUser().getTelefonoFijo()));					
						cstm.setString("E_TELEFONO_CELULAR", VO.getStringFormatted(prmObj.getUser().getTelefonoMovil()));
						cstm.setString("E_CORREO_ELECTRONICO", VO.getStringFormatted(prmObj.getUser().getCorreoElectronico()));
						cstm.setString("E_FLAG_AUTORIZO_AVISOS", VO.getStringFormatted(prmObj.getUser().getFlagAutorizoAvisos()));
						cstm.setString("E_FLAG_LABORA_ACTUALMENTE",VO.getStringFormatted(prmObj.getUser().getFlagLaboroActualmente()));;
						cstm.setString("E_NOMBRE_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getCentroLaboralNombre() ));
						cstm.setString("E_TIPO_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getTipo() ));
						cstm.setString("E_CODPAIS_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getDireccion().getCodPais()));
						cstm.setString("E_CIUDAD_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getDireccion().getNombreCiudad()));
						cstm.setString("E_TELEFONO_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getTelefono()));
						cstm.setString("E_DIRECCION_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getDireccion().getDireccion()));
						cstm.setString("E_FLAG_ESTUDIO_ACTUALMENTE", VO.getStringFormatted( prmObj.getUser().getFlagEstudioActualmente()));

						cstm.setString("E_NOMBRE_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getCentroEstudioNombre()));
						cstm.setString("E_TIPO_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getTipo()));
						cstm.setString("E_CODPAIS_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getDireccion().getCodPais()));
						cstm.setString("E_CIUDAD_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getDireccion().getNombreCiudad()));
						cstm.setString("E_TELEFONO_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getTelefono()));
						cstm.setString("E_DIRECCION_CENTRO_ESTUDIO", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getDireccion().getDireccion()));
						cstm.setString("E_FLAG_ACEPTO_ACTA_COMPROMISO", VO.getStringFormatted( prmObj.getUser().getFlagAceptoCompromiso()));
						cstm.setBinaryStream("E_FOTO_RENIEC", VO.getInputStream(prmObj.getUser().getFotoReniec().getRawFile()));
						cstm.setBinaryStream("E_FOTO_ADJUNTADA", VO.getInputStream(prmObj.getUser().getFotoAdjuntada().getRawFile()));
						cstm.setBinaryStream("E_ARCHIVO_CARTA_PRESENTACION", VO.getInputStream(prmObj.getUser().getCartaPresentacion().getRawFile()));
						cstm.setString("E_NOMBRE_CARTA_PRESENTACION", VO.getString(prmObj.getUser().getCartaPresentacion().getFileName()));
						cstm.setString("E_NOMBRE_FOTO_ADJUNTADA", VO.getString(prmObj.getUser().getFotoAdjuntada().getFileName()));
						cstm.setString("E_TIPO_INSTITUCION_AVALA", VO.getString(prmObj.getUser().getTipoInstitucionAvala()));
						cstm.setString("E_TEMA_INVESTIGACION", VO.getStringFormatted(prmObj.getUser().getTemaInvestigacion()));
						cstm.setString("E_ESTADO", VO.getString(prmObj.getEstadoRegistro()));
						cstm.setString("E_EXTENSION_FOTO_ADJUNTADA", VO.getString(prmObj.getUser().getFotoAdjuntada().getExtension()));
						cstm.setString("E_EXTENSION_CARTA_ADJUNTADA", VO.getString(prmObj.getUser().getCartaPresentacion().getExtension()));

						cstm.setString("E_CONCEPTO", VO.getString(prmObj.getConcepto()));
						cstm.setString("E_VIGENCIA", VO.getString(prmObj.getVigencia()));
						cstm.setDate("E_FECHA_INICIO", VO.getSQLDate(prmObj.getFechaInicio()));
						cstm.setDate("E_FECHA_FIN", VO.getSQLDate(prmObj.getFechaFin()));
						cstm.setDouble("E_COSTO", prmObj.getCosto());
						cstm.setString("E_MONEDA", VO.getString(prmObj.getMoneda()));
						cstm.setString("E_RECIBO_PAGO", VO.getStringFormatted(prmObj.getReciboPago()));
						cstm.setDate("E_FECHA_PAGO", VO.getSQLDate(prmObj.getFechaPago()));
						cstm.setString("E_PREFERENCIAS", prmObj.getUser().getCadenaPreferencias());
						cstm.setString("E_PERIODO_SUSPENSION", VO.getString(prmObj.getPeriodoSuspension()));
						cstm.setDate("E_FECHA_INICIO_SUSPENSION", VO.getSQLDate(prmObj.getFechaInicioSuspension()));
						cstm.setDate("E_FECHA_FIN_SUSPENSION", VO.getSQLDate(prmObj.getFechaFinSuspension()));
						cstm.setString("E_PREFERENCIA_ESPECIFICA", VO.getStringFormatted(VO.getString(prmObj.getUser().getPreferenciaEspecifica())));
						cstm.setLong("E_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());
						cstm.setBinaryStream("E_FOTO_CARNET_FRENTE", VO.getInputStream(prmObj.getFotoCarnetFrente().getRawFile()));
						cstm.setBinaryStream("E_FOTO_CARNET_REVERSO", VO.getInputStream(prmObj.getFotoCarnetReverso().getRawFile()));
						cstm.setInt("E_ESCALA_MANUAL_FOTO_CARNET", VO.getInteger(prmObj.getEscalaManualFotoCarnet()));
						cstm.setInt("E_POSX_MANUAL_FOTO_CARNET", VO.getInteger(prmObj.getPosicionXFotoCarnet()));
						cstm.setInt("E_POSY_MANUAL_FOTO_CARNET", VO.getInteger(prmObj.getPosicionYFotoCarnet()));
						
						cstm.setString("E_GRADO_INSTRUCCION_ID", VO.getString(prmObj.getUser().getIdGradoInstruccion()));
						cstm.setString("E_GRADO_INSTRUCCION", VO.getStringFormatted(prmObj.getUser().getNombreGradoInstruccion()));
						cstm.setString("E_NACIONALIDAD_ID", VO.getString(prmObj.getUser().getIdNacionalidad()));
						cstm.setString("E_NACIONALIDAD", VO.getStringFormatted(prmObj.getUser().getNombreNacionalidad()));
						cstm.setString("E_NOMBREPAIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombrePais()));
						cstm.setString("E_NOMBREDEP", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombreDep()));
						cstm.setString("E_NOMBREPRO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombrePro()));
						cstm.setString("E_NOMBREDIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombreDis()));
						cstm.setString("E_COD_BARRAS_LECTOR", VO.getString(prmObj.getCodBarrasAbsysnet()));
						
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_DETALLE_RESULTADO", OracleTypes.VARCHAR);	
						cstm.execute();
						String objectRes=(String)cstm.getObject("S_RESULTADO");
						String objectDesRes=(String)cstm.getObject("S_DETALLE_RESULTADO");
												
						if (objectRes.equals(Constants.SUCCESS_RESULT)) {
							cn.commit();
							prmObj.setResultadoTransaccion(true);
						}else {
							cn.rollback();
							prmObj.setResultadoTransaccion(false);
						}
						prmObj.setMensajeRespuesta(objectDesRes);
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:SolicitudDAOImpl:actualizarRegistroUsuario:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO "+ex);
						System.out.println("SQLException:SolicitudDAOImpl:actualizarRegistroUsuario:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultadoTransaccion(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultadoTransaccion(false);
			prmObj.setStackException(e,true);
			throw new PersistenceException("SQLException:SolicitudDAOImpl:insertar:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO"+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setResultado(prmObj.getResultadoTransaccion());
		result.setMensaje(prmObj.getMensajeRespuesta());
		return result;	
	}

	@Override
	public ResponseObject cambiarEstadoRegistroUsuario(final RegistroUsuario prmObj) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_UPDATE_ESTADO_USUARIO"
								+ " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
						cstm.setLong("E_REGISTRO_USUARIO_ID", prmObj.getId());
						cstm.setLong("E_USUARIO_BIBLIOTECA_ID", prmObj.getUser().getUsuarioBibliotecaId());
						cstm.setString("E_ESTADO", prmObj.getEstadoRegistro());
						cstm.setString("E_CONCEPTO", VO.getString(prmObj.getConcepto()));
						cstm.setString("E_VIGENCIA", VO.getString(prmObj.getVigencia()));
						cstm.setDate("E_FECHA_INICIO", VO.getSQLDate(prmObj.getFechaInicio()));
						cstm.setDate("E_FECHA_FIN", VO.getSQLDate(prmObj.getFechaFin()));
						cstm.setDouble("E_COSTO", prmObj.getCosto());
						cstm.setString("E_MONEDA", VO.getString(prmObj.getMoneda()));
						cstm.setString("E_RECIBO_PAGO", VO.getString(prmObj.getReciboPago()));
						cstm.setDate("E_FECHA_PAGO", VO.getSQLDate(prmObj.getFechaPago()));
						cstm.setString("E_TIPO_USUARIO_BIBLIOTECA", prmObj.getUser().getTipoUsuarioBiblioteca());

						cstm.setString("E_PERIODO_SUSPENSION", VO.getString(prmObj.getPeriodoSuspension()));
						cstm.setDate("E_FECHA_INICIO_SUSPENSION", VO.getSQLDate(prmObj.getFechaInicioSuspension()));
						cstm.setDate("E_FECHA_FIN_SUSPENSION", VO.getSQLDate(prmObj.getFechaFinSuspension()));
						
						cstm.setLong("E_USUARIO_ID_MODIFICACION", prmObj.getUsuarioIdModificacion());
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_DETALLE_RESULTADO", OracleTypes.VARCHAR);	
						cstm.execute();
						String objectRes=(String)cstm.getObject("S_RESULTADO");
						String objectDesRes=(String)cstm.getObject("S_DETALLE_RESULTADO");
												
						if (objectRes.equals(Constants.SUCCESS_RESULT)) {
							cn.commit();
							prmObj.setResultadoTransaccion(true);
						}else {
							cn.rollback();
							prmObj.setResultadoTransaccion(false);
						}
						prmObj.setMensajeRespuesta(objectDesRes);
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:RegistroDAOImpl:cambiarEstadoRegistroUsuario:PKG_GESTION_CIRCULACION.SP_UPDATE_ESTADO_USUARIO "+ex);
						System.out.println("SQLException:RegistroDAOImpl:cambiarEstadoRegistroUsuario:PKG_GESTION_CIRCULACION.SP_UPDATE_ESTADO_USUARIO "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultadoTransaccion(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultadoTransaccion(false);
			prmObj.setStackException(e,true);
			throw new PersistenceException("SQLException:RegistroDAOImpl:cambiarEstadoRegistroUsuario:PKG_GESTION_CIRCULACION.SP_UPDATE_ESTADO_USUARIO"+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setResultado(prmObj.getResultadoTransaccion());
		result.setMensaje(prmObj.getMensajeRespuesta());
		return result;	

	}

	@Override
	public List<HistorialRegistroDto> listarHistorialRegistroUsuario(final Long idRegistro) throws PersistenceException {
		final List<HistorialRegistroDto> listarpt = new ArrayList<HistorialRegistroDto>();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_LIST_HIST_REGI_USU" + "( ?, ? ) }");
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("E_REGISTRO_USUARIO_ID", idRegistro);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							HistorialRegistroDto rpt= new HistorialRegistroDto();
							rpt.setCosto(rs.getDouble("COSTO"));
							rpt.setTipoUsuarioBiblioteca(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.setTipoRegistro(rs.getString("CONCEPTO"));
							rpt.setEstado(rs.getString("ESTADO"));
							rpt.setMoneda(rs.getString("MONEDA"));
							rpt.setFechaMovimiento(rs.getString("FECHA_MOVIMIENTO"));
							rpt.setFechaInicio(rs.getString("FECHA_INICIO"));
							rpt.setFechaFin(rs.getString("FECHA_FIN"));
							rpt.setFechaPago(rs.getString("FECHA_PAGO"));
							rpt.setReciboPago(rs.getString("RECIBO_PAGO"));
							rpt.setVigencia(rs.getString("VIGENCIA"));
							
							rpt.setFechaInicioSuspension(rs.getString("FECHA_INICIO_SUSPENSION"));
							rpt.setFechaFinSuspension(rs.getString("FECHA_FIN_SUSPENSION"));
							rpt.setPeriodoSuspension(rs.getString("PERIODO_SUSPENSION"));
							
							rpt.setNombreUsuarioInterno(rs.getString("USUARIO"));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:RegistroDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_HIST_REGI_USU "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("Exception:RegistroDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_HIST_REGI_USU "+e.getMessage());
		}

		return listarpt;
	}

	@Override
	public List<Preference> listarPreferenciasUsuario(final Long idUsuarioBiblioteca) throws PersistenceException {
		final List<Preference> listarpt = new ArrayList<Preference>();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_LIST_PREF_USU" + "( ?, ? ) }");
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("E_USUARIO_BIBLIOTECA_ID", idUsuarioBiblioteca);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							Preference rpt= new Preference();
							rpt.setPreferenciaId(Long.parseLong(rs.getString("preferencia_usuario_id")));
							rpt.setPreferenciaNombre(rs.getString("preferencia_usuario_nombre"));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:RegistroDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_PREF_USU "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("Exception:RegistroDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_PREF_USU "+e.getMessage());
		}

		return listarpt;
	}

	@Override
	public List<RegistroVisitaASala> listarBandejaRegistroVisitasSala(final FiltroRegistroVisitaDto filtro) throws PersistenceException {
		final List<RegistroVisitaASala> listarpt = new ArrayList<RegistroVisitaASala>();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_VISITAS_SALA.SP_LIST_BAND_VISI_SALA" + "( ?, ?, ?, ?, ?, ?, ? ) }");
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.setDate("E_FECHA_VISITA_DESDE", VO.getSQLDate(filtro.getFechaVisitaDesde()));
						cstm.setDate("E_FECHA_VISITA_HASTA", VO.getSQLDate(filtro.getFechaVisitaHasta()));
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", filtro.getNumeroDocumentoIdentidad());
						cstm.setString("E_NOMBRES", filtro.getNombre());
						cstm.setString("E_APELLIDO_PATERNO", filtro.getApellidoPaterno());
						cstm.setString("E_APELLIDO_MATERNO", filtro.getApellidoMaterno());
						
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							RegistroVisitaASala rpt= new RegistroVisitaASala();
							rpt.setFechaVisita(rs.getString("FECHA_VISITA_SALA"));
							rpt.getRegistroUsuario().getUser().setNumeroDocumentoIdentidad(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.getRegistroUsuario().getUser().setTipoDocumentoIdentidadId(rs.getString("TIPO_DCTO_IDENTIDAD_ID"));
							rpt.getRegistroUsuario().getUser().setTipoDocumentoIdentidadNombre(rs.getString("TIPO_DCTO_IDENTIDAD_NOMBRE"));
							rpt.getSala().setIdSala(rs.getInt("SALA_ID"));
							rpt.getSala().setNombreSala(rs.getString("SALA_NOMBRE"));
							rpt.getSala().getSede().setNombreSede(rs.getString("SEDE_NOMBRE"));
							rpt.getRegistroUsuario().getUser().setTipoUsuarioBiblioteca(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.getRegistroUsuario().getUser().setNombre(rs.getString("NOMBRES"));
							rpt.getRegistroUsuario().getUser().setApellidoPaterno(rs.getString("APELLIDO_PATERNO"));
							rpt.getRegistroUsuario().getUser().setApellidoMaterno(rs.getString("APELLIDO_MATERNO"));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:ReporteBandejaDAOImpl:PKG_GESTION_CIRCULACION.SP_LIST_BAND_REGI_USU "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("Exception:ReporteBandejaDAOImpl:lstReporteBandejaAsistenteOAJ "+e.getMessage());
		}

		return listarpt;
	}

	@Override
	public ResponseObject registrarVisitaASala(final RegistroVisitaASala usuarioVisitante) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_VISITAS_SALA.SP_INSERTAR_VISITA_SALA" + "(?,?,?,?,?,?,?) }");
						
						cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD_ID", "1");//usuarioVisitante.getRegistroUsuario().getUser().getTipoDocumentoIdentidadId());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", VO.getStringFormatted(usuarioVisitante.getRegistroUsuario().getUser().getNumeroDocumentoIdentidad()));
						cstm.setInt("E_SALA_ID", usuarioVisitante.getSala().getIdSala());
						cstm.setLong("E_USUARIO_ID_REGISTRO", 999999);
						
						cstm.registerOutParameter("S_REGISTRO_VISITA_ID", OracleTypes.NUMBER);
						cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);	
						cstm.execute();

						String objectRes=(String)cstm.getObject("S_RESULTADO");
						
						if (objectRes.equals("Ingreso registrado de forma exitosa")) {
							cn.commit();
							usuarioVisitante.setResultadoTransaccion(true);
						}else {
							cn.rollback();
							usuarioVisitante.setResultadoTransaccion(false);
						}
						String nombreCompleto=(String)cstm.getObject("S_NOMBRE_COMPLETO");
						usuarioVisitante.setMensajeRespuesta(objectRes);
						usuarioVisitante.getRegistroUsuario().getUser().setNombreCompleto(nombreCompleto);
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:SolicitudDAOImpl:registrarVisitaASala:PKG_GESTION_VISITAS_SALA.SP_INSERTAR_VISITA_SALA "+ex);
						System.out.println("SQLException:SolicitudDAOImpl:registrarVisitaASala:PKG_GESTION_VISITAS_SALA.SP_INSERTAR_VISITA_SALA "+ex);
						e.printStackTrace();
						cn.rollback();
						usuarioVisitante.setResultadoTransaccion(false);
						usuarioVisitante.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			usuarioVisitante.setResultadoTransaccion(false);
			usuarioVisitante.setStackException(e,true);
			throw new PersistenceException("SQLException:SolicitudDAOImpl:insertar:PKG_GESTION_CIRCULACION.SP_INSERTAR_REGISTRO_USUARIO"+e.getMessage());
		}
		result.setResultado(usuarioVisitante.getResultadoTransaccion());
		result.setMensaje(usuarioVisitante.getMensajeRespuesta());
		result.setDetalleMensaje(usuarioVisitante.getRegistroUsuario().getUser().getNombreCompleto());
		return result;	
	}

	@Override
	public ResponseObject obtenerSalaPorDireccionIP(final RegistroVisitaASala registroVisita)
			throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					try {
						Integer idSala = 0;
						cstm = cn.prepareCall("{ Call PKG_GESTION_VISITAS_SALA.SP_GET_SALA_BY_IP_ADDRESS" + "(?,?,?) }");
						
						cstm.setString("E_DIRECCION_IP", registroVisita.getSala().getDireccionIp());
						cstm.registerOutParameter("S_SALA_ID", OracleTypes.NUMBER);
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);	
						cstm.execute();
						String objectRes=(String)cstm.getObject("S_RESULTADO");
						
						if (objectRes.equals("EXITO")) {
							idSala = (Integer)cstm.getObject("S_SALA_ID");
						}
						registroVisita.getSala().setIdSala(idSala);
						registroVisita.setMensajeRespuesta(objectRes);
						registroVisita.setResultadoTransaccion(true);
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:RegistroDAOImpl:obtenerSalaPorDireccionIP:PKG_GESTION_VISITAS_SALA.SP_INSERTAR_VISITA_SALA "+ex);
						System.out.println("SQLException:RegistroDAOImpl:obtenerSalaPorDireccionIP:PKG_GESTION_VISITAS_SALA.SP_INSERTAR_VISITA_SALA "+ex);
						e.printStackTrace();
						registroVisita.setResultadoTransaccion(false);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenceException("SQLException:RegistroDAOImpl:obtenerSalaPorDireccionIP:PKG_GESTION_VISITAS_SALA.SP_INSERTAR_VISITA_SALA"+e.getMessage());
		}
		result.setResultado(registroVisita.getResultadoTransaccion());
		return result;	
	}

	@Override
	public ResponseObject registrarVisitaASalaPorNumeroCarnet(
			final RegistroVisitaASala registroVisita) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_VISITAS_SALA.SP_INS_VIS_SALA_X_NRO_CARNET" + "(?,?,?,?,?,?) }");
						
						cstm.setString("E_NUMERO_CARNET_BIBLIOTECA", VO.getString(registroVisita.getRegistroUsuario().getNumeroCarnetBiblioteca()));//usuarioVisitante.getRegistroUsuario().getUser().getTipoDocumentoIdentidadId());
						cstm.setString("E_DISPOSITIVO_IP", VO.getString(registroVisita.getSala().getDireccionIp()));
						cstm.setLong("E_USUARIO_ID_REGISTRO", Constants.CODE_NEW_ONLINE_USER);
						
						cstm.registerOutParameter("S_REGISTRO_VISITA_ID", OracleTypes.NUMBER);
						cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);	
						cstm.execute();

						String objectRes=(String)cstm.getObject("S_RESULTADO");
						
						if (objectRes.equals("Ingreso registrado de forma exitosa")) {
							cn.commit();
							registroVisita.setResultadoTransaccion(true);
						}else {
							cn.rollback();
							registroVisita.setResultadoTransaccion(false);
						}
						String nombreCompleto=(String)cstm.getObject("S_NOMBRE_COMPLETO");
						registroVisita.setMensajeRespuesta(objectRes);
						registroVisita.getRegistroUsuario().getUser().setNombreCompleto(nombreCompleto);
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("SQLException:RegistroDAOImpl:registrarVisitaASalaPorNumeroCarnet:PKG_GESTION_VISITAS_SALA.SP_INS_VIS_SALA "+ex);
						System.out.println("SQLException:RegistroDAOImpl:registrarVisitaASalaPorNumeroCarnet:PKG_GESTION_VISITAS_SALA.SP_INS_VIS_SALA "+ex);
						e.printStackTrace();
						cn.rollback();
						registroVisita.setResultadoTransaccion(false);
						registroVisita.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			registroVisita.setResultadoTransaccion(false);
			registroVisita.setStackException(e,true);
			throw new PersistenceException("SQLException:RegistroDAOImpl:registrarVisitaASalaPorNumeroCarnet:PKG_GESTION_VISITAS_SALA.SP_INS_VIS_SALA"+e.getMessage());
		}
		result.setResultado(registroVisita.getResultadoTransaccion());
		result.setMensaje(registroVisita.getMensajeRespuesta());
		result.setDetalleMensaje(registroVisita.getRegistroUsuario().getUser().getNombreCompleto());
		return result;	
	}

	@Override
	public ResponseObject registrarUsoVerificacionEntidad(final VerificacionIdentidad verificacionIdentidad) throws PersistenceException {
		final ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_REG_USO_VERIF_IDENTIDAD(?,?,?,?,?,?,?,?,?) }");
						cstm.setString("E_IP_LECTOR", verificacionIdentidad.getIpLector());
						cstm.setString("E_ENTIDAD_CONSULTADA", verificacionIdentidad.getEntidadConsultada());
						cstm.setString("E_URL_CONSULTADA", verificacionIdentidad.getUrlConsultada());
						cstm.setString("E_NUM_DOC_IDENTIDAD", verificacionIdentidad.getNumDocConsultado());
						cstm.setString("E_TIPO_DOC_IDENTIDAD", verificacionIdentidad.getTipoDocConsultado());
						cstm.setString("E_PRIMER_APELLIDO", VO.getStringFormatted(verificacionIdentidad.getApellidoConsultado()));
						cstm.setString("E_ORIGEN_CONSULTA", verificacionIdentidad.getOrigenConsulta());
						cstm.setString("E_CREDENCIAL_USUARIO", verificacionIdentidad.getCredencialUsuario());
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);
						cstm.execute();
						String resultado=cstm.getString("S_RESULTADO");
						if (resultado!=null) {
							cn.commit();
						}else {
							cn.rollback();
						}
						result.setMensaje(resultado);
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_REG_USO_VERIF_IDENTIDAD"+ex);
						System.out.println("PKG_GESTION_CIRCULACION.SP_REG_USO_VERIF_IDENTIDAD "+ex);
						e.printStackTrace();
						cn.rollback();
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_REG_USO_VERIF_IDENTIDAD"+e.getMessage());
		}
		return result;	
	}

	@Override
	public ResponseObject registrarCambioTipoRegistroUsuario(final RegistroUsuario prmObj) throws PersistenceException {
		ResponseObject result = new ResponseObject();
		try {
			Session hbSession = em.unwrap(Session.class);
			//Session hbSession = this.getEntityManagerAvailable().unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					
					try {
						cn.setAutoCommit(false);
						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_REGI_TIPO_REGISTRO_USUARIO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
						cstm.setLong("E_REGISTRO_USUARIO_ID", prmObj.getId());
						cstm.setString("E_TIPO_REGISTRO", VO.getString(prmObj.getConcepto()));
						cstm.setString("E_VIGENCIA", VO.getString(prmObj.getVigencia()));
						cstm.setDate("E_FECHA_INICIO", VO.getSQLDate(prmObj.getFechaInicio()));
						cstm.setDate("E_FECHA_FIN", VO.getSQLDate(prmObj.getFechaFin()));
						cstm.setString("E_CODIGO_BIBLIOTECA", prmObj.getCodBiblioteca());
						cstm.setString("E_NOMBRE_BIBLIOTECA", prmObj.getNombreBiblioteca());
						cstm.setString("E_CODIGO_SUCURSAL", prmObj.getCodSucursal());
						cstm.setString("E_NOMBRE_SUCURSAL", prmObj.getNombreSucursal());
						cstm.setString("E_TIPO_USUARIO_BIBLIOTECA", prmObj.getUser().getTipoUsuarioBiblioteca());
						cstm.setString("E_NOMBRES", VO.getStringFormatted(prmObj.getUser().getNombre()));
						cstm.setString("E_APELLIDOS", VO.getStringFormatted(prmObj.getUser().obtenerApellidoCompleto()));
						cstm.setString("E_TIPO_DOCUMENTO_IDENTIDAD", prmObj.getUser().getTipoDocumentoIdentidadId());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", VO.getStringFormatted(prmObj.getUser().getNumeroDocumentoIdentidad()));
						cstm.setDate("E_FECHA_NACIMIENTO", VO.getSQLDate(prmObj.getUser().getFechaNacimiento()));
						cstm.setString("E_SEXO", VO.getStringFormatted(prmObj.getUser().getSexo()));
						cstm.setString("E_COD_BARRAS_LECTOR", VO.getString(prmObj.getCodBarrasAbsysnet()));
						cstm.setString("E_GRADO_INSTRUCCION", VO.getStringFormatted(prmObj.getUser().getNombreGradoInstruccion()));
						cstm.setString("E_NACIONALIDAD", VO.getStringFormatted(prmObj.getUser().getNombreNacionalidad()));
						cstm.setString("E_PAIS", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombrePais()));
						cstm.setString("E_DEPARTAMENTO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombreDep()));
						cstm.setString("E_PROVINCIA", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombrePro()));
						cstm.setString("E_DISTRITO", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getNombreDis()));
						cstm.setString("E_DIRECCION", VO.getStringFormatted(prmObj.getUser().getDireccion().getDireccionResidencia().getDireccion()));
						cstm.setString("E_CORREO_ELECTRONICO", VO.getStringFormatted(prmObj.getUser().getCorreoElectronico()));
						cstm.setString("E_TELEFONO_FIJO", VO.getStringFormatted(prmObj.getUser().getTelefonoFijo()));					
						cstm.setString("E_TELEFONO_MOVIL", VO.getStringFormatted(prmObj.getUser().getTelefonoMovil()));
						cstm.setString("E_CENTRO_ESTUDIOS", VO.getStringFormatted( prmObj.getUser().getCentroLaboral().getCentroLaboralNombre() ));
						cstm.setString("E_CENTRO_LABORAL", VO.getStringFormatted( prmObj.getUser().getCentroEstudio().getCentroEstudioNombre()));
						cstm.setString("E_TEMA_INVESTIGACION", VO.getStringFormatted(prmObj.getUser().getTemaInvestigacion()));
						cstm.setString("E_ESTADO", VO.getString(prmObj.getEstadoRegistro()));
						cstm.setDate("E_FECHA_INICIO_SUSPENSION", VO.getSQLDate(prmObj.getFechaInicioSuspension()));
						cstm.setDate("E_FECHA_FIN_SUSPENSION", VO.getSQLDate(prmObj.getFechaFinSuspension()));
						cstm.setLong("E_USUARIO_ID_REGISTRO", prmObj.getUsuarioIdModificacion());
						cstm.registerOutParameter("S_RESULTADO", OracleTypes.VARCHAR);
						cstm.execute();
						String objectRes=(String)cstm.getObject("S_RESULTADO");
												
						if (objectRes.equals(Constants.SUCCESS_RESULT)) {
							cn.commit();
							prmObj.setResultadoTransaccion(true);
						}else {
							cn.rollback();
							prmObj.setResultadoTransaccion(false);
						}
						prmObj.setMensajeRespuesta(objectRes);
												
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_REGI_TIPO_REGISTRO_USUARIO "+ex);
						System.out.println("PKG_GESTION_CIRCULACION.SP_REGI_TIPO_REGISTRO_USUARIO "+ex);
						e.printStackTrace();
						cn.rollback();
						prmObj.setResultadoTransaccion(false);
						prmObj.setStackException(e,true);
					}
					finally{
						Conexion.closeCallableStatement(cstm);
					}
				}
			});
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			prmObj.setResultadoTransaccion(false);
			prmObj.setStackException(e,true);
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_REGI_TIPO_REGISTRO_USUARIO "+e.getMessage());
		}
		result.setId(prmObj.getId());
		result.setResultado(prmObj.getResultadoTransaccion());
		result.setMensaje(prmObj.getMensajeRespuesta());
		return result;	
	}

	@Override
	public List<HistorialVigencia> listarVigenciasUsuario(final Long idRegistro)
			throws PersistenceException {
		final List<HistorialVigencia> listarpt = new ArrayList<HistorialVigencia>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_LIST_HIST_VIGE_USU" + "( ?, ? ) }");
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.setLong("E_REGISTRO_USUARIO_ID", idRegistro);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							HistorialVigencia rpt= new HistorialVigencia();
							rpt.setFechaTramiteString(rs.getString("FECHA_TRAMITE_STRING"));
							rpt.setTipoRegistro(rs.getString("TIPO_REGISTRO"));
							rpt.setTipoUsuario(rs.getString("TIPO_USUARIO_BIBLIOTECA"));
							rpt.setEstado(rs.getString("ESTADO"));
							rpt.setVigencia(rs.getString("VIGENCIA"));
							rpt.setFechaInicioString(rs.getString("FECHA_INICIO_STRING"));
							rpt.setFechaFinString(rs.getString("FECHA_FIN_STRING"));
							rpt.setUsuarioRegistroAuditoria(rs.getString("USUARIO"));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_LIST_HIST_VIGE_USU "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_LIST_HIST_VIGE_USU "+e.getMessage());
		}
		return listarpt;
	}
	
}

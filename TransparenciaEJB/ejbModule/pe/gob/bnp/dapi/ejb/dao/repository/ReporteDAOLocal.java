package pe.gob.bnp.dapi.ejb.dao.repository;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.salalectura.dto.SalaBiblioteca;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;

@Local
public interface ReporteDAOLocal extends GenericDAO<FiltroBusquedaReporte> {
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(FiltroBusquedaReporte filtro) throws PersistenceException;
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(FiltroBusquedaReporte filtro) throws PersistenceException;
	
	public List<UsuarioEnSala>  obtenerReporteDetalleAccesosASala(FiltroBusquedaReporte filtro) throws PersistenceException;	
	public List<SalaBiblioteca> listarSalasDisponibles(String codigoSede) throws PersistenceException;
	public List<Usuario> 		listarPersonalDAPI() throws PersistenceException;
	public List<ResumenAtencionDto> obtenerReporteResumenAccesoASala(FiltroBusquedaReporte filtro) throws PersistenceException;
}

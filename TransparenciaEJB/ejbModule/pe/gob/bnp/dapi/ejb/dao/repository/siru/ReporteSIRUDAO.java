package pe.gob.bnp.dapi.ejb.dao.repository.siru;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.ejb.dao.base.impl.GenericDAOImpl;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;
import pe.gob.bnp.dapi.ejb.dao.repository.ReporteDAOLocal;
import pe.gob.bnp.dapi.salalectura.dto.SalaBiblioteca;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.systems.util.sql.Conexion;
import pe.gob.servir.systems.util.validator.VO;

@Stateless
public class ReporteSIRUDAO extends GenericDAOImpl<FiltroBusquedaReporte> implements ReporteDAOLocal {
	private static final Logger logger = Logger.getLogger(ReporteSIRUDAO.class);
	 
	 
	@PersistenceContext(unitName = "PUCirculacion")
	private EntityManager em;	
	 
	@Override
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(final FiltroBusquedaReporte filtro) throws PersistenceException {
		final List<HistorialVigencia> listarpt = new ArrayList<HistorialVigencia>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_REPORTE_REGISTRO_USUARIO" + "( ?, ?, ? ) }");
						cstm.setDate("E_FECHA_DESDE", VO.getSQLDate(filtro.getRangoFechaDesde()));
						cstm.setDate("E_FECHA_HASTA", VO.getSQLDate(filtro.getRangoFechaHasta()));
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							HistorialVigencia rpt= new HistorialVigencia();
							rpt.setFila(VO.getString(rs.getString("FILA")));
							rpt.setFechaTramite(rs.getDate("FECHA_TRAMITE"));
							rpt.setTipoUsuario(VO.getString(rs.getString("TIPO_USUARIO_BIBLIOTECA")));
							rpt.setCodBarrasLector(VO.getString(rs.getString("COD_BARRAS_LECTOR")));
							rpt.setTipoDocumentoIdentidad(VO.getString(rs.getString("TIPO_DOCUMENTO_IDENTIDAD")));
							rpt.setNumeroDocumentoIdentidad(VO.getString(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD")));
							rpt.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
							rpt.setSexo(VO.getString(rs.getString("SEXO")));
							rpt.setTipoRegistro(VO.getString(rs.getString("TIPO_REGISTRO")));
							rpt.setFechaInicio(rs.getDate("FECHA_INICIO"));
							rpt.setFechaFin(rs.getDate("FECHA_FIN"));
							rpt.setGradoInstruccion(VO.getString(rs.getString("GRADO_INSTRUCCION")));
							rpt.setNacionalidad(VO.getString(rs.getString("NACIONALIDAD")));
							rpt.setPais(VO.getString(rs.getString("PAIS")));
							rpt.setDepartamento(VO.getString(rs.getString("DEPARTAMENTO")));
							rpt.setProvincia(VO.getString(rs.getString("PROVINCIA")));
							rpt.setDistrito(VO.getString(rs.getString("DISTRITO")));
							rpt.setEstado(VO.getString(rs.getString("ESTADO")));
							rpt.setOrigen(VO.getString(rs.getString("ORIGEN")));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_REPORTE_REGISTRO_USUARIO "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_REPORTE_REGISTRO_USUARIO "+e.getMessage());
		}
		return listarpt;

	}

	@Override
	public ResponseObject insert(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FiltroBusquedaReporte> list(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FiltroBusquedaReporte findById(FiltroBusquedaReporte entityT)
			throws PersistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(final FiltroBusquedaReporte filtro) throws PersistenceException {
		final List<ResumenAtencionDto> listarpt = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_GESTION_CIRCULACION.SP_REPORTE_RESUMEN_SALAUSUARIO" + "( ?,?,? ) }");
						cstm.setDate("E_PERIODO_DESDE", VO.getSQLDate(filtro.getRangoFechaDesde()));
						cstm.setDate("E_PERIODO_HASTA", VO.getSQLDate(filtro.getRangoFechaHasta()));
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							ResumenAtencionDto rpt= new ResumenAtencionDto();
							rpt.setYear(VO.getString(rs.getString("YEAR")));
							rpt.setMes(VO.getString(rs.getString("MES")));
							rpt.setTipoUsuario(VO.getString(rs.getString("TIPO_USUARIO_BIBLIOTECA")));
							rpt.setTipoRegistro(VO.getString(rs.getString("TIPO_REGISTRO")));
							rpt.setOrigen(VO.getString(rs.getString("ORIGEN")));
							rpt.setDia01(VO.getString(rs.getString("DIA01")));
							rpt.setDia02(VO.getString(rs.getString("DIA02")));
							rpt.setDia03(VO.getString(rs.getString("DIA03")));
							rpt.setDia04(VO.getString(rs.getString("DIA04")));
							rpt.setDia05(VO.getString(rs.getString("DIA05")));
							rpt.setDia06(VO.getString(rs.getString("DIA06")));
							rpt.setDia07(VO.getString(rs.getString("DIA07")));
							rpt.setDia08(VO.getString(rs.getString("DIA08")));
							rpt.setDia09(VO.getString(rs.getString("DIA09")));
							rpt.setDia10(VO.getString(rs.getString("DIA10")));
							rpt.setDia11(VO.getString(rs.getString("DIA11")));
							rpt.setDia12(VO.getString(rs.getString("DIA12")));
							rpt.setDia13(VO.getString(rs.getString("DIA13")));
							rpt.setDia14(VO.getString(rs.getString("DIA14")));
							rpt.setDia15(VO.getString(rs.getString("DIA15")));
							rpt.setDia16(VO.getString(rs.getString("DIA16")));
							rpt.setDia17(VO.getString(rs.getString("DIA17")));
							rpt.setDia18(VO.getString(rs.getString("DIA18")));
							rpt.setDia19(VO.getString(rs.getString("DIA19")));
							rpt.setDia20(VO.getString(rs.getString("DIA20")));
							rpt.setDia21(VO.getString(rs.getString("DIA21")));
							rpt.setDia22(VO.getString(rs.getString("DIA22")));
							rpt.setDia23(VO.getString(rs.getString("DIA23")));
							rpt.setDia24(VO.getString(rs.getString("DIA24")));
							rpt.setDia25(VO.getString(rs.getString("DIA25")));
							rpt.setDia26(VO.getString(rs.getString("DIA26")));
							rpt.setDia27(VO.getString(rs.getString("DIA27")));
							rpt.setDia28(VO.getString(rs.getString("DIA28")));
							rpt.setDia29(VO.getString(rs.getString("DIA29")));
							rpt.setDia30(VO.getString(rs.getString("DIA30")));
							rpt.setDia31(VO.getString(rs.getString("DIA31")));
							rpt.setTotalFila(VO.getString(rs.getString("TOTAL_FILA")));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_GESTION_CIRCULACION.SP_REPORTE_RESUMEN_SALAUSUARIO "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_GESTION_CIRCULACION.SP_REPORTE_RESUMEN_SALAUSUARIO "+e.getMessage());
		}
		return listarpt;
	}

	@Override
	public List<UsuarioEnSala> obtenerReporteDetalleAccesosASala(final FiltroBusquedaReporte filtro) throws PersistenceException {
		final List<UsuarioEnSala> listado = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {

					CallableStatement cstm = null;
					ResultSet rs = null;

					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ASISTENCIA_SALA.SP_LIST_REPO_ACCESO_USU_A_SALA" + "(?,?,?,?,?,?,?,?,?,?) }");
						cstm.setDate("E_FECHA_DESDE", VO.getSQLDate(filtro.getRangoFechaDesde()));
						cstm.setDate("E_FECHA_HASTA", VO.getSQLDate(filtro.getRangoFechaHasta()));
						cstm.setString("E_SALA_ID", filtro.getIdSala());
						cstm.setString("E_PERSONAL_INGRESO_ID", filtro.getIdTrajabadorIngreso());
						cstm.setString("E_PERSONAL_SALIDA_ID", filtro.getIdTrajabadorSalida());
						cstm.setString("E_ESTADO_ASISTENCIA", filtro.getEstadoAsistencia());
						cstm.setString("E_NUMERO_DOCUMENTO_IDENTIDAD", filtro.getNumeroDocumentoIdentidad());
						cstm.setString("E_COD_BARRAS_LECTOR", filtro.getCodigoLector());
						cstm.registerOutParameter("S_COD_MENSAJE",OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						
						cstm.execute();
						
						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						while (rs.next()) {							
							UsuarioEnSala rpt= new UsuarioEnSala();
							rpt.setFila(rs.getLong("FILA"));
							rpt.setIdRegistroAsistencia(rs.getLong("REGISTRO_ASISTENCIA_ID"));
							rpt.setIdRegistroUsuario(rs.getLong("REGISTRO_USUARIO_ID"));
							rpt.setNombreUsuario(rs.getString("NOMBRE_USUARIO"));
							rpt.setTipoDocIdentidad(rs.getString("TIPO_DOCUMENTO_IDENTIDAD"));
							rpt.setNumeroDocIdentidad(rs.getString("NUMERO_DOCUMENTO_IDENTIDAD"));
							rpt.setTipoUsuario(rs.getString("TIPO_USUARIO"));
							rpt.setFechaIngreso(rs.getString("INGRESO_FECHA"));
							rpt.setHoraIngreso(rs.getString("INGRESO_HORA"));
							rpt.setFechaHoraIngreso(rs.getDate("INGRESO_FECHA_HORA"));
							rpt.setEstadoAsistencia(rs.getString("ESTADO_ASISTENCIA"));
							rpt.setUserNameReferencistaIngreso(rs.getString("INGRESO_USUARIO_USERNAME"));
							rpt.setCodBarrasLector(rs.getLong("COD_BARRAS_LECTOR"));
							rpt.setTipoRegistro(rs.getString("TIPO_REGISTRO"));
							rpt.setOrigenRegistro(rs.getString("SISTEMA_ORIGEN"));
							rpt.setTemaInvestigacion(VO.getString(rs.getString("TEMA_INVESTIGACION")));
							rpt.setGradoInstruccion(VO.getString(rs.getString("GRADO_INSTRUCCION")));
							rpt.setSexo(VO.getString(rs.getString("SEXO")));
							rpt.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
							rpt.setNacionalidad(VO.getString(rs.getString("NACIONALIDAD")));
							rpt.setPais(VO.getString(rs.getString("PAIS")));
							rpt.setEstadoSIRU(VO.getString(rs.getString("ESTADO_SIRU")));
							rpt.setUserNameReferencistaSalida(VO.getString(rs.getString("SALIDA_USUARIO_USERNAME")));
							rpt.setNombreReferencistaIngreso(VO.getString(rs.getString("INGRESO_USUARIO_NOMBRE")));
							rpt.setNombreReferencistaSalida(VO.getString(rs.getString("SALIDA_USUARIO_NOMBRE")));
							rpt.setTurnoIngreso(VO.getString(rs.getString("INGRESO_TURNO_NOMBRE")));
							rpt.setPeriodoAcceso(VO.getString(rs.getString("INGRESO_PERIODO")));
							rpt.setMesAcceso(VO.getString(rs.getString("INGRESO_MES")));
							rpt.setDiaAcceso(VO.getString(rs.getString("INGRESO_DIA")));
							rpt.setTurnoSalida(VO.getString(rs.getString("SALIDA_TURNO_NOMBRE")));
							rpt.setFechaHoraSalida(rs.getDate("SALIDA_FECHA_HORA"));
							rpt.setFechaSalida(VO.getString(rs.getString("SALIDA_FECHA")));
							rpt.setHoraSalida(VO.getString(rs.getString("SALIDA_HORA")));
							rpt.setNombreSala(rs.getString("SALA_NOMBRE"));
							rpt.setPisoSala(rs.getString("SALA_PISO"));
							rpt.setDescripcionSalidaMotivo(VO.getString(rs.getString("SALIDA_MOTIVO_NOMBRE")));
							rpt.setSalidaJustificacion(VO.getString(rs.getString("SALIDA_JUSTIFICACION")));		 					
							listado.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_ASISTENCIA_SALA.SP_LIST_REPO_ACCESO_USU_A_SALA"+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_ASISTENCIA_SALA.SP_LIST_REPO_ACCESO_USU_A_SALA"+e.getMessage());
		}
		return listado;
	}

	@Override
	public List<SalaBiblioteca> listarSalasDisponibles(final String codigoSede) throws PersistenceException {
		final List<SalaBiblioteca> listarpt = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ASISTENCIA_SALA.SP_LIST_SALAS_SEDE" + "( ?, ? ) }");
						cstm.setString("E_SEDE_CODIGO_ABSYSNET", codigoSede);
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							SalaBiblioteca rpt= new SalaBiblioteca();
							rpt.setCodigoSedeAbsysnet(rs.getString("SEDE_CODIGO_ABSYSNET"));
							rpt.setIdSala(rs.getLong("SALA_ID"));
							rpt.setNombreSala(rs.getString("SALA_NOMBRE"));
							rpt.setUbicacionPiso(rs.getString("UBICACION_PISO"));
							
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_ASISTENCIA_SALA.SP_LIST_SALAS_SEDE "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_ASISTENCIA_SALA.SP_LIST_SALAS_SEDE "+e.getMessage());
		}
		return listarpt;
	}

	@Override
	public List<Usuario> listarPersonalDAPI() throws PersistenceException {
		final List<Usuario> listarpt = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ASISTENCIA_SALA.SP_LIST_USU_DAPI" + "( ?, ? ) }");
						cstm.registerOutParameter("S_COD_MENSAJE", OracleTypes.VARCHAR);
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							Usuario rpt= new Usuario();
							rpt.setId(rs.getLong("USUARIO_ID"));
							rpt.setNombreCompleto(rs.getString("NOMBRE_COMPLETO"));
							rpt.setUsuario(rs.getString("USUARIO"));
							rpt.setCorreo(rs.getString("CORREO"));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_ASISTENCIA_SALA.SP_LIST_USU_DAPI "+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_ASISTENCIA_SALA.SP_LIST_USU_DAPI "+e.getMessage());
		}
		return listarpt;
	}

	@Override
	public List<ResumenAtencionDto> obtenerReporteResumenAccesoASala(final FiltroBusquedaReporte filtro) throws PersistenceException {
		final List<ResumenAtencionDto> listarpt = new ArrayList<>();
		try {
			Session hbSession = em.unwrap(Session.class);
			hbSession.doWork(new Work() {
				@Override
				public void execute(Connection cn) throws SQLException {
					CallableStatement cstm = null;
					ResultSet rs = null;
					try {
						cn.setAutoCommit(false);

						cstm = cn.prepareCall("{ Call PKG_ASISTENCIA_SALA.SP_REPO_RESU_ACCESO_USU_A_SALA" + "( ?,?,?,?,? ) }");
						cstm.setDate("E_PERIODO_DESDE", VO.getSQLDate(filtro.getRangoFechaDesde()));
						cstm.setDate("E_PERIODO_HASTA", VO.getSQLDate(filtro.getRangoFechaHasta()));
						cstm.setLong("E_SALA_ID", Long.parseLong(filtro.getIdSala()));
						cstm.setString("E_ESTADO_ASISTENCIA", VO.getString(filtro.getEstadoAsistencia()));
						cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
						cstm.execute();

						rs = (ResultSet) cstm.getObject("S_C_CURSOR");
						
						while (rs.next()) {							
							ResumenAtencionDto rpt= new ResumenAtencionDto();
							rpt.setYear(VO.getString(rs.getString("YEAR")));
							rpt.setMes(VO.getString(rs.getString("MES")));
							rpt.setTipoUsuario(VO.getString(rs.getString("TIPO_USUARIO")));
							rpt.setTurnoIngreso(VO.getString(rs.getString("INGRESO_TURNO_NOMBRE")));
							rpt.setNacionalidad(VO.getString(rs.getString("NACIONALIDAD")));
							rpt.setSexo(VO.getString(rs.getString("SEXO")));
							rpt.setGradoInstruccion(VO.getString(rs.getString("GRADO_INSTRUCCION")));
							rpt.setDia01(VO.getString(rs.getString("DIA01")));
							rpt.setDia02(VO.getString(rs.getString("DIA02")));
							rpt.setDia03(VO.getString(rs.getString("DIA03")));
							rpt.setDia04(VO.getString(rs.getString("DIA04")));
							rpt.setDia05(VO.getString(rs.getString("DIA05")));
							rpt.setDia06(VO.getString(rs.getString("DIA06")));
							rpt.setDia07(VO.getString(rs.getString("DIA07")));
							rpt.setDia08(VO.getString(rs.getString("DIA08")));
							rpt.setDia09(VO.getString(rs.getString("DIA09")));
							rpt.setDia10(VO.getString(rs.getString("DIA10")));
							rpt.setDia11(VO.getString(rs.getString("DIA11")));
							rpt.setDia12(VO.getString(rs.getString("DIA12")));
							rpt.setDia13(VO.getString(rs.getString("DIA13")));
							rpt.setDia14(VO.getString(rs.getString("DIA14")));
							rpt.setDia15(VO.getString(rs.getString("DIA15")));
							rpt.setDia16(VO.getString(rs.getString("DIA16")));
							rpt.setDia17(VO.getString(rs.getString("DIA17")));
							rpt.setDia18(VO.getString(rs.getString("DIA18")));
							rpt.setDia19(VO.getString(rs.getString("DIA19")));
							rpt.setDia20(VO.getString(rs.getString("DIA20")));
							rpt.setDia21(VO.getString(rs.getString("DIA21")));
							rpt.setDia22(VO.getString(rs.getString("DIA22")));
							rpt.setDia23(VO.getString(rs.getString("DIA23")));
							rpt.setDia24(VO.getString(rs.getString("DIA24")));
							rpt.setDia25(VO.getString(rs.getString("DIA25")));
							rpt.setDia26(VO.getString(rs.getString("DIA26")));
							rpt.setDia27(VO.getString(rs.getString("DIA27")));
							rpt.setDia28(VO.getString(rs.getString("DIA28")));
							rpt.setDia29(VO.getString(rs.getString("DIA29")));
							rpt.setDia30(VO.getString(rs.getString("DIA30")));
							rpt.setDia31(VO.getString(rs.getString("DIA31")));
							rpt.setTotalFila(VO.getString(rs.getString("TOTAL_FILA")));
							listarpt.add(rpt);
						}
					} catch (SQLException e) {
						String ex = e.getMessage();
						logger.error("PKG_ASISTENCIA_SALA.SP_REPO_RESU_ACCESO_USU_A_SALA"+ ex);

					} finally {
						Conexion.closeSqlConnections(rs, cstm);
					}
				}
			});

		} catch (Exception e) {
			throw new PersistenceException("PKG_ASISTENCIA_SALA.SP_REPO_RESU_ACCESO_USU_A_SALA"+e.getMessage());
		}
		return listarpt;
	}


}

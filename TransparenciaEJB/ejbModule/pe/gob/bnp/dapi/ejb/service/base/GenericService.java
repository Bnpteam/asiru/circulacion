package pe.gob.bnp.dapi.ejb.service.base;

import java.io.Serializable;
import java.util.List;

import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;

public interface GenericService<T extends Serializable> {
	public ResponseObject insert(T entityT) throws ServiceException;
	public Boolean update(T entityT) throws ServiceException;
	public Boolean delete(T entityT) throws ServiceException;
	public List<T> list(T entityT) throws ServiceException;
	public T findById(T entityT) throws ServiceException;
}

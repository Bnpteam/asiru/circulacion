package pe.gob.bnp.dapi.ejb.service.remote;

import java.util.List;

import javax.ejb.Remote;

import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.domain.entity.Preference;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.domain.entity.RegistroVisitaASala;
import pe.gob.bnp.dapi.domain.entity.VerificacionIdentidad;
import pe.gob.bnp.dapi.ejb.service.base.GenericService;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.servir.sistemas.transparencia.model.dto.DocumentoIdentidadDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroUsuarioDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroVisitaDto;
import pe.gob.servir.sistemas.transparencia.model.dto.HistorialRegistroDto;

@Remote
public interface RegistroServiceRemote	extends GenericService<RegistroUsuario> {
	public ResponseObject validarDocumentoIdentidad(DocumentoIdentidadDto documentoIdentidadDto) throws ServiceException;
	public List<RegistroUsuario> listarBandejaRegistrosUsuario(FiltroRegistroUsuarioDto filtro) throws ServiceException;
	public RegistroUsuario obtenerRegistrosUsuario(Long idRegistroUsuario) throws ServiceException;
	public ResponseObject actualizarRegistroUsuario(RegistroUsuario registroUsuario) throws ServiceException;
	public ResponseObject cambiarEstadoRegistroUsuario(RegistroUsuario registroUsuario) throws ServiceException;
	public List<HistorialRegistroDto> listarHistorialRegistroUsuario(Long idRegistro) throws ServiceException;
	public List<Preference> listarPreferenciasUsuario(Long idUsuarioBiblioteca) throws ServiceException;
	public List<RegistroVisitaASala> listarBandejaRegistroVisitasSala(FiltroRegistroVisitaDto filtro) throws ServiceException;
	public ResponseObject registrarVisitaASala(RegistroVisitaASala usuarioVisitante) throws ServiceException;
	public ResponseObject registrarVisitaASalaPorNumeroCarnet(RegistroVisitaASala registroVisita) throws ServiceException;
	public ResponseObject obtenerSalaPorDireccionIP(RegistroVisitaASala registroVisita) throws ServiceException;
	public ResponseObject registrarUsoVerificacionEntidad(VerificacionIdentidad verificacionIdentidad) throws ServiceException;
	public ResponseObject registrarCambioTipoRegistroUsuario(RegistroUsuario registroUsuario) throws ServiceException;
	public List<HistorialVigencia> listarVigenciasUsuario(Long idRegistro) throws ServiceException;
}

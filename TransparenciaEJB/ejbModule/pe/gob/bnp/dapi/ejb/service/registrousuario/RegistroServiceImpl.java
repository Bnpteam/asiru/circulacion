package pe.gob.bnp.dapi.ejb.service.registrousuario;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.domain.entity.Preference;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.domain.entity.RegistroVisitaASala;
import pe.gob.bnp.dapi.domain.entity.VerificacionIdentidad;
import pe.gob.bnp.dapi.ejb.dao.repository.RegistroDAOLocal;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.ejb.service.local.RegistroServiceLocal;
import pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.dto.DocumentoIdentidadDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroUsuarioDto;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroVisitaDto;
import pe.gob.servir.sistemas.transparencia.model.dto.HistorialRegistroDto;

@Stateless
public class RegistroServiceImpl implements RegistroServiceRemote,RegistroServiceLocal{
   
	
	@EJB
	private RegistroDAOLocal registroDAOLocal;
	
	@Override
	public ResponseObject insert(RegistroUsuario prmT) throws ServiceException {
		try {
			return this.registroDAOLocal.insert(prmT);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public Boolean update(RegistroUsuario entityT) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(RegistroUsuario entityT) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RegistroUsuario> list(RegistroUsuario entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegistroUsuario findById(RegistroUsuario entityT)throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public ResponseObject validarDocumentoIdentidad(DocumentoIdentidadDto documentoIdentidadDto)throws ServiceException {
		try {
			return this.registroDAOLocal.validarDocumentoIdentidad(documentoIdentidadDto);
		} catch (Exception e) {
			throw new ServiceException (e);
		}		
	}

	@Override
	public List<RegistroUsuario> listarBandejaRegistrosUsuario(FiltroRegistroUsuarioDto filtro) throws ServiceException {
		try {
			return this.registroDAOLocal.listarBandejaRegistrosUsuario(filtro);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override
	public RegistroUsuario obtenerRegistrosUsuario(Long idRegistroUsuario) throws ServiceException {
		try {
			return this.registroDAOLocal.obtenerRegistrosUsuario(idRegistroUsuario);
		} catch (Exception e) {
			throw new ServiceException (e);
		}		
	}

	@Override
	public ResponseObject actualizarRegistroUsuario(RegistroUsuario registroUsuario) throws ServiceException {
		try {
			return this.registroDAOLocal.actualizarRegistroUsuario(registroUsuario);
		} catch (Exception e) {
			throw new ServiceException (e);
		}		
	}

	@Override
	public ResponseObject cambiarEstadoRegistroUsuario(RegistroUsuario registroUsuario) throws ServiceException {
		try {
			return this.registroDAOLocal.cambiarEstadoRegistroUsuario(registroUsuario);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public List<HistorialRegistroDto> listarHistorialRegistroUsuario(
			Long idRegistro) throws ServiceException {
		try {
			return this.registroDAOLocal.listarHistorialRegistroUsuario(idRegistro);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override
	public List<Preference> listarPreferenciasUsuario(Long idUsuarioBiblioteca)
			throws ServiceException {
		try {
			return this.registroDAOLocal.listarPreferenciasUsuario(idUsuarioBiblioteca);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public List<RegistroVisitaASala> listarBandejaRegistroVisitasSala(
			FiltroRegistroVisitaDto filtro) throws ServiceException {
		try {
			return this.registroDAOLocal.listarBandejaRegistroVisitasSala(filtro);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override
	public ResponseObject registrarVisitaASala(
			RegistroVisitaASala usuarioVisitante) throws ServiceException {
		try {
			return this.registroDAOLocal.registrarVisitaASala(usuarioVisitante);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override 
	public ResponseObject obtenerSalaPorDireccionIP(RegistroVisitaASala registroVisita)
			throws ServiceException {
		try {
			return this.registroDAOLocal.obtenerSalaPorDireccionIP(registroVisita);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override
	public ResponseObject registrarVisitaASalaPorNumeroCarnet(
			RegistroVisitaASala registroVisita) throws ServiceException {
		try {
			return this.registroDAOLocal.registrarVisitaASalaPorNumeroCarnet(registroVisita);
		} catch (Exception e) {
			throw new ServiceException (e);
		}	
	}

	@Override
	public ResponseObject registrarUsoVerificacionEntidad(
			VerificacionIdentidad verificacionIdentidad)
			throws ServiceException {
		try {
			return this.registroDAOLocal.registrarUsoVerificacionEntidad(verificacionIdentidad);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public ResponseObject registrarCambioTipoRegistroUsuario(
			RegistroUsuario registroUsuario) throws ServiceException {
		try {
			return this.registroDAOLocal.registrarCambioTipoRegistroUsuario(registroUsuario);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public List<HistorialVigencia> listarVigenciasUsuario(Long idRegistro)
			throws ServiceException {
		try {
			return this.registroDAOLocal.listarVigenciasUsuario(idRegistro);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}
}


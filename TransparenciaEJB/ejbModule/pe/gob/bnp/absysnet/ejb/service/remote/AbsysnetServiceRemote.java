package pe.gob.bnp.absysnet.ejb.service.remote;

import javax.ejb.Remote;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.base.GenericService;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;

@Remote
public interface AbsysnetServiceRemote extends GenericService<LectorAbsysnet>{
	public ResponseObject guardarLector(LectorAbsysnet lector) throws ServiceException;
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector) throws ServiceException;
}

package pe.gob.bnp.absysnet.ejb.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.absysnet.ejb.dao.local.AbsysnetDAOLocal;
import pe.gob.bnp.absysnet.ejb.service.local.AbsysnetServiceLocal;
import pe.gob.bnp.absysnet.ejb.service.remote.AbsysnetServiceRemote;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.systems.util.retorno.ReturnObject;

@Stateless
public class AbsysnetService implements AbsysnetServiceRemote, AbsysnetServiceLocal {
	@EJB
	private AbsysnetDAOLocal absysnetDAOLocal;


	
	@Override
	public ResponseObject guardarLector(LectorAbsysnet lector) throws ServiceException {
		try {
			return this.absysnetDAOLocal.guardarLector(lector);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector)
			throws ServiceException {
		try {
			return this.absysnetDAOLocal.cambiarEstadoLector(lector);
		} catch (Exception e) {
			throw new ServiceException (e);
		}
	}

	@Override
	public ResponseObject insert(LectorAbsysnet entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(LectorAbsysnet entityT) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(LectorAbsysnet entityT) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LectorAbsysnet> list(LectorAbsysnet entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LectorAbsysnet findById(LectorAbsysnet entityT)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnObject insertar(LectorAbsysnet prmT) throws ServicioException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean actualizar(LectorAbsysnet prmT) throws ServicioException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean eliminar(LectorAbsysnet prmT) throws ServicioException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LectorAbsysnet> listar(LectorAbsysnet prmT)
			throws ServicioException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LectorAbsysnet buscarXId(LectorAbsysnet prmT)
			throws ServicioException {
		// TODO Auto-generated method stub
		return null;
	}
}

package pe.gob.bnp.absysnet.ejb.service.local;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.servir.sistemas.transparencia.ejb.service.base.inf.GenericService;

@Local
public interface AbsysnetServiceLocal extends GenericService<LectorAbsysnet>{
	public ResponseObject guardarLector(LectorAbsysnet lector) throws ServiceException;
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector) throws ServiceException;
}

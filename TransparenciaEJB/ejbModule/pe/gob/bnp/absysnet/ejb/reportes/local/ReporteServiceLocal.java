package pe.gob.bnp.absysnet.ejb.reportes.local;

import java.util.List;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.ejb.service.base.GenericService;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.salalectura.dto.SalaBiblioteca;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;

@Local
public interface ReporteServiceLocal extends GenericService<FiltroBusquedaReporte> {
	public List<HistorialVigencia> obtenerListaReporteRegistroUsuarios(FiltroBusquedaReporte filtro) throws ServiceException;
	public List<ResumenAtencionDto> obtenerReporteResumenAtencionSalaUsuario(FiltroBusquedaReporte filtro) throws ServiceException;
	
	public List<UsuarioEnSala>  obtenerReporteDetalleAccesosASala(FiltroBusquedaReporte filtro) throws ServiceException;
	public List<SalaBiblioteca> listarSalasDisponibles(String codigoSede) throws ServiceException;
	public List<Usuario> 		listarPersonalDAPI() throws ServiceException;
	public List<ResumenAtencionDto> obtenerReporteResumenAccesoASala(FiltroBusquedaReporte filtro) throws ServiceException;
}

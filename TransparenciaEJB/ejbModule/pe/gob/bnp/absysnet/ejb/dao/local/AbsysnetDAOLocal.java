package pe.gob.bnp.absysnet.ejb.dao.local;

import javax.ejb.Local;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.ejb.dao.base.GenericDAO;
import pe.gob.bnp.dapi.ejb.dao.excepcion.PersistenceException;

@Local
public interface AbsysnetDAOLocal extends GenericDAO<LectorAbsysnet>  {
	public ResponseObject guardarLector(LectorAbsysnet lector) throws PersistenceException;
	public ResponseObject cambiarEstadoLector(LectorAbsysnet lector) throws PersistenceException;

}

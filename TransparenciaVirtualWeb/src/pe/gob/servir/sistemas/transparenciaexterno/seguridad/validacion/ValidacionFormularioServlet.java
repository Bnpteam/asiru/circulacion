package pe.gob.servir.sistemas.transparenciaexterno.seguridad.validacion;

import java.io.IOException;
import java.util.Properties;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote;
import pe.gob.servir.sistemas.transparenciaexterno.util.StringEncrypt;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.VO;

public class ValidacionFormularioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoSolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote")
	private MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote;

    public ValidacionFormularioServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codigoEnc = request.getParameter("user");	//User es el c�digo encriptado

    	System.out.println("Codigo doGet: " + codigoEnc);
		
		if(!VO.isEmpty(codigoEnc)){
			
			try {
				// Desencrypta valor.
				String codigo = StringEncrypt.decrypt(this.getString("KEY_ENCRYPT"), this.getString("VECTOR_ENCRYPT"), codigoEnc);
				// Corrobora si el codigo es v�lido y se encuentra programado para ejecutar.
				ReturnObject result = this.getMovimientoSolicitudServiceRemote().validarCodigoFormulario(codigo);
				
				if(result.getSw()){	//C�digo v�lido.

					HttpSession session = request.getSession(true);
					session.setAttribute("codigoEnc",codigoEnc);
					
					response.sendRedirect(request.getContextPath() + this.getString("url.Registra.Solicitud"));
					
				} else {	//C�digo no v�lido.
					response.sendRedirect(request.getContextPath() + this.getString("url.Niega.Solicitud"));
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				response.sendRedirect(request.getContextPath() + this.getString("url.Niega.Solicitud"));
			}
		} else {
			response.sendRedirect(request.getContextPath() + this.getString("url.Niega.Solicitud"));
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + this.getString("url.Niega.Solicitud"));
	}

	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		return "";
	}

	public MovimientoSolicitudServiceRemote getMovimientoSolicitudServiceRemote() {
		return movimientoSolicitudServiceRemote;
	}

	public void setMovimientoSolicitudServiceRemote(
			MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote) {
		this.movimientoSolicitudServiceRemote = movimientoSolicitudServiceRemote;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}	
}

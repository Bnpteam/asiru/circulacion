package pe.gob.servir.sistemas.transparenciaexterno.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

import com.sun.mail.smtp.SMTPMessage;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.properties.Property;
import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;
import pe.gob.servir.systems.util.smtp.ContentIdGenerator;
import pe.gob.servir.systems.util.smtp.SmtpObject;

public class SmtpSendServiceImpl implements Serializable, SmtpSendService{

	private static final Logger logger = Logger.getLogger(SmtpSendServiceImpl.class);
	private static final long serialVersionUID = 1L;

	public static boolean sendMailInlineImage(CorreoModel correo){
//		MimeBodyPart imagePart = new MimeBodyPart(); // ByteArrayOutputStream to File
//
//		imagePart.attachFile(file); // Pasar de 		
		
		return false;
	}
	
	@SuppressWarnings("finally")
	public static boolean Mail(CorreoModel correo) 
    {
		boolean envio = false;
    	System.out.println("Mail");
    	try {		
			   correo = convertCorreo(correo);
			   System.out.println("1");
			   MimeMessage msg = obtenerMimeMessageBNP(correo.getSmtpObject());
			   msg.setFrom(new InternetAddress(correo.getSmtpObject().getAccountMail(),correo.getSmtpObject().getPersonalName()));
			   msg.setSender(new InternetAddress(correo.getSmtpObject().getAccountMail()));
			   msg.setReplyTo(InternetAddress.parse(correo.getSmtpObject().getAccountMail()));
			   System.out.println("2");
			   if( correo != null && correo.getPara() != null &&  correo.getPara().length > 0 ){
					 for (String correitu: correo.getPara()) {
						 msg.addRecipient(Message.RecipientType.TO, new InternetAddress(correitu));
					 }
					 
					 //Para mandar las copias
					 if(correo.getConCopia()!=null && correo.getConCopia().length>0){
						 for(String correoCc: correo.getConCopia()){
							 msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(correoCc));
						 }
					 }
					 
			   }
			   System.out.println("3");
			   msg.setSubject(correo.getAsunto());	
			   
	            MimeMultipart multiParte = new MimeMultipart();
			   
	            MimeBodyPart texto = new MimeBodyPart();
	            texto.setContent(correo.getMensaje(), "text/html; charset=utf-8");
 	            multiParte.addBodyPart(texto);

 	            if (correo.getSmtpObject().getFilePathAdjunto()!=null){
		            MimeBodyPart  adjunto = new MimeBodyPart();   		 	
			   		adjunto.setDataHandler(new DataHandler(new FileDataSource(correo.getSmtpObject().getFilePathAdjunto())));
					adjunto.setFileName(correo.getSmtpObject().getFileNameAdjunto());
					multiParte.addBodyPart(adjunto);
	            }
			   
			   msg.setContent(multiParte);	            
//			   msg.setContent(correo.getMensaje(),"text/html; charset=UTF-8");
			   
			   System.out.println("4");
			   //Transport transport = session.getTransport("smtp");
			   //msg.saveChanges();
			   Transport.send(msg);
			   System.out.println("5");				   
			   envio = true;
			   System.out.println("6");
			   
	   } catch (AddressException e) {
		   logger.error("ERROR: AddressException Mail " + e.getMessage(),e);
		   envio = false;
	   } catch (MessagingException e) {
		   logger.error("ERROR: MessagingException Mail " + e.getMessage(),e);
		   envio = false;
	   } catch (Exception e) {
		   logger.error("ERROR: Exception Mail " + e.getMessage(),e);
		   envio = false;
	   }finally{
		   return envio;
	   }    	    	
    }
	
	// no funciona el envio de la imagen inline
	@SuppressWarnings("finally")
	public static boolean mailv2(CorreoModel correo){
		boolean envio = false;
    	System.out.println("Mail");
    	try {		
			   correo = convertCorreo(correo);
			   MimeMessage msg = obtenerMimeMessageBNP(correo.getSmtpObject());
			   msg.setFrom(new InternetAddress(correo.getSmtpObject().getAccountMail(),correo.getSmtpObject().getPersonalName()));
			   msg.setSender(new InternetAddress(correo.getSmtpObject().getAccountMail()));
			   msg.setReplyTo(InternetAddress.parse(correo.getSmtpObject().getAccountMail()));
			   if( correo != null && correo.getPara() != null &&  correo.getPara().length > 0 ){
					 for (String correitu: correo.getPara()) {
						 msg.addRecipient(Message.RecipientType.TO, new InternetAddress(correitu));
					 }
					 
					 //Para mandar las copias
					 if(correo.getConCopia()!=null && correo.getConCopia().length>0){
						 for(String correoCc: correo.getConCopia()){
							 msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(correoCc));
						 }
					 }
					 
			   }
			   msg.setSubject(correo.getAsunto());	
			   
	            MimeMultipart multiParte = new MimeMultipart();
			   
	            MimeBodyPart texto = new MimeBodyPart();
	            texto.setContent(correo.getMensaje(), "text/html; charset=utf-8");
	            multiParte.addBodyPart(texto);
	            //Archivo adjunto
 	            if (correo.getSmtpObject().getFilePathAdjunto()!=null){
		            MimeBodyPart  adjunto = new MimeBodyPart();   		 	
			   		adjunto.setDataHandler(new DataHandler(new FileDataSource(correo.getSmtpObject().getFilePathAdjunto())));
					adjunto.setFileName(correo.getSmtpObject().getFileNameAdjunto());
					multiParte.addBodyPart(adjunto);
	            }
// 	            //Imagen inline
// 	            for (int i = 0; i < correo.getSmtpObject().getListElementName().length; i++) {
// 				    MimeBodyPart imagePart = new MimeBodyPart();
// 				    //imagePart.attachFile(correo.getSmtpObject().getListElementPath()[i]+correo.getSmtpObject().getListElementName()[i]);
// 				    //imagePart.attachFile("D:\\Workspace\\Develop\\Carpeta\\SIRU\\QRImages\\1PER05361350.jpg");
// 				    
// 				    //imagePart.attachFile(new File("D:\\Workspace\\Develop\\Carpeta\\SIRU\\QRImages\\1PER05361350.jpg"));
// 				    imagePart.attachFile(new File(correo.getSmtpObject().getListElementPath()[i]+correo.getSmtpObject().getListElementName()[i]));
// 				    imagePart.setContentID('<' + correo.getSmtpObject().getListElementPath()[i]+correo.getSmtpObject().getListElementCID()[i] + '>');
// 				    imagePart.setDisposition(MimeBodyPart.INLINE);
// 				    imagePart.setHeader("Content-Type", "image/jpg");
// 				    multiParte.addBodyPart(imagePart); 	            
//				}
			   msg.setContent(multiParte);	            
			   Transport.send(msg);
			   envio = true;
	   } catch (AddressException e) {
		   logger.error("ERROR: AddressException Mail " + e.getMessage(),e);
		   envio = false;
	   } catch (MessagingException e) {
		   logger.error("ERROR: MessagingException Mail " + e.getMessage(),e);
		   envio = false;
	   } catch (Exception e) {
		   logger.error("ERROR: Exception Mail " + e.getMessage(),e);
		   envio = false;
	   }finally{
		   return envio;
	   }    	    	
    }	
	
    @SuppressWarnings("finally")
	public static boolean Mail_old(CorreoModel correo) 
    {
		boolean envio = false;
    	System.out.println("Mail");
    	try {		
			   correo = convertCorreo(correo);
			   System.out.println("1");
			   MimeMessage msg = obtenerMimeMessageBNP(correo.getSmtpObject());
			   msg.setFrom(new InternetAddress(correo.getSmtpObject().getAccountMail(),correo.getSmtpObject().getPersonalName()));
			   msg.setSender(new InternetAddress(correo.getSmtpObject().getAccountMail()));
			   msg.setReplyTo(InternetAddress.parse(correo.getSmtpObject().getAccountMail()));
			   System.out.println("2");
			   if( correo != null && correo.getPara() != null &&  correo.getPara().length > 0 ){
					 for (String correitu: correo.getPara()) {
						 msg.addRecipient(Message.RecipientType.TO, new InternetAddress(correitu));
					 }
					 
					 //Para mandar las copias
					 if(correo.getConCopia()!=null && correo.getConCopia().length>0){
						 for(String correoCc: correo.getConCopia()){
							 msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(correoCc));
						 }
					 }
					 
			   }
			   System.out.println("3");
			   msg.setSubject(correo.getAsunto());			    			   
			   msg.setContent(correo.getMensaje(),"text/html; charset=UTF-8");
			   System.out.println("4");
			   //Transport transport = session.getTransport("smtp");
			   //msg.saveChanges();
			   Transport.send(msg);
			   System.out.println("5");				   
			   envio = true;
			   System.out.println("6");
			   
	   } catch (AddressException e) {
		   logger.error("ERROR: AddressException Mail " + e.getMessage(),e);
		   envio = false;
	   } catch (MessagingException e) {
		   logger.error("ERROR: MessagingException Mail " + e.getMessage(),e);
		   envio = false;
	   } catch (Exception e) {
		   logger.error("ERROR: Exception Mail " + e.getMessage(),e);
		   envio = false;
	   }finally{
		   return envio;
	   }    	    	
    }
    
    public static CorreoModel convertCorreo(CorreoModel correo) throws UnsupportedEncodingException {		
		VelocityEngine ve = new VelocityEngine();
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
	    props.put("input.encoding", "utf-8");
	    props.put("output.encoding", "UTF-8");
        ve.init(props);        
        
        //agregar la ruta de los archivos dentro del contexto
        Map<String,String> map = new HashMap<String, String>();
        //map.put("QRRepositoryPath", Property.getValueKey(Constants.PROPERTY_QRCODE_REPOSITORY_PATH));
		VelocityContext context = new VelocityContext();	 
		System.out.println("convertCorreo IN");
		if(correo.getTemplate() == null){
			
		}else{	
			
			if(correo.getDataMensaje() != null){
				Set<String> indexes = correo.getDataMensaje().keySet();				
				for (String quei : indexes) context.put(quei, correo.getDataMensaje().get(quei));				
					
				StringWriter swOut = new StringWriter();
				 //getClass() logger
		        Reader templateReader = new BufferedReader(new InputStreamReader (cl.getResourceAsStream(correo.getTemplate().getRuta()), "UTF-8"));
		       
		        Velocity.evaluate(context, swOut, "log tag name", templateReader);
		        correo.setMensaje(swOut.toString());
			}
		}
		System.out.println("convertCorreo OUT");
        
		return correo;
	} 
    
    private static MimeMessage obtenerMimeMessageBNP(final SmtpObject smtpObject) throws AddressException, MessagingException {
		
    	Properties propiedades = new Properties();
        propiedades.setProperty("mail.smtp.host", smtpObject.getHost());
        propiedades.setProperty("mail.smtp.port", smtpObject.getPort());
        propiedades.setProperty("mail.smtp.auth", smtpObject.getAuth());
        propiedades.setProperty("mail.smtp.starttls.enable", smtpObject.getEnable());
        System.out.println("obtenerMimeMessageBNP IN");
        //Preparamos la Sesion autenticando al usuario
        Session session = Session.getInstance(propiedades, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(smtpObject.getAccountMail(), smtpObject.getAccountPass());
            }
        });

        //Preparamos el Mensaje
        MimeMessage message = new MimeMessage(session);
        System.out.println("obtenerMimeMessageBNP OUT");    	
		return message;
	}
    
	  public static Session buildMailSession(final SmtpObject smtpObject) {
		    Properties mailProps = new Properties();
		    mailProps.put("mail.transport.protocol", "smtp");
		    mailProps.put("mail.host", smtpObject.getHost());
		   // mailProps.put("mail.from", "desarrollo03.otie@bnp.gob.pe");
		    mailProps.put("mail.smtp.starttls.enable", smtpObject.getEnable());
		    mailProps.put("mail.smtp.port", smtpObject.getPort());
		    mailProps.put("mail.smtp.auth", smtpObject.getAuth());
		    // final, because we're using it in the closure below
		    final PasswordAuthentication usernamePassword = new PasswordAuthentication(
		    		smtpObject.getAccountMail(), smtpObject.getAccountPass());
		    Authenticator auth = new Authenticator() {
		      protected PasswordAuthentication getPasswordAuthentication() {
		        return usernamePassword;
		      }
		    };
		    Session session = Session.getInstance(mailProps, auth);
		    session.setDebug(true);
		    return session;
	  }    
    
    
//    public boolean envioCorreoUsuario(String titulo, String mensaje, String paraEmail, SmtpObject smtpObject, String none){
//    	return SmtpSendServiceImpl.Mail(titulo, mensaje, paraEmail, smtpObject, none);
//    }
//
//	public boolean envioCorreoUsuario(String titulo, String mensaje, String paraEmail, SmtpObject smtpObject) {
//		return SmtpSendServiceImpl.Mail(titulo, mensaje, paraEmail, smtpObject);
//	}

	@Override
	public boolean envioCorreoUsuario(CorreoModel c) {
		return SmtpSendServiceImpl.Mail(c);
	}

	@Override
	public boolean envioCorreoUsuarioSinTemplate(CorreoModel correo) {
		Session session = buildMailSession(correo.getSmtpObject());
		return SmtpSendServiceImpl.sendMailWithoutTemplate(correo, session);
	}

	public static boolean sendMailWithoutTemplate(CorreoModel correo, Session session){
		boolean envio = false;

		try {
			    SMTPMessage msg = new SMTPMessage(session);
			    MimeMultipart content = new MimeMultipart("related");
			    
			    // HTML part
			    MimeBodyPart textPart = new MimeBodyPart();
			    textPart.setText(correo.getTemplateTexto(),"US-ASCII", "html");			    
			    content.addBodyPart(textPart);

			    // Image part
			    MimeBodyPart imagePart = new MimeBodyPart();
			    imagePart.attachFile(correo.getSmtpObject().getImageInlinePath()+ correo.getSmtpObject().getImageInlineName());
			    imagePart.setContentID("<" + correo.getSmtpObject().getImageCID() + ">");
			    imagePart.setDisposition(MimeBodyPart.INLINE);
			    content.addBodyPart(imagePart);
			    
			    msg.setContent(content);
			    msg.setSubject(correo.getAsunto());
			    
				msg.setFrom(new InternetAddress(correo.getSmtpObject().getAccountMail(),correo.getSmtpObject().getPersonalName()));
				msg.setSender(new InternetAddress(correo.getSmtpObject().getAccountMail()));
				msg.setReplyTo(InternetAddress.parse(correo.getSmtpObject().getAccountMail()));
				
				if( correo != null && correo.getPara() != null &&  correo.getPara().length > 0 ){
					 for (String para: correo.getPara()) {
						 msg.addRecipient(Message.RecipientType.TO, new InternetAddress(para));
					 }
					 if(correo.getConCopia()!=null && correo.getConCopia().length>0){
						 for(String correoCc: correo.getConCopia()){
							 msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(correoCc));
						 }
					 }
						 
				}
			    Transport.send(msg);
			    envio = true;
		} catch (AddressException e) {
		   logger.error("ERROR: AddressException Mail " + e.getMessage(),e);
		   envio = false;
		} catch (MessagingException e) {
		   logger.error("ERROR: MessagingException Mail " + e.getMessage(),e);
		   envio = false;
		} catch (Exception e) {
		   logger.error("ERROR: Exception Mail " + e.getMessage(),e);
		   envio = false;
		}
		return envio;
    }




	
}

package pe.gob.servir.sistemas.transparenciaexterno.mail;

import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;

public interface SmtpSendService {
	public boolean envioCorreoUsuario(CorreoModel c);
	public boolean envioCorreoUsuarioSinTemplate(CorreoModel c);
}

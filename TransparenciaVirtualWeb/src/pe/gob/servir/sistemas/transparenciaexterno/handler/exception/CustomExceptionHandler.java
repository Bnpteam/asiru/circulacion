package pe.gob.servir.sistemas.transparenciaexterno.handler.exception;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

public class CustomExceptionHandler extends ExceptionHandlerWrapper {

	private ExceptionHandler wrapped;

	CustomExceptionHandler(ExceptionHandler exception) {
		this.wrapped = exception;
	}

	@Override
	public ExceptionHandler getWrapped() {
		return wrapped;
	}

	@Override
	public void handle() throws FacesException {

		final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents()
				.iterator();
		// System.out.println("Entro ==> ");

		while (i.hasNext()) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event
					.getSource();

			// get the exception from context
			Throwable t = context.getException();

			final FacesContext fc = FacesContext.getCurrentInstance();

			// final NavigationHandler nav =
			// fc.getApplication().getNavigationHandler();
			final NavigationHandler nav = fc.getApplication().getNavigationHandler();


			final Map<String, Object> requestMap = fc.getExternalContext()
					.getRequestMap();

			try {

				if (t instanceof ViewExpiredException) {
					try {
		                if(((ViewExpiredException) t).getViewId().equals("/inicio/inicio.xhtml")){
		                	FacesContext.getCurrentInstance().getExternalContext().redirect("/RegistroUsuarioWeb");
		                } else  {	
							FacesContext.getCurrentInstance().getExternalContext().redirect("/RegistroUsuarioWeb/faces/error/sesion_expired.xhtml");	
		                }
					} catch (IOException e) {
						//
						e.printStackTrace();
					}
				}else{
					nav.handleNavigation(fc, null, "/faces/error/error.xhtml");
				}

				fc.renderResponse();

			} finally {
				// remove it from queue
				i.remove();
			}
		}
		// parent hanle
		getWrapped().handle();
	}

}
package pe.gob.servir.sistemas.transparenciaexterno.presentacion.solicitud;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.primefaces.context.RequestContext;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import pe.gob.reniec.ws.PeticionConsulta;
import pe.gob.reniec.ws.ReniecConsultaDniPortTypeProxy;
import pe.gob.reniec.ws.ResultadoConsulta;
import pe.gob.servir.sisresu.models.entities.PersonaReniecEntity;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.sistemas.transparenciaexterno.mail.SmtpSendService;
import pe.gob.servir.sistemas.transparenciaexterno.mail.SmtpSendServiceImpl;
import pe.gob.servir.sistemas.transparenciaexterno.presentacion.base.BasicMB;
import pe.gob.servir.sistemas.transparenciaexterno.util.StringEncrypt;
import pe.gob.servir.systems.util.alfresco.Archivo;
import pe.gob.servir.systems.util.constante.Constantes;
import pe.gob.servir.systems.util.converter.Convert;
import pe.gob.servir.systems.util.enums.Enums.TemplateMail;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "solicitudExternoMB")
@SessionScoped
public class SolicitudExternoMB extends BasicMB  {

	private static final Logger logger = Logger.getLogger(SolicitudExternoMB.class);
	private static final long serialVersionUID = 1L;

//	propiedades de mantenimiento
	private Solicitud			solicitud;
	private List<T01Maestro> 	lstMedioEntrega; //Lista de Objetos T01Maestros
	//private List<T01Maestro> 	lstUnidadesOrganicas; //Lista de Objetos T01Maestros
	private List<String> 		lstMedioEntrStrg; //Lista de String T01Maestros --> Para CheckBoxMen�.
	private List<String>		lstMedioEntrSlc;
	private String 				txtmediosEntrega;
	private String 				txtmediosEntregaCode;
	private List<T01Maestro> 	lstT01TipoDocumento;
	private List<T01Maestro> 	lstt02UnidadesOragnica;
	
	private ReporteBandeja		reporteBandejaConsulta;
	private List<ReporteBandeja>  lstRptBandeja;  // Seguimiento.
	
	private List<T01Maestro> 	lstT03anio;
	
	private List<UbigeoReniec> 	lstDepartamento;
	private List<UbigeoReniec> 	lstProvincia;
	private List<UbigeoReniec> 	lstDistritos;
	
	private boolean 			swtipoDocumentoDNI;
	private boolean 			swtipoDocumentoCE;
	private boolean 			swtipoDocumentoPanel = true;
	
	private boolean 			swAutorizaPersona;
	private boolean 			swDeclaracionJurada;
	
	private String				nombresApesAdministrado;
	private String				nroSolicitud;
	
	private RequestContext 	    ReqContext;
	
	private String				tipoTransaccion;
	private boolean				mostrarBtnLimpiar;
	private String				txtBtnEnviar;
	
	private SmtpSendService 	mailSender;
	
	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/SolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote")				  
	private SolicitudServiceRemote solicitudServiceRemote;
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoSolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote")
	private MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote;
	
	// Variables EJB.
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/BandejaServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote")
	private BandejaServiceRemote bandejaServiceRemote;
		
	
	@PostConstruct
	public void init() {
		this.setMailSender(new SmtpSendServiceImpl());
		this.setTxtBtnEnviar("Enviar Solicitud");
		this.setMostrarBtnLimpiar(false);
		this.swtipoDocumentoDNI=true;
		this.swtipoDocumentoCE=false;
		this.setSolicitud(new Solicitud());
		this.getSolicitud().setFuncionarioResponsable(super.getT01MaestroValuesNL("T01_FUNCIONARIO_RESPONSABLE").get(0).getNombreLargo());		
		this.getLstT01TipoDocumento();
		this.getLstt02UnidadesOragnica();
		this.setReporteBandejaConsulta(new ReporteBandeja());
		Calendar cal= Calendar.getInstance();
		this.getReporteBandejaConsulta().setAnio_solicitud(String.valueOf(cal.get(Calendar.YEAR)));
		this.setLstRptBandeja(new ArrayList<ReporteBandeja>());
		this.setLstDepartamento(new ArrayList<UbigeoReniec>());
		this.setLstProvincia(new ArrayList<UbigeoReniec>());		
		this.setLstDistritos(new ArrayList<UbigeoReniec>());
		this.getLstDepartamento();
		this.getSolicitud().setTipoDocumentoIdentidadId("1");
		this.setTxtmediosEntrega("");
		this.getSolicitud().setFlag_autoriza_recojo("NO");
		this.setSwAutorizaPersona(false); //RENDERED:PANEL
		this.getSolicitud().setFlagDeclaracioJurada("NO");
		this.setSwDeclaracionJurada(true); //DISABLE:BTNGRABAR
		//this.setLstUndsOrgsSlc(new ArrayList<String>());
		this.setLstMedioEntrStrg(new ArrayList<String>());
		this.setLstMedioEntrSlc(new ArrayList<String>());
		this.llenarCombos();
	}
	
	public String irFormulario() {
		this.limpiar();
		this.setMostrarBtnLimpiar(true);
		return "registro_solicitud";
	}
	
	public void irBienvenida(){
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/TransparenciaVirtualWeb");
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		this.limpiar();
		this.setMostrarBtnLimpiar(false);
	}
	
	public String irSeguimiento() {
		return "lista_solicitud";
	}
	
	public void llenarCombos() {
		this.setLstT01TipoDocumento(super.getT01MaestroValuesNL("T01_TIPO_DOCUMENTO_IDENTIDAD"));
		this.setLstt02UnidadesOragnica(super.getT01MaestroValuesNL("T01_UNIDAD_ORGANICA"));
		this.setLstMedioEntrega(super.getT01MaestroValuesNL("T01_FORMAS_ENTREGA"));
		this.setLstMedioEntrStrg(new ArrayList<String>());
		for (T01Maestro prmT : this.getLstMedioEntrega()) {
			this.getLstMedioEntrStrg().add(prmT.getNombreLargo());
		}
	}
	
	public void limpiar() {
		this.getSolicitud().setId(0L);
		this.getSolicitud().setFuncionarioResponsable(
				super.getT01MaestroValuesNL("T01_FUNCIONARIO_RESPONSABLE")
						.get(0).getNombreLargo());
		this.getSolicitud().setTipoDocumentoIdentidadId("1");
		this.getSolicitud().setNumeroDocumentoIdentidad("");
		this.limpiarAutoReniec();
		this.getSolicitud().setCoddep("0");
		this.getSolicitud().setCodpro("0");
		this.getSolicitud().setCoddis("0");
		this.getSolicitud().setTelefono("");
		this.getSolicitud().setCelular("");
		this.getSolicitud().setCorreo_electronico("");
		this.getSolicitud().setDetalleSolicitud("");
		this.getSolicitud().setDependenciaDeInformacion("0");
		this.setTxtmediosEntrega("");
		this.setLstMedioEntrSlc(new ArrayList<String>());
		this.getSolicitud().setFlag_autoriza_recojo("NO");
		this.setSwAutorizaPersona(false); // RENDERED:PANEL
		this.getSolicitud().setPersAutonroDocumento("");
		this.getSolicitud().setPersAutoNombreApellidos("");
		this.getSolicitud().setObservacion("");
		this.getSolicitud().setFlagDeclaracioJurada("NO");
		this.setSwDeclaracionJurada(true); // DISABLE:BTNGRABAR
	}
	
	public void limpiarAutoReniec() {
		this.getSolicitud().setNombres("");
		this.getSolicitud().setApellidoPaterno("");
		this.getSolicitud().setApellidoMaterno("");
		this.getSolicitud().setDireccionDomicilio("");
	}
	
	public boolean validarRENIEC() {
		
		if(this.getSolicitud().getTipoDocumentoIdentidadId().equals("1")){
			
			if ((VO.isEmpty(this.getSolicitud().getNumeroDocumentoIdentidad().trim()) || 
					this.getSolicitud().getNumeroDocumentoIdentidad().trim().length() != 8)) {
				this.setMensajeAlerta("Ingrese un n�mero de documento v�lido.");
				return false;
			}
				
			ReniecConsultaDniPortTypeProxy reniecConsultaDniPortTypeProxy = new ReniecConsultaDniPortTypeProxy();
			reniecConsultaDniPortTypeProxy.setEndpoint(super.getString("reniec.pide.ws.wsbaseURL"));
			pe.gob.reniec.ws.PeticionConsulta  peticionConsulta = new PeticionConsulta();
			
			peticionConsulta.setNuDniConsulta(this.getSolicitud().getNumeroDocumentoIdentidad().trim());
			peticionConsulta.setNuDniUsuario(super.getString("reniec.pide.ws.dni.usuario"));
			peticionConsulta.setNuRucUsuario(super.getString("reniec.pide.ws.ruc.usuario"));
			peticionConsulta.setPassword(super.getString("reniec.pide.ws.password"));
			
			
			pe.gob.reniec.ws.ResultadoConsulta resultadoConsulta = new ResultadoConsulta();
			try {
				resultadoConsulta = reniecConsultaDniPortTypeProxy.consultar(peticionConsulta);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println(resultadoConsulta.toString());
			
	
			if (!resultadoConsulta.getCoResultado().equals(Constantes.OPERACION_EXITOSA_RENIEC)) {
//				super.setMensajeAlerta("El n�mero de DNI es incorrecto.");
				super.setMensajeAlerta(resultadoConsulta.getDeResultado());
				return false;
			} else {
				
				//this.getLstDepartamento();
				
				this.getSolicitud().setNombres(resultadoConsulta.getDatosPersona().getPrenombres().toUpperCase().trim());
				this.getSolicitud().setApellidoPaterno(resultadoConsulta.getDatosPersona().getApPrimer().toUpperCase().trim());
				this.getSolicitud().setApellidoMaterno(resultadoConsulta.getDatosPersona().getApSegundo().toUpperCase().trim());
				this.getSolicitud().setDireccionDomicilio(resultadoConsulta.getDatosPersona().getDireccion().toUpperCase().trim());
				
				PersonaReniecEntity persona = new PersonaReniecEntity();
				
				persona.setCod_ubigeo(cambiarFormatoUbigeo(resultadoConsulta.getDatosPersona().getUbigeo())); 					
				
				//persona.setCod_ubigeo("150101");
				//Si tiene un ubigeo V�lido.
				if(!VO.isEmpty(persona.getCod_ubigeo()) && persona.getCod_ubigeo().length() == 6){
					String codDep = persona.getCod_ubigeo().substring(0, 2);
					this.getSolicitud().setCoddep(codDep);
					String codPro = persona.getCod_ubigeo().substring(2, 4);
					this.getSolicitud().setCodpro(codPro);
					String codDis = persona.getCod_ubigeo().substring(4, 6);
					this.getSolicitud().setCoddis(codDis);
					
					this.updateProvincia();
					this.updateDistrito();
				}

				super.setMensajeAviso("Su datos han sido autorellenados exitosamente.");
				return true;
			}
		}  else {
			return true;
		}
	}
	
	public String cambiarFormatoUbigeo(String ubigeo){		
		System.out.println("Entre a cambiarFormatoUbigeo");
		System.out.println("ubigeo->"+ubigeo);
		  		  		
		StringTokenizer tokens = new StringTokenizer(ubigeo,"/");
		String nombreDepartamento=tokens.nextToken(); 
		String nombreProvincia=tokens.nextToken(); 
		String nombreDistrito ="";
		
		try {
			nombreDistrito=tokens.nextToken();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error");
			e.printStackTrace();
			
			nombreDistrito = "";			
			
		}finally{
			if(nombreDepartamento.equals("CALLAO")){
				nombreDistrito = nombreProvincia;
				nombreProvincia = nombreDepartamento;
			}
		}
					
		System.out.println(nombreDepartamento+"/"+nombreDistrito+"/"+nombreProvincia);
				
		return buscarCodigoUbigeo(nombreDepartamento, nombreProvincia, nombreDistrito);
	}
	
	public String buscarCodigoUbigeo(String nombreDepartamento, String nombreProvincia, String nombreDistrito){			
		return buscarDistrito(buscarProvincia(buscarDepartamento(nombreDepartamento), nombreProvincia), nombreDistrito);
	}
	
	public String buscarDepartamento(String nombreDepartamento){  
		List<UbigeoReniec> lst = new ArrayList<UbigeoReniec>();
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		String codigoDepartamento = "00";
		tmpubigeo.setDescripcion(nombreDepartamento);
		tmpubigeo.setCoddep("%");
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");		
		try {
			lst = super.getPersonaServiceRemote().listUbigeoXDescripcion(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
		
		if(lst!=null && lst.size()>0){
			codigoDepartamento = lst.get(0).getCoddep();			
		}

		return codigoDepartamento;
	}
	
	public String buscarProvincia(String codigoDepartamento, String nombreProvincia){ 
		List<UbigeoReniec> lst = new ArrayList<UbigeoReniec>();
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		String codigoProvincia = "00";
		tmpubigeo.setDescripcion(nombreProvincia);
		tmpubigeo.setCoddep(codigoDepartamento);
		tmpubigeo.setCodpro("%");
		tmpubigeo.setCoddis("00");		
		try {
			lst = super.getPersonaServiceRemote().listUbigeoXDescripcion(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
		
		if(lst!=null && lst.size()>0){
			codigoProvincia = lst.get(0).getCodpro();			
		}

		return codigoDepartamento+codigoProvincia;
	}
	
	public String buscarDistrito(String codigoDepartamentoProvincia, String nombreDistrito){ 
		List<UbigeoReniec> lst = new ArrayList<UbigeoReniec>();
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		String codigoDistrito = "00";
		tmpubigeo.setDescripcion(nombreDistrito);
		tmpubigeo.setCoddep(codigoDepartamentoProvincia.substring(0, 2));
		tmpubigeo.setCodpro(codigoDepartamentoProvincia.substring(2, 4));
		tmpubigeo.setCoddis("%");		
		try {
			lst = super.getPersonaServiceRemote().listUbigeoXDescripcion(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
		
		if(lst!=null && lst.size()>0){
			codigoDistrito = lst.get(0).getCoddis();			
		}

		return codigoDepartamentoProvincia+codigoDistrito;
	}
	
	
	public void listarSolicitudes() {
		try {
			//EJB Listar.
			this.setLstRptBandeja(this.getBandejaServiceRemote().lstReporteBandejaSeguimiento(this.getReporteBandejaConsulta()));
			
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al buscar solicitud!");
		}
	}
	
	public void limpiarConsulta() {
		this.setReporteBandejaConsulta(new ReporteBandeja());
	}
	
	public List<UbigeoReniec> getLstDepartamento() {
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep("%");
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");		
		try {
			lstDepartamento = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}

		return lstDepartamento;
	}
	
	
	public void updateProvincia(){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(this.getSolicitud().getCoddep());
		tmpubigeo.setCodpro("%");
		tmpubigeo.setCoddis("00");		
		try {
			this.lstProvincia = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	public void updateDistrito(){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(this.getSolicitud().getCoddep());
		tmpubigeo.setCodpro(this.getSolicitud().getCodpro());
		tmpubigeo.setCoddis("%");		
		try {
			this.lstDistritos = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	/*
	public ArrayList<String> textoAListaXToken(String prmK) {
		//Inicializa variable.
		ArrayList<String> resultado = new ArrayList<String>();
		if(!VO.isEmpty(prmK)){
			StringTokenizer tokens = new StringTokenizer(prmK,";");
			while (tokens.hasMoreTokens()) {
				String var = tokens.nextToken().trim();
				
				if (!VO.isEmpty(var)) { resultado.add(var); }
		    }
		}
		return resultado;
	}*/
	// Obtener un la lista nombres largos (LstSelect) a apartir una Cadena de c�digos.
	public ArrayList<String> obtenerLstNombresLargsXCadCode(String prmK) {
		//Inicializa variable.
		ArrayList<String> resultado = new ArrayList<String>();
		if(!VO.isEmpty(prmK)){
			StringTokenizer tokens = new StringTokenizer(prmK,";");
			while(tokens.hasMoreTokens()){
				String var = tokens.nextToken().trim();
				
				if(!VO.isEmpty(var)){
					T01Maestro tmpMaestro = this.getObtenerMaestroUOXCode(this.getLstMedioEntrega(), var);
					if(tmpMaestro != null){
						resultado.add(tmpMaestro.getNombreLargo());
					}
				}
		    }
		}
		return resultado;
	}
	
	// Obtener una cadena de nombres cortos (LstSelect) a apartir una Cadena de c�digos.
	public String obtenerCadNombresCortsXCadCode(String prmK) {
		//Inicializa variable.
		String resultado = "";
		if(!VO.isEmpty(prmK)){
			this.setTxtmediosEntregaCode(prmK);
			StringTokenizer tokens = new StringTokenizer(prmK,";");
			while(tokens.hasMoreTokens()){
				String var = tokens.nextToken().trim();
				
				if(!VO.isEmpty(var)){
					T01Maestro tmpMaestro = this.getObtenerMaestroUOXCode(this.getLstMedioEntrega(), var);
					if(tmpMaestro != null){
						if (VO.isEmpty(resultado)) {
							resultado = tmpMaestro.getNombreCorto();
						} else {
							resultado = resultado + "; " + tmpMaestro.getNombreCorto();
						}
					}
				}
		    }
		}
		
		return resultado;
	}

	//M�todo invocado en login.xhtml para validar la existencia del atributo codigoEnc
	public void validarSesionCodigoEnc() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		HttpSession session = request.getSession();
		
		String codigo = (String) session.getAttribute("codigoEnc");
		
		if (!VO.isEmpty(codigo)) { //Tener c�digo no es suficiente.
			
			String desifr = "";
			
			try {
				// Desifrado.
				desifr = StringEncrypt.decrypt(this.getString("KEY_ENCRYPT"), this.getString("VECTOR_ENCRYPT"), codigo);
				// Corrobora si el c�digo es v�lido y se encuentra programado para ejecutar.
				ReturnObject result = this.getMovimientoSolicitudServiceRemote().validarCodigoFormulario(desifr);
				
				if (result.getSw()) {
					
					//ReturnObject sw = new ReturnObject();
					// Traer data.
					Solicitud tmpSolicitud = new Solicitud();
					tmpSolicitud.setId(Long.parseLong(desifr));
					tmpSolicitud = this.getSolicitudServiceRemote().buscarXId(tmpSolicitud);
					
					if (tmpSolicitud != null) {
						this.getLstDepartamento();
						
						this.setSolicitud(tmpSolicitud);
						
						this.updateProvincia();
						this.updateDistrito();
						
						this.updateTipoDocumentoPanel();
						this.setLstMedioEntrSlc(this.obtenerLstNombresLargsXCadCode(this.getSolicitud().getFormaDeEntrega()));
						this.setTxtmediosEntrega(this.obtenerCadNombresCortsXCadCode(this.getSolicitud().getFormaDeEntrega()));
						this.updateAutoPersonaPanel();
						this.updateDeclaracionJurada();
						
						session.removeAttribute("codigoEnc");
						this.setMostrarBtnLimpiar(false);
						this.setTxtBtnEnviar("Reenviar Solicitud");
						super.setMensajeAviso("Modificaci�n registro solicitud.");
					} else {
						this.irBienvenida();
						super.setMensajeAlerta("El c�digo fue validado exit�samente pero no se encontr� la solicitud, por favor comun�quese con el �rea de TI.");
					}
				} else {
					//No saltar� aqui por que el servlet lo valida.
					this.irBienvenida();
					super.setMensajeAlerta("El c�digo no es v�lido.");
				}
			} catch (Exception e) {
				this.irBienvenida();
				super.setMensajeError("Ocurri� un error al validar el c�digo, por favor comun�quese con el �rea de TI.");
			}
		} else {
			this.limpiar();
			this.setMostrarBtnLimpiar(true);
			this.setTxtBtnEnviar("Enviar Solicitud");
			super.setMensajeAviso("Nuevo registro solicitud.");
		}
	}
	
	// +UPDATE
	public void updateMediosEntrega(){
		
		String dataLetra = "";
		String dataCode  = "";
		
		List<T01Maestro> tmpLstMedEntrSlc = new ArrayList<T01Maestro>();
		int conSlc = this.getLstMedioEntrSlc().size();
		for (int cont = 0; cont < conSlc; cont++) {
			T01Maestro T01MedEntr = this.getObtenerMaestroUOXNombre(this.getLstMedioEntrega(), this.getLstMedioEntrSlc().get(cont));
			tmpLstMedEntrSlc.add(T01MedEntr);
		}
		
		for (T01Maestro medEntr : tmpLstMedEntrSlc) {
			if(VO.isEmpty(dataLetra.trim())){
				dataLetra = medEntr.getNombreCorto();
			} else {
				dataLetra = dataLetra + "; " + medEntr.getNombreCorto();
			}
			
			if (VO.isEmpty(dataCode.trim())) {
				dataCode = medEntr.getCodigoRegistro();
			} else {
				dataCode = dataCode + "; " + medEntr.getCodigoRegistro();
			}
		}
		
		this.setTxtmediosEntrega(dataLetra);
		this.setTxtmediosEntregaCode(dataCode);
		//ReqContext = RequestContext.getCurrentInstance();		
		//ReqContext.update("frmRegSolicitud:pnFormaEntregaPanel");
	}
	// Obtener el Maestro por el Nombre.
	private T01Maestro getObtenerMaestroUOXNombre(List<T01Maestro> prmList01Maestro, String codigo) {

		for (T01Maestro t01Maestro : prmList01Maestro) {
			if (t01Maestro.getNombreLargo().equalsIgnoreCase(codigo)) {
				return t01Maestro;
			}
		}
		return null;
	}
	// Obtener el Maestro por el C�digo.
	private T01Maestro getObtenerMaestroUOXCode(List<T01Maestro> prmList01Maestro, String codigo) {

		for (T01Maestro t01Maestro : prmList01Maestro) {
			if (t01Maestro.getCodigoRegistro().equalsIgnoreCase(codigo)) {
				return t01Maestro;
			}
		}
		return null;
	}

	public void updateTipoDocumentoPanel(){
		if (this.getSolicitud().getTipoDocumentoIdentidadId().equals("1")) {
			this.setSwtipoDocumentoPanel(true);
			
			this.setSwtipoDocumentoDNI(true);
			this.setSwtipoDocumentoCE(false);
		} else {
			this.setSwtipoDocumentoPanel(false);
			
			this.setSwtipoDocumentoDNI(false);
			this.setSwtipoDocumentoCE(true);
		}
	}
	
	public void updateAutoPersonaPanel(){
		if(this.getSolicitud().getFlag_autoriza_recojo().equals("SI")){
			this.setSwAutorizaPersona(true);
		} else {
			this.setSwAutorizaPersona(false);
		}
	}
	
	public void updateDeclaracionJurada(){
		if(VO.isEmpty(this.getSolicitud().getFlagDeclaracioJurada())){
			this.setSwDeclaracionJurada(true); //DISABLE:BTNGRABAR
		}else if (this.getSolicitud().getFlagDeclaracioJurada().equals("SI")){
			this.setSwDeclaracionJurada(false); //DISABLE:BTNGRABAR
		}else{
			this.setSwDeclaracionJurada(true); //DISABLE:BTNGRABAR
		}
	}
	
	public void setLstDepartamento(List<UbigeoReniec> lstDepartamento) {
		this.lstDepartamento = lstDepartamento;
	}

	public String grabar(){
		String pagina = "registro_solicitud";
		Boolean envioCorrecto = false;
		if ( validarGrabado() ) {
			
			ReturnObject sw = new ReturnObject();
			
			try {
				if (this.getSolicitud().getId() == 0L) {
					
					this.getSolicitud().setTipoSolicitud(Constantes.SOLICITUD_VIRTUAL);
					this.getSolicitud().setUsuarioIdRegistro(this.getUsuario().getId());
					this.getSolicitud().setFormaDeEntrega(this.getTxtmediosEntregaCode());
					
					//this.getSolicitud().setDetalleSolicitud(CodeUtil.parseEncode(this.getSolicitud().getDetalleSolicitud()));
					
					sw = this.getSolicitudServiceRemote().insertar(this.getSolicitud());
					
					if (sw.getId() != 0) {
						
						this.setTipoTransaccion("el registro de su solicitud");//Para Mensaje de confirmaci�n.
						String nombres = this.getSolicitud().getNombres().toLowerCase();
						String apPat = this.getSolicitud().getApellidoPaterno().toLowerCase();
						String apMat = this.getSolicitud().getApellidoMaterno().toLowerCase();
						
						String nombsApes = Convert.toTitleCase(nombres + " " + apPat + " " + apMat + ",");

						this.setNombresApesAdministrado(nombsApes);
						this.setNroSolicitud(String.valueOf(sw.getId()));
							
						//1 creo el pdf
						String rutaDocumentoAdjunto = crearPDF(this.getNroSolicitud());
						System.out.println(rutaDocumentoAdjunto);
						
						//1.1 envio correo a ciudadano
						try {
							envioCorrecto = enviarEmailCiudadano(this.getSolicitud(),this.getNroSolicitud());
						} catch (Exception e) {
							logger.error("ERROR: enviarEmailCiudadano : "+ e.getMessage(), e);
							envioCorrecto = false;
						}
						
						System.out.println("Envio -> "+envioCorrecto);
						logger.info("Envio -> "+envioCorrecto);
						
						//2 insertar REGISTRO EN SISTRA WS adjuntar el pdf generado
						// insertar mediante ws soa o rest
												
						//3 creo tarea para el job: envio de correo												
						
						this.limpiar();
						pagina = "confirmacion_solicitud";
						super.setMensajeAviso("�xito al enviar la solicitud.");
					} else {
						super.setMensajeAlerta("Error al enviar la solicitud.");	
					}
					
				} else {
					
					this.getSolicitud().setUsuarioIdModificacion(this.getUsuario().getId());
					this.getSolicitud().setFormaDeEntrega(this.getTxtmediosEntregaCode());
					this.getSolicitud().setVersion_solicitud(this.getSolicitud().getVersion_solicitud() + 1);
					
					sw.setSw(this.getSolicitudServiceRemote().actualizar(this.getSolicitud()));
					
					if (sw.getSw()) {
						//Mensaje de confirmaci�n.
						this.setTipoTransaccion("la modificaci�n de su solicitud");
						String nombres = this.getSolicitud().getNombres().toLowerCase();
						String apPat = this.getSolicitud().getApellidoPaterno().toLowerCase();
						String apMat = this.getSolicitud().getApellidoMaterno().toLowerCase();
						
						String nombsApes = Convert.toTitleCase(nombres + " " + apPat + " " + apMat + ",");
						
						this.setNombresApesAdministrado(nombsApes);
						this.setNroSolicitud(String.valueOf(this.getSolicitud().getCorrelativo()));
						
						this.limpiar();
						pagina = "confirmacion_solicitud";
						super.setMensajeAviso("�xito al reenviar la solicitud.");
					} else {
						super.setMensajeAlerta("Error al reenviar la solicitud.");	
					}
				}
				
			} catch (ServicioException e) {
				super.setMensajeAlerta("Error al reenviar la solicitud!");		
				e.printStackTrace();
			}
		}
		
		return pagina;
	}

	public boolean validarGrabadoPre(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarGrabado()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}

		ReqContext.execute("PF('dlgModal1').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	private boolean validarGrabado() {

		int contador = 0;
		String mensaje = "";
		
		if (VO.isEmpty(this.getSolicitud().getNumeroDocumentoIdentidad().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un n�mero de documento v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getNombres().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un Nombre v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getApellidoPaterno().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un apellido Paterno v�lido, si posee un solo apellido dig�telo tambien en la otra casilla (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getApellidoMaterno().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un apellido Materno v�lido, si posee un solo apellido dig�telo tambien en la otra casilla (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getNombres().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un Nombre v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getDireccionDomicilio().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un Domicilio Legal v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
//		if (VO.isEmpty(this.getSolicitud().getDireccionDomicilio().trim())) {
//			mensaje = mensaje.equals("") ? "Ingrese un Domicilio Legal v�lido (Punto II)."
//					: mensaje;
//			contador++;
//		}
		
		if (("0").equalsIgnoreCase(this.getSolicitud().getCoddep().trim())) {
			mensaje = mensaje.equals("") ? "Seleccione un Departamento (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (("0").equalsIgnoreCase(this.getSolicitud().getCodpro().trim())) {
			mensaje = mensaje.equals("") ? "Seleccione una Provincia (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (("0").equalsIgnoreCase(this.getSolicitud().getCoddis().trim())) {
			mensaje = mensaje.equals("") ? "Seleccione un Distrito (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getTelefono().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un n�mero de tel�fono v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getCelular().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un n�mero de celular v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getCorreo_electronico().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un correo electr�nico (Punto II)."
					: mensaje;
			contador++;
		} else if (!VO.validateEmail(this.getSolicitud().getCorreo_electronico().trim())) {
			mensaje = mensaje.equals("") ? this.getSolicitud().getCorreo_electronico().trim() + " no es un correo electr�nico v�lido (Punto II)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitud().getDetalleSolicitud().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un Detalle de informaci�n solicitada v�lida (Punto III)."
					: mensaje;
			contador++;
		}
		
		if (this.getSolicitud().getDependenciaDeInformacion().equalsIgnoreCase("0")) {
			mensaje = mensaje.equals("") ? "Ingrese una dependencia de informaci�n (Punto IV)."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getTxtmediosEntregaCode())) {
			mensaje = mensaje.equals("") ? "Seleccione un Medio de entrega (Punto V)."
					: mensaje;
			contador++;
		}
		
		if (this.getSolicitud().getFlag_autoriza_recojo().equalsIgnoreCase("SI")) {
			
			if (this.getSolicitud().getPersAutonroDocumento().trim().length() != 8) {
				mensaje = mensaje.equals("") ? "Ingrese un n�mero de documento v�lido (Punto VI)."
						: mensaje;
				contador++;
			}
			
			if (VO.isEmpty(this.getSolicitud().getPersAutoNombreApellidos().trim())) {
				mensaje = mensaje.equals("") ? "Ingrese los nombres y apellidos v�lidos (Punto VI)."
						: mensaje;
				contador++;
			}
		}
		
		if (VO.isEmpty(this.getSolicitud().getObservacion().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un detalle de observaci�n v�lida (Punto VII)."
					: mensaje;
			contador++;
		}/**/
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		logger.debug("Validado con �xito:");
		return true;
	}
	
	private String crearPDF(String codigoP){
		TemplateMail t = null;
		String ruta = null; 
		try {
			
			t = TemplateMail.ADJUNTO_PDF_REGISTRO_SISTRA;
			
			Map<String,Object> mensaje = new HashMap<String, Object>();
		  			
			mensaje.put("cif", codigoP);			
			mensaje.put("solicitante",  WordUtils.capitalizeFully("Manuel Aza�ero Elguera"));
			
			CorreoModel c = new CorreoModel(t);
			c.setDataMensaje(mensaje);
			
			ruta = prepararRutaFormatoPlantilla(c, codigoP);
			
		} catch (Exception e) {
			logger.error("ERROR: enviarEmailObservacion : "+ e.getMessage(), e);
		}
	
		return ruta;
	}
	
	public String prepararRutaFormatoPlantilla(CorreoModel correo, String codigoP) {
		
		String ruta = null;
		
		try {
				correo = convertCorreo(correo);		
				
				if(correo.getTemplate()!=null)				
					ruta = createPdfTemporal(correo.getMensaje(), codigoP);
				
				
	   } catch (Exception e) {
		   logger.error("ERROR: Exception enviarEmailConcytec " + e.getMessage(),e);
	   }	   
		   
		return ruta;
	}
	
	public CorreoModel convertCorreo(CorreoModel correo) throws UnsupportedEncodingException {		
		VelocityEngine ve = new VelocityEngine();
		
		Properties props = new Properties();
	    props.put("input.encoding", "utf-8");
	    props.put("output.encoding", "UTF-8");
//	    props.put("runtime.log.name", "org.apache.velocity");
//	    props.put("runtime.log.invalid.references", "true");
	    
        ve.init(props);        
		VelocityContext context = new VelocityContext();	 
		
		if(correo.getTemplate() != null){
			
			if(correo.getDataMensaje() != null){
				Set<String> indexes = correo.getDataMensaje().keySet();				
				for (String quei : indexes) context.put(quei, correo.getDataMensaje().get(quei));				
					
				StringWriter swOut = new StringWriter();
		        Reader templateReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(correo.getTemplate().getRuta()), "UTF-8"));
		       
		        Velocity.evaluate(context, swOut, "log tag name", templateReader);
		        correo.setMensaje(swOut.toString());
			}
		}

        
		return correo;
	}
	
	private String createPdfTemporal(String html, String codigoP){
		String rutaPdf = "";
		String rutaXml;
		
		try {

			Calendar s = Calendar.getInstance();
			String sdf = new SimpleDateFormat("yyyyMMddHHmmss").format(s.getTime());

			String temporal = "temporal";
			String raiz = super.getString("ruta.documentos.adjunto.email");
			String pdfs = "pdfs";
			String proyecto = VO.isEmpty(codigoP) ? "p-" + sdf :  "p-" + codigoP;
			String fileNamePdf = codigoP;

			fileNamePdf = fileNamePdf + ".pdf";
			String fileNameXhtml = sdf + ".xhtml";
			
			rutaPdf = FilenameUtils.concat(raiz,pdfs);
			Archivo.createFolder(rutaPdf);

			rutaPdf = FilenameUtils.concat(rutaPdf,proyecto);
			Archivo.createFolder(rutaPdf);

			rutaPdf = FilenameUtils.concat(rutaPdf,fileNamePdf);

			rutaXml = FilenameUtils.concat(raiz, temporal);
			
			Archivo.createFolder(rutaXml);
			rutaXml = FilenameUtils.concat(rutaXml, fileNameXhtml);

			File newHtmlFile = new File(rutaXml);
			html = html.replace("&", "&amp; ");
			FileUtils.writeStringToFile(newHtmlFile, html, "UTF-8");

			File checkFile = new File(rutaPdf);
			if(checkFile.exists())
				checkFile.delete();

			// step 1
			Document document = new Document();

			// step 2               
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(rutaPdf));

			// step 3
			document.open();

			// step 4         
			XMLWorkerHelper.getInstance().parseXHtml(writer, document,new FileInputStream(rutaXml), Charset.forName("UTF-8"));

			//step 5
			document.close(); 


		} catch (Exception e) {
			logger.error(e);
		}

		return rutaPdf;
	}
	
	/* CORREO ***********************************************************************************/
	@SuppressWarnings("finally")
	private boolean enviarEmailCiudadano(Solicitud solicitud, String nroSolicitud){
		
		TemplateMail t = null;
		Boolean soloUno = false;
		Boolean envioCorrecto = false;
//		String mensajeCorreo = super.getString("correo.plantilla.ciudadano");
				
		try {
			
//			mensajeCorreo = mensajeCorreo.replace("<CUERPO>", super.getString("mensaje.ciudadano"));
//			mensajeCorreo = mensajeCorreo.replace("{nroSolicitud}", !VO.isEmpty(nroSolicitud) ? nroSolicitud : "-");
//			mensajeCorreo = mensajeCorreo.replace("{nombreCiudadano}", !VO.isEmpty(solicitud.getNombres()) ? (solicitud.getNombres()+" "+solicitud.getApellidoPaterno()+" "+solicitud.getApellidoMaterno()):  "-");
			
			t = TemplateMail.CORREO_NOTIFICACION_CIUDADANO;
			soloUno = true;
										
			Map<String,Object> mensaje = new HashMap<String, Object>();
//			Date date = new Date();
//
//		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
//			//$dia de $mes de $anio
//			mensaje.put("dia", simpleDateFormat.format(date));
//
//		    simpleDateFormat = new SimpleDateFormat("MMMM");
//			mensaje.put("mes", simpleDateFormat.format(date));
//
//		    simpleDateFormat = new SimpleDateFormat("yyyy");			   
//			mensaje.put("anio", simpleDateFormat.format(date));
//				
			mensaje.put("cif", VO.isEmpty(nroSolicitud) ? "N/A" : nroSolicitud );
			mensaje.put("solicitante", !VO.isEmpty(solicitud.getNombres()) ? (solicitud.getNombres()+" "+solicitud.getApellidoPaterno()+" "+solicitud.getApellidoMaterno()):  "-");
//			
//			SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
			//mensaje.put("fechatasapago", contribuyente.getFechaTasaPago() == null ? "N/A": dt1.format(contribuyente.getFechaTasaPago()));
						
			   
			CorreoModel c = new CorreoModel(t);			
			c.getSmtpObject().setAccountMail(super.getString("SMTP_ACCOUNT_MAIL"));
			c.getSmtpObject().setAccountPass(super.getString("SMTP_ACCOUNT_PASS"));
			c.getSmtpObject().setPersonalName(super.getString("SMTP_PERSONAL_NAME"));
			c.getSmtpObject().setHost(super.getString("SMTP_HOST"));
			c.getSmtpObject().setPort(super.getString("SMTP_PORT"));
			c.getSmtpObject().setAuth(super.getString("SMTP_AUTH"));
			c.getSmtpObject().setEnable(super.getString("SMTP_ENABLE"));
			
			c.addPara(solicitud.getCorreo_electronico());
			c.addConCopia("ma.elguera@gmail.com");
			c.setAsunto("Sistema Acceso a la Informaci�n P�blica - BNP: N� de Solicitud "+ nroSolicitud);
			//c.setMensaje(mensajeCorreo);
			c.setDataMensaje(mensaje);
					
			envioCorrecto = mailSender.envioCorreoUsuario(c);
			
		} catch (Exception e) {
			logger.error("ERROR: enviarEmailDeclaracionJurada : "+ e.getMessage(), e);
			envioCorrecto = false;
		}finally{
			return envioCorrecto;
		}
		
	}
	
	

	/* GETTERS AND SETTERS **********************************************************************/
	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}

	public List<T01Maestro> getLstT01TipoDocumento() {
		return lstT01TipoDocumento;
	}

	public void setLstT01TipoDocumento(List<T01Maestro> lstT01TipoDocumento) {
		this.lstT01TipoDocumento = lstT01TipoDocumento;
	}

	public List<T01Maestro> getLstt02UnidadesOragnica() {
		return lstt02UnidadesOragnica;
	}

	public void setLstt02UnidadesOragnica(List<T01Maestro> lstt02UnidadesOragnica) {
		this.lstt02UnidadesOragnica = lstt02UnidadesOragnica;
	}

	public List<ReporteBandeja> getLstRptBandeja() {
		return lstRptBandeja;
	}

	public void setLstRptBandeja(List<ReporteBandeja> lstRptBandeja) {
		this.lstRptBandeja = lstRptBandeja;
	}

	public List<T01Maestro> getLstT03anio() {
		return lstT03anio;
	}

	public void setLstT03anio(List<T01Maestro> lstT03anio) {
		this.lstT03anio = lstT03anio;
	}

	public boolean isSwtipoDocumentoDNI() {
		return swtipoDocumentoDNI;
	}

	public void setSwtipoDocumentoDNI(boolean swtipoDocumentoDNI) {
		this.swtipoDocumentoDNI = swtipoDocumentoDNI;
	}

	public boolean isSwtipoDocumentoCE() {
		return swtipoDocumentoCE;
	}

	public void setSwtipoDocumentoCE(boolean swtipoDocumentoCE) {
		this.swtipoDocumentoCE = swtipoDocumentoCE;
	}

	public List<UbigeoReniec> getLstProvincia() {
		return lstProvincia;
	}

	public void setLstProvincia(List<UbigeoReniec> lstProvincia) {
		this.lstProvincia = lstProvincia;
	}

	public List<UbigeoReniec> getLstDistritos() {
		return lstDistritos;
	}

	public void setLstDistritos(List<UbigeoReniec> lstDistritos) {
		this.lstDistritos = lstDistritos;
	}

	public boolean isSwtipoDocumentoPanel() {
		return swtipoDocumentoPanel;
	}

	public void setSwtipoDocumentoPanel(boolean swtipoDocumentoPanel) {
		this.swtipoDocumentoPanel = swtipoDocumentoPanel;
	}

	public List<T01Maestro> getLstMedioEntrega() {
		return lstMedioEntrega;
	}

	public void setLstMedioEntrega(List<T01Maestro> lstMedioEntrega) {
		this.lstMedioEntrega = lstMedioEntrega;
	}

	public String getTxtmediosEntrega() {
		return txtmediosEntrega;
	}

	public void setTxtmediosEntrega(String txtmediosEntrega) {
		this.txtmediosEntrega = txtmediosEntrega;
	}

	public String getTxtmediosEntregaCode() {
		return txtmediosEntregaCode;
	}

	public void setTxtmediosEntregaCode(String txtmediosEntregaCode) {
		this.txtmediosEntregaCode = txtmediosEntregaCode;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isSwAutorizaPersona() {
		return swAutorizaPersona;
	}

	public void setSwAutorizaPersona(boolean swAutorizaPersona) {
		this.swAutorizaPersona = swAutorizaPersona;
	}

	public boolean isSwDeclaracionJurada() {
		return swDeclaracionJurada;
	}

	public void setSwDeclaracionJurada(boolean swDeclaracionJurada) {
		this.swDeclaracionJurada = swDeclaracionJurada;
	}
	
	public List<String> getLstMedioEntrStrg() {
		return lstMedioEntrStrg;
	}

	public void setLstMedioEntrStrg(List<String> lstMedioEntrStrg) {
		this.lstMedioEntrStrg = lstMedioEntrStrg;
	}
	
	public List<String> getLstMedioEntrSlc() {
		return lstMedioEntrSlc;
	}

	public void setLstMedioEntrSlc(List<String> lstMedioEntrSlc) {
		this.lstMedioEntrSlc = lstMedioEntrSlc;
	}

	public String getNombresApesAdministrado() {
		return nombresApesAdministrado;
	}

	public void setNombresApesAdministrado(String nombresApesAdministrado) {
		this.nombresApesAdministrado = nombresApesAdministrado;
	}

	public String getNroSolicitud() {
		return nroSolicitud;
	}

	public void setNroSolicitud(String nroSolicitud) {
		this.nroSolicitud = nroSolicitud;
	}

	public RequestContext getReqContext() {
		return ReqContext;
	}

	public void setReqContext(RequestContext reqContext) {
		ReqContext = reqContext;
	}

	public String getTipoTransaccion() {
		return tipoTransaccion;
	}

	public void setTipoTransaccion(String tipoTransaccion) {
		this.tipoTransaccion = tipoTransaccion;
	}

	public ReporteBandeja getReporteBandejaConsulta() {
		return reporteBandejaConsulta;
	}

	public void setReporteBandejaConsulta(ReporteBandeja reporteBandejaConsulta) {
		this.reporteBandejaConsulta = reporteBandejaConsulta;
	}

	public boolean isMostrarBtnLimpiar() {
		return mostrarBtnLimpiar;
	}

	public void setMostrarBtnLimpiar(boolean mostrarBtnLimpiar) {
		this.mostrarBtnLimpiar = mostrarBtnLimpiar;
	}

	public String getTxtBtnEnviar() {
		return txtBtnEnviar;
	}

	public void setTxtBtnEnviar(String txtBtnEnviar) {
		this.txtBtnEnviar = txtBtnEnviar;
	}

	//EJB.
	public SolicitudServiceRemote getSolicitudServiceRemote() {
		return solicitudServiceRemote;
	}

	public void setSolicitudServiceRemote(
			SolicitudServiceRemote solicitudServiceRemote) {
		this.solicitudServiceRemote = solicitudServiceRemote;
	}
	
	public MovimientoSolicitudServiceRemote getMovimientoSolicitudServiceRemote() {
		return movimientoSolicitudServiceRemote;
	}

	public void setMovimientoSolicitudServiceRemote(
			MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote) {
		this.movimientoSolicitudServiceRemote = movimientoSolicitudServiceRemote;
	}

	public BandejaServiceRemote getBandejaServiceRemote() {
		return bandejaServiceRemote;
	}

	public void setBandejaServiceRemote(BandejaServiceRemote bandejaServiceRemote) {
		this.bandejaServiceRemote = bandejaServiceRemote;
	}

	public SmtpSendService getMailSender() {
		return mailSender;
	}

	public void setMailSender(SmtpSendService mailSender) {
		this.mailSender = mailSender;
	}
	
}

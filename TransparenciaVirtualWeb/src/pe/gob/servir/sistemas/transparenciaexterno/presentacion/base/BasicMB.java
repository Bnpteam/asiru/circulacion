package pe.gob.servir.sistemas.transparenciaexterno.presentacion.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.service.remote.ServiceParameterTableRemote;
import pe.gob.servir.sisresu.models.entities.PersonaReniecEntity;
import pe.gob.servir.sisresu.service.SRELServiceWS;
import pe.gob.servir.sisresu.service.Impl.SRELServiceWSImpl;
import pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.remoto.T01MaestroServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;
import pe.gob.servir.sistemas.transparencia.model.maestra.Persona;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.systems.util.converter.Convert;
import pe.gob.servir.systems.util.validator.VO;
import pe.gob.servir.systems.util.web.net.Net;

@ManagedBean(name = "basicMB")
public class BasicMB implements Serializable{


	private static final long serialVersionUID = 1L;
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	protected 	String 					currentPage;
	protected	String					msgOperacion;
	protected 	String					msgListaVacia;
	
	protected	String					mensaje;
	
	protected	boolean					swEliminar;	
		
	protected 	List<T01Maestro>		lstSituaciones;	
		    
    private 	StreamedContent 		streamedContent;
    
    protected 	String					ruta;

	private 	boolean					swRegistroHabilitado;
	
	private		String					nombreSistema;
	
	


    
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/T01MaestroServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.mantenimiento.inf.remoto.T01MaestroServiceRemote")	
	private T01MaestroServiceRemote t01MaestroServiceRemote;  

	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/PersonaServiceImpl!pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote")				  
	private PersonaServiceRemote personaServiceRemote;
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/ServiceParameterTable!pe.gob.bnp.dapi.ejb.service.remote.ServiceParameterTableRemote")
	private ServiceParameterTableRemote serviceParameterTableRemote;


	
	public ServiceParameterTableRemote getServiceParameterTableRemote() {
		return serviceParameterTableRemote;
	}

	public void setServiceParameterTableRemote(ServiceParameterTableRemote serviceParameterTableRemote) {
		this.serviceParameterTableRemote = serviceParameterTableRemote;
	}
	
	public List<ParameterTable> getListParameterTableIdentificationDocumentType(String tableCode, String filter) {
		List<ParameterTable> parametersTable = new ArrayList<ParameterTable>();
		try {
			parametersTable = this.getServiceParameterTableRemote().listParameterTableIdentificationDocumentType(tableCode,filter);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			parametersTable = new ArrayList<ParameterTable>();
			this.setMensajeError("Error serviceParameterTableRemote.listParameterTableIdentificationDocumentType: Tabla TBL_PARAMETROS ");
		}
		return parametersTable;
	}
	//
	
	public List<T01Maestro> getT01MaestroValuesNL(String codigoTabla) {
		List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		try {	
			lstT01Maestro = this.getT01MaestroServiceRemote().listarXCodigoTablaNL(codigoTabla);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			lstT01Maestro = new ArrayList<T01Maestro>();
			this.setMensajeError("Error al consultar la Tabla Maestra 01");
		}
		return lstT01Maestro;
	}
	
	public BasicMB() {
		super();
		this.init();
	}

	private void init(){		
		this.setMsgListaVacia("No se han encontrado registros");	
		this.setLstSituaciones(new ArrayList<T01Maestro>());
		this.setNombreSistema("Sistema de Gesti�n de Reclamaciones");
		//this.solicitud.setTblTipoDocumento(new T01Maestro());
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public String getMsgOperacion() {
		return msgOperacion;
	}

	public void setMsgOperacion(String msgOperacion) {
		this.msgOperacion = msgOperacion;
	}
	

	public String getMsgListaVacia() {
		return msgListaVacia;
	}

	public void setMsgListaVacia(String msgListaVacia) {
		this.msgListaVacia = msgListaVacia;
	}	
	
	

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ServletContext getServletContext() {		
		return (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
	}
	
	public String getContextPath() {
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String realPath = ctx.getContextPath();
		return realPath;
	}	

	public String getContextRealPath() {
		ServletContext ctx = this.getServletContext();
		String realPath = ctx.getRealPath("/");
		return realPath;
	}	

	
	public HttpServletResponse getHttpServletResponse() {			
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();		
		return httpServletResponse;	
	}

	public HttpServletRequest getHttpServletRequest() {			
		HttpServletRequest httpServletRequest= (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		return httpServletRequest;	
	}
		
	public HttpSession getHttpSession() {			
		return this.getHttpServletRequest().getSession(true);	
	}

	public Usuario getUsuario() {		
		Usuario usuario= (Usuario)this.getHttpSession().getAttribute("usuario");
		if (usuario==null) {
			usuario=new Usuario();
		}
		return usuario;
	}

	
	public boolean isSwEliminar() {
		return swEliminar;
	}

	public void setSwEliminar(boolean swEliminar) {
		this.swEliminar = swEliminar;
	}

	public List<T01Maestro> getLstSituaciones() {
		return lstSituaciones;
	}

	public void setLstSituaciones(List<T01Maestro> lstSituaciones) {
		this.lstSituaciones = lstSituaciones;
	}


	/**********************************************************************************************************
	 * 	
	 * Auditoria
	 * 
	 **********************************************************************************************************/
		
	public void setUsuarioRegistro(BasicObject objeto) {		
		Usuario usuario=this.getUsuario();
		if (usuario!=null) {
			objeto.setUsuarioIdRegistro(usuario.getId());
			objeto.setIpRegistro(Net.getClientIPAddres(this.getHttpServletRequest()));
		}
		
	}

	public void setUsuarioModificacion(BasicObject objeto) {		
		Usuario usuario=this.getUsuario();
		if (usuario!=null) {
			objeto.setUsuarioIdModificacion(usuario.getId());		
			objeto.setIpModificacion(Net.getClientIPAddres(this.getHttpServletRequest()));
		}
		
	}

	/**********************************************************************************************************
	 * 	
	 * Mensajes
	 * 
	 **********************************************************************************************************/
	
	public void setMensaje(String titulo, String mensaje) {
		FacesMessage msg = new FacesMessage(titulo, mensaje);  
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void  setMensajeAviso(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso",mensaje) );               
	}
	
	public void  setMensajeAlerta(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso",mensaje) );               
	}

	public void  setMensajeError(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Aviso",mensaje) );               
	}

	public boolean copiarArchivo(String fileName, InputStream in) {
       try {
           OutputStream out = new FileOutputStream(new File(this.getRuta() + fileName));
           int read = 0;
           byte[] bytes = new byte[1024];
           while ((read = in.read(bytes)) != -1) {
               out.write(bytes, 0, read);
           }
           in.close();
           out.flush();
           out.close();
           return true;
       } catch (IOException e) {
    	   logger.error(e);
           e.printStackTrace();
           return false;
       }
	}

	
   public boolean superEliminarArchivo(String archivo) {

       try {
           String strFile = this.getRuta() + archivo;
           File file = new File(strFile);
           file.delete();
           return true;
       } catch (Exception e) {
    	   logger.error(e);
           e.printStackTrace();
           return false;
       }
   		
   }
   
   
   public void descargar(String archivo) {
       String strFile = this.getRuta() + archivo;
       File file = new File(strFile);
       InputStream inputStream;
       try {
           inputStream = new FileInputStream(file);
           streamedContent = new DefaultStreamedContent(inputStream, "application/pdf", archivo);
       } catch (FileNotFoundException e) {
    	   logger.error(e);
           e.printStackTrace();
       }
   }
   

   public String getRuta() {
	    return this.getString(this.ruta);
   }

   public void setRuta(String ruta) {
		this.ruta = ruta;
   }

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}
	
	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}   
	

	public List<T01Maestro> getT01MaestroValues(String codigoTabla) {
		
		List<T01Maestro> lstT01Maestro = new ArrayList<T01Maestro>();
		
		try {	
			lstT01Maestro = this.getT01MaestroServiceRemote().listarXCodigoTabla(codigoTabla);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			lstT01Maestro = new ArrayList<T01Maestro>();
		}
		return lstT01Maestro;
	}

	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		return "";
	}

	public T01MaestroServiceRemote getT01MaestroServiceRemote() {
		return t01MaestroServiceRemote;
	}

	public void setT01MaestroServiceRemote(
			T01MaestroServiceRemote t01MaestroServiceRemote) {
			this.t01MaestroServiceRemote = t01MaestroServiceRemote;
	}

	public boolean isSwRegistroHabilitado() {
		return swRegistroHabilitado;
	}

	public void setSwRegistroHabilitado(boolean swRegistroHabilitado) {
		this.swRegistroHabilitado = swRegistroHabilitado;
	}

	public String getNombreSistema() {
		return nombreSistema;
	}

	public void setNombreSistema(String nombreSistema) {
		this.nombreSistema = nombreSistema;
	}

	protected void setStyleLisTitulo(HSSFWorkbook workbook, Cell cell) {
        HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 14);
        font.setFontName("Arial Black");
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setUnderline((byte)1);
        font.setItalic(false);
        
        CellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(cell.getCellStyle());
        newCellStyle.setFont(font);
        cell.setCellStyle(newCellStyle);

    }
        
    protected void setStyleLisCabecera(HSSFWorkbook workbook, Cell cell) {
        HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");
        font.setColor(IndexedColors.WHITE.getIndex());
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(false);

        
        CellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(cell.getCellStyle());

        newCellStyle.setFillForegroundColor(HSSFColor.RED.index);
        newCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        newCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        newCellStyle.setBorderTop((short) 1); // single line border
        newCellStyle.setBorderBottom((short) 1); // single line border
        newCellStyle.setBorderRight((short) 1);
        newCellStyle.setBorderLeft((short) 1);
        newCellStyle.setFont(font);

        cell.setCellStyle(newCellStyle);
    }

	
    protected void setStyleCellAlign(HSSFWorkbook workbook, Cell cell, int tipo) {
        CellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(cell.getCellStyle());
        if (tipo==0) {
            newCellStyle.setAlignment(CellStyle.ALIGN_LEFT);
		} else if(tipo==1) {
	        newCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		} else if(tipo==2) {
	        newCellStyle.setAlignment(CellStyle.ALIGN_RIGHT);
		}
        
        cell.setCellStyle(newCellStyle);
    }


    protected void setStyleFontSize(HSSFWorkbook workbook, Cell cell, int size) {
        HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) size);
        CellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(cell.getCellStyle());
        newCellStyle.setFont(font);
        cell.setCellStyle(newCellStyle);
    }

	public String validarSesion() {
		
		if (this.getUsuario()==null) {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			try {
				response.sendRedirect("../../faces/seguridad/login.xhtml");
			} catch (IOException e) {
				logger.error(e);
				e.printStackTrace();
			}	
		}
			
		return "";
	}
	
	
//	public String copiarArchivoAlfresco(Documentos documento ,String fileName) {
//		 String uiid="";
//			try {
//	    	   
//	    	   String fileNameAbsolute=this.getRuta() + fileName;
//	    	   logger.info("copiarArchivoAlfresco NombreArchivo: "+fileName);
//			   logger.info("copiarArchivoAlfresco RutaArchivo: "+fileNameAbsolute);
//	           ArchivoAlfresco archivoAlfresco=new ArchivoAlfresco();
//	           documento.setNombreArchivo(fileName);
//	           
//			   uiid= archivoAlfresco.subirArchivoLR(documento, fileNameAbsolute);
//	           //logger.info("uiid"+uiid);
//	          
//	       } catch (Exception e) {
//	    	   logger.error(e);
//	           e.printStackTrace();
//	       }
//			return uiid;
//	}
//	public String copiarArchivoAlfrescoLR(Documentos documento ,String fileName) {
//	       String uiid="";
//			try {
//	    	   
//	    	   String fileNameAbsolute=this.getRuta() + fileName;
//	    	   logger.info("copiarArchivoAlfresco NombreArchivo: "+fileName);
//			   logger.info("copiarArchivoAlfresco RutaArchivo: "+fileNameAbsolute);
//	           ArchivoAlfresco archivoAlfresco=new ArchivoAlfresco();
//	           documento.setNombreArchivo(fileName);
//	           
//			   uiid= archivoAlfresco.subirArchivoLR(documento, fileNameAbsolute);
//	           //logger.info("uiid"+uiid);
//	          
//	       } catch (Exception e) {
//	    	   logger.error(e);
//	           e.printStackTrace();
//	       }
//			return uiid;
//	}
	
//	public String actualizarArchivoAlfresco(Archivo archivo,String fileName) {
//	       String msj="";
//			try {
//	    	   
//	    	   String fileNameAbsolute=this.getRuta() + fileName;
//	           ArchivoAlfresco archivoAlfresco=new ArchivoAlfresco();
//	           archivo.setNombreArchivo(fileName);
//	           
//	           logger.info("actualizarArchivoAlfresco fileName"+fileName);
//		       logger.info("actualizarArchivoAlfresco fileNameAbsolute"+fileNameAbsolute);
//	           
//	           msj= archivoAlfresco.actualizarArchivo(archivo, fileNameAbsolute);
//	           //logger.info("MENSAJE BasicMB : "+msj);
//	       } catch (Exception e) {
//	    	   logger.error(e);
//	           e.printStackTrace();
//	       }
//			return msj;
//	}




	public PersonaServiceRemote getPersonaServiceRemote() {
		return personaServiceRemote;
	}

	public void setPersonaServiceRemote(PersonaServiceRemote personaServiceRemote) {
		this.personaServiceRemote = personaServiceRemote;
	}
	
	private Persona buscarPersonaReniec(String prmDni) {
		
		try 
		{
			Persona rsPersona = new Persona();
			SRELServiceWS srelServiceWS = new SRELServiceWSImpl();
			PersonaReniecEntity reniecEntity = srelServiceWS.getPersona(prmDni);
			if ((reniecEntity!=null)&&((!VO.isEmpty(reniecEntity.getDni())))&&(reniecEntity.getF_fallecimiento()==null)) 
			{
				rsPersona.setTipoDocumentoNom("DNI");
				rsPersona.setNroDocumento(reniecEntity.getDni());
				rsPersona.setApPaterno(reniecEntity.getPrimer_apellido());
				rsPersona.setApMaterno(reniecEntity.getSegundo_apellido());
				rsPersona.setNombres(reniecEntity.getNombre());
				rsPersona.setNombreCompleto(reniecEntity.getPrimer_apellido()+" "+reniecEntity.getSegundo_apellido()+", "+reniecEntity.getNombre());
				rsPersona.setDireccion(reniecEntity.getDireccion());
				rsPersona.setUbgDepartamento(reniecEntity.getCod_ubigeo().substring(0, 2));
				rsPersona.setUbgProvincia(reniecEntity.getCod_ubigeo().substring(2, 4));
				rsPersona.setUbgDistrito(reniecEntity.getCod_ubigeo().substring(4, 6));
				rsPersona.setNomDepartamento(reniecEntity.getDepartamento());
				rsPersona.setNomProvincia(reniecEntity.getProvincia());
				rsPersona.setNomDistrito(reniecEntity.getDistrito());
				rsPersona.setSexo(reniecEntity.getSexo());
				rsPersona.setSexoNom(rsPersona.getSexo().equals("F") ? "FEMENINO" : "MASCULINO");
				rsPersona.setEstadoCivil(reniecEntity.getEstado_civil());
				rsPersona.setFechaNacimiento(Convert.toDate(reniecEntity.getF_nacimiento()));
				rsPersona.setNomPadre(reniecEntity.getNom_padre());
				rsPersona.setNomMadre(reniecEntity.getNom_madre());
				rsPersona.setFechaFallecimiento(Convert.toDate(reniecEntity.getF_fallecimiento()));
				rsPersona.setFechaCaducidad(Convert.toDate(reniecEntity.getF_caducidad()));
				
				try {
					personaServiceRemote.insertar(rsPersona);
				} catch (Exception e) {
					logger.error(e);
					e.printStackTrace();
				}
				
				return rsPersona; 
			}else {
				return null;
			}
					
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			return null;
		}		
	}
	
	public Persona buscarPersonaXDni(String prmDni) {
		
		Persona rsPersona = new Persona();		
		try {
			rsPersona = personaServiceRemote.buscarPersonaXDni(prmDni);
			
			if ((rsPersona!=null)&&((rsPersona.getNroDocumento()!=null)&&(rsPersona.getNroDocumento().length()>0))){
				return rsPersona;
			}			
			else {
				rsPersona = this.buscarPersonaReniec(prmDni);			
//				rsPersona = personaServiceRemote.buscarPersonaXDni(rsPersona.getNroDocumento());
				return rsPersona;
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();			
			return null;
		}		
	}

}

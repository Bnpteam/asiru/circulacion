package pe.gob.servir.sistemas.transparenciaexterno.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class HeaderFilter implements Filter {

	private String modeXFrame = "SAMEORIGIN"; //DENY
	private String modeXSSProt = "1; mode=block";
	private String modeXContType = "nosniff";
	
	@Override
	public void destroy() {
		// 
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain filterChain) throws IOException, ServletException {
		
		HttpServletResponse response = (HttpServletResponse) res; 
		response.addHeader("X-FRAME-OPTIONS", modeXFrame );
        response.setHeader("X-XSS-Protection", modeXSSProt); 
        response.setHeader("X-Content-Type-Options", modeXContType); 
        filterChain.doFilter(req, res); 
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
		String configModeXFrame = filterConfig.getInitParameter("modeXFrame");
		String configModeXSSProt = filterConfig.getInitParameter("modeXSSProt");
		String configmodeXContType = filterConfig.getInitParameter("modeXContType");
		
		if ( configModeXFrame != null ) {
			modeXFrame = configModeXFrame;
		}
		
		if ( configModeXSSProt != null ) {
			modeXSSProt = configModeXSSProt;
		}
		
		if ( configmodeXContType != null ) {
			modeXContType = configmodeXContType;
		}
	}

}

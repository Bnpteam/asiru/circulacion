package pe.gob.bnp.dapi.registroonlineusuario.controller;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.common.util.hardware.Device;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.RegistroVisitaASala;
import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroVisitaDto;
import pe.gob.servir.systems.util.validator.VO;


@ManagedBean(name = "bandejaRegistroVisitasSalaMB")
@SessionScoped
public class BandejaRegistroVisitasSalaMB extends BaseControllerExtern { // extends
	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(BandejaRegistroVisitasSalaMB.class);

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/SolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote")
	private SolicitudServiceRemote solicitudServiceRemote;

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/RegistroServiceImpl!pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote")
	private RegistroServiceRemote registroServiceRemote;
	

	private Date fechaActual;
	  
	private RegistroVisitaASala registroVisita;
	
	private String motivoRechazoIngresoSala;
	
	private List<ParameterTable> tiposDocumentoIdentidad;	
	
	private List<Pais>			lstPais;
	
	private String numeroDocumentoCEoDni;
	private String numeroDocumentoPasaporte;
	
	@PostConstruct
	public void init() {
		this.nombreSistema = this.getString(Constants.NOMBRE_SISTEMA_PROPERTIES);
		this.versionSistema = this.getString(Constants.VERSION_SISTEMA_PROPERTIES);


		this.fechaActual = new Date();

		this.registroVisita = new RegistroVisitaASala();
		this.lstPais = new ArrayList<Pais>();
		this.loadComboLists();
		this.numeroDocumentoCEoDni = Constants.EMPTY_STRING;
		this.numeroDocumentoPasaporte = Constants.EMPTY_STRING;
		this.registroVisita.getRegistroUsuario().init();
	}
	
	public void loadComboLists() {
		this.lstPais =  super.getListPaises();
		//this.setTiposDocumentoIdentidad(super.getListParameterTableIdentificationDocumentType(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD, "1"));
	}	
	
	public void restablecerCampos(){
		this.registroVisita.getRegistroUsuario().getUser().setCodPaisPasaporte(Constants.EMPTY_STRING);
		this.registroVisita.getRegistroUsuario().setNumeroCarnetBiblioteca(Constants.EMPTY_STRING);
		this.numeroDocumentoCEoDni = Constants.EMPTY_STRING;
		this.numeroDocumentoPasaporte = Constants.EMPTY_STRING;
	}
	
	public String registrarVisitaASala() {//nuevos usuarios
		String pagina = Constants.PAGINA_INICIO_REGISTRO_VISITA;
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		try{
			ResponseObject resultado = null;
			this.motivoRechazoIngresoSala = "";
			String direccionIp = httpServletRequest.getRemoteAddr();	
			this.registroVisita.getSala().setDireccionIp(direccionIp);
			resultado = registroServiceRemote.registrarVisitaASalaPorNumeroCarnet(this.registroVisita);
			if (resultado != null){
				this.registroVisita.getRegistroUsuario().getUser().setNombreCompleto(resultado.getDetalleMensaje());
				if (resultado.getMensaje().equals(Constants.VISITA_SALA_REGISTRO_EXITOSO)){
					pagina = "accesoPermitidoSala";
				}else{
					this.motivoRechazoIngresoSala = resultado.getMensaje();
					pagina = "accesoDenegadoSala";
				}
				super.setMensajeAlerta(resultado.getMensaje());
				//this.init();
			}else{
				super.setMensajeAlerta("No se pudo realizar la acci�n");
			}
			this.restablecerCampos();
		}catch (ServiceException e) {
				super.setMensajeAlerta("Error al guardar registro de visita!");
				e.printStackTrace();
		}
		return pagina;
	}		

	public String registrarVisitaConCodigoQR(){
		this.registroVisita.getRegistroUsuario().obtenerDatosIdentidadPorCodigoQR();
		return this.registrarVisitaASala();
	}
	
	public String registrarVisitaConDNI(){
		this.registroVisita.getRegistroUsuario().getUser().setTipoDocumentoIdentidadId(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE);
		this.registroVisita.getRegistroUsuario().getUser().setCodPaisPasaporte(Constants.COUNTRY_CODE_PERU);
		this.registroVisita.getRegistroUsuario().obtenerDatosIdentidadPorDocIdentidad();
		return this.registrarVisitaASala();
	}	

	public String registrarVisitaConCE(){
		this.registroVisita.getRegistroUsuario().getUser().setTipoDocumentoIdentidadId(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE);
		this.registroVisita.getRegistroUsuario().getUser().setCodPaisPasaporte(Constants.COUNTRY_CODE_PERU);
		this.registroVisita.getRegistroUsuario().obtenerDatosIdentidadPorDocIdentidad();
		return this.registrarVisitaASala();
	}	

	public String registrarVisitaConPasaporte(){
		this.registroVisita.getRegistroUsuario().getUser().setTipoDocumentoIdentidadId(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_CODE);
		this.registroVisita.getRegistroUsuario().obtenerDatosIdentidadPorDocIdentidad();
		return this.registrarVisitaASala();
	}	
	

	public String iniciarRegistroVisitaASala(){
		return "ingresarASala";
	}	
	
	public String iniciarRegistroVisitaConCodigoEscrito(){
		return "escribirCodigo";
	}
	
	//
	public String registrarVisitaConCodigoEscaneado(){
		this.registroVisita.getRegistroUsuario().setNumeroCarnetBiblioteca(this.registroVisita.getRegistroUsuario().getNumeroCarnetBiblioteca().trim().toUpperCase());
		if (this.registroVisita.getRegistroUsuario().getNumeroCarnetBiblioteca().equals(Constants.EMPTY_STRING)){
			super.setMensajeAlerta("Debe ingresar el c�digo");
			return "ingresaSala";
		}
		return this.registrarVisitaASala();
	}
	
	public String registrarVisitaEscribiendoCEoDNI(){
		if (this.numeroDocumentoCEoDni.trim().equals(Constants.EMPTY_STRING)){
			super.setMensajeAlerta("Debe ingresar el n�mero de documento");
			return "escribirCodigo";
		}		
		this.registroVisita.getRegistroUsuario().setNumeroCarnetBiblioteca(this.numeroDocumentoCEoDni.trim().toUpperCase());
		return this.registrarVisitaASala();
	}
	
	public String registrarVisitaEscribiendoPasaporte(){
		if (this.numeroDocumentoPasaporte.trim().equals(Constants.EMPTY_STRING)){
			super.setMensajeAlerta("Debe ingresar el n�mero de documento");
			return "escribirCodigo";
		}
		if (this.registroVisita.getRegistroUsuario().getUser().getCodPaisPasaporte().trim().equals(Constants.ZERO_VALUE_STRING)){
			super.setMensajeAlerta("Debe seleccionar el pa�s de emisi�n");
			return "escribirCodigo";
		}		
		this.registroVisita.getRegistroUsuario().setNumeroCarnetBiblioteca(this.registroVisita.getRegistroUsuario().getUser().getCodPaisPasaporte() + this.numeroDocumentoPasaporte.trim().toUpperCase());
		return this.registrarVisitaASala();
	}	
	
	public String continuarRegistroVisitaSala(String paso) {
		this.registroVisita.getRegistroUsuario().init();
		if (paso.equals("irInicioRegistroVisita")) {
			return "iniciarRegistroVisita";
		}		
		if (paso.equals("irMenuPrincipal")) {
			return "menuPrincipal";
		}		
		if (paso.equals("irOpcionesCodigoQR")) {
			return "opcionesCodigoQR";
		}
		if (paso.equals("irOpcionesSinCodigoQR")) {
			return "opcionesSinCodigoQR";
		}
		if (paso.equals("irOpcionTengoDNI")) {
			return "opcionTengoDNI";
		}
		if (paso.equals("irOpcionTengoCE")) {
			return "opcionTengoCE";
		}
		if (paso.equals("irOpcionTengoPasaporte")) {
			return "opcionTengoPasaporte";
		}
		if (paso.equals("irPasarDNI")) {
			return "pasarDNI";
		}
		if (paso.equals("irIngresarDNI")) {
			return "ingresarDNI";
		}	
		if (paso.equals("irIngresarPasaporte")) {
			return "ingresarPasaporte";
		}	
		if (paso.equals("irIngresarCE")) {
			return "ingresarCE";
		}		
		return "iniciarRegistroVisita";
	}

	public String getAddressMac() {
		String macAddress = "";
		try {
			InetAddress ip = InetAddress.getLocalHost();
			System.out.println("Current IP address : " + ip.getHostAddress());
			
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			
			byte[] mac = network.getHardwareAddress();
				
			System.out.print("Current MAC address : ");
				
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
			}	
			macAddress = sb.toString();
		} catch (SocketException  e) {
			e.printStackTrace();
		} catch ( UnknownHostException e) {
			e.printStackTrace();
		}	
		return macAddress;
	}
	
	public String goToHomePage() {
		return Constants.PAGE_HOME_INTERN;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}


	public String getMotivoRechazoIngresoSala() {
		return motivoRechazoIngresoSala;
	}


	public void setMotivoRechazoIngresoSala(String motivoRechazoIngresoSala) {
		this.motivoRechazoIngresoSala = motivoRechazoIngresoSala;
	}

	public List<ParameterTable> getTiposDocumentoIdentidad() {
		return tiposDocumentoIdentidad;
	}

	public void setTiposDocumentoIdentidad(
			List<ParameterTable> tiposDocumentoIdentidad) {
		this.tiposDocumentoIdentidad = tiposDocumentoIdentidad;
	}

	public RegistroVisitaASala getRegistroVisita() {
		return registroVisita;
	}

	public void setRegistroVisita(RegistroVisitaASala registroVisita) {
		this.registroVisita = registroVisita;
	}

	public List<Pais> getLstPais() {
		return lstPais;
	}

	public void setLstPais(List<Pais> lstPais) {
		this.lstPais = lstPais;
	}

	public String getNumeroDocumentoCEoDni() {
		return numeroDocumentoCEoDni;
	}

	public void setNumeroDocumentoCEoDni(String numeroDocumentoCEoDni) {
		this.numeroDocumentoCEoDni = numeroDocumentoCEoDni;
	}

	public String getNumeroDocumentoPasaporte() {
		return numeroDocumentoPasaporte;
	}

	public void setNumeroDocumentoPasaporte(String numeroDocumentoPasaporte) {
		this.numeroDocumentoPasaporte = numeroDocumentoPasaporte;
	}

	
}

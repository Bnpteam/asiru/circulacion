package pe.gob.bnp.dapi.registroonlineusuario.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.ByteArrayContent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.tempuri.AbsysNETSoapProxy;

import pe.gob.bnp.absysnet.domain.entity.LectorAbsysnet;
import pe.gob.bnp.absysnet.ejb.service.remote.AbsysnetServiceRemote;
import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.RegistroUsuario;
import pe.gob.bnp.dapi.domain.entity.VerificacionIdentidad;
import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote;
import pe.gob.migraciones.usuarios.consulta.ws.MigraCarnetdeExtrajeriaPortTypeProxy;
import pe.gob.migraciones.usuarios.consulta.ws.SolicitudBean;
//import pe.gob.migraciones.usuarios.consulta.ws.MigraCarnetdeExtrajeriaPortTypeProxy;
//import pe.gob.migraciones.usuarios.consulta.ws.SolicitudBean;
import pe.gob.reniec.ws.PeticionConsulta;
import pe.gob.reniec.ws.ReniecConsultaDniPortTypeProxy;
import pe.gob.reniec.ws.ResultadoConsulta;
import pe.gob.servir.sisresu.models.entities.PersonaReniecEntity;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.smtp.EmailUsuario;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.dto.DocumentoIdentidadDto;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.systems.util.converter.Convert;
import pe.gob.servir.systems.util.validator.VO;

import com.itextpdf.text.pdf.BarcodeQRCode;
import com.lowagie.text.Element;
import com.lowagie.text.pdf.Barcode39;

@ManagedBean(name = "userCreationMB")
@SessionScoped
public class OnlineUserCreation extends BaseControllerExtern { // extends
																// BasicMB {
								
	// //BaseControllerExtern
	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(OnlineUserCreation.class);

	private List<ParameterTable> tiposDocumentoIdentidad;

	private String tipoDocumentoIdentidadxValidar;
	private String numeroDocumentoIdentidadxValidar;
	private String primerApellidoxValidar;

	private RegistroUsuario request;

	private boolean showPanelBusquedaReniec;
	private boolean showPanelBusquedaMigraciones;
	private boolean showTipoDocumentoDNI;
	private boolean showTipoDocumentoCE;
	private boolean showTipoDocumentoPasaporte;	
	private boolean showPaisPeru;
	private boolean isDisabledBotonSave;
	private boolean showFotoReniec; 
	
	private List<UbigeoReniec> 	lstDepartamento;
	private List<UbigeoReniec> 	lstProvincia;
	private List<UbigeoReniec> 	lstDistritos;
	private List<Pais>			lstPais;
	
	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/SolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote")				  
	private SolicitudServiceRemote solicitudServiceRemote;                         

	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/RegistroServiceImpl!pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote")
	private RegistroServiceRemote registroServiceRemote;

	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/AbsysnetService!pe.gob.bnp.absysnet.ejb.service.remote.AbsysnetServiceRemote")				  
	private AbsysnetServiceRemote absysnetServiceRemote;
	
	private RequestContext 	    requestContext;	

	private StreamedContent streamedContentPhotoReniec;
	private StreamedContent streamedContentPhotoAdjuntada;
	
	private Boolean showSeccionInvestigacion;
	
    private List<ParameterTable> listaPreferencias;
    
    private Date fechaActual;
    
	private int currentYear;
	
	private List<ParameterTable> listaNacionalidad;
	private List<ParameterTable> listaGradoInstruccion;
	
	@PostConstruct
	public void init() {
		this.nombreSistema = this.getString(Constants.NOMBRE_SISTEMA_PROPERTIES);
		this.versionSistema = this.getString(Constants.VERSION_SISTEMA_PROPERTIES);
		this.tipoDocumentoIdentidadxValidar = Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE;
		this.numeroDocumentoIdentidadxValidar = Constants.EMPTY_STRING;
		this.primerApellidoxValidar = Constants.EMPTY_STRING;
		this.showPanelBusquedaReniec = true;
		this.showPanelBusquedaMigraciones = false;
		this.showTipoDocumentoDNI = true;
		this.showTipoDocumentoCE = false;
		this.showTipoDocumentoPasaporte = false;
		
		this.showPaisPeru= true;
		this.isDisabledBotonSave = true;
		this.showFotoReniec = true;
		
		this.request = new RegistroUsuario();
//		this.request.getUser().setNumeroDocumentoIdentidad("40037569");
//		this.request.getUser().setTipoDocumentoIdentidadId("1");
		
		this.lstDepartamento =new ArrayList<UbigeoReniec>();
		this.lstProvincia = new ArrayList<UbigeoReniec>();		
		this.lstDistritos =new ArrayList<UbigeoReniec>();	
		this.lstPais = new ArrayList<Pais>();
		this.getLstDepartamento();		
		this.showSeccionInvestigacion = false;
		
		listaPreferencias = new ArrayList<ParameterTable>();
		
		fechaActual = new Date();
		
		this.currentYear = Calendar.getInstance().get(Calendar.YEAR);
		super.setStreamedContent(null);
		this.streamedContentPhotoReniec = null; 
		this.streamedContentPhotoAdjuntada = null;
		this.loadComboLists();
		
	}
	
	public void definirElementosActivos(){
		this.isDisabledBotonSave = true;
		
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
			this.showSeccionInvestigacion = false;
		}else{
			this.showSeccionInvestigacion = true;
		}
		
		if (this.request.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE)){
			this.showFotoReniec =true;
		}else{
			this.showFotoReniec =false;
		}
		
	}	

	public void downloadCartaPresentacionUsuario() {
		try {
			byte[] pdf = this.request.getUser().getCartaPresentacion().getRawFile();
			StreamedContent streamedContent = new DefaultStreamedContent(new ByteArrayInputStream(pdf), "application/"+this.request.getUser().getCartaPresentacion().getExtension(),"carta_de_presentacion"+this.request.getUser().getNumeroDocumentoIdentidad()+"."+this.request.getUser().getCartaPresentacion().getExtension());
			super.setStreamedContent(streamedContent);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void updateLetterAcceptance(){
		if(this.request.getUser().getFlagAceptoCompromiso().equals(Constants.YES_VALUE)){
			this.isDisabledBotonSave = false;
		}else{
			this.isDisabledBotonSave = true;
		}
	}
	
	
	public void handleFileUploadLetter(FileUploadEvent event) {
		String fileName;
		try {
			UploadedFile fu = event.getFile();
			byte[] fileBytes = fu.getContents();
			fileName = fu.getFileName();
			this.request.getUser().getCartaPresentacion().setFileName(fileName);
			this.request.getUser().getCartaPresentacion().setExtension(fileName.substring(fileName.lastIndexOf(".") + 1));
			this.request.getUser().getCartaPresentacion().setRawFile(fileBytes);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al cargar el archivo");
		}
	}	
	
	public void handleFileUploadPhoto(FileUploadEvent event) {
		String fileName;
		try {
			UploadedFile fu = event.getFile();
			byte[] fileBytes = fu.getContents();
			fileName = fu.getFileName();
			this.request.getUser().getFotoAdjuntada().setFileName(fileName);
			this.request.getUser().getFotoAdjuntada().setExtension(fileName.substring(fileName.lastIndexOf(".") + 1)); 
			this.request.getUser().getFotoAdjuntada().setRawFile(fileBytes);
			this.streamedContentPhotoAdjuntada = new DefaultStreamedContent(fu.getInputstream(), Constants.FILE_CONTENT_TYPE_IMAGE_PREFIX + this.request.getUser().getFotoAdjuntada().getExtension());
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al cargar el archivo");
		}
	}	

	public boolean validatePreSave(){
		requestContext = RequestContext.getCurrentInstance();
		if (!this.validateSave()){return false;}
			//requestContext.execute("PF('dlgModal1').show()");
		this.save();
		return true;
	}
	
	private boolean validateSaveBase() {
		int contador = 0;
		String mensaje = "";
		
		if (VO.isEmpty(this.request.getUser().getTipoDocumentoIdentidadId())) {
			mensaje = mensaje.equals("") ? "Ingrese el tipo de documento de identidad. (Use la secci�n "+Constants.SECCION_VERIFICACION_IDENTIDAD+")."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.request.getUser().getNumeroDocumentoIdentidad().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese el n�mero de documento de identidad. (Use la secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(this.request.getUser().getNombre().trim())) {
			mensaje = mensaje.equals("") ? "Ingresar el nombre. (Use la secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(this.request.getUser().getApellidoPaterno().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese su apellido paterno. (Use la secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
					: mensaje;
			contador++;
		}		

//		if (VO.isEmpty(this.request.getUser().getApellidoMaterno())) {
//			if (this.request.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE)){
//				mensaje = mensaje.equals("") ? "Ingrese su apellido materno. (secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
//						: mensaje;
//				contador++;
//			}
//		}

		if (VO.isEmpty(this.request.getUser().getFechaNacimiento())) {
			mensaje = mensaje.equals("") ? "Ingrese la fecha de nacimiento. (secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
					: mensaje;
			contador++;
		}
		
		if (VO.yearMayoraActual(this.request.getUser().getFechaNacimiento())){
			mensaje = mensaje.equals("") ? "El a�o de nacimiento no debe ser mayor al a�o actual. (secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
					: mensaje;
			contador++;
		}

		if (!VO.yearValidoPorRango(this.request.getUser().getFechaNacimiento(),100)){
			mensaje = mensaje.equals("") ? "Verificar la fecha de nacimiento. (secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
					: mensaje;
			contador++;
		}
		
		
		//validar si es menor de edad que el a�o actual - a�o de nacimiento sea menos o igual a 18 a�os
		if (this.request.getUser().getFlagMayorDeEdad().equals(Constants.NO_VALUE)){//reniec
			if (!VO.fechaEstaDentroDeRango(this.request.getUser().getFechaNacimiento(),18)){
				mensaje = mensaje.equals("") ? "La fecha de nacimiento debe corresponder a un menor de edad . (secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
						: mensaje;
				contador++;
			}
		}else{//es mayor de edad, a�o de nacimiento debe ser mayor o igual a 17
			if (VO.fechaEstaDentroDeRango(this.request.getUser().getFechaNacimiento(),16)){
				mensaje = mensaje.equals("") ? "La fecha de nacimiento debe corresponder a un mayor de edad . (secci�n "+Constants.SECCION_DATOS_PERSONALES+")."
						: mensaje;
				contador++;
			}
		}		

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request.getUser().getSexo().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese el sexo. (secci�n "+Constants.SECCION_DATOS_PERSONALES+").."
					: mensaje;
			contador++;
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request.getUser().getIdNacionalidad().trim())) {
			mensaje = mensaje.equals("") ? "Seleccione su nacionalidad. (secci�n "+Constants.SECCION_DATOS_PERSONALES+").."
					: mensaje;
			contador++;
		}
		
		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request.getUser().getIdGradoInstruccion().trim())) {
			mensaje = mensaje.equals("") ? "Seleccione su grado de instrucci�n. (secci�n "+Constants.SECCION_DATOS_PERSONALES+").."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.request.getUser().getDireccion().getDireccionResidencia().getDireccion())) {
			mensaje = mensaje.equals("") ? "Ingrese la direcci�n. (secci�n "+Constants.SECCION_DATOS_DIRECCION+")."
					: mensaje;
			contador++;
		}

		if ((Constants.ZERO_VALUE_STRING)
				.equalsIgnoreCase(this.request.getUser().getDireccion().getDireccionResidencia().getCodPais())) {
			mensaje = mensaje.equals("") ? "Seleccione el pa�s de residencia. (secci�n "+Constants.SECCION_DATOS_DIRECCION+")."
					: mensaje;
			contador++;
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request.getUser().getDireccion().getDireccionResidencia().getCodDep())) {
			if (this.request.getUser().getDireccion().getDireccionResidencia().getCodPais().equals(Constants.COUNTRY_CODE_PERU)) {
				mensaje = mensaje.equals("") ? "Seleccione el departamento. (secci�n "+Constants.SECCION_DATOS_DIRECCION+")."
						: mensaje;
				contador++;
			}
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request.getUser().getDireccion().getDireccionResidencia().getCodPro())) {
			if (this.request.getUser().getDireccion().getDireccionResidencia().getCodPais().equals(Constants.COUNTRY_CODE_PERU)) {
				mensaje = mensaje.equals("") ? "Seleccione la provincia. (secci�n "+Constants.SECCION_DATOS_DIRECCION+")."
						: mensaje;
				contador++;
			}
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request
				.getUser().getDireccion().getDireccionResidencia().getCodDis())) {
			if (this.request.getUser().getDireccion().getDireccionResidencia().getCodPais().equals(Constants.COUNTRY_CODE_PERU)) {
				mensaje = mensaje.equals("") ? "Seleccione el distrito. (secci�n "+Constants.SECCION_DATOS_DIRECCION+")."
						: mensaje;
				contador++;
			}
		}
		if (!VO.isEmptyString(this.request.getUser().getCorreoElectronico())) {
			if (!VO.validateEmail(this.request.getUser().getCorreoElectronico().trim())) {
				mensaje = mensaje.equals("") ? this.request.getUser().getCorreoElectronico().trim() + " no es un correo electr�nico v�lido (secci�n "+Constants.SECCION_DATOS_CONTACTO+")."
						: mensaje;
				contador++;
			}				
		}

//		if (!VO.isEmptyString(this.request.getUser().getCorreoElectronico())) {
//			mensaje = mensaje.equals("") ? "Ingrese un correo electr�nico. (secci�n "+Constants.SECCION_DATOS_CONTACTO+")."
//					: mensaje;
//			contador++;
//		}
//
//		if (VO.isEmpty(this.request.getUser().getTelefonoMovil())) {
//			mensaje = mensaje.equals("") ? "Ingrese un tel�fono m�vil. (secci�n "+Constants.SECCION_DATOS_CONTACTO+")."
//					: mensaje;
//			contador++;
//		}

		if (VO.isEmpty(this.request.getUser().getCorreoElectronico()) && VO.isEmpty(this.request.getUser().getTelefonoMovil()) && VO.isEmpty(this.request.getUser().getTelefonoFijo())  ) {
			mensaje = mensaje.equals("") ? "Debe ingresar al menos uno de los datos de contacto (correo electr�nico, tel�fono fijo, tel�fono m�vil) . (secci�n "+Constants.SECCION_DATOS_CONTACTO+")."
					: mensaje;
			contador++;
		}		
		

		if (VO.isEmpty(this.request.getUser().getFlagLaboroActualmente())) {
			mensaje = mensaje.equals("") ? "Seleccione si labora actualmente. (secci�n "+Constants.SECCION_DATOS_LABORAL+")."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(VO.getString(this.request.getUser().getCentroLaboral()
				.getCentroLaboralNombre()))) {
			if (this.request.getUser().getFlagLaboroActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Ingrese el nombre del centro laboral. (secci�n "+Constants.SECCION_DATOS_LABORAL+")."
						: mensaje;
				contador++;
			}
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request
				.getUser().getCentroLaboral().getTipo())) {
			if (this.request.getUser().getFlagLaboroActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Seleccione el tipo de centro laboral. (secci�n "+Constants.SECCION_DATOS_LABORAL+")."
						: mensaje;
				contador++;
			}
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request
				.getUser().getCentroLaboral().getDireccion().getCodPais())) {
			if (this.request.getUser().getFlagLaboroActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Seleccione el pais del centro laboral. (secci�n "+Constants.SECCION_DATOS_LABORAL+")."
						: mensaje;
				contador++;
			}
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getCentroLaboral()
				.getDireccion().getNombreCiudad()))) {
			if (this.request.getUser().getFlagLaboroActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Ingrese la ciudad. (secci�n "+Constants.SECCION_DATOS_LABORAL+")."
						: mensaje;
				contador++;
			}
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getCentroLaboral()
				.getDireccion().getDireccion()))) {
			if (this.request.getUser().getFlagLaboroActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Ingrese la direcci�n del centro laboral. (secci�n "+Constants.SECCION_DATOS_LABORAL+")."
						: mensaje;
				contador++;
			}
		}

		if (VO.isEmpty(this.request.getUser().getFlagEstudioActualmente())) {
			mensaje = mensaje.equals("") ? "Seleccione si estudia actualmente. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getCentroEstudio()
				.getCentroEstudioNombre()))) {
			if (this.request.getUser().getFlagEstudioActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Ingrese el nombre del centro de estudio. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
						: mensaje;
				contador++;
			}
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request
				.getUser().getCentroEstudio().getTipo())) {
			if (this.request.getUser().getFlagEstudioActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Seleccione el tipo de centro de estudio. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
						: mensaje;
				contador++;
			}
		}

		if ((Constants.ZERO_VALUE_STRING).equalsIgnoreCase(this.request
				.getUser().getCentroEstudio().getDireccion().getCodPais())) {
			if (this.request.getUser().getFlagEstudioActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Seleccione el pais del centro de estudio. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
						: mensaje;
				contador++;
			}
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getCentroEstudio()
				.getDireccion().getNombreCiudad()))) {
			if (this.request.getUser().getFlagEstudioActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Ingrese la ciudad. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
						: mensaje;
				contador++;
			}
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getCentroEstudio()
				.getDireccion().getDireccion()))) {
			if (this.request.getUser().getFlagEstudioActualmente()
					.equals(Constants.YES_VALUE)) {
				mensaje = mensaje.equals("") ? "Ingrese la direcci�n del centro de estudio. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
						: mensaje;
				contador++;
			}
		}

		if (VO.isEmpty(this.request.getUser().getFlagAceptoCompromiso())) {
			mensaje = mensaje.equals("") ? "Seleccione si acepta el acta de compromiso. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")."
					: mensaje;
			contador++;
		}
			
		if (contador > 0) {
			super.setAlertMessage(mensaje);
			return false;
		}
		logger.debug("Validado con �xito:");		
		return true;
	}

	private boolean validateSaveGeneralUser() {
		return true;
	}

	private boolean validateSaveResearcherUser() {
		int contador = 0;
		String mensaje = "";

		if (VO.isEmpty(VO.getString(this.request.getUser().getTemaInvestigacion()))) {
			mensaje = mensaje.equals("") ? "Ingrese el Tema de Investigaci�n. (secci�n "+Constants.SECCION_DATOS_INVESTIGACION+")."
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getCartaPresentacion().getFileName()))) {
			mensaje = mensaje.equals("") ? "Adjunte la Carta de Presentaci�n. (secci�n "+Constants.SECCION_DATOS_INVESTIGACION+")." 
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(VO.getString(this.request.getUser().getTipoInstitucionAvala()))) {
			mensaje = mensaje.equals("") ? "Seleccione la Instituci�n aval. (secci�n "+Constants.SECCION_DATOS_INVESTIGACION+")."
					: mensaje;
			contador++;
		}
		
		if (VO.getString(this.request.getUser().getTipoInstitucionAvala()).equals(Constants.TIPO_INSTITUCION_AVAL_CENTRO_ESTUDIOS)){
			if(!this.request.getUser().getFlagEstudioActualmente().equals(Constants.YES_VALUE)){
				mensaje = mensaje.equals("") ? "Debe registrar los datos de la Instituci�n aval. (secci�n "+Constants.SECCION_DATOS_ESTUDIO+")." : mensaje;
				contador++;
			}
		}

		if (VO.getString(this.request.getUser().getTipoInstitucionAvala()).equals(Constants.TIPO_INSTITUCION_AVAL_CENTRO_LABORAL)){
			if(!this.request.getUser().getFlagLaboroActualmente().equals(Constants.YES_VALUE)){
				mensaje = mensaje.equals("") ? "Debe registrar los datos de la Instituci�n aval. (secci�n "+Constants.SECCION_DATOS_LABORAL+")." : mensaje;
				contador++;
			}
		}
		if (contador > 0) {
			super.setAlertMessage(mensaje);
			return false;
		}
		return true;
	}

	private boolean validateSave() {
		if (!validateSaveBase()){ return false;}
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
			if (!validateSaveGeneralUser()){ return false;}
		}
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){
			if (!validateSaveResearcherUser()){ return false;}
		}
		return true;
	}
	
	
	//de String[] a String
	public void setearStringCadenaPreferencias(){
		String cadena= "";
		if (this.request.getUser().getPreferenciasUsuario()== null || this.request.getUser().getPreferenciasUsuario().length==0) {
			this.request.getUser().setCadenaPreferencias(Constants.EMPTY_STRING); 
		}else{
			cadena = "";
			for (int i = 0; i < this.request.getUser().getPreferenciasUsuario().length; i++) {
				String prefNombre = this.request.getUser().getPreferenciasUsuario()[i];
				String prefId = this.obtenerIdPreferencia(prefNombre);
				if (!prefId.equals(Constants.EMPTY_STRING) && !prefId.equals(Constants.ZERO_VALUE_STRING) ){
					if (i==(this.request.getUser().getPreferenciasUsuario().length - 1)){
						cadena += String.valueOf(prefId);
					}else{
						cadena += String.valueOf(prefId)+",";
					}
				}
			}	
		}
		this.request.getUser().setCadenaPreferencias(cadena);	
		
	}

	public String obtenerIdPreferencia(String nomPref){
		String resultado = Constants.EMPTY_STRING;
		for (int i = 0; i < this.listaPreferencias.size(); i++) {
			ParameterTable pref = this.listaPreferencias.get(i);
			String nomPrefItem =  pref.getNombreCorto();
			if (nomPrefItem.equals(nomPref)){
				resultado =pref.getCodigoRegistro();
				return resultado;
			}
		}
		return resultado;
	}
	
	public void inicializarVigencia(){
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
			request.setConcepto(Constants.USER_CONCEPTO_ALTA);
			request.setVigencia(Constants.USER_VIGENCIA_1A�O);
			request.setFechaInicio(new Date());
			request.setFechaFin(VO.sumarFecha(request.getFechaInicio(), Calendar.YEAR, 1));
			request.setCosto(0D);
			request.setFechaPago(null);
			request.setReciboPago(Constants.EMPTY_STRING); 
			request.setMoneda(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){
			request.setConcepto(Constants.USER_CONCEPTO_ALTA);
			request.setVigencia(Constants.USER_VIGENCIA_1A�O);
			request.setFechaInicio(new Date());
			request.setFechaFin(VO.sumarFecha(request.getFechaInicio(), Calendar.YEAR, 1));
			request.setCosto(0D);
			request.setFechaPago(null);
			request.setReciboPago(Constants.EMPTY_STRING);
			request.setMoneda(Constants.EMPTY_STRING);
		}
	}

	public int getTipoUsuarioBibliotecaAbsysnet(){
		int resultado = 1; //en caso extremo que sea general
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){ resultado = Constants.USER_TYPE_GENERAL_ABSYSNET; }
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){ resultado = Constants.USER_TYPE_RESEARCHER_ABSYSNET; }
		return resultado;
	}

	public Calendar getFechaCalendar(Date fechaDate){
		Calendar fechaCalendar = Calendar.getInstance();
		if (fechaDate!= null){
			fechaCalendar.setTime(fechaDate);
		}
		return fechaCalendar;
	}
	
	public Calendar getFechaActualCalendar(){
		Calendar fechaCalendar = Calendar.getInstance();
		fechaCalendar.setTime(new Date());
		return fechaCalendar;
	}	
	
	public String getNombreInstitucionAvala(){
		String resultado="-";
		if (this.request.getUser().getTipoInstitucionAvala().equals(Constants.TIPO_INSTITUCION_AVAL_CENTRO_ESTUDIOS)){
			resultado = this.request.getUser().getCentroEstudio().getCentroEstudioNombre()+" / "+this.request.getUser().getCentroEstudio().getDireccion().getDireccion();
		}
		if (this.request.getUser().getTipoInstitucionAvala().equals(Constants.TIPO_INSTITUCION_AVAL_CENTRO_LABORAL)){
			resultado = this.request.getUser().getCentroLaboral().getCentroLaboralNombre()+" / "+this.request.getUser().getCentroLaboral().getDireccion().getDireccion();
		}
		return resultado;
	}
	
	public String obtenerStringRecortado(String valor, int longitud){
		String resultado = "";
		String cadena = VO.getStringFormatted(valor);
		if (cadena.length()>50){
			resultado = cadena.substring(0, longitud);
		}else{
			resultado = cadena;
		}
		return resultado;
	}		
	
	
	public String generarUsuarioAbsysnet(){
		String resultado ="-";
		try{
			AbsysNETSoapProxy absysNETSoapProxy = new AbsysNETSoapProxy(this.getString(Constants.WS_BASE_URL_ABSYSNET));
			resultado = absysNETSoapProxy.registrarActualizarUsuario(
					this.getTipoUsuarioBibliotecaAbsysnet(),//PTIPO_LECTOR
					this.request.getEstadoRegistro(), //PESTADO
					this.getFechaActualCalendar(), //PFECHA_REGISTRO
					this.getFechaCalendar(this.request.getFechaInicio()), //PFECHA_INICIO
					this.getFechaCalendar(this.request.getFechaFin()), //PFECHA_FIN
					Integer.parseInt(this.request.getUser().getTipoDocumentoIdentidadId()), //PTIPO_DOC
					this.request.getUser().getNumeroDocumentoIdentidad(),//PNUMDOC
					VO.getStringFormatted(this.request.getUser().getNombre()),//PNOMBRES
					VO.getStringFormatted(this.request.getUser().getApellidoPaterno()+ " "+this.request.getUser().getApellidoMaterno()),//PAPELLIDOS
					this.getFechaCalendar(this.request.getUser().getFechaNacimiento()),//PFECHA_NACIMIENTO
					this.request.getUser().getSexo(),//PSEXO
					VO.getStringFormatted(this.obtenerStringRecortado(this.request.getUser().getDireccion().getDireccionResidencia().getDireccion(), 50)),//PDIRECCION
					VO.getStringFormatted(this.obtenerStringRecortado(this.request.getUser().getTelefonoFijo(), 15)),//PTELEFONO_FIJO
					VO.getStringFormatted(this.obtenerStringRecortado(this.request.getUser().getTelefonoMovil(), 15)),//PTELEFONO_CELULAR
					VO.getStringFormatted(this.request.getUser().getCorreoElectronico()),//PCORREO
					VO.getStringFormatted(this.obtenerStringRecortado(this.request.getUser().getCentroEstudio().getCentroEstudioNombre(), 50)),//PCENTRO_ESTUDIOS
					VO.getStringFormatted(this.obtenerStringRecortado(this.request.getUser().getCentroEstudio().getTipo()+"/"+this.request.getUser().getCentroEstudio().getDireccion().getDireccion(), 50)),//PTIPO_CENTRO_ESTUDIOS
					VO.getStringFormatted(this.getNombreInstitucionAvala()),//PTIPO_INSTITUCION_AV
					VO.getStringFormatted(this.request.getUser().getTemaInvestigacion()),
					this.getFechaCalendar(this.request.getFechaInicioSuspension()) //PFECHA_SUSPENSION
					);
		}catch(RemoteException e){
			System.out.println("Error: "+e.getMessage());
			logger.error(e);
			resultado = Constants.ERROR_RESULT;
		}
		return resultado;
	}	
	
	public void setearCamposSelect(){
		if (this.request.getUser().getDireccion().getDireccionResidencia().getCodPais().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getDireccion().getDireccionResidencia().setCodPais(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getDireccion().getDireccionResidencia().getCodPro().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getDireccion().getDireccionResidencia().setCodPro(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getDireccion().getDireccionResidencia().getCodDis().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getDireccion().getDireccionResidencia().setCodDis(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getDireccion().getDireccionResidencia().getCodDis().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getDireccion().getDireccionResidencia().setCodDis(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getCentroLaboral().getTipo().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getCentroLaboral().setTipo(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getCentroLaboral().getDireccion().getCodPais().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getCentroLaboral().getDireccion().setCodPais(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getCentroEstudio().getTipo().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getCentroEstudio().setTipo(Constants.EMPTY_STRING);
		}
		if (this.request.getUser().getCentroEstudio().getDireccion().getCodPais().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().getCentroEstudio().getDireccion().setCodPais(Constants.EMPTY_STRING);
		}

		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().setTipoUsuarioBiblioteca(Constants.EMPTY_STRING);
		}

		if (this.request.getConcepto().equals(Constants.ZERO_VALUE_STRING)){
			this.request.setConcepto(Constants.EMPTY_STRING);
		}
		
		if (this.request.getVigencia().equals(Constants.ZERO_VALUE_STRING)){
			this.request.setVigencia(Constants.EMPTY_STRING);
		}

		if (this.request.getMoneda().equals(Constants.ZERO_VALUE_STRING)){
			this.request.setMoneda(Constants.EMPTY_STRING);
		}

		if (this.request.getUser().getIdNacionalidad().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().setIdNacionalidad(Constants.EMPTY_STRING);
		}		

		if (this.request.getUser().getIdGradoInstruccion().equals(Constants.ZERO_VALUE_STRING)){
			this.request.getUser().setIdGradoInstruccion(Constants.EMPTY_STRING);
		}		
		
		if(this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){//en caso cambie de investigador a general se limpiaran los campos relacionados a investigacion
			this.request.getUser().setTemaInvestigacion(Constants.EMPTY_STRING);
			this.request.getUser().setTipoInstitucionAvala(Constants.EMPTY_STRING);
			this.request.getUser().getCartaPresentacion().setExtension(Constants.EMPTY_STRING);
			this.request.getUser().getCartaPresentacion().setFileName(Constants.EMPTY_STRING);
			this.request.getUser().getCartaPresentacion().setRawFile(null);
		}
		
	}
	
	public void setearAMayusculasyQuitarEspacios(){
		if (this.getRequest().getUser().getNumeroDocumentoIdentidad()!=null){
			this.getRequest().getUser().setNumeroDocumentoIdentidad(this.getRequest().getUser().getNumeroDocumentoIdentidad().toUpperCase().trim());
		}
		if (this.getRequest().getNumeroCarnetBiblioteca()!=null){
			this.getRequest().setNumeroCarnetBiblioteca(this.getRequest().getNumeroCarnetBiblioteca().toUpperCase().trim());
		}
		if (this.getRequest().getCodigoQRValor()!=null){
			this.getRequest().setCodigoQRValor(this.getRequest().getCodigoQRValor().toUpperCase().trim());
		}
		if (this.getRequest().getUser().getNombre()!=null){
			this.getRequest().getUser().setNombre(this.getRequest().getUser().getNombre().toUpperCase().trim());
		}		
		if (this.getRequest().getUser().getApellidoPaterno()!=null){
			this.getRequest().getUser().setApellidoPaterno(this.getRequest().getUser().getApellidoPaterno().toUpperCase().trim());
		}		
		if (this.getRequest().getUser().getApellidoMaterno()!=null){
			this.getRequest().getUser().setApellidoMaterno(this.getRequest().getUser().getApellidoMaterno().toUpperCase().trim());
		}		
		if (this.getRequest().getUser().getDireccion().getDireccionResidencia().getDireccion()!=null){
			this.getRequest().getUser().getDireccion().getDireccionResidencia().setDireccion(this.getRequest().getUser().getDireccion().getDireccionResidencia().getDireccion().toUpperCase().trim());
		}		
		if (this.getRequest().getUser().getCentroLaboral().getDireccion().getDireccion()!=null){
			this.getRequest().getUser().getCentroLaboral().getDireccion().setDireccion(this.getRequest().getUser().getCentroLaboral().getDireccion().getDireccion().toUpperCase().trim());
		}	
		if (this.getRequest().getUser().getCentroLaboral().getCentroLaboralNombre()!=null){
			this.getRequest().getUser().getCentroLaboral().setCentroLaboralNombre(this.getRequest().getUser().getCentroLaboral().getCentroLaboralNombre().toUpperCase().trim());
		}	
		if (this.getRequest().getUser().getCentroLaboral().getDireccion().getNombreCiudad()!=null){
			this.getRequest().getUser().getCentroLaboral().getDireccion().setNombreCiudad(this.getRequest().getUser().getCentroLaboral().getDireccion().getNombreCiudad().toUpperCase().trim());
		}			
		if (this.getRequest().getUser().getCentroEstudio().getDireccion().getDireccion()!=null){
			this.getRequest().getUser().getCentroEstudio().getDireccion().setDireccion(this.getRequest().getUser().getCentroEstudio().getDireccion().getDireccion().toUpperCase().trim());
		}	
		if (this.getRequest().getUser().getCentroEstudio().getCentroEstudioNombre()!=null){
			this.getRequest().getUser().getCentroEstudio().setCentroEstudioNombre(this.getRequest().getUser().getCentroEstudio().getCentroEstudioNombre().toUpperCase().trim());
		}	
		if (this.getRequest().getUser().getCentroEstudio().getDireccion().getNombreCiudad()!=null){
			this.getRequest().getUser().getCentroEstudio().getDireccion().setNombreCiudad(this.getRequest().getUser().getCentroEstudio().getDireccion().getNombreCiudad().toUpperCase().trim());
		}			
		if (this.getRequest().getUser().getTemaInvestigacion()!=null){
			this.getRequest().getUser().setTemaInvestigacion(this.getRequest().getUser().getTemaInvestigacion().toUpperCase().trim());
		}		
		if (this.getRequest().getUser().getPreferenciaEspecifica()!=null){
			this.getRequest().getUser().setPreferenciaEspecifica(this.getRequest().getUser().getPreferenciaEspecifica().toUpperCase().trim());
		}
		if (this.getRequest().getUser().getCorreoElectronico()!=null){
			this.getRequest().getUser().setCorreoElectronico(this.getRequest().getUser().getCorreoElectronico().toUpperCase().trim());
		}		
		if (this.getRequest().getUser().getTelefonoFijo()!=null){
			this.getRequest().getUser().setTelefonoFijo(this.getRequest().getUser().getTelefonoFijo().toUpperCase().trim());
		}	
		if (this.getRequest().getUser().getTelefonoMovil()!=null){
			this.getRequest().getUser().setTelefonoMovil(this.getRequest().getUser().getTelefonoMovil().toUpperCase().trim());
		}			
		this.setNumeroDocumentoIdentidadxValidar(this.getNumeroDocumentoIdentidadxValidar().trim().toUpperCase()); 
	}

	//setear los nombres del departamento, provincia y distrito
	public void setearNombresUbigeoLector(){
		String codDepartamento = this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodDep();
		String codProvincia = this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodPro();
		String codDistrito = this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodDis();
		if (codDepartamento!=null && !codDepartamento.equals(Constants.EMPTY_STRING)){
			for(UbigeoReniec departamento: lstDepartamento){
				if (codDepartamento.equals(departamento.getCoddep())){
					this.getRequest().getUser().getDireccion().getDireccionResidencia().setNombreDep(departamento.getDescripcion());
					break;
				}
			}
		}
		if (codProvincia!=null && !codProvincia.equals(Constants.EMPTY_STRING)){
			for(UbigeoReniec provincia: lstProvincia){
				if (codProvincia.equals(provincia.getCodpro())){
					this.getRequest().getUser().getDireccion().getDireccionResidencia().setNombrePro(provincia.getDescripcion());
					break;
				}
			}
		}
		if (codDistrito!=null && !codDistrito.equals(Constants.EMPTY_STRING)){
			for(UbigeoReniec distrito: lstDistritos){
				if (codDistrito.equals(distrito.getCoddis())){
					this.getRequest().getUser().getDireccion().getDireccionResidencia().setNombreDis(distrito.getDescripcion());
					break;
				}
			}
		}
		if ((this.request.getUser().getDireccion().getDireccionResidencia().getCodPais()).equals(Constants.COUNTRY_CODE_PERU)){
			this.request.getUser().getDireccion().getDireccionResidencia().setNombrePais(Constants.COUNTRY_DESC_PERU);
		}
	}
	
	//obtener la descripcion del grado de instrucci�n y nacionalidad
	public void setearNombresDeCombos(){
		//obtener la descripcion del grado de instrucci�n
		if (!VO.getString(this.request.getUser().getIdGradoInstruccion()).equals(Constants.ZERO_VALUE_STRING)){
			for(ParameterTable gradoInstruccion: listaGradoInstruccion){
				if (this.request.getUser().getIdGradoInstruccion().equals(gradoInstruccion.getCodigoRegistro())){
					this.getRequest().getUser().setNombreGradoInstruccion(gradoInstruccion.getNombreCorto());
					break;
				}
			}
		}
		//obtener la descripcion de la nacionalidad		
		if (!VO.getString(this.request.getUser().getIdNacionalidad()).equals(Constants.ZERO_VALUE_STRING)){
			for(ParameterTable nacionalidad: listaNacionalidad){
				if (this.request.getUser().getIdNacionalidad().equals(nacionalidad.getCodigoRegistro())){
					this.getRequest().getUser().setNombreNacionalidad(nacionalidad.getNombreCorto());
					break;
				}
			}
		}
	}	
	
	//con datasource ds_absysnet
	public String save(){
		this.setearAMayusculasyQuitarEspacios();
		String pagina = "";
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
			pagina = Constants.PAGE_GENERAL_USER_REGISTRATION;
			this.request.setEstadoRegistro(Constants.REGISTER_USER_STATE_ENABLED);
		}
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){
			pagina = Constants.PAGE_RESEARCHER_USER_REGISTRATION;
			this.request.setEstadoRegistro(Constants.REGISTER_USER_STATE_REGISTERED);
		}
		if ( validateSave() ) {
			DocumentoIdentidadDto dto = new DocumentoIdentidadDto(this.getNumeroDocumentoIdentidadxValidar().trim().toUpperCase(),this.getTipoDocumentoIdentidadxValidar().trim().toUpperCase(), this.request.getUser().getCodPaisPasaporte(), "");
			if (existeDocumentoIdentidad(dto)) return pagina;
			ResponseObject sw = new ResponseObject();
			try {
				if (this.getRequest().getId() == 0L) {
					this.inicializarVigencia();//por default
					this.setearNombresUbigeoLector();
					this.setearNombresDeCombos();
					this.setearCamposSelect();
					LectorAbsysnet lectorAbsysnet = new LectorAbsysnet();
					lectorAbsysnet.cargarObjetoLector(this.getRequest());//cargar usuario
					ResponseObject responseAbsysnet = absysnetServiceRemote.guardarLector(lectorAbsysnet);
					String resultadoAbsysnet = responseAbsysnet.getMensaje();
					if (resultadoAbsysnet.equals(Constants.SUCCESS_RESULT)){	
						String codigoBarrasLectorAbsysnet = String.valueOf(responseAbsysnet.getId());
						this.getRequest().setCodBarrasAbsysnet(codigoBarrasLectorAbsysnet);
						this.request.setUsuarioIdRegistro(Constants.CODE_NEW_ONLINE_USER);
						this.setearStringCadenaPreferencias();
						this.request.generarNumeroCarnet();
						this.cargarImagenCarnet();
						this.request.setOrigenRegistro(Constants.TBL_REGISTRO_USUARIO_ORIGEN_REGISTRO_ONLINE);
						sw = registroServiceRemote.insert(this.getRequest());
						if (sw.getId() != 0) {
							if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
								pagina = Constants.PAGE_GENERAL_USER_SUCCESSFULLY_SAVE;
							}
							if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){
								pagina = Constants.PAGE_RESEARCHER_USER_SUCCESSFULLY_SAVE;
							}
							this.getRequest().getUser().setNombreCompleto();
							try {
								 this.generateQRFile(this.request.getCodigoQRValor());//siempre se genera y guarda la imagen del QR
								 if (!VO.getString(this.getRequest().getUser().getCorreoElectronico()).equals(Constants.EMPTY_STRING)){//solo si se ingreso el correo
									 EmailUsuario email = new EmailUsuario();
									 email.enviarEmailCreacionUsuario(this.getRequest().getUser().getCorreoElectronico(), this.request.getUser().getTipoUsuarioBiblioteca(), this.request.getCodigoQRValor(), this.request.getUser().obtenerNombrePropio(), this.request.getUser().getSexo());
								 }
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								super.setMensajeAlerta(e.getMessage());	
							}
							
							this.clearFields();
							this.request.getUser().setTipoUsuarioBiblioteca(Constants.EMPTY_STRING);
							super.setMensajeAviso("Exito al registrar usuario. pagina:" +pagina);
						} else {
							super.setMensajeAlerta("Error al registrar usuario.");	
						}
					}else{//no se pudo registrar en absysnet
						super.setMensajeAlerta("No se pudo registrar usuario en Absysnet. Intente de nuevo.");
					}
				}else{
					super.setMensajeAlerta("Error: Id de registro de usuario es mayor a cero");
				}
			} catch (ServiceException e) {
				super.setMensajeAlerta("Error al guardar registro de usuario!");		
				e.printStackTrace();
			}
		}
		return pagina;
	}	
	
	//con ws absysnet
	public String save_old(){
		this.setearAMayusculasyQuitarEspacios();
		String pagina = "";
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
			pagina = Constants.PAGE_GENERAL_USER_REGISTRATION;
			this.request.setEstadoRegistro(Constants.REGISTER_USER_STATE_ENABLED);
		}
		if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){
			pagina = Constants.PAGE_RESEARCHER_USER_REGISTRATION;
			this.request.setEstadoRegistro(Constants.REGISTER_USER_STATE_REGISTERED);
		}
		if ( validateSave() ) {
			DocumentoIdentidadDto dto = new DocumentoIdentidadDto(this.getNumeroDocumentoIdentidadxValidar().trim().toUpperCase(),this.getTipoDocumentoIdentidadxValidar().trim().toUpperCase(), this.request.getUser().getCodPaisPasaporte(), "");
			if (existeDocumentoIdentidad(dto)) return pagina;
			ResponseObject sw = new ResponseObject();
			try {
				if (this.getRequest().getId() == 0L) {
					this.setearNombresUbigeoLector();
					String resultadoAbsysnet = this.generarUsuarioAbsysnet();
					String separador = Pattern.quote("|");
					String[] resultadosAbsysnetArray = resultadoAbsysnet.split(separador);
					String codigoRptaAbsysnet= resultadosAbsysnetArray[0];
					if (codigoRptaAbsysnet.equals(Constants.SUCCESS_RESULT)){	
						String codigoBarrasLectorAbsysnet = resultadosAbsysnetArray[1];
						this.getRequest().setCodBarrasAbsysnet(codigoBarrasLectorAbsysnet);
						this.inicializarVigencia();//por default
						this.setearCamposSelect();
						this.request.setUsuarioIdRegistro(Constants.CODE_NEW_ONLINE_USER);
						this.setearStringCadenaPreferencias();
						this.request.generarNumeroCarnet();
						this.cargarImagenCarnet();
						sw = registroServiceRemote.insert(this.getRequest());
						if (sw.getId() != 0) {
							if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_GENERAL)){
								pagina = Constants.PAGE_GENERAL_USER_SUCCESSFULLY_SAVE;
							}
							if (this.request.getUser().getTipoUsuarioBiblioteca().equals(Constants.USER_TYPE_RESEARCHER)){
								pagina = Constants.PAGE_RESEARCHER_USER_SUCCESSFULLY_SAVE;
							}
							this.getRequest().getUser().setNombreCompleto();
							try {
								 this.generateQRFile(this.request.getCodigoQRValor());//siempre se genera y guarda la imagen del QR
								 if (!VO.getString(this.getRequest().getUser().getCorreoElectronico()).equals(Constants.EMPTY_STRING)){//solo si se ingreso el correo
									 EmailUsuario email = new EmailUsuario();
									 email.enviarEmailCreacionUsuario(this.getRequest().getUser().getCorreoElectronico(), this.request.getUser().getTipoUsuarioBiblioteca(), this.request.getCodigoQRValor(), this.request.getUser().obtenerNombrePropio(), this.request.getUser().getSexo());
								 }
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								super.setMensajeAlerta(e.getMessage());	
							}
							
							this.clearFields();
							this.request.getUser().setTipoUsuarioBiblioteca(Constants.EMPTY_STRING);
							super.setMensajeAviso("Exito al registrar usuario.");
						} else {
							super.setMensajeAlerta("Error al registrar usuario.");	
						}
					}else{//no se pudo registrar en absysnet
						super.setMensajeAlerta("No se pudo registrar usuario en Absysnet. Intente de nuevo.");
					}
				}else{
					super.setMensajeAlerta("Error: Id de registro de usuario es mayor a cero");
				}
			} catch (ServiceException e) {
				super.setMensajeAlerta("Error al guardar registro de usuario!");		
				e.printStackTrace();
			}
		}
		return pagina;
	}	

	public void updatePaisResidenciaSelected(){ 
		if (this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodPais().equals(Constants.COUNTRY_CODE_PERU)){
			this.showPaisPeru = true;
		}else{
			this.showPaisPeru = false;
			this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodDep(Constants.EMPTY_STRING);
			this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodPro(Constants.EMPTY_STRING);
			this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodDis(Constants.EMPTY_STRING);
		}
	}
	
	public void updatePanelVerificacionIdentidad(){
		this.request.getUser().setCodPaisPasaporte(Constants.COUNTRY_CODE_PERU);
		this.numeroDocumentoIdentidadxValidar = Constants.EMPTY_STRING;
		this.primerApellidoxValidar= Constants.EMPTY_STRING;
		if (this.getTipoDocumentoIdentidadxValidar().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE)){
			this.showPanelBusquedaReniec = true;
			this.showPanelBusquedaMigraciones = false;
			this.showTipoDocumentoDNI = true;
			this.showTipoDocumentoCE = false;
		}
		if (this.getTipoDocumentoIdentidadxValidar().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE)){
			this.showPanelBusquedaReniec = false;
			this.showPanelBusquedaMigraciones = true;
			this.showTipoDocumentoDNI = false;
			this.showTipoDocumentoCE = true;
		}		
		this.getRequest().getUser().setFlagMayorDeEdad(Constants.EMPTY_STRING);
	}
	
	public void cleanIdentityVerificationFields() {
		this.streamedContentPhotoReniec = null;
		this.streamedContentPhotoAdjuntada = null;
		this.getRequest().getUser().setTipoDocumentoIdentidadId(Constants.EMPTY_STRING);
		this.getRequest().getUser().setTipoDocumentoIdentidadNombre(Constants.EMPTY_STRING);
		this.getRequest().getUser().setNumeroDocumentoIdentidad(Constants.EMPTY_STRING);
		this.getRequest().getUser().setNombre(Constants.EMPTY_STRING);
		this.getRequest().getUser().setApellidoMaterno(Constants.EMPTY_STRING);
		this.getRequest().getUser().setApellidoPaterno(Constants.EMPTY_STRING);
		this.getRequest().getUser().setSexo(Constants.EMPTY_STRING);
		this.getRequest().getUser().getDireccion().getDireccionResidencia().setDireccion(Constants.EMPTY_STRING);
		this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodPais(Constants.EMPTY_STRING);
		this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodDep(Constants.EMPTY_STRING);
		this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodPro(Constants.EMPTY_STRING);
		this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodDis(Constants.EMPTY_STRING);
		this.getRequest().getUser().setFechaNacimiento(null);
	}
	
	public void mensajeInicio() {
		super.setNotificationMessage("Registrese como usuario aqu�.");
	}

	public void clearFields() {
		this.showPanelBusquedaReniec = true;
		this.showPanelBusquedaMigraciones = false;
		this.showTipoDocumentoDNI = true;
		this.showTipoDocumentoCE = false;
		this.showTipoDocumentoPasaporte = false;
		this.showPaisPeru= true;
		this.isDisabledBotonSave = true;
		this.tipoDocumentoIdentidadxValidar = Constants.EMPTY_STRING;
		this.numeroDocumentoIdentidadxValidar = Constants.EMPTY_STRING;
		this.primerApellidoxValidar= Constants.EMPTY_STRING;
		this.streamedContentPhotoReniec = null;
		this.streamedContentPhotoAdjuntada = null;
		this.showFotoReniec = true;
		this.fechaActual = new Date();
		this.request.init();
	}
	
	public boolean existeDocumentoIdentidad(DocumentoIdentidadDto documentoIdentidadDto){
		ResponseObject response;
		try{
			if (this.getTipoDocumentoIdentidadxValidar().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_CODE)) {//setear el valor ingresado manualmente
				documentoIdentidadDto.setNumeroDocumento(this.request.getUser().getNumeroDocumentoIdentidad());
			}			
			response = registroServiceRemote.validarDocumentoIdentidad(documentoIdentidadDto);
			if (response.getMensaje().equals("EXISTE-TDI")) {
				super.setMensajeAlerta("El tipo, n�mero y pa�s de emisi�n de su documento de identidad ya se encuentra registrado. Solicite orientaci�n en la Sala del Usuario.");
				return true; 
			}
			if (response.getMensaje().equals("EXISTE-NCB")) {
				super.setMensajeAlerta("El n�mero de su documento de identidad ya se encuentra registrado. Solicite orientaci�n en la Sala del Usuario.");
				return true; 
			}
			if (response.getMensaje().equals("NO EXISTE")) {return false; }
		} catch (ServiceException e) {
			super.setMensajeAlerta("Error al realizar validaci�n de documento de identidad!");		
			e.printStackTrace();
		}
		super.setMensajeAlerta("Error al validar documento de identidad!");		
		return true;
	}
	
	public boolean validateCEMigraciones() throws Exception {
		this.cleanIdentityVerificationFields();
		if (this.getTipoDocumentoIdentidadxValidar().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE)) {
			if ((VO.isEmpty(this.getNumeroDocumentoIdentidadxValidar().trim()) || this.getNumeroDocumentoIdentidadxValidar().trim().length() > Constants.TIPO_DOCUMENTO_IDENTIDAD_CE_LONGITUD)) {
				this.setAlertMessage("Ingrese un n�mero de documento v�lido.");
				return false;
			}
			if ((VO.isEmpty(this.primerApellidoxValidar.trim()))) {
				this.setAlertMessage("Ingrese el primer apellido.");
				return false;
			}			
			if ((VO.isEmpty(this.tipoDocumentoIdentidadxValidar.trim()) || this.tipoDocumentoIdentidadxValidar.trim().equals(Constants.ZERO_VALUE_STRING))) {
				this.setAlertMessage("Seleccione el tipo de documento de identidad.");
				return false;
			}		
			
			DocumentoIdentidadDto dto = new DocumentoIdentidadDto(this.getNumeroDocumentoIdentidadxValidar().trim().toUpperCase(),this.getTipoDocumentoIdentidadxValidar().trim().toUpperCase(), this.request.getUser().getCodPaisPasaporte(), "");
			if (existeDocumentoIdentidad(dto)) return false;
			MigraCarnetdeExtrajeriaPortTypeProxy migraCarnetdeExtrajeriaPortTypeProxy = new MigraCarnetdeExtrajeriaPortTypeProxy();
			migraCarnetdeExtrajeriaPortTypeProxy.setEndpoint(this.getString(Constants.WS_BASE_URL_MIGRACIONES_PIDE));
			
			pe.gob.migraciones.usuarios.consulta.ws.SolicitudBean solicitud = new SolicitudBean();
			solicitud.setStrCodInstitucion(this.getString(Constants.COD_INSTITUCION_MIGRACIONES_PIDE));
			solicitud.setStrMac(this.getString(Constants.MAC_MIGRACIONES_PIDE));
			solicitud.setStrNroIp(this.getString(Constants.IP_MIGRACIONES_PIDE));
			solicitud.setStrTipoDocumento(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_SHORT_NAME);			
			solicitud.setStrNumDocumento(this.getNumeroDocumentoIdentidadxValidar().trim());
			pe.gob.migraciones.usuarios.consulta.ws.RespuestaBean respuestaBean = null;

			ResponseObject responseObject;
			responseObject= this.registrarConsultaVerificacionIdentidad("MIGRACIONES", this.getString(Constants.WS_BASE_URL_MIGRACIONES_PIDE), solicitud.getStrNumDocumento(), solicitud.getStrTipoDocumento(), this.primerApellidoxValidar.trim(), "Online",this.getString(Constants.COD_INSTITUCION_MIGRACIONES_PIDE));
			if (responseObject!=null &&  responseObject.getMensaje().equals(Constants.ERROR_UMBRAL_CONSULTA_RENIEC)){
				super.setAlertMessage("Usted lleg� al l�mite de consultas diarias para validaci�n de identidad.");
				return false;
			}
			
			try {
				respuestaBean = migraCarnetdeExtrajeriaPortTypeProxy.consultarDocumento(solicitud);
			} catch (RemoteException e) {
				e.printStackTrace();
				super.setAlertMessage("Error en consulta a ws migraciones: "+e.getMessage());
			}			
			if (respuestaBean!= null && respuestaBean.getStrNumRespuesta()!= null &&  !respuestaBean.getStrNumRespuesta().equals(Constants.SUCCESSFUL_OPERATION_MIGRACIONES_CODE)) {
				super.setAlertMessage("No se pudo obtener los datos");
				return false;
			} else {
				if (respuestaBean.getStrPrimerApellido().equals(this.primerApellidoxValidar.toUpperCase().trim())){
					this.getRequest().getUser().setNombre(respuestaBean.getStrNombres().toUpperCase().trim());
					this.getRequest().getUser().setTipoDocumentoIdentidadId(this.getTipoDocumentoIdentidadxValidar().trim()	);
					this.getRequest().getUser().setTipoDocumentoIdentidadNombre(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_NAME);
					this.getRequest().getUser().setNumeroDocumentoIdentidad(this.getNumeroDocumentoIdentidadxValidar().trim());
					this.getRequest().getUser().setApellidoPaterno(respuestaBean.getStrPrimerApellido().toUpperCase().trim());
					this.getRequest().getUser().setApellidoMaterno(respuestaBean.getStrSegundoApellido().toUpperCase().trim());
					this.getRequest().getUser().getDireccion().getDireccionResidencia().setDireccion(Constants.EMPTY_STRING);
					this.getRequest().getUser().getDireccion().getDireccionReniec().setDireccion(Constants.EMPTY_STRING);
					this.showFotoReniec = false;		
					this.getRequest().getUser().setFlagMayorDeEdad(Constants.YES_VALUE);
					super.setNotificationMessage("Su datos han sido autorellenados exitosamente.");
					return true;
				}else{
					super.setNotificationMessage("Datos no coinciden, favor verificar la informaci�n registrada.");
					return false;
				}
			}
		}else{
			return true;
		}
	}	
	
	public boolean validateDniRENIEC() {
		this.cleanIdentityVerificationFields();
		if (this.getTipoDocumentoIdentidadxValidar().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE)) {
			if ((VO.isEmpty(this.getNumeroDocumentoIdentidadxValidar().trim()) || this.getNumeroDocumentoIdentidadxValidar().trim().length() != Constants.TIPO_DOCUMENTO_IDENTIDAD_DNI_LONGITUD)) {
				this.setAlertMessage("Ingrese un n�mero de documento v�lido.");
				return false;
			}
			if ((VO.isEmpty(this.primerApellidoxValidar.trim()))) {
				this.setAlertMessage("Ingrese el primer apellido.");
				return false;
			}
			if ((VO.isEmpty(this.tipoDocumentoIdentidadxValidar.trim()) || this.tipoDocumentoIdentidadxValidar.trim().equals(Constants.ZERO_VALUE_STRING))) {
				this.setAlertMessage("Seleccione el tipo de documento de identidad.");
				return false;
			}			
			DocumentoIdentidadDto dto = new DocumentoIdentidadDto(this.getNumeroDocumentoIdentidadxValidar().trim().toUpperCase(),this.getTipoDocumentoIdentidadxValidar().trim().toUpperCase(), this.request.getUser().getCodPaisPasaporte(), "");
			if (existeDocumentoIdentidad(dto)) return false;
			ReniecConsultaDniPortTypeProxy reniecConsultaDniPortTypeProxy = new ReniecConsultaDniPortTypeProxy();
			reniecConsultaDniPortTypeProxy.setEndpoint(this.getString(Constants.WS_BASE_URL_RENIEC_PIDE));
			pe.gob.reniec.ws.PeticionConsulta peticionConsulta = new PeticionConsulta();
			peticionConsulta.setNuDniConsulta(this.getNumeroDocumentoIdentidadxValidar().trim());
			peticionConsulta.setNuDniUsuario(this.getString(Constants.DNI_BNP_RENIEC_PIDE));
			peticionConsulta.setNuRucUsuario(this.getString(Constants.RUC_BNP_RENIEC_PIDE));
			peticionConsulta.setPassword(this.getString(Constants.PASSWORD_BNP_RENIEC_PIDE));
			pe.gob.reniec.ws.ResultadoConsulta resultadoConsulta = new ResultadoConsulta();
			
			ResponseObject responseObject;
			responseObject= this.registrarConsultaVerificacionIdentidad("RENIEC", this.getString(Constants.WS_BASE_URL_RENIEC_PIDE), peticionConsulta.getNuDniConsulta(), "DNI", this.primerApellidoxValidar.trim(), "Online",this.getString(Constants.DNI_BNP_RENIEC_PIDE));
			if (responseObject!=null &&  responseObject.getMensaje().equals(Constants.ERROR_UMBRAL_CONSULTA_RENIEC)){
				super.setAlertMessage("Usted lleg� al l�mite de consultas diarias para validaci�n de identidad.");
				return false;
			}			
			try {
				resultadoConsulta = reniecConsultaDniPortTypeProxy.consultar(peticionConsulta);
			} catch (RemoteException e) {
				e.printStackTrace();
				super.setAlertMessage("Error al momento de consultar servicio RENIEC");
				return false;
			}
			if (resultadoConsulta.getCoResultado().equals(Constants.MENOR_EDAD_OPERATION_RENIEC_CODE)){
				this.request.getUser().setFlagMayorDeEdad(Constants.NO_VALUE);
			}else{
				this.request.getUser().setFlagMayorDeEdad(Constants.YES_VALUE);
			}
			
			
			if (!resultadoConsulta.getCoResultado().equals(Constants.SUCCESSFUL_OPERATION_RENIEC_CODE)) {
				if (resultadoConsulta.getCoResultado().equals(Constants.MENOR_EDAD_OPERATION_RENIEC_CODE)){
					super.setAlertMessage("No se pudo verificar la identidad, ac�rcate a la Sala de Atenci�n al Usuario (San Borja-Lima) o comun�cate con nosotros al tel�fono (01)5136900 anexo 7194/7250.");
				}else{
					super.setAlertMessage(resultadoConsulta.getDeResultado());
				}
				return false;
			} else {
				if (resultadoConsulta.getDatosPersona().getApPrimer().equals(this.primerApellidoxValidar.toUpperCase().trim())){
					this.getRequest().getUser().setNombre(resultadoConsulta.getDatosPersona().getPrenombres().toUpperCase().trim());
					this.getRequest().getUser().setTipoDocumentoIdentidadId(this.getTipoDocumentoIdentidadxValidar().trim()	);
					this.getRequest().getUser().setTipoDocumentoIdentidadNombre(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_NAME);
					this.getRequest().getUser().setNumeroDocumentoIdentidad(this.getNumeroDocumentoIdentidadxValidar().trim());
					this.getRequest().getUser().setApellidoPaterno(resultadoConsulta.getDatosPersona().getApPrimer().toUpperCase().trim());
					this.getRequest().getUser().setApellidoMaterno(resultadoConsulta.getDatosPersona().getApSegundo().toUpperCase().trim());
					this.getRequest().getUser().getDireccion().getDireccionResidencia().setDireccion(resultadoConsulta.getDatosPersona().getDireccion().toUpperCase().trim());;
					this.getRequest().getUser().getDireccion().getDireccionReniec().setDireccion(resultadoConsulta.getDatosPersona().getDireccion().toUpperCase().trim());;
					this.convertPhotoReniec(resultadoConsulta.getDatosPersona().getFoto());	
					PersonaReniecEntity persona = new PersonaReniecEntity();
					persona.setCod_ubigeo(cambiarFormatoUbigeo(resultadoConsulta.getDatosPersona().getUbigeo())); 					
					if(!VO.isEmpty(persona.getCod_ubigeo()) && persona.getCod_ubigeo().length() == 6){
						String codDep = persona.getCod_ubigeo().substring(0, 2);
						this.getRequest().getUser().getDireccion().getDireccionReniec().setCodDep(codDep);
						this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodDep(codDep);
						this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodPais(Constants.COUNTRY_CODE_PERU);
						String codPro = persona.getCod_ubigeo().substring(2, 4);
						this.getRequest().getUser().getDireccion().getDireccionReniec().setCodPro(codPro);
						this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodPro(codPro);
						String codDis = persona.getCod_ubigeo().substring(4, 6);
						this.getRequest().getUser().getDireccion().getDireccionReniec().setCodDis(codDis);
						this.getRequest().getUser().getDireccion().getDireccionResidencia().setCodDis(codDis);
						this.updateProvincia();
						this.updateDistrito();
					}
					this.showFotoReniec = true;
					super.setNotificationMessage("Su datos han sido autorellenados exitosamente.");
					return true;
				}else{
					super.setNotificationMessage("Datos no coinciden, favor verificar la informaci�n registrada.");
					return false;
				}
			}
		} else {
			return true;
		}
	}	

	public void updateProvincia(){
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodDep());
		tmpubigeo.setCodpro("%");
		tmpubigeo.setCoddis("00");		
		try {
			this.lstProvincia = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	public void updateDistrito(){
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodDep());
		tmpubigeo.setCodpro(this.getRequest().getUser().getDireccion().getDireccionResidencia().getCodPro());
		tmpubigeo.setCoddis("%");		
		try {
			this.lstDistritos = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	public void loadComboLists() {
		this.setTiposDocumentoIdentidad(super.getListParameterTableIdentificationDocumentType(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD, "2"));
		this.setListaPreferencias(super.getListParameterTableByCode(Constants.TBL_PARAMETRO_TIPO_PREFERENCIAS_USUARIO));
		this.setListaGradoInstruccion(super.getListParameterTableByCode(Constants.TBL_PARAMETRO_CODIGO_TABLA_GRADO_INSTRUCCION));
		this.setListaNacionalidad(super.getListParameterTableByCode(Constants.TBL_PARAMETRO_CODIGO_TABLA_NACIONALIDAD));
		
		this.lstPais =  super.getListPaises();
	}

	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream(Constants.PROPERTIES_FILE));
			return properties.getProperty(key);
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return Constants.EMPTY_STRING;
	}

	public void downloadPresentationLetter() {
		try {
			//Path pdfPath = Paths.get("D:/Workspace/Develop/WSpacePhoton/Circulacion/TransparenciaVirtualWeb/WebContent/resources/files/carta_de_presentacion.pdf");
			//Path pdfPath = Paths.get("/usr/share/siru/pdf/modelo_carta_de_presentacion.pdf");
			Path pdfPath = Paths.get(this.getString("ruta.archivos.estaticos")+this.getString("nombre.archivo.modelo.carta.presentacion"));
			byte[] pdf = Files.readAllBytes(pdfPath);
			StreamedContent streamedContent = new DefaultStreamedContent(new ByteArrayInputStream(pdf), "application/msword",this.getString("nombre.archivo.modelo.carta.presentacion"));
			super.setStreamedContent(streamedContent);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void downloadReglamentoServicioLectura() {
		try {
			Path pdfPath = Paths.get(this.getString("ruta.archivos.estaticos")+this.getString("nombre.archivo.reglamento.uso.biblioteca"));
			byte[] pdf = Files.readAllBytes(pdfPath);
			StreamedContent streamedContent = new DefaultStreamedContent(new ByteArrayInputStream(pdf), "application/pdf",this.getString("nombre.archivo.reglamento.uso.biblioteca"));
			super.setStreamedContent(streamedContent);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}	
	

	public void convertPhotoReniec(byte[] fotoBytes){
		try {
			this.getRequest().getUser().getFotoReniec().setRawFile(fotoBytes);	
			this.streamedContentPhotoReniec =  new ByteArrayContent(fotoBytes); 
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	
	public String cambiarFormatoUbigeo(String ubigeo){		
		System.out.println("Entre a cambiarFormatoUbigeo");
		System.out.println("ubigeo->"+ubigeo);
		  		  		
		StringTokenizer tokens = new StringTokenizer(ubigeo,"/");
		String nombreDepartamento=tokens.nextToken(); 
		String nombreProvincia=tokens.nextToken(); 
		String nombreDistrito ="";
		
		try {
			nombreDistrito=tokens.nextToken();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error");
			e.printStackTrace();
			
			nombreDistrito = "";			
			
		}finally{
			if(nombreDepartamento.equals("CALLAO")){
				nombreDistrito = nombreProvincia;
				nombreProvincia = nombreDepartamento;
			}
		}
		System.out.println(nombreDepartamento+"/"+nombreDistrito+"/"+nombreProvincia);
		return buscarCodigoUbigeo(nombreDepartamento, nombreProvincia, nombreDistrito);
	}
	
	public String buscarCodigoUbigeo(String nombreDepartamento, String nombreProvincia, String nombreDistrito){			
		return buscarDistrito(buscarProvincia(buscarDepartamento(nombreDepartamento), nombreProvincia), nombreDistrito);
	}
	
	public String buscarDepartamento(String nombreDepartamento){  
		List<UbigeoReniec> lst = new ArrayList<UbigeoReniec>();
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		String codigoDepartamento = "00";
		tmpubigeo.setDescripcion(nombreDepartamento);
		tmpubigeo.setCoddep("%");
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");		
		try {
			lst = super.getPersonaServiceRemote().listUbigeoXDescripcion(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
		
		if(lst!=null && lst.size()>0){
			codigoDepartamento = lst.get(0).getCoddep();			
		}

		return codigoDepartamento;
	}
	
	public String buscarProvincia(String codigoDepartamento, String nombreProvincia){ 
		List<UbigeoReniec> lst = new ArrayList<UbigeoReniec>();
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		String codigoProvincia = "00";
		tmpubigeo.setDescripcion(nombreProvincia);
		tmpubigeo.setCoddep(codigoDepartamento);
		tmpubigeo.setCodpro("%");
		tmpubigeo.setCoddis("00");		
		try {
			lst = super.getPersonaServiceRemote().listUbigeoXDescripcion(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
		
		if(lst!=null && lst.size()>0){
			codigoProvincia = lst.get(0).getCodpro();			
		}

		return codigoDepartamento+codigoProvincia;
	}
	
	public String buscarDistrito(String codigoDepartamentoProvincia, String nombreDistrito){ 
		List<UbigeoReniec> lst = new ArrayList<UbigeoReniec>();
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		String codigoDistrito = "00";
		tmpubigeo.setDescripcion(nombreDistrito);
		tmpubigeo.setCoddep(codigoDepartamentoProvincia.substring(0, 2));
		tmpubigeo.setCodpro(codigoDepartamentoProvincia.substring(2, 4));
		tmpubigeo.setCoddis("%");		
		try {
			lst = super.getPersonaServiceRemote().listUbigeoXDescripcion(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
		
		if(lst!=null && lst.size()>0){
			codigoDistrito = lst.get(0).getCoddis();			
		}

		return codigoDepartamentoProvincia+codigoDistrito;
	}	

	public String startUserRegistration(String userType) {
		this.clearFields();
		this.request.getUser().setTipoUsuarioBiblioteca(userType);
		this.definirElementosActivos();		
		return userType.equals(Constants.USER_TYPE_GENERAL) ? 
					(Constants.PAGE_GENERAL_USER_REGISTRATION):(
					(userType.equals(Constants.USER_TYPE_RESEARCHER) ? Constants.PAGE_RESEARCHER_USER_REGISTRATION: Constants.PAGE_GENERAL_DEFAULT));
	}

	public String goToStartPage() {
		return Constants.PAGE_GENERAL_DEFAULT;
	}

	public String goToTestPage() {
		return Constants.PAGE_GENERAL_DEFAULT;
	}

	public List<ParameterTable> getTiposDocumentoIdentidad() {
		return tiposDocumentoIdentidad;
	}

	public void setTiposDocumentoIdentidad(
			List<ParameterTable> tiposDocumentoIdentidad) {
		this.tiposDocumentoIdentidad = tiposDocumentoIdentidad;
	}


	public String getTipoDocumentoIdentidadxValidar() {
		return tipoDocumentoIdentidadxValidar;
	}

	public void setTipoDocumentoIdentidadxValidar(
			String tipoDocumentoIdentidadxValidar) {
		this.tipoDocumentoIdentidadxValidar = tipoDocumentoIdentidadxValidar;
	}

	public String getNumeroDocumentoIdentidadxValidar() {
		return numeroDocumentoIdentidadxValidar;
	}

	public void setNumeroDocumentoIdentidadxValidar(
			String numeroDocumentoIdentidadxValidar) {
		this.numeroDocumentoIdentidadxValidar = numeroDocumentoIdentidadxValidar;
	}

	public String getPrimerApellidoxValidar() {
		return primerApellidoxValidar;
	}

	public void setPrimerApellidoxValidar(String primerApellidoxValidar) {
		this.primerApellidoxValidar = primerApellidoxValidar;
	}

	public boolean isShowPanelBusquedaReniec() {
		return showPanelBusquedaReniec;
	}

	public void setShowPanelBusquedaReniec(boolean showPanelBusquedaReniec) {
		this.showPanelBusquedaReniec = showPanelBusquedaReniec;
	}

	public boolean isShowTipoDocumentoDNI() {
		return showTipoDocumentoDNI;
	}

	public void setShowTipoDocumentoDNI(boolean showTipoDocumentoDNI) {
		this.showTipoDocumentoDNI = showTipoDocumentoDNI;
	}

	public boolean isShowTipoDocumentoCE() {
		return showTipoDocumentoCE;
	}

	public void setShowTipoDocumentoCE(boolean showTipoDocumentoCE) {
		this.showTipoDocumentoCE = showTipoDocumentoCE;
	}

	public List<UbigeoReniec> getLstDepartamento() {
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep("%");
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");		
		try {
			lstDepartamento = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}

		return lstDepartamento;
	}

	public void setLstDepartamento(List<UbigeoReniec> lstDepartamento) {
		this.lstDepartamento = lstDepartamento;
	}

	public List<UbigeoReniec> getLstProvincia() {
		return lstProvincia;
	}

	public void setLstProvincia(List<UbigeoReniec> lstProvincia) {
		this.lstProvincia = lstProvincia;
	}

	public List<UbigeoReniec> getLstDistritos() {
		return lstDistritos;
	}

	public void setLstDistritos(List<UbigeoReniec> lstDistritos) {
		this.lstDistritos = lstDistritos;
	}

	public List<Pais> getLstPais() {
		return lstPais;
	}

	public void setLstPais(List<Pais> lstPais) {
		this.lstPais = lstPais;
	}

	public RegistroUsuario getRequest() {
		return request;
	}

	public void setRequest(RegistroUsuario request) {
		this.request = request;
	}

	public StreamedContent getStreamedContentPhotoReniec() {
		return streamedContentPhotoReniec;
	}

	public void setStreamedContentPhotoReniec(
			StreamedContent streamedContentPhotoReniec) {
		this.streamedContentPhotoReniec = streamedContentPhotoReniec;
	}

	public boolean isShowPanelBusquedaMigraciones() {
		return showPanelBusquedaMigraciones;
	}

	public void setShowPanelBusquedaMigraciones(boolean showPanelBusquedaMigraciones) {
		this.showPanelBusquedaMigraciones = showPanelBusquedaMigraciones;
	}

	public boolean isShowPaisPeru() {
		return showPaisPeru;
	}

	public void setShowPaisPeru(boolean showPaisPeru) {
		this.showPaisPeru = showPaisPeru;
	}

	public StreamedContent getStreamedContentPhotoAdjuntada() {
		return streamedContentPhotoAdjuntada;
	}

	public void setStreamedContentPhotoAdjuntada(
			StreamedContent streamedContentPhotoAdjuntada) {
		this.streamedContentPhotoAdjuntada = streamedContentPhotoAdjuntada;
	}

	public boolean isDisabledBotonSave() {
		return isDisabledBotonSave;
	}

	public void setDisabledBotonSave(boolean isDisabledBotonSave) {
		this.isDisabledBotonSave = isDisabledBotonSave;
	}

	public boolean isShowFotoReniec() {
		return showFotoReniec;
	}

	public void setShowFotoReniec(boolean showFotoReniec) {
		this.showFotoReniec = showFotoReniec;
	}

	public Boolean getShowSeccionInvestigacion() {
		return showSeccionInvestigacion;
	}

	public void setShowSeccionInvestigacion(Boolean showSeccionInvestigacion) {
		this.showSeccionInvestigacion = showSeccionInvestigacion;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public int getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	public List<ParameterTable> getListaPreferencias() {
		return listaPreferencias;
	}

	public void setListaPreferencias(List<ParameterTable> listaPreferencias) {
		this.listaPreferencias = listaPreferencias;
	}

	public void cargarImagenCarnet(){
		if (this.request.getUser().getFotoAdjuntada().getRawFile()!= null){
			this.request.getFotoCarnetFrente().setRawFile(this.obtenerFondoCarnetAnversoDefault(this.request.getEscalaManualFotoCarnet(), this.request.getPosicionXFotoCarnet(), this.request.getPosicionYFotoCarnet()));
			this.request.getFotoCarnetReverso().setRawFile(this.obtenerFondoCarnetReversoDefault());
		}
	}	
	

	
	public byte[]  obtenerFondoCarnetAnversoDefault(int escalaFotoCarnet, int posXFotoCarnet, int posYFotoCarnet){
		BufferedImage img;
		try {
			String nombre = Convert.toTitleCase(VO.getString(this.request.getUser().getNombre()).toLowerCase());
			String apellidoPaterno =Convert.toTitleCase(VO.getString(this.request.getUser().getApellidoPaterno()).toLowerCase()); 
			String apellidoMaterno = Convert.toTitleCase(VO.getString(this.request.getUser().getApellidoMaterno()).toLowerCase());
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String fechaFinVigencia = df.format(this.request.getFechaFin());
			byte[] fotoCarnet = this.request.getUser().getFotoAdjuntada().getRawFile();
			Image imgCarnet= this.progressiveScaling(ImageIO.read(VO.getInputStream(fotoCarnet)),Constants.CARNET_BIBLIOTECA_FOTO_ESCALA_DEFAULT+ escalaFotoCarnet); 
			
			img = ImageIO.read(new File(this.getString(Constants.CARNET_BIBLIOTECA_FOTO_FRENTE_FONDO_ARCHIVO)));
			Graphics2D graphic = img.createGraphics();
			
			//calcular el tama�o de la letra en funcion del tama�o del texto
			String apellidos = apellidoPaterno+" "+apellidoMaterno;

			double stringLenApellidos = graphic.getFontMetrics().getStringBounds(apellidos, graphic).getWidth();
			double stringLenNombres = graphic.getFontMetrics().getStringBounds(nombre, graphic).getWidth();
			
			double stringLenMaximo = stringLenApellidos > stringLenNombres? stringLenApellidos:stringLenNombres;
			
			int longitudCoordenadaTexto = 590;
			int largoDisponibleTexto = 112;
			double ratioCoordenadaTexto = longitudCoordenadaTexto/largoDisponibleTexto;
			int posInicialXApellidos = 30;
			int posInicialYApellidos = 300;
			int posInicialXNombres = 30;
			int posInicialYNombres = 380;
			int posXApellidos = posInicialXApellidos;
			int posXNombres = posInicialXNombres;
			double largoFuenteDefault = 65;
			double largoFuenteFinal = largoFuenteDefault;
			double ratioOriginalFuente = stringLenMaximo/largoDisponibleTexto;
			if (ratioOriginalFuente>1){//el largo del apellido es mayor al espacio disponible
				//disminiuir el tama�o de la fuente
				largoFuenteFinal = largoFuenteDefault/ratioOriginalFuente;
				//ya no centrar la posicion x
				if (stringLenMaximo==stringLenApellidos){
					posXApellidos = posInicialXApellidos;
				}else{
					posXApellidos = (int)(posInicialXApellidos + (longitudCoordenadaTexto/2) - (((stringLenApellidos*ratioCoordenadaTexto/ratioOriginalFuente)/2)));
				}
				if (stringLenMaximo==stringLenNombres){
					posXNombres = posInicialXNombres;
				}else{
					posXNombres = (int)(posInicialXNombres + (longitudCoordenadaTexto/2) - (((stringLenNombres*ratioCoordenadaTexto/ratioOriginalFuente)/2)));
				}
				
			}else{//caso normal - el largo si entra dentro del espacio disponible
				//se mantiene el tama�o de la fuente normal
				largoFuenteFinal = largoFuenteDefault;
				//se puede centrar
				posXApellidos = (int)(posInicialXApellidos + (longitudCoordenadaTexto/2) - (((stringLenApellidos*ratioCoordenadaTexto)/2)));
				posXNombres = (int)(posInicialXNombres + (longitudCoordenadaTexto/2) - (((stringLenNombres*ratioCoordenadaTexto)/2)));
			}
			
			graphic.setFont(new Font("Arial", Font.PLAIN, (int)largoFuenteFinal));
			Color color = new Color(42,77,86);
			graphic.setColor(color);
			graphic.drawString(apellidos, posXApellidos, posInicialYApellidos);
			graphic.drawString(nombre, posXNombres, posInicialYNombres);
			graphic.setFont(new Font("Arial", Font.PLAIN, 50));
			graphic.drawString("C�digo: "+this.request.getNumeroCarnetBiblioteca().toUpperCase(), 45, 500);
			graphic.setFont(new Font("Arial", Font.BOLD, 35));
			graphic.drawString("Vence: ", 640, 550);
			graphic.setColor(Color.RED);
			graphic.drawString(fechaFinVigencia, 768, 550);
			graphic.drawImage(imgCarnet, Constants.CARNET_BIBLIOTECA_FOTO_POSX_DEFAULT + posXFotoCarnet , Constants.CARNET_BIBLIOTECA_FOTO_POSY_DEFAULT + posYFotoCarnet, null); //660, 50
			graphic.dispose();
			
			ByteArrayOutputStream outArrayStream = new ByteArrayOutputStream();
			ImageIO.write(img,"jpg",outArrayStream);
			return outArrayStream==null?null:outArrayStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public byte[]  obtenerFondoCarnetReversoDefault(){
		BufferedImage img;
		try {
			int fontSize = 41 ;
			Barcode39 code39 = new Barcode39 ();	 
			code39.setTextAlignment(Element.ALIGN_CENTER);
			code39.setCode(this.request.getNumeroCarnetBiblioteca().toUpperCase());
			Image image39ext = code39.createAwtImage(Color.BLACK, Color.WHITE);
			int w = (int) (code39.getBarcodeSize().getWidth())*4;
			int h = (int)(code39.getBarcodeSize().getHeight())*4;
			 
			String texto = this.request.getNumeroCarnetBiblioteca().toUpperCase();
				
			img = ImageIO.read(new File(this.getString(Constants.CARNET_BIBLIOTECA_FOTO_REVERSO_FONDO_ARCHIVO)));
			Graphics2D graphic = img.createGraphics(); 
			graphic.setTransform(AffineTransform.getRotateInstance(Math.toRadians(-90),750, 520));
			graphic.drawImage(image39ext, 750, 520,w + 12, h + 12, null);
			
			double stringLen = graphic.getFontMetrics().getStringBounds(texto, graphic).getWidth();
			double start = w / 2 - stringLen / 2;
			graphic.setFont(new Font("Arial", Font.PLAIN, fontSize));
			graphic.setColor(Color.BLACK);
			graphic.drawString(texto, 840, 700);
			
			graphic.dispose();
			
			ByteArrayOutputStream outArrayStream = new ByteArrayOutputStream();
			ImageIO.write(img,"jpg",outArrayStream);
			return outArrayStream==null?null:outArrayStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private boolean generateQRFile(String qrCodeText) throws IOException{
		boolean resultado = false;
		try {
			File fileQR = null;
			Image imageQRCode = null;
			BufferedImage bufferedImage = null;
			if (!qrCodeText.equals(Constants.EMPTY_STRING)){
				BarcodeQRCode qRCode = new  BarcodeQRCode(qrCodeText,Constants.QR_IMAGE_WIDTH,Constants.QR_IMAGE_HEIGHT,null);
				imageQRCode = qRCode.createAwtImage(Color.BLACK, Color.WHITE);
				
			    bufferedImage = new BufferedImage(imageQRCode.getWidth(null), imageQRCode.getHeight(null),BufferedImage.TYPE_INT_RGB);
			    Graphics g = bufferedImage.createGraphics();
			    g.drawImage(imageQRCode, 0, 0, null);
			    g.dispose();
			    ImageIO.write(bufferedImage, Constants.FILE_CONTENT_TYPE_IMAGE_DEFAULT, new File(this.getString(Constants.PROPERTY_QRCODE_REPOSITORY_PATH) +qrCodeText+Constants.FILE_CONTENT_TYPE_IMAGE_EXTENSION_DEFAULT));
			    resultado = true;
			    
			}			
		} catch (Exception e) {
			 resultado = false;
			 e.printStackTrace();
			 logger.error("Error al generar archivo QR: " + e.getMessage());
		}
		return resultado ;
	}	

	
	private BufferedImage progressiveScaling(BufferedImage before, Integer longestSideLength) {//beatiful code
	    if (before != null) {
	        Integer w = before.getWidth();
	        Integer h = before.getHeight();

	        Double ratio = h > w ? longestSideLength.doubleValue() / h : longestSideLength.doubleValue() / w;

	        //Multi Step Rescale operation
	        //This technique is describen in Chris Campbell�s blog The Perils of Image.getScaledInstance(). As Chris mentions, when downscaling to something less than factor 0.5, you get the best result by doing multiple downscaling with a minimum factor of 0.5 (in other words: each scaling operation should scale to maximum half the size).
	        while (ratio < 0.5) {
	            BufferedImage tmp = scale(before, 0.5);
	            before = tmp;
	            w = before.getWidth();
	            h = before.getHeight();
	            ratio = h > w ? longestSideLength.doubleValue() / h : longestSideLength.doubleValue() / w;
	        }
	        BufferedImage after = scale(before, ratio);
	        return after;
	    }
	    return null;
	}

	private static BufferedImage scale(BufferedImage imageToScale, Double ratio) {
	    Integer dWidth = ((Double) (imageToScale.getWidth() * ratio)).intValue();
	    Integer dHeight = ((Double) (imageToScale.getHeight() * ratio)).intValue();
	    BufferedImage scaledImage = new BufferedImage(dWidth, dHeight, BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics2D = scaledImage.createGraphics();
	    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
	    graphics2D.dispose();
	    return scaledImage;
	}

	public boolean isShowTipoDocumentoPasaporte() {
		return showTipoDocumentoPasaporte;
	}

	public void setShowTipoDocumentoPasaporte(boolean showTipoDocumentoPasaporte) {
		this.showTipoDocumentoPasaporte = showTipoDocumentoPasaporte;
	}

	public List<ParameterTable> getListaNacionalidad() {
		return listaNacionalidad;
	}

	public void setListaNacionalidad(List<ParameterTable> listaNacionalidad) {
		this.listaNacionalidad = listaNacionalidad;
	}

	public List<ParameterTable> getListaGradoInstruccion() {
		return listaGradoInstruccion;
	}

	public void setListaGradoInstruccion(List<ParameterTable> listaGradoInstruccion) {
		this.listaGradoInstruccion = listaGradoInstruccion;
	}

	public ResponseObject registrarConsultaVerificacionIdentidad(String entidad, String urlConsultada, String numDocIdentidad, String tipoDocIdentidad, String apellido, String origenConsulta, String credencialUsuario){
		ResponseObject resultado = null;
		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String direccionIp = httpServletRequest.getRemoteAddr();
			direccionIp = VO.getString(direccionIp).equals(Constants.EMPTY_STRING)?"-":VO.getString(direccionIp);

			VerificacionIdentidad verificacionIdentidad = new VerificacionIdentidad(direccionIp,entidad,urlConsultada, numDocIdentidad,tipoDocIdentidad, apellido,origenConsulta,credencialUsuario);
			resultado=registroServiceRemote.registrarUsoVerificacionEntidad(verificacionIdentidad);
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
		return resultado;
	}
	
	
}

package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;
public class ReporteVerRespuestas extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long    solicitudMovimientoId;
	private Long 	solicitudTransparenciaId;
	
	//> Emisor
	private String  t01UndOrgEmiId;
	private String  t01UndOrgEmiDeta;
	private Long	perfilIdEmisor;
	private String  perfilDetaEmisor;
	private Long  	usuarioIdEmisor;
	private String  usuarioDetaEmisor;

	private Long 	situSoliIdEmision;
	private String 	situSoliDetaEmision;
	private Long 	estaSoliIdEmision;
	private String 	estaSoliDetaEmision;
	
	private String  detalleAsignacion;
	
	private Long    usuarioIdRegistroEmi;
	private Long    usuarioDetaRegistroEmi;
	private String	fechaRegistroEmision;
	
	//> Receptor
	private String 	t01UndOrgRecId;
	private String 	t01UndOrgRecDeta;
	private Long	perfilIdReceptor;
	private String  perfilDetaReceptor;
	private Long 	usuarioIdReceptor;
	private String 	usuarioDetaReceptor;

	private String 	detalleRespuesta;
	
	private Long 	situSoliIdRecepcion;
	private String 	situSoliDetaRecepcion;
	private Long 	estaSoliIdRecepcion;
	private String 	estaSoliDetaRecepcion;
	
	private Long    usuarioIdRegistroRec;
	private Long    usuarioDetaRegistroRec;
	private String	fechaRegistroRecepcion;

	public ReporteVerRespuestas() {
		super();
		this.setSolicitudTransparenciaId(0L);
		this.setDetalleRespuesta("");
		this.setDetalleAsignacion("");
	}

	public Long getSolicitudMovimientoId() {
		return solicitudMovimientoId;
	}

	public void setSolicitudMovimientoId(Long solicitudMovimientoId) {
		this.solicitudMovimientoId = solicitudMovimientoId;
	}

	public Long getSolicitudTransparenciaId() {
		return solicitudTransparenciaId;
	}

	public void setSolicitudTransparenciaId(Long solicitudTransparenciaId) {
		this.solicitudTransparenciaId = solicitudTransparenciaId;
	}

	public String getT01UndOrgEmiId() {
		return t01UndOrgEmiId;
	}

	public void setT01UndOrgEmiId(String t01UndOrgEmiId) {
		this.t01UndOrgEmiId = t01UndOrgEmiId;
	}

	public String getT01UndOrgEmiDeta() {
		return t01UndOrgEmiDeta;
	}

	public void setT01UndOrgEmiDeta(String t01UndOrgEmiDeta) {
		this.t01UndOrgEmiDeta = t01UndOrgEmiDeta;
	}

	public Long getPerfilIdEmisor() {
		return perfilIdEmisor;
	}

	public void setPerfilIdEmisor(Long perfilIdEmisor) {
		this.perfilIdEmisor = perfilIdEmisor;
	}

	public String getPerfilDetaEmisor() {
		return perfilDetaEmisor;
	}

	public void setPerfilDetaEmisor(String perfilDetaEmisor) {
		this.perfilDetaEmisor = perfilDetaEmisor;
	}

	public Long getUsuarioIdEmisor() {
		return usuarioIdEmisor;
	}

	public void setUsuarioIdEmisor(Long usuarioIdEmisor) {
		this.usuarioIdEmisor = usuarioIdEmisor;
	}

	public String getUsuarioDetaEmisor() {
		return usuarioDetaEmisor;
	}

	public void setUsuarioDetaEmisor(String usuarioDetaEmisor) {
		this.usuarioDetaEmisor = usuarioDetaEmisor;
	}

	public Long getSituSoliIdEmision() {
		return situSoliIdEmision;
	}

	public void setSituSoliIdEmision(Long situSoliIdEmision) {
		this.situSoliIdEmision = situSoliIdEmision;
	}

	public String getSituSoliDetaEmision() {
		return situSoliDetaEmision;
	}

	public void setSituSoliDetaEmision(String situSoliDetaEmision) {
		this.situSoliDetaEmision = situSoliDetaEmision;
	}

	public Long getEstaSoliIdEmision() {
		return estaSoliIdEmision;
	}

	public void setEstaSoliIdEmision(Long estaSoliIdEmision) {
		this.estaSoliIdEmision = estaSoliIdEmision;
	}

	public String getEstaSoliDetaEmision() {
		return estaSoliDetaEmision;
	}

	public void setEstaSoliDetaEmision(String estaSoliDetaEmision) {
		this.estaSoliDetaEmision = estaSoliDetaEmision;
	}

	public String getDetalleAsignacion() {
		return detalleAsignacion;
	}

	public void setDetalleAsignacion(String detalleAsignacion) {
		this.detalleAsignacion = detalleAsignacion;
	}

	public Long getUsuarioIdRegistroEmi() {
		return usuarioIdRegistroEmi;
	}

	public void setUsuarioIdRegistroEmi(Long usuarioIdRegistroEmi) {
		this.usuarioIdRegistroEmi = usuarioIdRegistroEmi;
	}

	public Long getUsuarioDetaRegistroEmi() {
		return usuarioDetaRegistroEmi;
	}

	public void setUsuarioDetaRegistroEmi(Long usuarioDetaRegistroEmi) {
		this.usuarioDetaRegistroEmi = usuarioDetaRegistroEmi;
	}

	public String getFechaRegistroEmision() {
		return fechaRegistroEmision;
	}

	public void setFechaRegistroEmision(String fechaRegistroEmision) {
		this.fechaRegistroEmision = fechaRegistroEmision;
	}

	public String getT01UndOrgRecId() {
		return t01UndOrgRecId;
	}

	public void setT01UndOrgRecId(String t01UndOrgRecId) {
		this.t01UndOrgRecId = t01UndOrgRecId;
	}

	public String getT01UndOrgRecDeta() {
		return t01UndOrgRecDeta;
	}

	public void setT01UndOrgRecDeta(String t01UndOrgRecDeta) {
		this.t01UndOrgRecDeta = t01UndOrgRecDeta;
	}

	public Long getPerfilIdReceptor() {
		return perfilIdReceptor;
	}

	public void setPerfilIdReceptor(Long perfilIdReceptor) {
		this.perfilIdReceptor = perfilIdReceptor;
	}

	public String getPerfilDetaReceptor() {
		return perfilDetaReceptor;
	}

	public void setPerfilDetaReceptor(String perfilDetaReceptor) {
		this.perfilDetaReceptor = perfilDetaReceptor;
	}

	public Long getUsuarioIdReceptor() {
		return usuarioIdReceptor;
	}

	public void setUsuarioIdReceptor(Long usuarioIdReceptor) {
		this.usuarioIdReceptor = usuarioIdReceptor;
	}

	public String getUsuarioDetaReceptor() {
		return usuarioDetaReceptor;
	}

	public void setUsuarioDetaReceptor(String usuarioDetaReceptor) {
		this.usuarioDetaReceptor = usuarioDetaReceptor;
	}

	public String getDetalleRespuesta() {
		return detalleRespuesta;
	}

	public void setDetalleRespuesta(String detalleRespuesta) {
		this.detalleRespuesta = detalleRespuesta;
	}

	public Long getSituSoliIdRecepcion() {
		return situSoliIdRecepcion;
	}

	public void setSituSoliIdRecepcion(Long situSoliIdRecepcion) {
		this.situSoliIdRecepcion = situSoliIdRecepcion;
	}

	public String getSituSoliDetaRecepcion() {
		return situSoliDetaRecepcion;
	}

	public void setSituSoliDetaRecepcion(String situSoliDetaRecepcion) {
		this.situSoliDetaRecepcion = situSoliDetaRecepcion;
	}

	public Long getEstaSoliIdRecepcion() {
		return estaSoliIdRecepcion;
	}

	public void setEstaSoliIdRecepcion(Long estaSoliIdRecepcion) {
		this.estaSoliIdRecepcion = estaSoliIdRecepcion;
	}

	public String getEstaSoliDetaRecepcion() {
		return estaSoliDetaRecepcion;
	}

	public void setEstaSoliDetaRecepcion(String estaSoliDetaRecepcion) {
		this.estaSoliDetaRecepcion = estaSoliDetaRecepcion;
	}

	public Long getUsuarioIdRegistroRec() {
		return usuarioIdRegistroRec;
	}

	public void setUsuarioIdRegistroRec(Long usuarioIdRegistroRec) {
		this.usuarioIdRegistroRec = usuarioIdRegistroRec;
	}

	public Long getUsuarioDetaRegistroRec() {
		return usuarioDetaRegistroRec;
	}

	public void setUsuarioDetaRegistroRec(Long usuarioDetaRegistroRec) {
		this.usuarioDetaRegistroRec = usuarioDetaRegistroRec;
	}

	public String getFechaRegistroRecepcion() {
		return fechaRegistroRecepcion;
	}

	public void setFechaRegistroRecepcion(String fechaRegistroRecepcion) {
		this.fechaRegistroRecepcion = fechaRegistroRecepcion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

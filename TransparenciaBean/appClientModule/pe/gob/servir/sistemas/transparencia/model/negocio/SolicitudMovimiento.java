package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;
public class SolicitudMovimiento extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long 	solicitudTransparenciaId;
	private Long  	usuarioIdEmisor;
	private String  usuarioDetaEmisor;
	private Long	perfilIdEmisor;
	private String  perfilDetaEmisor;
	private String  t01UndOrgEmiId;
	private String  t01UndOrgEmiDeta;
	private String 	t01UndOrgRecId;
	private String 	t01UndOrgRecDeta;
	private String 	situacionSolicitudId;
	private String 	estadoSolicitudId;
	private Long	perfilIdReceptor;
	private String  perfilDetaReceptor;
	private Long 	usuarioIdReceptor;
	private String 	usuarioDetaReceptor;
	private String 	correo;
	private String 	asunto;
	private String 	detalleRespuesta;
	private String  listaProyeccionUndOrg; //Lista temporal de las unidades organicas que se desea enviar por el asistente.
	private String  detalleAsignacion;
	private List<DetalleMovimiento> listaDocsRespuestas;
	private Date 	fechaAsignacion;
	private String  habilitaModificacion;

	public SolicitudMovimiento() {
		super();
		this.setSolicitudTransparenciaId(0L);
		this.setListaDocsRespuestas(new ArrayList<DetalleMovimiento>());
		this.setCorreo("");
		this.setAsunto("");
		this.setDetalleRespuesta("");
		this.setListaProyeccionUndOrg("");
		this.setDetalleAsignacion("");
	}

	public Long getSolicitudTransparenciaId() {
		return solicitudTransparenciaId;
	}

	public void setSolicitudTransparenciaId(Long solicitudTransparenciaId) {
		this.solicitudTransparenciaId = solicitudTransparenciaId;
	}

	public Long getUsuarioIdEmisor() {
		return usuarioIdEmisor;
	}

	public void setUsuarioIdEmisor(Long usuarioIdEmisor) {
		this.usuarioIdEmisor = usuarioIdEmisor;
	}

	public String getUsuarioDetaEmisor() {
		return usuarioDetaEmisor;
	}

	public void setUsuarioDetaEmisor(String usuarioDetaEmisor) {
		this.usuarioDetaEmisor = usuarioDetaEmisor;
	}

	public Long getPerfilIdEmisor() {
		return perfilIdEmisor;
	}

	public void setPerfilIdEmisor(Long perfilIdEmisor) {
		this.perfilIdEmisor = perfilIdEmisor;
	}

	public String getPerfilDetaEmisor() {
		return perfilDetaEmisor;
	}

	public void setPerfilDetaEmisor(String perfilDetaEmisor) {
		this.perfilDetaEmisor = perfilDetaEmisor;
	}

	public String getT01UndOrgEmiId() {
		return t01UndOrgEmiId;
	}
	
	public void setT01UndOrgEmiId(String t01UndOrgEmiId) {
		this.t01UndOrgEmiId = t01UndOrgEmiId;
	}

	public String getT01UndOrgRecId() {
		return t01UndOrgRecId;
	}

	public String getT01UndOrgEmiDeta() {
		return t01UndOrgEmiDeta;
	}

	public void setT01UndOrgEmiDeta(String t01UndOrgEmiDeta) {
		this.t01UndOrgEmiDeta = t01UndOrgEmiDeta;
	}
	
	public void setT01UndOrgRecId(String t01UndOrgRecId) {
		this.t01UndOrgRecId = t01UndOrgRecId;
	}

	public String getT01UndOrgRecDeta() {
		return t01UndOrgRecDeta;
	}

	public void setT01UndOrgRecDeta(String t01UndOrgRecDeta) {
		this.t01UndOrgRecDeta = t01UndOrgRecDeta;
	}

	public String getSituacionSolicitudId() {
		return situacionSolicitudId;
	}

	public void setSituacionSolicitudId(String situacionSolicitudId) {
		this.situacionSolicitudId = situacionSolicitudId;
	}

	public String getEstadoSolicitudId() {
		return estadoSolicitudId;
	}

	public void setEstadoSolicitudId(String estadoSolicitudId) {
		this.estadoSolicitudId = estadoSolicitudId;
	}

	public Long getPerfilIdReceptor() {
		return perfilIdReceptor;
	}

	public void setPerfilIdReceptor(Long perfilIdReceptor) {
		this.perfilIdReceptor = perfilIdReceptor;
	}

	public String getPerfilDetaReceptor() {
		return perfilDetaReceptor;
	}

	public void setPerfilDetaReceptor(String perfilDetaReceptor) {
		this.perfilDetaReceptor = perfilDetaReceptor;
	}

	public Long getUsuarioIdReceptor() {
		return usuarioIdReceptor;
	}

	public void setUsuarioIdReceptor(Long usuarioIdReceptor) {
		this.usuarioIdReceptor = usuarioIdReceptor;
	}

	public String getUsuarioDetaReceptor() {
		return usuarioDetaReceptor;
	}

	public void setUsuarioDetaReceptor(String usuarioDetaReceptor) {
		this.usuarioDetaReceptor = usuarioDetaReceptor;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getDetalleRespuesta() {
		return detalleRespuesta;
	}

	public void setDetalleRespuesta(String detalleRespuesta) {
		this.detalleRespuesta = detalleRespuesta;
	}

	public String getListaProyeccionUndOrg() {
		return listaProyeccionUndOrg;
	}

	public void setListaProyeccionUndOrg(String listaProyeccionUndOrg) {
		this.listaProyeccionUndOrg = listaProyeccionUndOrg;
	}

	public String getDetalleAsignacion() {
		return detalleAsignacion;
	}

	public void setDetalleAsignacion(String detalleAsignacion) {
		this.detalleAsignacion = detalleAsignacion;
	}

	public List<DetalleMovimiento> getListaDocsRespuestas() {
		return listaDocsRespuestas;
	}

	public void setListaDocsRespuestas(List<DetalleMovimiento> listaDocsRespuestas) {
		this.listaDocsRespuestas = listaDocsRespuestas;
	}

	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public String getHabilitaModificacion() {
		return habilitaModificacion;
	}

	public void setHabilitaModificacion(String habilitaModificacion) {
		this.habilitaModificacion = habilitaModificacion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

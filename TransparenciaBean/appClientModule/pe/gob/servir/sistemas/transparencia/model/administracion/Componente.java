package pe.gob.servir.sistemas.transparencia.model.administracion;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class Componente extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private String padreId;
	private String nombreExterno;
	private String nombreInterno;
	
	public Componente(){
		this.init();
	}
	
	public Componente(Long id){
		super.setId(id);
		this.init();
	}
	
	private void init(){
		this.setNombreInterno("");
		this.setNombreExterno("");
	}

	public String getPadreId() {
		return padreId;
	}

	public void setPadreId(String padreId) {
		this.padreId = padreId;
	}

	public String getNombreExterno() {
		return nombreExterno;
	}

	public void setNombreExterno(String nombreExterno) {
		this.nombreExterno = nombreExterno;
	}

	public String getNombreInterno() {
		return nombreInterno;
	}

	public void setNombreInterno(String nombreInterno) {
		this.nombreInterno = nombreInterno;
	}
	
	
}


package pe.gob.servir.sistemas.transparencia.model.maestra;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;



public class UbigeoReniec extends BasicObject implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String coddep;

	private String coddis;

	private String codpro;

	private String descripcion;

	private String estado;

	public UbigeoReniec() {
	}

	public String getCoddep() {
		return this.coddep;
	}

	public void setCoddep(String coddep) {
		this.coddep = coddep;
	}

	public String getCoddis() {
		return this.coddis;
	}

	public void setCoddis(String coddis) {
		this.coddis = coddis;
	}

	public String getCodpro() {
		return this.codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
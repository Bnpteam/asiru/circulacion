package pe.gob.servir.sistemas.transparencia.model.negocio;

import java.io.Serializable;
import java.util.Date;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;

public class Solicitud extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private T01Maestro T01TipoDocumento;
	
	private long solicitudTransparenciaId;
	private long correlativo;
	private int  version_solicitud;
	private long tipo_documentoId;
	//private long tipoDocumentoIdentidadId;
	
	private String tipoDocumentoIdentidadId;
	private String numeroDocumentoIdentidad;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String direccionDomicilio;
	private String coddep;
	private String codpro;
	private String coddis;
	private String telefono;
	private String fax;
	private String celular;
	private String correo_electronico;
	private String detalleSolicitud;
	private String dependenciaDeInformacion;
	private String formaDeEntrega;
	private String flag_autoriza_recojo;
	private String persAutonroDocumento;
	private String persAutoNombreApellidos;
	private String observacion;
	private String flagDeclaracioJurada;
	private String flagNotifElectronica;	
	private String fechaIngresostd;
	private String asuntoStd;
	private String rutaAlfrescoSTD;	
	private String numRegistro;
	private String remitentePersona;
	private String remitenteEntidad;
	private String codigo;
	private String fechaIngresooaj;
	private String funcionarioResponsable;
	private String tipoSolicitud; // V: VIRTUAL , F: FISICA
	private Date fechaIngreso;
	private String numRegistroSistra;
	
	public Solicitud() {
		super();
		this.setId(0L);
		this.setTipoDocumentoIdentidadId("");
		this.setNumeroDocumentoIdentidad("");
		this.setCoddep("0");
		this.setPersAutonroDocumento("");
		this.setPersAutoNombreApellidos("");
		this.setNumRegistroSistra("");
	}

	public long getSolicitudTransparenciaId() {
		return solicitudTransparenciaId;
	}

	public void setSolicitudTransparenciaId(long solicitudTransparenciaId) {
		this.solicitudTransparenciaId = solicitudTransparenciaId;
	}	

	public long getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(long correlativo) {
		this.correlativo = correlativo;
	}

	public int getVersion_solicitud() {
		return version_solicitud;
	}

	public void setVersion_solicitud(int version_solicitud) {
		this.version_solicitud = version_solicitud;
	}

	public long getTipo_documentoId() {
		return tipo_documentoId;
	}

	public void setTipo_documentoId(long tipo_documentoId) {
		this.tipo_documentoId = tipo_documentoId;
	}

	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}


	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getApellidoMaterno() {
		return apellidoMaterno;
	}


	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}


	public String getNombres() {
		return nombres;
	}


	public void setNombres(String nombres) {
		this.nombres = nombres;
	}


	public String getDireccionDomicilio() {
		return direccionDomicilio;
	}


	public void setDireccionDomicilio(String direccionDomicilio) {
		this.direccionDomicilio = direccionDomicilio;
	}


	public String getCoddep() {
		return coddep;
	}


	public void setCoddep(String coddep) {
		this.coddep = coddep;
	}


	public String getCodpro() {
		return codpro;
	}


	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}


	public String getCoddis() {
		return coddis;
	}


	public void setCoddis(String coddis) {
		this.coddis = coddis;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getCorreo_electronico() {
		return correo_electronico;
	}


	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}


	public String getDetalleSolicitud() {
		return detalleSolicitud;
	}


	public void setDetalleSolicitud(String detalleSolicitud) {
		this.detalleSolicitud = detalleSolicitud;
	}


	public String getDependenciaDeInformacion() {
		return dependenciaDeInformacion;
	}


	public void setDependenciaDeInformacion(String dependenciaDeInformacion) {
		this.dependenciaDeInformacion = dependenciaDeInformacion;
	}

	public String getFormaDeEntrega() {
		return formaDeEntrega;
	}

	public void setFormaDeEntrega(String formaDeEntrega) {
		this.formaDeEntrega = formaDeEntrega;
	}

	public String getFlag_autoriza_recojo() {
		return flag_autoriza_recojo;
	}

	public void setFlag_autoriza_recojo(String flag_autoriza_recojo) {
		this.flag_autoriza_recojo = flag_autoriza_recojo;
	}

	public String getPersAutonroDocumento() {
		return persAutonroDocumento;
	}

	public void setPersAutonroDocumento(String persAutonroDocumento) {
		this.persAutonroDocumento = persAutonroDocumento;
	}


	public String getPersAutoNombreApellidos() {
		return persAutoNombreApellidos;
	}


	public void setPersAutoNombreApellidos(String persAutoNombreApellidos) {
		this.persAutoNombreApellidos = persAutoNombreApellidos;
	}


	public String getObservacion() {
		return observacion;
	}


	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}


	public String getFlagDeclaracioJurada() {
		return flagDeclaracioJurada;
	}


	public void setFlagDeclaracioJurada(String flagDeclaracioJurada) {
		this.flagDeclaracioJurada = flagDeclaracioJurada;
	}


	public String getFlagNotifElectronica() {
		return flagNotifElectronica;
	}


	public void setFlagNotifElectronica(String flagNotifElectronica) {
		this.flagNotifElectronica = flagNotifElectronica;
	}


	public String getFechaIngresostd() {
		return fechaIngresostd;
	}


	public void setFechaIngresostd(String fechaIngresostd) {
		this.fechaIngresostd = fechaIngresostd;
	}


	public String getAsuntoStd() {
		return asuntoStd;
	}


	public void setAsuntoStd(String asuntoStd) {
		this.asuntoStd = asuntoStd;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getFechaIngresooaj() {
		return fechaIngresooaj;
	}


	public void setFechaIngresooaj(String fechaIngresooaj) {
		this.fechaIngresooaj = fechaIngresooaj;
	}


	public String getFuncionarioResponsable() {
		return funcionarioResponsable;
	}


	public void setFuncionarioResponsable(String funcionarioResponsable) {
		this.funcionarioResponsable = funcionarioResponsable;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTipoDocumentoIdentidadId() {
		return tipoDocumentoIdentidadId;
	}

	public void setTipoDocumentoIdentidadId(String tipoDocumentoIdentidadId) {
		this.tipoDocumentoIdentidadId = tipoDocumentoIdentidadId;
	}

	public T01Maestro getT01TipoDocumento() {
		return T01TipoDocumento;
	}

	public void setT01TipoDocumento(T01Maestro t01TipoDocumento) {
		T01TipoDocumento = t01TipoDocumento;
	}

	public String getRutaAlfrescoSTD() {
		return rutaAlfrescoSTD;
	}

	public void setRutaAlfrescoSTD(String rutaAlfrescoSTD) {
		this.rutaAlfrescoSTD = rutaAlfrescoSTD;
	}

	public String getRemitentePersona() {
		return remitentePersona;
	}

	public void setRemitentePersona(String remitentePersona) {
		this.remitentePersona = remitentePersona;
	}

	public String getRemitenteEntidad() {
		return remitenteEntidad;
	}

	public void setRemitenteEntidad(String remitenteEntidad) {
		this.remitenteEntidad = remitenteEntidad;
	}

	public String getNumRegistro() {
		return numRegistro;
	}

	public void setNumRegistro(String numRegistro) {
		this.numRegistro = numRegistro;
	}

	public String getTipoSolicitud() {
		return tipoSolicitud;
	}

	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNumRegistroSistra() {
		return numRegistroSistra;
	}

	public void setNumRegistroSistra(String numRegistroSistra) {
		this.numRegistroSistra = numRegistroSistra;
	}
	
	
}

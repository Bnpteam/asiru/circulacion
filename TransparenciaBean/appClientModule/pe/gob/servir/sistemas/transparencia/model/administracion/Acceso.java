package pe.gob.servir.sistemas.transparencia.model.administracion;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class Acceso extends BasicObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private Componente componente;
	private Perfil perfil;
	private Boolean asignado;

	public Acceso() {
		this.init();
	}

	public Acceso(Long id) {
		super.setId(id);
		this.init();
	}

	private void init() {
		this.setComponente(new Componente());
		this.setPerfil(new Perfil());
	}

	public Componente getComponente() {
		return componente;
	}

	public void setComponente(Componente componente) {
		this.componente = componente;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public boolean isAsignado() {
		return asignado;
	}

	public void setAsignado(boolean asignado) {
		this.asignado = asignado;
	}

}

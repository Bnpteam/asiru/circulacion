package pe.gob.servir.sistemas.transparencia.model.maestra;

import java.io.Serializable;

import pe.gob.servir.sistemas.transparencia.model.basic.BasicObject;

public class T01Maestro extends BasicObject implements Serializable {
	private static final long serialVersionUID = 1L;

	private String codigoTabla;
	private String codigoRegistro;
	
	private String nombreCorto;
	private String nombreLargo;
	private Integer orden;
	private String valor1;
	private String valor2;
	private String valor3;
	private String valor4;
	private String valor5;
	
	private String t01SituacionMaestro;
	
	private String t01SituacionMaestroNc;
	
	private boolean swEditable;
	private boolean swVisible;
	private boolean swDefault;
	
	public T01Maestro() {
	
		this.init();
	}
	
	private void init(){
		
		this.setCodigoTabla("");
		this.setCodigoRegistro("");
		
		this.setNombreCorto("");
		this.setNombreLargo("");
		
		this.setOrden(0);
		
		this.setValor1("");
		this.setValor2("");
		this.setValor3("");
		this.setValor4("");
		this.setValor5("");
	}

	public String getCodigoTabla() {
		return codigoTabla;
	}
	public void setCodigoTabla(String codigoTabla) {
		this.codigoTabla = codigoTabla;
	}
	public String getCodigoRegistro() {
		return codigoRegistro;
	}
	public void setCodigoRegistro(String codigoRegistro) {
		this.codigoRegistro = codigoRegistro;
	}




	public String getNombreCorto() {
		return nombreCorto;
	}


	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}


	public String getNombreLargo() {
		return nombreLargo;
	}


	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}


	public Integer getOrden() {
		return orden;
	}


	public void setOrden(Integer orden) {
		this.orden = orden;
	}


	public String getValor1() {
		return valor1;
	}


	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}


	public String getValor2() {
		return valor2;
	}


	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}


	public String getValor3() {
		return valor3;
	}


	public void setValor3(String valor3) {
		this.valor3 = valor3;
	}


	public String getValor4() {
		return valor4;
	}


	public void setValor4(String valor4) {
		this.valor4 = valor4;
	}


	public String getValor5() {
		return valor5;
	}


	public void setValor5(String valor5) {
		this.valor5 = valor5;
	}

	public String getT01SituacionMaestro() {
		return t01SituacionMaestro;
	}

	public void setT01SituacionMaestro(String t01SituacionMaestro) {
		this.t01SituacionMaestro = t01SituacionMaestro;
	}

	public String getT01SituacionMaestroNc() {
		return t01SituacionMaestroNc;
	}

	public void setT01SituacionMaestroNc(String t01SituacionMaestroNc) {
		this.t01SituacionMaestroNc = t01SituacionMaestroNc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "T01Maestro [codigoTabla=" + codigoTabla + ", codigoRegistro="
				+ codigoRegistro + ", nombreCorto=" + nombreCorto
				+ ", nombreLargo=" + nombreLargo + ", orden=" + orden
				+ ", valor1=" + valor1 + ", valor2=" + valor2 + ", valor3="
				+ valor3 + ", valor4=" + valor4 + ", valor5=" + valor5
				+ ", t01SituacionMaestro=" + t01SituacionMaestro
				+ ", t01SituacionMaestroNc=" + t01SituacionMaestroNc
				+ ", swEditable=" + swEditable + ", swVisible=" + swVisible
				+ ", swDefault=" + swDefault + "]";
	}

	public boolean isSwEditable() {
		return swEditable;
	}

	public void setSwEditable(boolean swEditable) {
		this.swEditable = swEditable;
	}

	public boolean isSwVisible() {
		return swVisible;
	}

	public void setSwVisible(boolean swVisible) {
		this.swVisible = swVisible;
	}

	public boolean isSwDefault() {
		return swDefault;
	}

	public void setSwDefault(boolean swDefault) {
		this.swDefault = swDefault;
	}


}
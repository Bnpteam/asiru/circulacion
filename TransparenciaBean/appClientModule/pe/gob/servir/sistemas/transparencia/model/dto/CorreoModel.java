package pe.gob.servir.sistemas.transparencia.model.dto;

import java.util.ArrayList;
import java.util.Map;

import pe.gob.servir.systems.util.enums.Enums.TemplateMail;
import pe.gob.servir.systems.util.smtp.SmtpObject;

public class CorreoModel{
	private ArrayList<String> para;
	private ArrayList<String> conCopia;
	private String asunto;
	private String mensaje;
	private Map<String,Object> DataMensaje;
	private TemplateMail template;
	private String templateTexto;
	private SmtpObject smtpObject;
	
	public CorreoModel() {
		para = new ArrayList<>();
		conCopia = new ArrayList<>();
		template = null;
		smtpObject = new SmtpObject();
	}
	
	public CorreoModel(TemplateMail t) {
		this();		
		this.template = t;
		this.smtpObject = new SmtpObject();
	}

	public CorreoModel(String templateTexto) {
		this();		
		this.templateTexto = templateTexto;
		this.smtpObject = new SmtpObject();
	}

	
	public CorreoModel addPara(String correo){
		para.add(correo);
		return this;
	}

	public CorreoModel addConCopia(String correo){
		conCopia.add(correo);
		return this;
	}
	
	public CorreoModel limpiarPara(){
		para = new ArrayList<>();
		return this;
	}
	
	public CorreoModel limpiarConCopia(){
		para = new ArrayList<>();
		return this;
	}
	
	public CorreoModel  setAsunto(String asunto) {
		this.asunto = asunto;
		return this;
	}

	public CorreoModel  setMensaje(String mensaje) {
		this.mensaje = mensaje;
		return this;
	}

	public String[] getPara() {
		String[] lista = new String[para.size()];
		para.toArray(lista);
		return lista;
	}

	public String[] getConCopia() {
		String[] lista = new String[conCopia.size()];
		conCopia.toArray(lista);
		return lista;
	}

	public String getAsunto() {
		return asunto;
	}

	public String getMensaje() {
		return mensaje;
	}

	public TemplateMail getTemplate() {
		return template;
	}

	public Map<String, Object> getDataMensaje() {
		return DataMensaje;
	}

	public CorreoModel setDataMensaje(Map<String, Object> dataMensaje) {
		this.DataMensaje = dataMensaje;
		return this;
	}

	public SmtpObject getSmtpObject() {
		return smtpObject;
	}

	public void setSmtpObject(SmtpObject smtpObject) {
		this.smtpObject = smtpObject;
	}

	public String getTemplateTexto() {
		return templateTexto;
	}

	public void setTemplateTexto(String templateTexto) {
		this.templateTexto = templateTexto;
	}
	
	
}

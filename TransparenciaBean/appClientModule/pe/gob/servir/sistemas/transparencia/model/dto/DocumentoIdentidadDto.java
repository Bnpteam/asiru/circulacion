package pe.gob.servir.sistemas.transparencia.model.dto;

import java.io.Serializable;

public class DocumentoIdentidadDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String numeroDocumento;
	private String tipoDocumento;
	private String codPaisEmision;
	private String resultado;
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento.trim();
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
	public String getCodPaisEmision() {
		return codPaisEmision;
	}
	public void setCodPaisEmision(String codPaisEmision) {
		this.codPaisEmision = codPaisEmision;
	}
	
	public DocumentoIdentidadDto() {
	}
	
	public DocumentoIdentidadDto(String numeroDocumento, String tipoDocumento, String codPaisEmision,
			String resultado) {
		super();
		this.numeroDocumento = numeroDocumento.trim();
		this.tipoDocumento = tipoDocumento.trim();
		this.resultado = resultado;
		this.codPaisEmision = codPaisEmision.trim();
	}
	
	
}

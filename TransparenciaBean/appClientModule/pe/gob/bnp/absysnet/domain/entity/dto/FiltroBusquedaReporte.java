package pe.gob.bnp.absysnet.domain.entity.dto;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class FiltroBusquedaReporte implements Serializable {
	private static final long serialVersionUID = 1L;
	public Date rangoFechaDesde;
	public Date rangoFechaHasta;
	private String idTrajabadorIngreso;
	private String idTrajabadorSalida;
	private String idSala;
	private String estadoAsistencia;
	private String numeroDocumentoIdentidad;
	private String codigoLector;
	
	
	
	public FiltroBusquedaReporte() {
		this.rangoFechaDesde 			= new Date();
		this.rangoFechaHasta 			= new Date();
		this.idTrajabadorIngreso 		= Constants.EMPTY_STRING;
		this.idTrajabadorSalida 		= Constants.EMPTY_STRING;
		this.idSala 					= Constants.EMPTY_STRING;
		this.estadoAsistencia 			= Constants.EMPTY_STRING;
		this.numeroDocumentoIdentidad 	= Constants.EMPTY_STRING;
		this.codigoLector		 		= Constants.EMPTY_STRING;
	}
	
	public void reset() {
		this.rangoFechaDesde 			= new Date();
		this.rangoFechaHasta 			= new Date();
		this.idTrajabadorIngreso 		= Constants.EMPTY_STRING;
		this.idTrajabadorSalida 		= Constants.EMPTY_STRING;
		this.idSala 					= Constants.EMPTY_STRING;
		this.estadoAsistencia 			= Constants.EMPTY_STRING;
		this.numeroDocumentoIdentidad 	= Constants.EMPTY_STRING;
		this.codigoLector		 		= Constants.EMPTY_STRING;
	}	
	
	public Date getRangoFechaDesde() {
		return rangoFechaDesde;
	}
	public void setRangoFechaDesde(Date rangoFechaDesde) {
		this.rangoFechaDesde = rangoFechaDesde;
	}
	public Date getRangoFechaHasta() {
		return rangoFechaHasta;
	}
	public void setRangoFechaHasta(Date rangoFechaHasta) {
		this.rangoFechaHasta = rangoFechaHasta;
	}

	public String getIdSala() {
		return idSala;
	}

	public void setIdSala(String idSala) {
		this.idSala = idSala;
	}

	public String getEstadoAsistencia() {
		return estadoAsistencia;
	}

	public void setEstadoAsistencia(String estadoAsistencia) {
		this.estadoAsistencia = estadoAsistencia;
	}

	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}

	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}

	public String getCodigoLector() {
		return codigoLector;
	}

	public void setCodigoLector(String codigoLector) {
		this.codigoLector = codigoLector;
	}

	public String getIdTrajabadorIngreso() {
		return idTrajabadorIngreso;
	}

	public void setIdTrajabadorIngreso(String idTrajabadorIngreso) {
		this.idTrajabadorIngreso = idTrajabadorIngreso;
	}

	public String getIdTrajabadorSalida() {
		return idTrajabadorSalida;
	}

	public void setIdTrajabadorSalida(String idTrajabadorSalida) {
		this.idTrajabadorSalida = idTrajabadorSalida;
	}
	
	
}

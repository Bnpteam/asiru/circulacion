package pe.gob.bnp.dapi.domain.master.entity;

import java.io.Serializable;

public class Pais implements Serializable {
	private static final long serialVersionUID = 1L;	
	private String codPais;
	private String nombrePais;

	public Pais() {
	}
	
	public Pais(String codPais, String nombrePais) {
		this.codPais = codPais;
		this.nombrePais = nombrePais;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getNombrePais() {
		return nombrePais;
	}

	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}

}

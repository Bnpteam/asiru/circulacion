package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;

public class RegistroVisitaASala extends EntityGeneral implements Serializable{
	private static final long serialVersionUID = 1L;
	private RegistroUsuario registroUsuario;
	private Sala  sala;
	private String fechaVisita;

	public RegistroVisitaASala() {
		this.registroUsuario = new RegistroUsuario();
		this.sala = new Sala();
	}
	
	public RegistroUsuario getRegistroUsuario() {
		return registroUsuario;
	}
	public void setRegistroUsuario(RegistroUsuario registroUsuario) {
		this.registroUsuario = registroUsuario;
	}
	public Sala getSala() {
		return sala;
	}
	public void setSala(Sala sala) {
		this.sala = sala;
	}
	public String getFechaVisita() {
		return fechaVisita;
	}
	public void setFechaVisita(String fechaVisita) {
		this.fechaVisita = fechaVisita;
	}

	
}

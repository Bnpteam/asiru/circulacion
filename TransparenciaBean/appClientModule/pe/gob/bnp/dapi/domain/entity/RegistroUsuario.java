package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;
import pe.gob.servir.sistemas.transparencia.model.dto.CorreoModel;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.enums.Enums.TemplateMail;
import pe.gob.servir.systems.util.validator.VO;

public class RegistroUsuario extends EntityGeneral implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String estadoRegistro;
	private String concepto;
	private String vigencia;
	private Date fechaInicio;
	private Date fechaFin;
	private Double costo;
	private String moneda;
	private String reciboPago;
	private Date fechaPago;
	private UserLibrary user;
	private String fechaSolicitud;
	private Date fechaInicioSuspension;
	private Date fechaFinSuspension;
	private String periodoSuspension; 
	private FileAlfresco fotoCarnetFrente;
	private FileAlfresco fotoCarnetReverso;
	private int escalaManualFotoCarnet;
	private int posicionXFotoCarnet;
	private int posicionYFotoCarnet;
	private String numeroCarnetBiblioteca;
	private String codigoQRValor;
	private String codBarrasAbsysnet;
	private String origenRegistro;
	private String codBiblioteca;
	private String codSucursal;
	private String nombreBiblioteca;
	private String nombreSucursal;
	
	public RegistroUsuario() {
		this.id = 0L;
		this.estadoRegistro = Constants.EMPTY_STRING;
		this.concepto = Constants.EMPTY_STRING;
		this.vigencia = Constants.EMPTY_STRING;
		this.fechaInicio = null;
		this.fechaFin = null;
		this.costo = 0D;
		this.moneda = Constants.EMPTY_STRING;
		this.reciboPago = Constants.EMPTY_STRING;
		this.fechaPago =null;
		this.fechaSolicitud = Constants.EMPTY_STRING;
		this.periodoSuspension = Constants.EMPTY_STRING;
		this.fechaInicioSuspension = null;
		this.fechaFinSuspension = null;
		this.user = new UserLibrary();
		this.fotoCarnetFrente = new FileAlfresco();
		this.fotoCarnetReverso = new FileAlfresco();
		this.escalaManualFotoCarnet = Constants.EMPTY_INTEGER;
		this.posicionXFotoCarnet = Constants.EMPTY_INTEGER;
		this.posicionYFotoCarnet = Constants.EMPTY_INTEGER;
		this.numeroCarnetBiblioteca = Constants.EMPTY_STRING;
		this.codigoQRValor = Constants.EMPTY_STRING;
		this.codBarrasAbsysnet = Constants.EMPTY_STRING;
		this.origenRegistro = Constants.EMPTY_STRING;
		this.codBiblioteca	= Constants.ABSYSNET_LECTOR_LECOBI_BNP;
		this.codSucursal	= Constants.ABSYSNET_LECTOR_LECOSU_BNP;	
		this.nombreBiblioteca= Constants.ABSYSNET_BIBLIO_BIDESC_BNP;
		this.nombreSucursal	= Constants.ABSYSNET_SUCURS_SUDESC_BNP;	
	}
	
	public void init(){
		this.id = 0L;
		this.estadoRegistro = Constants.EMPTY_STRING;
		this.concepto = Constants.EMPTY_STRING;
		this.vigencia = Constants.EMPTY_STRING;
		this.fechaInicio = null;
		this.fechaFin = null;
		this.costo = 0D;
		this.moneda = Constants.EMPTY_STRING;
		this.reciboPago = Constants.EMPTY_STRING;
		this.fechaPago =null;
		this.fechaSolicitud = Constants.EMPTY_STRING;
		this.periodoSuspension = Constants.EMPTY_STRING;
		this.fechaInicioSuspension = null;
		this.fechaFinSuspension = null;
		this.fotoCarnetFrente.init();
		this.fotoCarnetReverso.init();
		this.numeroCarnetBiblioteca = Constants.EMPTY_STRING;
		this.codigoQRValor = Constants.EMPTY_STRING;
		this.codBarrasAbsysnet = Constants.EMPTY_STRING;
		this.origenRegistro = Constants.EMPTY_STRING;
		this.codBiblioteca	= Constants.ABSYSNET_LECTOR_LECOBI_BNP;
		this.codSucursal	= Constants.ABSYSNET_LECTOR_LECOSU_BNP;	
		this.nombreBiblioteca= Constants.ABSYSNET_BIBLIO_BIDESC_BNP;
		this.nombreSucursal	= Constants.ABSYSNET_SUCURS_SUDESC_BNP;	
		user.init();
	}
	
	public void generarNumeroCarnet(){
//		if (this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE) ||
//			this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE)){
//			this.numeroCarnetBiblioteca = this.getUser().getNumeroDocumentoIdentidad();//unique
//		}else{//pasaporte
//			this.numeroCarnetBiblioteca = this.user.getCodPaisPasaporte()+this.getUser().getNumeroDocumentoIdentidad();
//		}			
		this.numeroCarnetBiblioteca = this.getUser().getNumeroDocumentoIdentidad();
		this.generarValorCodigoQR();
	}
	
	public void generarValorCodigoQR(){
//		String primeraParte = this.user.getTipoDocumentoIdentidadId();
//		String segundaParte = "";
//		String terceraParte = this.user.getNumeroDocumentoIdentidad();
//		if (this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE) ||
//			this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE) ){
//			segundaParte = Constants.COUNTRY_CODE_PERU;
//		}else{
//			segundaParte = this.user.getCodPaisPasaporte();
//		}
//		this.codigoQRValor = primeraParte + segundaParte+ terceraParte;
		this.codigoQRValor = this.numeroCarnetBiblioteca; 
	}
	
	public Boolean obtenerDatosIdentidadPorCodigoQR(){
//		Boolean seObtuvo = false;
//		if (this.codigoQRValor.length()>0){
//			seObtuvo = true;
//			String tipoDocumentoIdentidad =  this.codigoQRValor.substring(0,1);
//			String codigoPais = this.codigoQRValor.substring(1, 1+ Constants.COUNTRY_CODE_LENGTH);
//			String numeroDocumentoIdentidad = this.codigoQRValor.substring(Constants.COUNTRY_CODE_LENGTH + 1, this.codigoQRValor.length() - Constants.COUNTRY_CODE_LENGTH - 1 -1 );
//			this.getUser().setTipoDocumentoIdentidadId(tipoDocumentoIdentidad);
//			this.getUser().setCodPaisPasaporte(codigoPais);
//			this.getUser().setNumeroDocumentoIdentidad(numeroDocumentoIdentidad);
//		}
//		return seObtuvo;
		this.numeroCarnetBiblioteca = this.codigoQRValor;//para dni y ce se guardar el valor como tal, para pasaporte se guarda el tipo/codigoPais/numero
		//en caso los valores que se impriman en el codigo QR cambien se debe modificar la logica de este metodo para poder obtener a parter de este valor el numero de carnet de biblioteca
		return true;
	}
	
	public Boolean obtenerDatosIdentidadPorDocIdentidad(){
		if (this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_CODE)){
			this.numeroCarnetBiblioteca = this.getUser().getCodPaisPasaporte() + this.getUser().getNumeroDocumentoIdentidad().toUpperCase();
		}
		if (this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE) || this.getUser().getTipoDocumentoIdentidadId().equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE)){
			this.numeroCarnetBiblioteca =  this.getUser().getNumeroDocumentoIdentidad().toUpperCase();
		}
		return true;
	}
	
	public RegistroUsuario(String estadoSolicitud) {
		this.estadoRegistro = estadoSolicitud;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public UserLibrary getUser() {
		return user;
	}

	public void setUser(UserLibrary user) {
		this.user = user;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getReciboPago() {
		return reciboPago;
	}

	public void setReciboPago(String reciboPago) {
		this.reciboPago = reciboPago;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Date getFechaInicioSuspension() {
		return fechaInicioSuspension;
	}

	public void setFechaInicioSuspension(Date fechaInicioSuspension) {
		this.fechaInicioSuspension = fechaInicioSuspension;
	}

	public Date getFechaFinSuspension() {
		return fechaFinSuspension;
	}

	public void setFechaFinSuspension(Date fechaFinSuspension) {
		this.fechaFinSuspension = fechaFinSuspension;
	}

	public String getPeriodoSuspension() {
		return periodoSuspension;
	}

	public void setPeriodoSuspension(String periodoSuspension) {
		this.periodoSuspension = periodoSuspension;
	}

	public FileAlfresco getFotoCarnetFrente() {
		return fotoCarnetFrente;
	}

	public void setFotoCarnetFrente(FileAlfresco fotoCarnetFrente) {
		this.fotoCarnetFrente = fotoCarnetFrente;
	}

	public FileAlfresco getFotoCarnetReverso() {
		return fotoCarnetReverso;
	}

	public void setFotoCarnetReverso(FileAlfresco fotoCarnetReverso) {
		this.fotoCarnetReverso = fotoCarnetReverso;
	}

	public int getEscalaManualFotoCarnet() {
		return escalaManualFotoCarnet;
	}

	public void setEscalaManualFotoCarnet(int escalaManualFotoCarnet) {
		this.escalaManualFotoCarnet = escalaManualFotoCarnet;
	}

	public int getPosicionXFotoCarnet() {
		return posicionXFotoCarnet;
	}

	public void setPosicionXFotoCarnet(int posicionXFotoCarnet) {
		this.posicionXFotoCarnet = posicionXFotoCarnet;
	}

	public int getPosicionYFotoCarnet() {
		return posicionYFotoCarnet;
	}

	public void setPosicionYFotoCarnet(int posicionYFotoCarnet) {
		this.posicionYFotoCarnet = posicionYFotoCarnet;
	}

	public String getNumeroCarnetBiblioteca() {
		return numeroCarnetBiblioteca;
	}

	public void setNumeroCarnetBiblioteca(String numeroCarnetBiblioteca) {
		this.numeroCarnetBiblioteca = numeroCarnetBiblioteca;
	}

	public String getCodigoQRValor() {
		return codigoQRValor;
	}

	public void setCodigoQRValor(String codigoQRValor) {
		this.codigoQRValor = codigoQRValor;
	}

	public String getCodBarrasAbsysnet() {
		return codBarrasAbsysnet;
	}

	public void setCodBarrasAbsysnet(String codBarrasAbsysnet) {
		this.codBarrasAbsysnet = codBarrasAbsysnet;
	}

	public String getOrigenRegistro() {
		return origenRegistro;
	}

	public void setOrigenRegistro(String origenRegistro) {
		this.origenRegistro = origenRegistro;
	}

	public String getCodBiblioteca() {
		return codBiblioteca;
	}

	public void setCodBiblioteca(String codBiblioteca) {
		this.codBiblioteca = codBiblioteca;
	}

	public String getCodSucursal() {
		return codSucursal;
	}

	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}

	public String getNombreBiblioteca() {
		return nombreBiblioteca;
	}

	public void setNombreBiblioteca(String nombreBiblioteca) {
		this.nombreBiblioteca = nombreBiblioteca;
	}

	public String getNombreSucursal() {
		return nombreSucursal;
	}

	public void setNombreSucursal(String nombreSucursal) {
		this.nombreSucursal = nombreSucursal;
	}
	
	
}


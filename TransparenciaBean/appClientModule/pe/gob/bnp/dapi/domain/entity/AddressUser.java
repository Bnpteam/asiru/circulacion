package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

public class AddressUser implements Serializable {

	private static final long serialVersionUID = 3L;
	private Address direccionReniec;
	private Address direccionResidencia;

	public AddressUser() {
		direccionReniec = new Address();
		direccionResidencia = new Address();
	}
	
	public void init(){
		direccionReniec.init();
		direccionResidencia.init();
	}
	
	public Address getDireccionResidencia() {
		return direccionResidencia;
	}

	public void setDireccionResidencia(Address direccionResidencia) {
		this.direccionResidencia = direccionResidencia;
	}

	public Address getDireccionReniec() {
		return direccionReniec;
	}

	public void setDireccionReniec(Address direccionReniec) {
		this.direccionReniec = direccionReniec;
	}

}

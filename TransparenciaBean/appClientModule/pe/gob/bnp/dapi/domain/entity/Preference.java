package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class Preference implements Serializable {

	private static final long serialVersionUID = 7L;
	private long preferenciaId;
	private String preferenciaNombre;
	
	public Preference() {
		this.preferenciaId = 0;
		this.preferenciaNombre = Constants.EMPTY_STRING;
	}
	
	public Preference(long preferenciaId, String preferenciaNombre) {
		this.preferenciaId = preferenciaId;
		this.preferenciaNombre = preferenciaNombre;
	}

	public void init() {
		this.preferenciaId = 0;
		this.preferenciaNombre = Constants.EMPTY_STRING;
	}
	
	
	public long getPreferenciaId() {
		return preferenciaId;
	}
	public void setPreferenciaId(long preferenciaId) {
		this.preferenciaId = preferenciaId;
	}
	public String getPreferenciaNombre() {
		return preferenciaNombre;
	}
	public void setPreferenciaNombre(String preferenciaNombre) {
		this.preferenciaNombre = preferenciaNombre;
	}
}

package pe.gob.bnp.dapi.domain.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.entity.base.EntityGeneral;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.converter.Convert;
import pe.gob.servir.systems.util.validator.VO;

public class UserLibrary extends EntityGeneral implements Serializable {
	private static final long serialVersionUID = 2L;
	private Long usuarioBibliotecaId;
	private String tipoDocumentoIdentidadId;
	private String tipoDocumentoIdentidadNombre;
	private String numeroDocumentoIdentidad;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private Date fechaNacimiento;
	private String sexo;
	private String correoElectronico;
	private String telefonoFijo;
	private String telefonoMovil;
	private AddressUser direccion;
	private WorkCenter centroLaboral;
	private String flagLaboroActualmente;
	private StudyCenter centroEstudio;
	private String flagEstudioActualmente;
	private String[] preferenciasUsuario;
	private String cadenaPreferencias;
	
	private String temaInvestigacion;
	private String tipoInstitucionAvala;
	private String flagAutorizoAvisos;
	private String flagAceptoCompromiso;
	
	private String nombreCompleto;
	
	private String tipoUsuarioBiblioteca;
	
	private FileAlfresco fotoReniec;
	private FileAlfresco fotoAdjuntada;
	private FileAlfresco cartaPresentacion;
	
	private String flagMayorDeEdad;
	
	private String preferenciaEspecifica;
	private String codPaisPasaporte;
	private String idGradoInstruccion;
	private String nombreGradoInstruccion;
	private String idNacionalidad;
	private String nombreNacionalidad;
	
	public UserLibrary() {
		this.tipoUsuarioBiblioteca = Constants.EMPTY_STRING;
		this.tipoInstitucionAvala = Constants.EMPTY_STRING;
		this.temaInvestigacion = Constants.EMPTY_STRING;
		this.usuarioBibliotecaId = Constants.EMPTY_LONG;
		this.tipoDocumentoIdentidadId = Constants.EMPTY_STRING;
		this.tipoDocumentoIdentidadNombre = Constants.EMPTY_STRING;
		this.numeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.apellidoPaterno = Constants.EMPTY_STRING;
		this.apellidoMaterno = Constants.EMPTY_STRING;
		this.fechaNacimiento = null;
		this.sexo = Constants.EMPTY_STRING;
		this.correoElectronico = Constants.EMPTY_STRING;
		this.telefonoFijo = Constants.EMPTY_STRING;
		this.telefonoMovil = Constants.EMPTY_STRING;
		this.direccion = new AddressUser();
		this.centroLaboral = new WorkCenter();
		this.flagLaboroActualmente = Constants.EMPTY_STRING;
		this.centroEstudio = new StudyCenter();
		this.flagEstudioActualmente = Constants.EMPTY_STRING;
		this.fotoReniec = new FileAlfresco();
		this.fotoAdjuntada = new FileAlfresco();
		this.temaInvestigacion = Constants.EMPTY_STRING;
		this.tipoInstitucionAvala = Constants.EMPTY_STRING;
		this.flagAutorizoAvisos = Constants.EMPTY_STRING;
		this.flagAceptoCompromiso = Constants.EMPTY_STRING;
		this.cartaPresentacion = new FileAlfresco();
		this.flagMayorDeEdad =  Constants.EMPTY_STRING;
		this.cadenaPreferencias =  Constants.EMPTY_STRING;
		this.preferenciaEspecifica = Constants.EMPTY_STRING;
		this.codPaisPasaporte = Constants.EMPTY_STRING;
		this.idGradoInstruccion = Constants.EMPTY_STRING;
		this.nombreGradoInstruccion = Constants.EMPTY_STRING;
		this.idNacionalidad = Constants.EMPTY_STRING;
		this.nombreNacionalidad = Constants.EMPTY_STRING;
	}
	
	public void init() {
		this.tipoUsuarioBiblioteca = Constants.EMPTY_STRING;
		this.tipoInstitucionAvala = Constants.EMPTY_STRING;
		this.temaInvestigacion = Constants.EMPTY_STRING;
		this.usuarioBibliotecaId = Constants.EMPTY_LONG;
		this.tipoDocumentoIdentidadId = Constants.EMPTY_STRING;
		this.tipoDocumentoIdentidadNombre = Constants.EMPTY_STRING;
		this.numeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.apellidoPaterno = Constants.EMPTY_STRING;
		this.apellidoMaterno = Constants.EMPTY_STRING;
		this.fechaNacimiento = null;
		this.sexo = Constants.EMPTY_STRING;
		this.correoElectronico = Constants.EMPTY_STRING;
		this.telefonoFijo = Constants.EMPTY_STRING;
		this.telefonoMovil = Constants.EMPTY_STRING;
		this.flagLaboroActualmente = Constants.EMPTY_STRING;
		this.flagEstudioActualmente = Constants.EMPTY_STRING;
		this.temaInvestigacion = Constants.EMPTY_STRING;
		this.tipoInstitucionAvala = Constants.EMPTY_STRING;
		this.flagAutorizoAvisos = Constants.EMPTY_STRING;
		this.flagAceptoCompromiso = Constants.EMPTY_STRING;

		this.direccion.init();
		this.centroLaboral.init();
		this.centroEstudio.init();
		this.fotoReniec.init();
		this.fotoAdjuntada.init();
		this.cartaPresentacion.init();
		this.preferenciasUsuario = null;
		this.flagMayorDeEdad =  Constants.EMPTY_STRING;
		
		this.cadenaPreferencias =  Constants.EMPTY_STRING;
		this.preferenciaEspecifica = Constants.EMPTY_STRING;
		this.codPaisPasaporte = Constants.EMPTY_STRING;
		
		this.idGradoInstruccion = Constants.EMPTY_STRING;
		this.nombreGradoInstruccion = Constants.EMPTY_STRING;
		this.idNacionalidad = Constants.EMPTY_STRING;
		this.nombreNacionalidad = Constants.EMPTY_STRING;
	}

	public String getNombreInstitucionAvala(){
		String resultado=Constants.EMPTY_STRING;
		if (this.tipoInstitucionAvala.equals(Constants.TIPO_INSTITUCION_AVAL_CENTRO_ESTUDIOS)){
			resultado = this.centroEstudio.getCentroEstudioNombre();
		}
		if (this.tipoInstitucionAvala.equals(Constants.TIPO_INSTITUCION_AVAL_CENTRO_LABORAL)){
			resultado = this.centroLaboral.getCentroLaboralNombre();
		}
		return resultado;
	}
	
	public String obtenerApellidoCompleto(){
		String respuesta="";
		respuesta = this.apellidoPaterno;
		respuesta = respuesta +  ( (this.apellidoMaterno==null||this.apellidoMaterno.equals(""))?"":" "+this.apellidoMaterno);
		return respuesta;
	}	
	
	public WorkCenter getCentroLaboral() {
		return centroLaboral;
	}

	public void setCentroLaboral(WorkCenter centroLaboral) {
		this.centroLaboral = centroLaboral;
	}

	public StudyCenter getCentroEstudio() {
		return centroEstudio;
	}

	public void setCentroEstudio(StudyCenter centroEstudio) {
		this.centroEstudio = centroEstudio;
	}

	public Long getUsuarioBibliotecaId() {
		return usuarioBibliotecaId;
	}

	public void setUsuarioBibliotecaId(Long usuarioBibliotecaId) {
		this.usuarioBibliotecaId = usuarioBibliotecaId;
	}

	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}

	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}


	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public AddressUser getDireccion() {
		return direccion;
	}

	public void setDireccion(AddressUser direccion) {
		this.direccion = direccion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTipoDocumentoIdentidadId() {
		return tipoDocumentoIdentidadId;
	}

	public void setTipoDocumentoIdentidadId(String tipoDocumentoIdentidadId) {
		this.tipoDocumentoIdentidadId = tipoDocumentoIdentidadId;
	}

	public String getTipoDocumentoIdentidadNombre() {
		return tipoDocumentoIdentidadNombre;
	}

	public void setTipoDocumentoIdentidadNombre(String tipoDocumentoIdentidadNombre) {
		this.tipoDocumentoIdentidadNombre = tipoDocumentoIdentidadNombre;
	}

	public String getTemaInvestigacion() {
		return temaInvestigacion;
	}

	public void setTemaInvestigacion(String temaInvestigacion) {
		this.temaInvestigacion = temaInvestigacion;
	}

	public String getTipoInstitucionAvala() {
		return tipoInstitucionAvala;
	}

	public void setTipoInstitucionAvala(String tipoInstitucionAvala) {
		this.tipoInstitucionAvala = tipoInstitucionAvala;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getFlagLaboroActualmente() {
		return flagLaboroActualmente;
	}

	public void setFlagLaboroActualmente(String flagLaboroActualmente) {
		this.flagLaboroActualmente = flagLaboroActualmente;
	}

	public String getFlagEstudioActualmente() {
		return flagEstudioActualmente;
	}

	public void setFlagEstudioActualmente(String flagEstudioActualmente) {
		this.flagEstudioActualmente = flagEstudioActualmente;
	}

	public String getFlagAutorizoAvisos() {
		return flagAutorizoAvisos;
	}

	public void setFlagAutorizoAvisos(String flagAutorizoAvisos) {
		this.flagAutorizoAvisos = flagAutorizoAvisos;
	}

	public String getFlagAceptoCompromiso() {
		return flagAceptoCompromiso;
	}

	public void setFlagAceptoCompromiso(String flagAceptoCompromiso) {
		this.flagAceptoCompromiso = flagAceptoCompromiso;
	}
	
	public String getNombreCompleto(){
		return this.nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getTipoUsuarioBiblioteca() {
		return tipoUsuarioBiblioteca;
	}

	public void setTipoUsuarioBiblioteca(String tipoUsuarioBiblioteca) {
		this.tipoUsuarioBiblioteca = tipoUsuarioBiblioteca;
	}

	public FileAlfresco getCartaPresentacion() {
		return cartaPresentacion;
	}

	public void setCartaPresentacion(FileAlfresco cartaPresentacion) {
		this.cartaPresentacion = cartaPresentacion;
	}

	
	public FileAlfresco getFotoAdjuntada() {
		return fotoAdjuntada;
	}

	public void setFotoAdjuntada(FileAlfresco fotoAdjuntada) {
		this.fotoAdjuntada = fotoAdjuntada;
	}

	public void setNombreCompleto() {
		String nombreMinuscula = VO.getString(this.getNombre()).toLowerCase();
		String apellidoPaternoMinuscula =VO.getString(this.getApellidoPaterno()).toLowerCase(); 
		String apellidoMaternoMinuscula = VO.getString(this.getApellidoMaterno()).toLowerCase();
		
		this.nombreCompleto = Convert.toTitleCase(nombreMinuscula + " " + apellidoPaternoMinuscula + " " + apellidoMaternoMinuscula );		
	}
	
	public void setearNombreTipoDocumentoIdentidad(){
		if (this.tipoDocumentoIdentidadId.equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE)){
			this.tipoDocumentoIdentidadNombre = Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_SHORT_NAME;
		}
		if (this.tipoDocumentoIdentidadId.equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE)){
			this.tipoDocumentoIdentidadNombre = Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_SHORT_NAME;
		}
		if (this.tipoDocumentoIdentidadId.equals(Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_CODE)){
			this.tipoDocumentoIdentidadNombre = Constants.TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_SHORT_NAME;
		}
	}

	public FileAlfresco getFotoReniec() {
		return fotoReniec;
	}

	public void setFotoReniec(FileAlfresco fotoReniec) {
		this.fotoReniec = fotoReniec;
	}

	public String getFlagMayorDeEdad() {
		return flagMayorDeEdad;
	}

	public void setFlagMayorDeEdad(String flagMayorDeEdad) {
		this.flagMayorDeEdad = flagMayorDeEdad;
	}

	public String[] getPreferenciasUsuario() {
		return preferenciasUsuario;
	}

	public void setPreferenciasUsuario(String[] preferenciasUsuario) {
		this.preferenciasUsuario = preferenciasUsuario;
	}

	public String getCadenaPreferencias() {
		return cadenaPreferencias;
	}

	public void setCadenaPreferencias(String cadenaPreferencias) {
		this.cadenaPreferencias = cadenaPreferencias;
	}

	public String getPreferenciaEspecifica() {
		return preferenciaEspecifica;
	}

	public void setPreferenciaEspecifica(String preferenciaEspecifica) {
		this.preferenciaEspecifica = preferenciaEspecifica;
	}

	public String getCodPaisPasaporte() {
		return codPaisPasaporte;
	}

	public void setCodPaisPasaporte(String codPaisPasaporte) {
		this.codPaisPasaporte = codPaisPasaporte;
	}
	
	public String obtenerNombrePropio(){
		if (VO.getEmpty(this.nombre).equals(Constants.EMPTY_STRING)){ return Constants.EMPTY_STRING;}
		String nombre = Convert.toTitleCase(VO.getString(this.nombre.toLowerCase()));
		return nombre;
	}
	
	public String obtenerApellidoPaternoPropio(){
		if (VO.getEmpty(this.apellidoPaterno).equals(Constants.EMPTY_STRING)){ return Constants.EMPTY_STRING;}
		String apellidoPaterno =Convert.toTitleCase(VO.getString(this.apellidoPaterno).toLowerCase());
		return apellidoPaterno;
	}	

	public String obtenerApellidoMaternoPropio(){
		if (VO.getEmpty(this.apellidoMaterno).equals(Constants.EMPTY_STRING)){ return Constants.EMPTY_STRING;}
		String apellidoMaterno =Convert.toTitleCase(VO.getString(this.apellidoMaterno).toLowerCase());
		return apellidoMaterno;
	}
	
	public String obtenerNombreCompletoPropio(){
		return this.obtenerNombrePropio()+ " "+this.obtenerApellidoPaternoPropio()+ " "+ this.obtenerApellidoMaternoPropio();
	}

	public String getIdGradoInstruccion() {
		return idGradoInstruccion;
	}

	public void setIdGradoInstruccion(String idGradoInstruccion) {
		this.idGradoInstruccion = idGradoInstruccion;
	}

	public String getNombreGradoInstruccion() {
		return nombreGradoInstruccion;
	}

	public void setNombreGradoInstruccion(String nombreGradoInstruccion) {
		this.nombreGradoInstruccion = nombreGradoInstruccion;
	}

	public String getIdNacionalidad() {
		return idNacionalidad;
	}

	public void setIdNacionalidad(String idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}

	public String getNombreNacionalidad() {
		return nombreNacionalidad;
	}

	public void setNombreNacionalidad(String nombreNacionalidad) {
		this.nombreNacionalidad = nombreNacionalidad;
	}
	
	
}

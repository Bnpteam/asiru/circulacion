package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class UsuarioEnSala implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long idRegistroAsistencia;
	private Long idRegistroUsuario;
	private String nombreUsuario;
	private String tipoDocIdentidad;
	private String numeroDocIdentidad;
	private String tipoUsuario;
	private String fechaIngreso;
	private String horaIngreso;
	private Date fechaHoraIngreso;
	private String estadoAsistencia;
	private String userNameReferencistaIngreso;
	
	private Long idSesionReferencistaRegistraSalida;
	private Long idSalidaMotivo;
	private String descripcionSalidaMotivo;
	private String salidaJustificacion;
	private Long	codBarrasLector;
	
	private String tipoRegistro;
	private String origenRegistro;
	private String temaInvestigacion;
	private String gradoInstruccion;
	private String sexo;
	private Date fechaNacimiento;
	private String nacionalidad;
	private String pais;
	private String estadoSIRU;
	private String userNameReferencistaSalida;
	private String nombreReferencistaIngreso;
	private String nombreReferencistaSalida;
	private String nombreSala;
	private String pisoSala;
	private String turnoIngreso;
	private String periodoAcceso;
	private String mesAcceso;
	private String diaAcceso;
	private String turnoSalida;
	private String fechaSalida;
	private String horaSalida;
	private Date fechaHoraSalida;
	private Long fila;
	
	public UsuarioEnSala() {
		this.idRegistroAsistencia 	= Constants.EMPTY_LONG;
		this.idRegistroUsuario 		= Constants.EMPTY_LONG;
		this.nombreUsuario 			= Constants.EMPTY_STRING;
		this.tipoDocIdentidad 		= Constants.EMPTY_STRING;
		this.numeroDocIdentidad 	= Constants.EMPTY_STRING;
		this.tipoUsuario 			= Constants.EMPTY_STRING;
		this.fechaIngreso 			= Constants.EMPTY_STRING;
		this.horaIngreso 			= Constants.EMPTY_STRING;
		this.fechaHoraIngreso 		= null;
		this.estadoAsistencia 		= Constants.EMPTY_STRING;
		this.userNameReferencistaIngreso 			= Constants.EMPTY_STRING;
		this.idSesionReferencistaRegistraSalida 	= Constants.EMPTY_LONG;
		this.idSalidaMotivo							= Constants.EMPTY_LONG;
		this.descripcionSalidaMotivo				= Constants.EMPTY_STRING;
		this.salidaJustificacion					= Constants.EMPTY_STRING;
		this.codBarrasLector						= Constants.EMPTY_LONG;
		
		this.tipoRegistro							= Constants.EMPTY_STRING;
		this.origenRegistro							= Constants.EMPTY_STRING;
		this.temaInvestigacion						= Constants.EMPTY_STRING;
		this.gradoInstruccion						= Constants.EMPTY_STRING;
		this.sexo									= Constants.EMPTY_STRING;
		this.fechaNacimiento						= null;
		this.nacionalidad							= Constants.EMPTY_STRING;
		this.pais									= Constants.EMPTY_STRING;
		this.estadoSIRU								= Constants.EMPTY_STRING;
		this.userNameReferencistaSalida 			= Constants.EMPTY_STRING;
		this.nombreReferencistaIngreso				= Constants.EMPTY_STRING;
		this.nombreReferencistaSalida				= Constants.EMPTY_STRING;
		this.nombreSala								= Constants.EMPTY_STRING;
		this.pisoSala								= Constants.EMPTY_STRING;
		this.turnoIngreso							= Constants.EMPTY_STRING;
		this.periodoAcceso							= Constants.EMPTY_STRING;
		this.mesAcceso								= Constants.EMPTY_STRING;
		this.diaAcceso								= Constants.EMPTY_STRING;
		this.turnoSalida							= Constants.EMPTY_STRING;
		this.fechaSalida							= Constants.EMPTY_STRING;
		this.horaSalida								= Constants.EMPTY_STRING;
		this.fechaHoraSalida						= null;
		this.fila 									= Constants.EMPTY_LONG;
	}

	public Long getIdRegistroAsistencia() {
		return idRegistroAsistencia;
	}

	public void setIdRegistroAsistencia(Long idRegistroAsistencia) {
		this.idRegistroAsistencia = idRegistroAsistencia;
	}

	public Long getIdRegistroUsuario() {
		return idRegistroUsuario;
	}

	public void setIdRegistroUsuario(Long idRegistroUsuario) {
		this.idRegistroUsuario = idRegistroUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getTipoDocIdentidad() {
		return tipoDocIdentidad;
	}

	public void setTipoDocIdentidad(String tipoDocIdentidad) {
		this.tipoDocIdentidad = tipoDocIdentidad;
	}

	public String getNumeroDocIdentidad() {
		return numeroDocIdentidad;
	}

	public void setNumeroDocIdentidad(String numeroDocIdentidad) {
		this.numeroDocIdentidad = numeroDocIdentidad;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getHoraIngreso() {
		return horaIngreso;
	}

	public void setHoraIngreso(String horaIngreso) {
		this.horaIngreso = horaIngreso;
	}

	public Date getFechaHoraIngreso() {
		return fechaHoraIngreso;
	}

	public void setFechaHoraIngreso(Date fechaHoraIngreso) {
		this.fechaHoraIngreso = fechaHoraIngreso;
	}

	public String getEstadoAsistencia() {
		return estadoAsistencia;
	}

	public void setEstadoAsistencia(String estadoAsistencia) {
		this.estadoAsistencia = estadoAsistencia;
	}

	public String getUserNameReferencistaIngreso() {
		return userNameReferencistaIngreso;
	}

	public void setUserNameReferencistaIngreso(String userNameReferencistaIngreso) {
		this.userNameReferencistaIngreso = userNameReferencistaIngreso;
	}

	public Long getIdSesionReferencistaRegistraSalida() {
		return idSesionReferencistaRegistraSalida;
	}

	public void setIdSesionReferencistaRegistraSalida(
			Long idSesionReferencistaRegistraSalida) {
		this.idSesionReferencistaRegistraSalida = idSesionReferencistaRegistraSalida;
	}

	public Long getIdSalidaMotivo() {
		return idSalidaMotivo;
	}

	public void setIdSalidaMotivo(Long idSalidaMotivo) {
		this.idSalidaMotivo = idSalidaMotivo;
	}

	public String getDescripcionSalidaMotivo() {
		return descripcionSalidaMotivo;
	}

	public void setDescripcionSalidaMotivo(String descripcionSalidaMotivo) {
		this.descripcionSalidaMotivo = descripcionSalidaMotivo;
	}

	public String getSalidaJustificacion() {
		return salidaJustificacion;
	}

	public void setSalidaJustificacion(String salidaJustificacion) {
		this.salidaJustificacion = salidaJustificacion;
	}

	public Long getCodBarrasLector() {
		return codBarrasLector;
	}

	public void setCodBarrasLector(Long codBarrasLector) {
		this.codBarrasLector = codBarrasLector;
	}

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getOrigenRegistro() {
		return origenRegistro;
	}

	public void setOrigenRegistro(String origenRegistro) {
		this.origenRegistro = origenRegistro;
	}

	public String getTemaInvestigacion() {
		return temaInvestigacion;
	}

	public void setTemaInvestigacion(String temaInvestigacion) {
		this.temaInvestigacion = temaInvestigacion;
	}

	public String getGradoInstruccion() {
		return gradoInstruccion;
	}

	public void setGradoInstruccion(String gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstadoSIRU() {
		return estadoSIRU;
	}

	public void setEstadoSIRU(String estadoSIRU) {
		this.estadoSIRU = estadoSIRU;
	}

	public String getUserNameReferencistaSalida() {
		return userNameReferencistaSalida;
	}

	public void setUserNameReferencistaSalida(String userNameReferencistaSalida) {
		this.userNameReferencistaSalida = userNameReferencistaSalida;
	}

	public String getNombreReferencistaIngreso() {
		return nombreReferencistaIngreso;
	}

	public void setNombreReferencistaIngreso(String nombreReferencistaIngreso) {
		this.nombreReferencistaIngreso = nombreReferencistaIngreso;
	}

	public String getNombreReferencistaSalida() {
		return nombreReferencistaSalida;
	}

	public void setNombreReferencistaSalida(String nombreReferencistaSalida) {
		this.nombreReferencistaSalida = nombreReferencistaSalida;
	}

	public String getNombreSala() {
		return nombreSala;
	}

	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}

	public String getPisoSala() {
		return pisoSala;
	}

	public void setPisoSala(String pisoSala) {
		this.pisoSala = pisoSala;
	}

	public String getTurnoIngreso() {
		return turnoIngreso;
	}

	public void setTurnoIngreso(String turnoIngreso) {
		this.turnoIngreso = turnoIngreso;
	}

	public String getPeriodoAcceso() {
		return periodoAcceso;
	}

	public void setPeriodoAcceso(String periodoAcceso) {
		this.periodoAcceso = periodoAcceso;
	}

	public String getMesAcceso() {
		return mesAcceso;
	}

	public void setMesAcceso(String mesAcceso) {
		this.mesAcceso = mesAcceso;
	}

	public String getDiaAcceso() {
		return diaAcceso;
	}

	public void setDiaAcceso(String diaAcceso) {
		this.diaAcceso = diaAcceso;
	}

	public String getTurnoSalida() {
		return turnoSalida;
	}

	public void setTurnoSalida(String turnoSalida) {
		this.turnoSalida = turnoSalida;
	}

	public String getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(String fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public String getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(String horaSalida) {
		this.horaSalida = horaSalida;
	}

	public Date getFechaHoraSalida() {
		return fechaHoraSalida;
	}

	public void setFechaHoraSalida(Date fechaHoraSalida) {
		this.fechaHoraSalida = fechaHoraSalida;
	}

	public Long getFila() {
		return fila;
	}

	public void setFila(Long fila) {
		this.fila = fila;
	}

	
}

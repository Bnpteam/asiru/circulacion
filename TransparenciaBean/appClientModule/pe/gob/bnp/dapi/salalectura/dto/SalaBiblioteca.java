package pe.gob.bnp.dapi.salalectura.dto;

import java.io.Serializable;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class SalaBiblioteca implements Serializable {
	private static final long serialVersionUID = 1L;
	private long idSala;
	private String codigoSedeAbsysnet;
	private String nombreSede;
	private String nombreSala;
	private String ubicacionPiso;
	
	public SalaBiblioteca() {
		this.idSala 			= Constants.EMPTY_LONG;
		this.codigoSedeAbsysnet = Constants.EMPTY_STRING;
		this.nombreSala 		= Constants.EMPTY_STRING;
		this.ubicacionPiso 		= Constants.EMPTY_STRING;
		this.nombreSede 		= Constants.EMPTY_STRING;
	}

	public long getIdSala() {
		return idSala;
	}

	public void setIdSala(long idSala) {
		this.idSala = idSala;
	}

	public String getCodigoSedeAbsysnet() {
		return codigoSedeAbsysnet;
	}

	public void setCodigoSedeAbsysnet(String codigoSedeAbsysnet) {
		this.codigoSedeAbsysnet = codigoSedeAbsysnet;
	}

	public String getNombreSala() {
		return nombreSala;
	}

	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}

	public String getUbicacionPiso() {
		return ubicacionPiso;
	}

	public void setUbicacionPiso(String ubicacionPiso) {
		this.ubicacionPiso = ubicacionPiso;
	}

	public String getNombreSede() {
		return nombreSede;
	}

	public void setNombreSede(String nombreSede) {
		this.nombreSede = nombreSede;
	}
	
	

}

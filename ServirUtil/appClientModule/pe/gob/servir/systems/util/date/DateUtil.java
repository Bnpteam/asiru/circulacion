package pe.gob.servir.systems.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {


	public static boolean isValid(Date fechaInicio, Date fechaFin) {

        Calendar fecIni = Calendar.getInstance();
        fecIni.setTime(fechaInicio);

        Calendar fecFin = Calendar.getInstance();
        fecFin.setTime(fechaFin);

        if (fecFin.before(fecIni)) {
        	return false;
        }else{
        	return true;
        }
	}
	
	public static boolean isToday(Date fechaInicio, Date fechaFin) {

        Calendar fecIni = Calendar.getInstance();
        fecIni.setTime(fechaInicio);

        Calendar fecFin = Calendar.getInstance();
        fecFin.setTime(fechaFin);
        
        fecFin.add(Calendar.DAY_OF_YEAR,1);
        fecFin.getTime();

        if (fecFin.before(fecIni)) {
        	return false;
        }else{
        	return true;
        }
	}
	
	
	public static String convertDateAlfresco(Date fecha){
		String fechaFormato = null;
		if(fecha != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
			fechaFormato = sdf.format(fecha); 
		}
		return fechaFormato;
	}
	
	public static String getStringDate(Date fecha){

		String fechaFormato = null;
		if(fecha != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
			fechaFormato = sdf.format(fecha); 
		}else{
			fechaFormato="";
		}
		return fechaFormato;
	}
	
	public static String getStringDateTime(Date fecha){

		String fechaFormato = null;
		if(fecha != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			fechaFormato = sdf.format(fecha); 
		}else{
			fechaFormato="";
		}
		return fechaFormato;
	}
	
	public static String datePathRepositoryAlfresco(String pathAlfresco){
		StringBuilder pathAlfrescoFull = new StringBuilder();
		
		Date fecha = new Date();
        SimpleDateFormat formatDia = new SimpleDateFormat("dd");
        SimpleDateFormat formatMes = new SimpleDateFormat("MM");
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
        
        String dia = formatDia.format(fecha);
        String mes = formatMes.format(fecha).toUpperCase();
        String anio = formatAnio.format(fecha);

        pathAlfrescoFull.append(pathAlfresco);
        //pathAlfrescoFull.append("/");
        pathAlfrescoFull.append(anio);
        pathAlfrescoFull.append("/");
        pathAlfrescoFull.append(mes);
        pathAlfrescoFull.append("/");
        pathAlfrescoFull.append(dia);
		
		return pathAlfrescoFull.toString();
	}
	
	public static String convertNameFile(String fileNameComplete){
		StringBuilder fileNameFull = new StringBuilder();
		
		int lengthComplete = fileNameComplete.length();
		int lengthFileName = lengthComplete - 4;
		String fileName = fileNameComplete.substring(0, lengthFileName);
		
		Date hora = new Date();
        
        SimpleDateFormat formatHora = new SimpleDateFormat("HH");
        SimpleDateFormat formatMin = new SimpleDateFormat("mm");
        SimpleDateFormat formatSeg = new SimpleDateFormat("ss");
        
        String hh = formatHora.format(hora);
        String mm = formatMin.format(hora);
        String ss = formatSeg.format(hora);
        
        fileNameFull.append(fileName);
        fileNameFull.append("_");
        fileNameFull.append(hh);
        fileNameFull.append("-");
        fileNameFull.append(mm);
        fileNameFull.append("-");
        fileNameFull.append(ss);
        fileNameFull.append(".");
        fileNameFull.append(fileNameComplete.substring(fileNameComplete.lastIndexOf(".") + 1));
		
		return fileNameFull.toString();
	}
	
	public static String toNameFileWithDateTime(String fileNameComplete){
		StringBuilder fileNameFull = new StringBuilder();
		
		int lengthComplete = fileNameComplete.length();
		int lengthFileName = lengthComplete - 4;
		String fileName = fileNameComplete.substring(0, lengthFileName);
		
		Date fechaHora = new Date();
        
        SimpleDateFormat formatDia = new SimpleDateFormat("dd");
        SimpleDateFormat formatMes = new SimpleDateFormat("MM");
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
		
        SimpleDateFormat formatHora = new SimpleDateFormat("HH");
        SimpleDateFormat formatMin = new SimpleDateFormat("mm");
        SimpleDateFormat formatSeg = new SimpleDateFormat("ss");
        
        String dia = formatDia.format(fechaHora);
        String mes = formatMes.format(fechaHora).toUpperCase();
        String anio = formatAnio.format(fechaHora);
        
        String hh = formatHora.format(fechaHora);
        String mm = formatMin.format(fechaHora);
        String ss = formatSeg.format(fechaHora);
        
        fileNameFull.append(fileName);
        
        fileNameFull.append("_");
        fileNameFull.append(dia);
        fileNameFull.append("-");
        fileNameFull.append(mes);
        fileNameFull.append("-");
        fileNameFull.append(anio);
        fileNameFull.append("_T");
        fileNameFull.append(hh);
        fileNameFull.append("-");
        fileNameFull.append(mm);
        fileNameFull.append("-");
        fileNameFull.append(ss);
        fileNameFull.append(".");
        fileNameFull.append(fileNameComplete.substring(fileNameComplete.lastIndexOf(".") + 1));
        
        return fileNameFull.toString();
	}
	
	public static int getDias(java.util.Date fInicial, java.util.Date fFinal){
		
        Calendar ci = Calendar.getInstance();
        ci.setTime(fInicial);

        Calendar cf = Calendar.getInstance();
        cf.setTime(fFinal);

        long ntime = cf.getTimeInMillis() - ci.getTimeInMillis();

        return (int)Math.ceil((double)ntime / 1000 / 3600 / 24);
    }

	public static String getAnioActual(){
		Date fecha = new Date();
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
        String anio = formatAnio.format(fecha);
        
        return anio;
    }
	
	public static Date getFechaRegistro(String mes, String anio){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); 
		String strDate = "01" + "-" + mes + "-" + anio;
		
		Date date = new Date();
		
		try {
			date = sdf.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}
	
	public static String getMesFechaExt(Date fecha){
        SimpleDateFormat formatMes = new SimpleDateFormat("MM");
        String mes = formatMes.format(fecha);
        
        return mes;
	}
	
	public static String getAnioFechaExt(Date fecha){
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy"); 
        String anio = formatAnio.format(fecha);
        
        return anio;
	}
	
	public static String getMesActual() {
		Date fecha = new Date();
        SimpleDateFormat formatMes = new SimpleDateFormat("MM");
        String mes = formatMes.format(fecha);
        
        return mes;
	}
	
	
	/**
     * M�todo que calcula los meses que han pasado dese fecha inicio hasta
     * fecha fin.
     * @param fechaInicio: fecha de inicio de comparaci�n.
     * @param fechaFin: fecha de finalizaci�n de comparaci�n.
     * @return 0 si no ha pasado un mes o si se presenta alguna exepci�n.
     * > 0 si han pasado almenos un mes.
     */
    public static int calcularMesesAFecha(Date fechaInicio, Date fechaFin) {
        try {
            //Fecha inicio en objeto Calendar
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(fechaInicio);
            //Fecha finalizaci�n en objeto Calendar
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(fechaFin);
            //C�lculo de meses para las fechas de inicio y finalizaci�n
            int startMes = (startCalendar.get(Calendar.YEAR) * 12) + startCalendar.get(Calendar.MONTH);
            int endMes = (endCalendar.get(Calendar.YEAR) * 12) + endCalendar.get(Calendar.MONTH);
            //Diferencia en meses entre las dos fechas
            int diffMonth = endMes - startMes;
            //Si la el d�a de la fecha de finalizaci�n es menor que el d�a de la fecha inicio
            //se resta un mes, puesto que no estaria cumpliendo otro periodo.
            //Para esto ocupo el metoddo ponerAnioMesActual
            Date aFecha = ponerAnioMesActual(fechaInicio,fechaFin).getTime();
            if(formatearDate(fechaFin, "dd/MM/yyyy").compareTo(
                    formatearDate(aFecha,"dd/MM/yyyy")) < 0){
                diffMonth = diffMonth - 1;
            }
            //Si la fecha de finalizaci�n es menor que la fecha de inicio, retorno que los meses
            // transcurridos entre las dos fechas es 0
            if(diffMonth < 0){
                diffMonth = 0;
            }
            return diffMonth;
        } catch (Exception e) {
            return 0;
        }
    }
    
    /**
     * M�todo que formatea un afecha en base al formato pasado como
     * par�metro.
     * @param fecha: fecha a formatear
     * @param pattern: formato que se dar� a la fecha.
     * @return Fecha formateada en base al formato de pattern.
     * null si se presenta alguna excepci�n
     */
    public static Date formatearDate(Date fecha, String pattern) {
        SimpleDateFormat formato = new SimpleDateFormat(pattern);
        Date fechaFormateada = null;
        try {
            fechaFormateada = formato.parse(formato.format(fecha));
            return fechaFormateada;
        } catch (Exception ex) {
            return fechaFormateada;
        }
    }    
 
    /**
     * M�todo que remplaza el a�o y el mes de fecha y pone
     * el a�o y mes de fechaActual
     * @param fecha: fecha a remplazar el mes y el a�o
     * @param fechaActual: fecha de la cual se tomara el mes y el a�o
     * y se colocara en fecha
     * @return Calendar con la nueva fecha calculada.
     */
    public static Calendar ponerAnioMesActual(Date fecha, Date fechaActual) {
        try {
            Calendar cActual = Calendar.getInstance();
            cActual.setTime(fechaActual);
            Calendar cfecha = Calendar.getInstance();
            cfecha.setTime(fecha);
            //la fecha nueva
            Calendar c1 = Calendar.getInstance();
            c1.set(cActual.get(Calendar.YEAR), cActual.get(Calendar.MONTH), cfecha.get(Calendar.DATE));
            return c1;
        } catch (Exception e) {
            return null;
        }
    }
    
    // Suma los d�as recibidos a la fecha  
    public static Date sumarRestarMesesFecha(Date fecha, int meses){
         Calendar calendar = Calendar.getInstance();
         calendar.setTime(fecha); // Configuramos la fecha que se recibe
         calendar.add(Calendar.MONTH, meses);  // numero de d�as a a�adir, o restar en caso de d�as<0   �
         return calendar.getTime(); // Devuelve el objeto Date con los nuevos d�as a�adidos   �
    }

	
}

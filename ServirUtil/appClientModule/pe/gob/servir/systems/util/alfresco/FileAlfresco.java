package pe.gob.servir.systems.util.alfresco;

import java.io.InputStream;
import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.util.Date;

import pe.gob.bnp.dapi.common.util.constants.Constants;

public class FileAlfresco implements Serializable{
	private static final long serialVersionUID = 2L;
	
	private String		fileName;
	private String		extension;
	private byte[]		rawFile;
	
	private String		model;
	private String		tipoDoc;
	
	private String		uiid;
	
	private String		numDocumento;
	private Date		fechaDocumento;
	private String		rutaServidorTemp;

	public FileAlfresco(){
		this.fileName = Constants.EMPTY_STRING;
		this.extension = Constants.EMPTY_STRING;
		this.rawFile = null;
		this.model = Constants.EMPTY_STRING;
		this.tipoDoc = Constants.EMPTY_STRING;
		this.uiid = Constants.EMPTY_STRING;
		this.numDocumento = Constants.EMPTY_STRING;
		this.fechaDocumento = null;
		this.rutaServidorTemp = Constants.EMPTY_STRING;		
	}
	
	public void init(){
		this.fileName = Constants.EMPTY_STRING;
		this.extension = Constants.EMPTY_STRING;
		this.rawFile = null;
		this.model = Constants.EMPTY_STRING;
		this.tipoDoc = Constants.EMPTY_STRING;
		this.uiid = Constants.EMPTY_STRING;
		this.numDocumento = Constants.EMPTY_STRING;
		this.fechaDocumento = null;
		this.rutaServidorTemp = Constants.EMPTY_STRING;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public byte[] getRawFile() {
		return rawFile;
	}

	public void setRawFile(byte[] rawFile) {
		this.rawFile = rawFile;
	}
	
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getUiid() {
		return uiid;
	}

	public void setUiid(String uiid) {
		this.uiid = uiid;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getRutaServidorTemp() {
		return rutaServidorTemp;
	}

	public void setRutaServidorTemp(String rutaServidorTemp) {
		this.rutaServidorTemp = rutaServidorTemp;
	}

	
	
}

package pe.gob.servir.systems.util.alfresco;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Alfresco {

	protected byte[] documento;

	@SuppressWarnings("resource")
	public static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

	public String getDocumentoBase64Encoder() {
		BASE64Encoder encoder = new BASE64Encoder();

		String encodedBytes = encoder.encodeBuffer(this.getDocumento());

		return encodedBytes;
	}

	public void setDocumentoBase64Decoder(String recurso64) {
		BASE64Decoder decoder = new BASE64Decoder();

		try {
			byte[] decodedBytes = decoder.decodeBuffer(recurso64);
			this.setDocumento(decodedBytes);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(byte[] documento) {
		this.documento = documento;
	}
	
}

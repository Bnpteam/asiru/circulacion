package pe.gob.bnp.dapi.common.util.constants;

public interface Constants {
	public static final String EMPTY_STRING = "";
	public static final Long EMPTY_LONG = 0L;
	public static final Double ZERO_DOUBLE = 0D;
	public static final Integer EMPTY_INTEGER = 0;
	public static final String ZERO_VALUE_STRING= "0";
	public static final String YES_VALUE= "SI";
	public static final String NO_VALUE= "NO";
	public static final Long CODE_NEW_ONLINE_USER = 999999L;
	
	public static final String SUCCESS_RESULT= "EXITO";
	public static final String ERROR_RESULT= "ERROR";
	public static final String ERROR_UMBRAL_CONSULTA_RENIEC= "UMBRAL-MAXIMO";

	public static final String USER_TYPE_GENERAL = "General";
	public static final String USER_TYPE_RESEARCHER = "Investigador";
	public static final String ABSYSNET_LECOLP_INVESTIGADOR="INV01";
	public static final String ABSYSNET_LECOLP_GENERAL="INV02";
	
	public static final String ABSYSNET_LECOL1_INVESTIGADOR="CA01";
	public static final String ABSYSNET_LECOL1_GENERAL="CA02";
	

	public static final int USER_TYPE_GENERAL_ABSYSNET = 1;
	public static final int USER_TYPE_RESEARCHER_ABSYSNET = 2;
	
	public static final String COUNTRY_CODE_PERU = "PER";
	public static final String COUNTRY_DESC_PERU = "PER�";
	public static final int COUNTRY_CODE_LENGTH = 3;

	
	public static final String REGISTER_USER_STATE_REGISTERED = "Registrado";
	public static final String REGISTER_USER_STATE_ENABLED = "Habilitado";
	public static final String REGISTER_USER_STATE_SUSPENDED = "Suspendido";
	public static final String REGISTER_USER_STATE_ANNULLED = "Anulado";
	public static final String ABSYSNET_LECONF_ACTIVE="1";
	public static final String ABSYSNET_LECONF_INACTIVE="0";
	
	
	public static final String PAGE_TEMP = "demo";
	public static final String PAGE_GENERAL_DEFAULT = "inicio";
	public static final String PAGE_GENERAL_USER_REGISTRATION = "registro_usuario_general";
	public static final String PAGE_GENERAL_USER_EDIT = "editar_usuario_general";
	public static final String PAGE_HOME_INTERN = "home";
	public static final String PAGE_GENERAL_USER_SUCCESSFULLY_SAVE = "confirmacion_registro_usuario_general";
	public static final String PAGE_RESEARCHER_USER_SUCCESSFULLY_SAVE = "confirmacion_registro_usuario_investigador";
	public static final String PAGE_INTERN_GENERAL_USER_CREATE = "nuevoUsuarioGeneralInt";
	public static final String PAGE_INTERN_RESEARCH_USER_CREATE = "nuevoUsuarioInvestigadorInt";

	
	public static final String PAGE_RESEARCHER_USER_REGISTRATION = "registro_usuario_investigador";
	public static final String PAGE_RESEARCHER_USER_EDIT = "editar_usuario_investigador";
	public static final String PAGE_BANDEJA_SALA_USUARIO = "bandejaSalaUsuario";
	
	public static final String PAGINA_INICIO_REGISTRO_VISITA = "iniciarRegistroVisita";
	
	//TABLA TBL_PARAMETROS
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD = "T01_TIPO_DOCUMENTO_IDENTIDAD";
	public static final String TBL_PARAMETRO_TIPO_PREFERENCIAS_USUARIO = "T01_PREFERENCIA_USUARIO_BIBLIOTECA";
	public static final String TBL_PARAMETRO_CODIGO_TABLA_GRADO_INSTRUCCION = "LECTOR_GRADO_INSTRUCCION";
	public static final String TBL_PARAMETRO_CODIGO_TABLA_NACIONALIDAD = "LECTOR_NACIONALIDAD";

	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_CODE = "1";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_CODE = "2";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_CODE = "3";
	public static final String ABSYSNET_LECTOR_LECOL3_PASAPORTE = "PAS";
	public static final String ABSYSNET_LECTOR_LECOL3_DNI = "DNI";
	public static final String ABSYSNET_LECTOR_LECOL3_CE = "CEX";
	
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_NAME = "DNI";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_NAME = "Carnet de Extranjer�a";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_NAME = "Pasaporte";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_DNI_SHORT_NAME = "DNI";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_CE_SHORT_NAME = "CE";
	public static final String TBL_PARAMETRO_TIPO_DOCUMENTO_IDENTIDAD_PASAPORTE_SHORT_NAME = "Pasaporte";
	

	public static final Integer TIPO_DOCUMENTO_IDENTIDAD_DNI_LONGITUD = 8;
	public static final Integer TIPO_DOCUMENTO_IDENTIDAD_CE_LONGITUD = 12;

	public static final String PROPERTIES_FILE = "config.properties";

	public static final String WS_BASE_URL_MIGRACIONES_PIDE = "migraciones.pide.ws.wsbaseURL";
	public static final String COD_INSTITUCION_MIGRACIONES_PIDE = "migraciones.pide.ws.codInstitucion";
	public static final String MAC_MIGRACIONES_PIDE = "migraciones.pide.ws.Mac";
	public static final String IP_MIGRACIONES_PIDE = "migraciones.pide.ws.Ip";
	public static final String SUCCESsFUL_OPERATION_MIGRACIONES_CODE = "0000";	
	
	public static final String WS_BASE_URL_RENIEC_PIDE = "reniec.pide.ws.wsbaseURL";
	public static final String DNI_BNP_RENIEC_PIDE = "reniec.pide.ws.dni.usuario";
	public static final String RUC_BNP_RENIEC_PIDE = "reniec.pide.ws.ruc.usuario";
	public static final String PASSWORD_BNP_RENIEC_PIDE = "reniec.pide.ws.password";
	public static final String SUCCESSFUL_OPERATION_RENIEC_CODE = "0000";
	public static final String SUCCESSFUL_OPERATION_MIGRACIONES_CODE = "0000";
	public static final String MENOR_EDAD_OPERATION_RENIEC_CODE = "0001";
	
	public static final String WS_BASE_URL_ABSYSNET = "absysnet.ws.wsbaseURL";
	
	public static final String FILE_CONTENT_TYPE_IMAGE_PREFIX ="image/";
	public static final String FILE_CONTENT_TYPE_IMAGE_DEFAULT ="jpg";
	public static final String FILE_CONTENT_TYPE_IMAGE_EXTENSION_DEFAULT ="."+FILE_CONTENT_TYPE_IMAGE_DEFAULT;
	
	public static final String USER_VIGENCIA_1A�O = "1 a�o";	
	public static final String USER_VIGENCIA_2MESES = "2 meses";
	public static final String USER_VIGENCIA_4MESES = "4 meses";
	public static final String USER_VIGENCIA_6MESES = "6 meses";
	public static final String USER_VIGENCIA_8MESES = "8 meses";
	public static final String USER_VIGENCIA_10MESES = "10 meses";
	
	public static final String USER_CONCEPTO_ALTA = "Alta";
	public static final String USER_CONCEPTO_RENOVACION = "Renovaci�n";
	public static final String USER_CONCEPTO_DUPLICADO = "Duplicado";
	

	public static final String SECCION_VERIFICACION_IDENTIDAD = "VERIFICACI�N DE IDENTIDAD";
	public static final String SECCION_DATOS_PERSONALES = "DATOS PERSONALES";
	public static final String SECCION_DATOS_DIRECCION= "DOMICILIO ACTUAL";
	public static final String SECCION_DATOS_CONTACTO= "CONTACTO";
	public static final String SECCION_HISTORIAL_REGISTRO= "HISTORIAL DE REGISTRO";
	public static final String SECCION_VERIFICAR_IDENTIDAD= "VERIFICACI�N DE IDENTIDAD";
	public static final String SECCION_DATOS_REGISTRO= "DATOS DE REGISTRO";
	public static final String SECCION_DATOS_ESTUDIO= "CENTRO DE ESTUDIO";
	public static final String SECCION_DATOS_LABORAL= "CENTRO LABORAL";
	public static final String SECCION_DATOS_INVESTIGACION= "DATOS DE INVESTIGACI�N";
	
	public static final String TIPO_INSTITUCION_AVAL_CENTRO_ESTUDIOS = "Centro de Estudios";
	public static final String TIPO_INSTITUCION_AVAL_CENTRO_LABORAL = "Centro Laboral";
	
	public static final String NOMBRE_SISTEMA_PROPERTIES = "sistema.nombre";
	public static final String VERSION_SISTEMA_PROPERTIES = "sistema.version";
	
	public static final int CARNET_BIBLIOTECA_FOTO_ESCALA_DEFAULT = 357;
	public static final int CARNET_BIBLIOTECA_FOTO_ESCALA_UNIDAD_AUMENTO = 5;
	public static final int CARNET_BIBLIOTECA_FOTO_ESCALA_UNIDAD_DISMINUCION = 5;
	public static final int CARNET_BIBLIOTECA_FOTO_POSX_DEFAULT = 660;
	public static final int CARNET_BIBLIOTECA_FOTO_POSX_UNIDAD_AUMENTO = 2;
	public static final int CARNET_BIBLIOTECA_FOTO_POSX_UNIDAD_DISMINUCION= 2;
	public static final int CARNET_BIBLIOTECA_FOTO_POSY_DEFAULT = 50;
	public static final int CARNET_BIBLIOTECA_FOTO_POSY_UNIDAD_AUMENTO = 2;
	public static final int CARNET_BIBLIOTECA_FOTO_POSY_UNIDAD_DISMINUCION = 2;
	public static final String CARNET_BIBLIOTECA_FOTO_FRENTE_FONDO_ARCHIVO = "carnetbiblioteca.foto.anverso.default.archivo";
	public static final String CARNET_BIBLIOTECA_FOTO_REVERSO_FONDO_ARCHIVO = "carnetbiblioteca.foto.reverso.default.archivo";
	
	public static final String VISITA_SALA_REGISTRO_EXITOSO = "Ingreso registrado de forma exitosa";
	
	public static final String PROPERTY_QRCODE_REPOSITORY_PATH = "carnetbiblioteca.qrcode.repositorio";
	
	public static final String PERSONA_GENERO_MASCULINO_CODIGO = "M";
	public static final String PERSONA_GENERO_FEMENINO_CODIGO = "F";
	
	public static final int QR_IMAGE_WIDTH= 250;
	public static final int QR_IMAGE_HEIGHT = 250;
	
	public static final String TBL_REGISTRO_USUARIO_ORIGEN_REGISTRO_ONLINE="Online";
	public static final String TBL_REGISTRO_USUARIO_ORIGEN_REGISTRO_PRESENCIAL="Presencial";

	public static final String ABSYSNET_LECTOR_LECOBI_BNP = "BNP";
	public static final String ABSYSNET_BIBLIO_BIDESC_BNP = "Biblioteca Nacional del Per�";
	public static final String ABSYSNET_LECTOR_LECOSU_BNP = "BNP";
	public static final String ABSYSNET_SUCURS_SUDESC_BNP = "Biblioteca Nacional del Per�";

}

package pe.gob.servir.sistemas.transparencia.util;

import java.io.InputStream;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import net.sf.jxls.transformer.XLSTransformer;

public class ReporteUtil {
	
	public static void reporteXLS(String pathPlatillaXLS, Map beans, String nombreSalida) throws Exception{

		   InputStream is = FacesContext.getCurrentInstance().getExternalContext()
					.getResourceAsStream(pathPlatillaXLS);

			XLSTransformer transformer2 = new XLSTransformer();
			HSSFWorkbook wb = (HSSFWorkbook) transformer2.transformXLS(is, beans);

			HttpServletResponse response = (HttpServletResponse) FacesContext
					.getCurrentInstance().getExternalContext().getResponse();

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+nombreSalida);

				ServletOutputStream out = response.getOutputStream();

				wb.write(out);
				out.flush();
				out.close();

				FacesContext.getCurrentInstance().responseComplete();

	   }
	
	public static void reporteXLSGroup(String pathPlatillaXLS, Map beans, String nombreSalida, String nombreColeccion) throws Exception{

		   InputStream is = FacesContext.getCurrentInstance().getExternalContext()
					.getResourceAsStream(pathPlatillaXLS);

			XLSTransformer transformer2 = new XLSTransformer();
			transformer2.groupCollection(nombreColeccion);
			HSSFWorkbook wb = (HSSFWorkbook) transformer2.transformXLS(is, beans);

			HttpServletResponse response = (HttpServletResponse) FacesContext
					.getCurrentInstance().getExternalContext().getResponse();

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition",
					"attachment; filename="+nombreSalida);

				ServletOutputStream out = response.getOutputStream();

				wb.write(out);
				out.flush();
				out.close();

				FacesContext.getCurrentInstance().responseComplete();

	   }
}

package pe.gob.servir.sistemas.transparencia.servicios.alfresco;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import ws.alfresco.servir.gob.pe.AlfrescoService;
import ws.alfresco.servir.gob.pe.AlfrescoService_Service;
import ws.alfresco.servir.gob.pe.ArchivoType;
import ws.alfresco.servir.gob.pe.AuditoriaType;
import ws.alfresco.servir.gob.pe.AutenticacionType;
import ws.alfresco.servir.gob.pe.ResponseAutenticacionType;
import ws.alfresco.servir.gob.pe.ResponseConsultarContenidoType;
import ws.alfresco.servir.gob.pe.ResponseDescargarArchivoType;
import ws.alfresco.servir.gob.pe.ResponseModificarContenidoType;
import ws.alfresco.servir.gob.pe.ResponseRegistrarContenidoType;
import ws.alfresco.servir.gob.pe.SeguridadType;
import ws.alfresco.servir.gob.pe.TokenType;

public class AlfrescoBase {

	private static final Logger logger = Logger.getLogger(AlfrescoBase.class);
	
	private String 							sWsdl;
	private String 							endPoint;
	private String 							wsURI;
	
    private SeguridadType 					seguridad;
    private AuditoriaType 					auditoria;
    private AutenticacionType 				autenticacion;
    private	TokenType 						token;
    private	ResponseAutenticacionType 		responseAutenticacion;
	
    private	URL 							url;
    private	QName 							qws;

    private	AlfrescoService_Service 		service;
    private AlfrescoService 				port;
    
    private ResponseRegistrarContenidoType	responseRegistrarContenidoType;
    private ResponseModificarContenidoType	responseModificarContenidoType;
    
    private ResponseDescargarArchivoType	responseDescargarArchivoType;
	
    public AlfrescoBase(){
    	this.init();
    }
    
    private void init(){
    	//this.setsWsdl(this.getString("alfresco.wsdl"));
    	//this.setEndPoint(this.getString("alfresco.endpoint"));
    	//this.setWsURI(this.getString("alfresco.ws.uri"));
    	
    	this.setSeguridad(new SeguridadType());
    	this.setAuditoria(new AuditoriaType());
    	this.setAutenticacion(new AutenticacionType());
    	this.setToken(new TokenType());
    	this.setResponseAutenticacion(new ResponseAutenticacionType());
    	
    	this.setResponseRegistrarContenidoType(new ResponseRegistrarContenidoType());

    	this.inicializarValores();
    }
    
    private void inicializarValores(){
    	this.setsWsdl(this.getString("alfresco.wsdl"));
    	this.setEndPoint(this.getString("alfresco.endpoint"));
    	this.setWsURI(this.getString("alfresco.ws.uri"));
    	
    	this.setQws(new QName(wsURI, this.getString("alfresco.service")));
    	
    	try {
			this.setUrl(new URL(this.getsWsdl()));
		} catch (MalformedURLException e) {
			logger.error(e);
			e.printStackTrace();
		}
    	
    	this.setService(new AlfrescoService_Service(this.getUrl(), this.getQws()));
    	this.setPort(this.getService().getAlfrescoServiceImplPort());
    	
    	((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
    	
    	this.getSeguridad().setCodAplicativo(this.getString("alfresco.codigo"));
    	this.getAuditoria().setIpPc("127.0.0.1");
    	this.getAuditoria().setMacAddressPc("00:00:00:00:00");
    	
    	this.getAutenticacion().setUsuario(this.getString("alfresco.usuario"));
    	this.getAutenticacion().setPassword(this.getString("alfresco.password"));
    	
    	this.setResponseAutenticacion(this.getPort().autenticacion(this.getSeguridad(), this.getAuditoria(), this.getAutenticacion()));
    }
    
	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		return "";
	}
/*	
	public byte[] buscarArchivoBinario(String uiid){
		ArchivoType archivoType = new ArchivoType();
		archivoType.setFormato("pdf");
		archivoType.setUiidRecurso(uiid);
		
		this.setResponseAutenticacion(this.getPort().autenticacion(this.getSeguridad(), this.getAuditoria(), this.getAutenticacion()));
		this.setToken(this.getResponseAutenticacion().getToken());
		
		return this.getPort().consultarContenido(this.getToken(), this.getSeguridad(), this.getAuditoria(), archivoType).getArchivo().getDocumento();
	}
*/	
	public FileAlfresco obtenerArchivoAlfresco(String uiid){
		
		FileAlfresco fileAlfresco = new FileAlfresco();
		ResponseConsultarContenidoType rcct = new ResponseConsultarContenidoType();
		
		ArchivoType archivoType = new ArchivoType();
		archivoType.setUiidRecurso(uiid);
		
		this.setResponseAutenticacion(this.getPort().autenticacion(this.getSeguridad(), this.getAuditoria(), this.getAutenticacion()));
		this.setToken(this.getResponseAutenticacion().getToken());
		
		rcct = this.getPort().consultarContenido(this.getToken(), this.getSeguridad(), this.getAuditoria(), archivoType);
		
		fileAlfresco.setRawFile(rcct.getArchivo().getDocumento());
		fileAlfresco.setFileName(rcct.getMetadata().getParameter2());
		
		//String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		fileAlfresco.setExtension(fileAlfresco.getFileName().substring(fileAlfresco.getFileName().lastIndexOf(".") + 1));
		
		return fileAlfresco;
	}
	
	
/*	
	public FileAlfresco buscarArchivoBinario(String uiid, String extension){
		
		FileAlfresco fileAlfresco = new FileAlfresco();
		ResponseConsultarContenidoType rcct = new ResponseConsultarContenidoType();
		
		ArchivoType archivoType = new ArchivoType();
		//archivoType.setFormato(extension);
		archivoType.setUiidRecurso(uiid);
		
		this.setResponseAutenticacion(this.getPort().autenticacion(this.getSeguridad(), this.getAuditoria(), this.getAutenticacion()));
		this.setToken(this.getResponseAutenticacion().getToken());
		
		rcct = this.getPort().consultarContenido(this.getToken(), this.getSeguridad(), this.getAuditoria(), archivoType);
		
		fileAlfresco.setRawFile(rcct.getArchivo().getDocumento());
		fileAlfresco.setFileName(rcct.getMetadata().getParameter2());
		
		//return this.getPort().consultarContenido(this.getToken(), this.getSeguridad(), this.getAuditoria(), archivoType).getArchivo().getDocumento();
		
		return fileAlfresco;
	}
*/	

	public String getsWsdl() {
		return sWsdl;
	}

	public void setsWsdl(String sWsdl) {
		this.sWsdl = sWsdl;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getWsURI() {
		return wsURI;
	}

	public void setWsURI(String wsURI) {
		this.wsURI = wsURI;
	}

	public SeguridadType getSeguridad() {
		return seguridad;
	}

	public void setSeguridad(SeguridadType seguridad) {
		this.seguridad = seguridad;
	}

	public AuditoriaType getAuditoria() {
		return auditoria;
	}

	public void setAuditoria(AuditoriaType auditoria) {
		this.auditoria = auditoria;
	}

	public AutenticacionType getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(AutenticacionType autenticacion) {
		this.autenticacion = autenticacion;
	}

	public TokenType getToken() {
		return token;
	}

	public void setToken(TokenType token) {
		this.token = token;
	}

	public ResponseAutenticacionType getResponseAutenticacion() {
		return responseAutenticacion;
	}

	public void setResponseAutenticacion(
			ResponseAutenticacionType responseAutenticacion) {
		this.responseAutenticacion = responseAutenticacion;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public QName getQws() {
		return qws;
	}

	public void setQws(QName qws) {
		this.qws = qws;
	}

	public AlfrescoService_Service getService() {
		return service;
	}

	public void setService(AlfrescoService_Service service) {
		this.service = service;
	}

	public AlfrescoService getPort() {
		return port;
	}

	public void setPort(AlfrescoService port) {
		this.port = port;
	}

	public ResponseRegistrarContenidoType getResponseRegistrarContenidoType() {
		return responseRegistrarContenidoType;
	}

	public void setResponseRegistrarContenidoType(
			ResponseRegistrarContenidoType responseRegistrarContenidoType) {
		this.responseRegistrarContenidoType = responseRegistrarContenidoType;
	}

	public ResponseModificarContenidoType getResponseModificarContenidoType() {
		return responseModificarContenidoType;
	}

	public void setResponseModificarContenidoType(
			ResponseModificarContenidoType responseModificarContenidoType) {
		this.responseModificarContenidoType = responseModificarContenidoType;
	}

	public ResponseDescargarArchivoType getResponseDescargarArchivoType() {
		return responseDescargarArchivoType;
	}

	public void setResponseDescargarArchivoType(
			ResponseDescargarArchivoType responseDescargarArchivoType) {
		this.responseDescargarArchivoType = responseDescargarArchivoType;
	}
	
}

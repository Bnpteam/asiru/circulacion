package pe.gob.servir.sistemas.transparencia.presentacion.bandejas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.context.RequestContext;

import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;
import pe.gob.servir.sistemas.transparencia.model.negocio.RegistroSISTRA;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.sistemas.transparencia.model.negocio.SolicitudMovimiento;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.sistemas.transparencia.servicios.alfresco.AlfrescoDocumentoAdjunto;
import pe.gob.servir.std.service.ws.ResponseDatosTramiteCEType;
import pe.gob.servir.std.service.ws.STDServiceInterface;
import pe.gob.servir.std.service.ws.STDWebService;
import pe.gob.servir.systems.util.alfresco.Archivo;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.alfresco.ResultAlfresco;
import pe.gob.servir.systems.util.constante.Constantes;
import pe.gob.servir.systems.util.converter.Convert;
import pe.gob.servir.systems.util.ipaddress.IpAddressPc;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "bandejaSecretariaOAJMB")
@SessionScoped
public class BandejaSecretariaOAJMB extends BasicMB {

	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(this.getClass());
	
	private ReporteBandeja       reporteBandeja; //Para b�squeda.
	private String				 sysSelected;
	private List<ReporteBandeja> lstReporteBandeja;
	private ReporteBandeja		 tmpFormularioBndRep; //Para Extraer a Formulario Bandeja.
	private List<T01Maestro> 	 lstT01TipoDocumento;
	private List<T01Maestro> 	 lstUnidadesOrganicas; //Lista de Objetos T01Maestros
	private List<String> 		 lstUndsOrgsStrg; //Lista de String T01Maestros --> Para CheckBoxMen�.
	private List<String>		 lstUndsOrgsSlc;
	private boolean 			 swAutorizaPersona;
	private String				 txtUnidOrgDerivar;
	private List<T01Maestro>	 lstPlantillaCorreo;
	List<DetalleMovimiento> 	 lstDetalleMovimiento; //Listar documento adjuntados por el responsable Organo.
	private int 				 maximoDocumentoAdj;
	private boolean				 vistaAdjuntarDoc;
	
	RequestContext 				 ReqContext;
	
	private List<UbigeoReniec>   lstDepartamento;
	private List<UbigeoReniec>   lstProvincia;
	private List<UbigeoReniec>   lstDistrito;
	
	private boolean				 vistaRegistroSelecc;
	
	private boolean				 habilitaBtnDerivArea; //btn considerar !disable en seteo
	private boolean				 habilitaBtnDerivUser; //btn considerar !disable en seteo
	private boolean				 habilitaBtnCancelar; //btn considerar !disable en seteo
	
	private boolean				 vistaTramiteOAJ; //Panel
	
	private boolean				 vistaRespuestaProyeccion;
	private boolean				 habilitaRespuestaProyeccion;

	private boolean				 vistaRespuestaUsuario;
	private boolean				 habilitaRespuestaUsuario;
	/*
	private boolean				 vistaRespuestaCargo;
	private boolean				 habilitaRespuestaCargo;
	*/
	private ReporteBandeja		 tmpReporteBandeja; //Ver detalle
	private SolicitudMovimiento	 solicitudMovimientoArea;
	private SolicitudMovimiento	 solicitudMovimientoAdministrado;
	private DetalleMovimiento	 detalleMovimiento;
	private String				 nombreAdjunto01;
	
	private int 				 contFilesUp; //Declarado como global por el handleUpload, se ejecuta con cada acci�n.
	
	private List<RegistroSISTRA> listaRegistrosSISTRA;
	private List<T01Maestro> 	lstt02UnidadesOragnica;
	private List<String> 		lstMedioEntrStrg; //Lista de String T01Maestros --> Para CheckBoxMen�.
	private List<String>		lstMedioEntrSlc;
	private List<T01Maestro> 	lstMedioEntrega; //Lista de Objetos T01Maestros
	private String 				txtmediosEntregaCode;
	
	// Variables EJB.
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/BandejaServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote")
	private BandejaServiceRemote bandejaServiceRemote;
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoSolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote")
	private MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote;

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoDetalleServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote")
	private MovimientoDetalleServiceRemote movimientoDetalleServiceRemote;
	
	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/SolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote")				  
	private SolicitudServiceRemote solicitudServiceRemote;

	@PostConstruct
	public void init(){
		this.setContFilesUp(1);
		this.setNombreAdjunto01("");
		this.setReporteBandeja(new ReporteBandeja());
		this.setTmpFormularioBndRep(new ReporteBandeja());
		this.setDetalleMovimiento(new DetalleMovimiento());
		this.setSysSelected("0");
		this.setLstReporteBandeja(new ArrayList<ReporteBandeja>());
		this.setLstT01TipoDocumento(new ArrayList<T01Maestro>());
		this.setLstUnidadesOrganicas(new ArrayList<T01Maestro>());
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		this.setVistaAdjuntarDoc(true);
		this.setMaximoDocumentoAdj(5);
		this.setLstDepartamento(new ArrayList<UbigeoReniec>());
		this.setLstProvincia(new ArrayList<UbigeoReniec>());
		this.setLstDistrito(new ArrayList<UbigeoReniec>());
		this.setVistaRegistroSelecc(true);
		this.setHabilitaBtnDerivArea(false);
		this.setHabilitaBtnCancelar(false);
		this.setHabilitaBtnDerivUser(false);
		//---->
		//CASO 1:
		this.setVistaTramiteOAJ(false); //Panel
			// A
			this.setVistaRespuestaProyeccion(false);
			this.setHabilitaRespuestaProyeccion(false);
			// B
			this.setVistaRespuestaUsuario(false);
			this.setHabilitaRespuestaUsuario(false);
		/*//CASO 2:
		this.setVistaRespuestaCargo(false);
		this.setHabilitaRespuestaCargo(false);
		//----<
		this.habilitaPanelxArea();*/
		this.setLstUndsOrgsSlc(new ArrayList<String>());
		this.setLstUndsOrgsStrg(new ArrayList<String>());
		//this.llenarCombos();
		
		this.setSolicitudMovimientoArea(new SolicitudMovimiento());
		this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
		super.setFechaHoy(new Date());
		this.getReporteBandeja().setFechaRegistroDesde(new Date());
		this.setListaRegistrosSISTRA(new ArrayList<RegistroSISTRA>());
		this.getLstt02UnidadesOragnica();
		this.setLstMedioEntrStrg(new ArrayList<String>());
		this.setLstMedioEntrSlc(new ArrayList<String>());
		this.getLstDepartamento();
		
	}
	
	public String regresarListado() {
		String pg = "bandejaSecretariaOAJ";
		this.cargarListaConsultas();
		return pg;
	}
	
	public void cancelarSeleccionOAJ(){
		this.setTmpFormularioBndRep(new ReporteBandeja());
		this.setSolicitudMovimientoArea(new SolicitudMovimiento());
		this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		this.borrarAdjuntos();
		//this.habilitaPanelxArea();
		this.setVistaTramiteOAJ(false);//Panel
		this.setHabilitaBtnDerivArea(false);//BTn del panel
		this.setHabilitaBtnDerivUser(false);
		this.setHabilitaBtnCancelar(false);
		this.setHabilitaRespuestaProyeccion(false);
		this.limpiarDerivacionArea();
		this.setHabilitaRespuestaUsuario(false);
		this.limpiarDerivacionAdministrado();
		
		this.setLstProvincia(new ArrayList<UbigeoReniec>());
		this.setLstDistrito(new ArrayList<UbigeoReniec>());
		this.setLstMedioEntrSlc(new ArrayList<String>());
		this.getTmpFormularioBndRep().setFlag_autoriza_recojo("NO");
		this.setSwAutorizaPersona(false); // RENDERED:PANEL
		
	}
	
	public void limpiarDerivacionArea(){
		
		this.setTxtUnidOrgDerivar("");
		this.setLstUndsOrgsSlc(new ArrayList<String>());
		this.getSolicitudMovimientoArea().setDetalleRespuesta("");
		//this.habilitaPanelxArea();
	}
	
	public void limpiarDerivacionAdministrado(){
		
		this.getSolicitudMovimientoAdministrado().setAsunto("");
		this.getSolicitudMovimientoAdministrado().setDetalleRespuesta("");
		this.borrarAdjuntos();
	}

	public void llenarCombos() {
		this.setLstT01TipoDocumento(super.getT01MaestroValuesNL("T01_TIPO_DOCUMENTO_IDENTIDAD"));
		this.setLstUnidadesOrganicas(new ArrayList<T01Maestro>());
		this.setLstUnidadesOrganicas(super.getUndOrg_CnSecretarias(4)); // 4: Perfil: Secreataria UO
		this.setLstUndsOrgsStrg(new  ArrayList<String>());
		for(T01Maestro prmT : this.getLstUnidadesOrganicas()){
			this.getLstUndsOrgsStrg().add(prmT.getNombreCorto());
		}
		
		this.setLstt02UnidadesOragnica(super.getT01MaestroValuesNL("T01_UNIDAD_ORGANICA"));
		this.setLstMedioEntrega(super.getT01MaestroValuesNL("T01_FORMAS_ENTREGA"));
		this.setLstMedioEntrStrg(new ArrayList<String>());
		for (T01Maestro prmT : this.getLstMedioEntrega()) {
			this.getLstMedioEntrStrg().add(prmT.getNombreLargo());
		}
		
		
	}
	
	public void listarDepartamentos(String departamento){
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(departamento);
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");
		try {
			this.setLstDepartamento(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	public void updateProvincia(String codDep, String codProv){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(codDep);
		tmpubigeo.setCodpro(codProv);
		tmpubigeo.setCoddis("00");		
		try {
			this.setLstProvincia(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}

	public void updateDistrito(String codDep, String codProv, String codDis){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(codDep);
		tmpubigeo.setCodpro(codProv);
		tmpubigeo.setCoddis(codDis);		
		try {
			this.setLstDistrito(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	/*
	public void habilitaPanelxArea() {
		if(this.getUsuario().getIdUnidadOrganica().equalsIgnoreCase("8")){ //OAJ
			this.setVistaValidaInfoOAJ(true);
			this.setVistaDerivacionArea(false);
			this.setVistaRespuestaCargo(false);
		} else { //OTRAS AREAS
			this.setVistaValidaInfoOTROS(true);
			this.setVistaDerivacionArea(false);
			this.setVistaRespuestaCargo(false);
		}
	}
	*/
	
	// Boton Acci�n.
	public void verRespuestaProyeccion() {
		
		this.setHabilitaRespuestaProyeccion(true);
		this.setHabilitaRespuestaUsuario(false);
	}
	// Boton Acci�n.
	public void verRespuestaUsuario() {
		this.setHabilitaRespuestaProyeccion(false);
		this.setHabilitaRespuestaUsuario(true);
	}
	
	public void cargarListaConsultas(){
		
		//super.setSwBtnNuevo(false);
		
		if (this.getSysSelected().equals("1") ){				// SOLICITUD MANUAL SLCT
//			this.setLstReporteBandeja(new ArrayList<ReporteBandeja>());			
			
			if(validarBusquedaRegistroSistra()){
				this.setListaRegistrosSISTRA(new ArrayList<RegistroSISTRA>());
				ReporteBandeja reporteBandeja = new ReporteBandeja();
				
				if(this.getReporteBandeja()!=null){				
					if(VO.isEmpty(this.getReporteBandeja().getNumero_documento_identidad())){					
						reporteBandeja.setNumero_documento_identidad("0");					
					}								
					reporteBandeja.setFechaRegistroDesde(this.getReporteBandeja().getFechaRegistroDesde());
					reporteBandeja.setFechaRegistroHasta(this.getReporteBandeja().getFechaRegistroHasta());
					this.setListaRegistrosSISTRA(super.getListaRegistrosSISTRAWS(reporteBandeja));
				}	
			}
			
			this.setTmpFormularioBndRep(new ReporteBandeja());
			this.setSolicitudMovimientoArea(new SolicitudMovimiento());
			this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
			this.setVistaRegistroSelecc(true);
			
		} else if ( this.getSysSelected().equals("2") ) {		// SOLICITUD VIRTUAL SLCT
//			try {
//				this.setVistaRegistroSelecc(true);
//				this.setHabilitaRespuestaProyeccion(false);
//				this.setHabilitaRespuestaUsuario(false);
//				this.cancelarSeleccionOAJ();
//				//this.getReporteBandeja().setUsuario_id_responsable(this.getUsuario().getId());
//				this.getReporteBandeja().setEstado_solicitud_id(1L);
//				//EJB Listar.
//				this.buscarConsulta2(); // <--- Listar.
//				
//			} catch (Exception e) {
//				e.printStackTrace();
//				super.setMensajeAlerta("Ocurri� un error al cargar las solicitudes.");
//			}

		} else {											// any selected
//			this.setLstReporteBandeja(new ArrayList<ReporteBandeja>());
			
			this.setListaRegistrosSISTRA(new ArrayList<RegistroSISTRA>());
			this.setTmpFormularioBndRep(new ReporteBandeja());
			this.setSolicitudMovimientoArea(new SolicitudMovimiento());
			this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
			this.setVistaRegistroSelecc(false);
			this.setVistaTramiteOAJ(false);
			this.setVistaRespuestaUsuario(false);
			
		}
		
		//super.calcularAlturaTabla();
	}
	
	public void seleccionarRegistro(RegistroSISTRA prmRptBan){
		if(prmRptBan!=null){
			this.cancelarSeleccionOAJ(); //Resetea todo.
			this.setTmpFormularioBndRep(new ReporteBandeja());
			this.getTmpFormularioBndRep().setRegistroSISTRA(prmRptBan);
			this.getTmpFormularioBndRep().setNombres(prmRptBan.getRemite());
			this.getTmpFormularioBndRep().setDetalle_solicitud(prmRptBan.getAsunto());
			this.getTmpFormularioBndRep().setFlag_autoriza_recojo("NO");
			
			this.updateAutoPersonaPanel();
//			this.listarDepartamentos(this.getTmpFormularioBndRep().getCoddep());
//			this.updateProvincia(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro());
//			this.updateDistrito(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro(),this.getTmpFormularioBndRep().getCoddis());
			
			//Seteo de Ids.
//			this.getSolicitudMovimientoArea().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
//			this.getSolicitudMovimientoAdministrado().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
			
			this.setHabilitaBtnCancelar(true);
			super.setMensajeAviso("�xito al seleccionar la solicitud.");
			
		}else{
			super.setMensajeAviso("Seleccione una solicitud v�lida.");
		}
	}
	
	// ++
	public void seleccionarRegistroOld(ReporteBandeja prmRptBan){
		if(prmRptBan!=null){
			this.cancelarSeleccionOAJ(); //Resetea todo.
			this.setTmpFormularioBndRep(new ReporteBandeja());
			this.setTmpFormularioBndRep(prmRptBan);
			this.updateAutoPersonaPanel();
			this.listarDepartamentos(this.getTmpFormularioBndRep().getCoddep());
			this.updateProvincia(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro());
			this.updateDistrito(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro(),this.getTmpFormularioBndRep().getCoddis());
			//Seteo de Ids.
			this.getSolicitudMovimientoArea().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
			this.getSolicitudMovimientoAdministrado().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
			
			if(this.getTmpFormularioBndRep().getEstado_solicitud_id() == 1L) { //REGISTRADO. O CORREGIDO POR EL ADMINISTRADO.
				this.getSolicitudMovimientoAdministrado().setCorreo(this.getTmpFormularioBndRep().getCorreo_electronico().trim());
				this.getSolicitudMovimientoAdministrado().setAsunto("OBSERVACI�N DE SOLICITUD");
				
				String cuerpoCorreo = this.getRegistroPlantillaCorreo("2");
				cuerpoCorreo = cuerpoCorreo.replace("{t_contacto}", Convert.toTitleCase(prmRptBan.getNombres()));
				this.getSolicitudMovimientoAdministrado().setDetalleRespuesta(cuerpoCorreo);
					//botones (Estos botones habilian vistas)
					this.setHabilitaBtnDerivArea(true); //Botones selectores.
					this.setHabilitaBtnDerivUser(true);
					this.setHabilitaBtnCancelar(true);
					
					this.setVistaTramiteOAJ(true);
					/*
					 // Panel de pesta�as.
					this.setHabilitaRespuestaProyeccion(false);
					this.setHabilitaRespuestaUsuario(false);*/
					
					
			} else if(this.getTmpFormularioBndRep().getEstado_solicitud_id() == 8L) { // 8: Respuesta Asignado a Asistente OAJ.
				
				this.getSolicitudMovimientoAdministrado().setCorreo(this.getTmpFormularioBndRep().getCorreo_electronico().trim());
				this.getSolicitudMovimientoAdministrado().setAsunto("RESPUESTA DE SOLICITUD");
				
				String cuerpoCorreo = this.getRegistroPlantillaCorreo("3");
				cuerpoCorreo = cuerpoCorreo.replace("{t_contacto}", Convert.toTitleCase(prmRptBan.getNombres()));
				this.getSolicitudMovimientoAdministrado().setDetalleRespuesta(cuerpoCorreo);
				
				//Id entrega el SOLICITUD_MOVIMIENTO_ID del detalle derivaci�n para hacer las b�squedas.
				this.getSolicitudMovimientoAdministrado().setId(this.getTmpFormularioBndRep().getSolicitud_movimiento_id());
				Long soliMoviId = this.getSolicitudMovimientoAdministrado().getId();
				this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));
				//Si excede de 5 documentos anula.(SIN USO AUN)
				int canDocIng = this.getLstDetalleMovimiento().size();
				this.setMaximoDocumentoAdj(this.getMaximoDocumentoAdj() - canDocIng);
				// Bloqueo de adjuntar mas documento si se el maximo llega a 0.
				if(this.getMaximoDocumentoAdj() == 0){
					this.setVistaAdjuntarDoc(false);
				} else {
					this.setVistaAdjuntarDoc(true);
				}
				
				//this.setHabilitaBtnDerivArea(false); //Botones selectores.
				//Habilita botones.
				this.setHabilitaBtnDerivUser(true);
				this.setHabilitaBtnCancelar(true);
				
				this.setVistaTramiteOAJ(true); // Panel de pesta�as.
				this.setHabilitaRespuestaUsuario(true);
				/*this.setHabilitaRespuestaProyeccion(false);
				this.setHabilitaRespuestaUsuario(false); //Por default unicamente se muestra esta sub pantalla.
				this.verRespuestaUsuario(); //Proyectar respuesta a jefe.*/
			} else {
				super.setMensajeAviso("No se ha encontrado una acci�n extra para esta solicitud.");
			}
				
			super.setMensajeAviso("�xito al extraer la solicitud.");
		} else {
			super.setMensajeAviso("Error al extraer la solicitud.");
		}
		
	}
	
	//Necesita solo la Id de solicitudMovimientoId
	public List<DetalleMovimiento> listarDocumentosIngresados(Long SoliMoviId){
		List<DetalleMovimiento> tmpDetaSoli = new ArrayList<DetalleMovimiento>();
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		this.setMaximoDocumentoAdj(5); //Setear maximo documento adjuntados.
		
		try {
			tmpDetaSoli = this.getMovimientoDetalleServiceRemote().lstDocumentosIngresados(SoliMoviId);
			//this.setLstDetalleMovimiento(this.getMovimientoDetalleServiceRemote().lstDocumentosIngresados(tmpDetalleMovimiento));

		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al cargar los documentos Adjuntados Proyecci�n Respuesta.");
		}
		
		return tmpDetaSoli;
	}

	public void limpiarBusqueda2(){
		this.setReporteBandeja(new ReporteBandeja());
		this.getReporteBandeja().setEstado_solicitud_id(1L);
	}
	
	public void updateUnidOrgDerivar(){
		
		String data = "";
		List<String> tmpLstUniOrg = this.getLstUndsOrgsSlc();
		
		for (int i =0; i < tmpLstUniOrg.size(); i++) {
			
			if(VO.isEmpty(data.trim())){
				data = tmpLstUniOrg.get(i);
			} else {
				data= data + "; " + tmpLstUniOrg.get(i);
			}
		}
		
		this.setTxtUnidOrgDerivar(data);
	}
	
	public void updateAutoPersonaPanel(){
		if(this.getTmpFormularioBndRep().getFlag_autoriza_recojo().equals("SI")){
			this.setSwAutorizaPersona(true);
		} else {
			this.setSwAutorizaPersona(false);
		}
	}

	// REGISTRAR
	public String grabar1() {

		String pagina = "bandejaSecretariaOAJ";

		ReturnObject sw = new ReturnObject();

		if (validarDerivArea()) {

			try {
				this.getSolicitudMovimientoArea().setUsuarioIdEmisor(this.getUsuario().getId());
				this.getSolicitudMovimientoArea().setPerfilIdEmisor(6L); //6: Secretar�a OAJ
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMovimientoArea().setT01UndOrgEmiId(unOrgEmisorId); //
				this.getSolicitudMovimientoArea().setT01UndOrgRecId("8"); //Asistente a su jefe misma �rea 8.
				this.getSolicitudMovimientoArea().setPerfilIdReceptor(3L); //3: Jefe OAJ
				this.getSolicitudMovimientoArea().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
				this.getSolicitudMovimientoArea().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMovimientoArea().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				this.getSolicitudMovimientoArea().setEstadoSolicitudId("3"); // 3: Proyectado Derivaci�n a Jefe OAJ.
				this.getSolicitudMovimientoArea().setListaProyeccionUndOrg(this.getTxtUnidOrgDerivar());
				//this.parsearDocumento();

				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMovimientoArea());

				if (sw.getId() != 0) {
					super.setMensajeAviso("�xito al enviar la Proyecci�n de Derivaci�n a Jefe.");
					this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar.
				} else {
					super.setMensajeError("Error al enviar la Proyecci�n de Derivaci�n a Jefe.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al enviar la Proyecci�n de Derivaci�n a Jefe.");
				logger.error(e);
			}

		}
		return pagina;
	}
	
	public void cargarplantillas() {
		this.lstPlantillaCorreo = super.getT01MaestroValuesNL("PLANTILLA_CORREO");
	}
	
	private String getRegistroPlantillaCorreo(String codigo) {

		for (T01Maestro t01Maestro : lstPlantillaCorreo) {
			if (t01Maestro.getCodigoRegistro().equals(codigo)) {
				return t01Maestro.getNombreLargo();
			}
		}
		return "";
	}
	
	// REGISTRAR
	public String grabar2() {

		String pagina = "bandejaSecretariaOAJ";

		ReturnObject sw = new ReturnObject();
		//Listade documentos subido con el FileUpload;
		int cantDocs = this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().size();

		if (validarDerivAdministrado()) {
				
			boolean swAlfresco = true;
				
			String nameFileAlfrescoError = "";
			
			if ( cantDocs > 0) {
				
				for(int i = 0; i < cantDocs; i++) {
					
					AlfrescoDocumentoAdjunto alfrescoAdjunto = new AlfrescoDocumentoAdjunto();
	
					ResultAlfresco resultAlfresco01 = this.subirArchivoAlfresco(alfrescoAdjunto, 
							this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco());
	
					if (resultAlfresco01.getCodigo().equals(Constantes.OPERACION_EXITOSA_ALFRESCO)) {
						this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().setUiid(resultAlfresco01.getUiid());
						this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().setRawFile(null);
						this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).setUuidDocumento(resultAlfresco01.getUiid());
					} else {
						if(VO.isEmpty(nameFileAlfrescoError)){
							nameFileAlfrescoError = this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().getFileName();
						} else {
							nameFileAlfrescoError = nameFileAlfrescoError + "; " +this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().getFileName();
						}
						
						swAlfresco = false;
	
						logger.error(resultAlfresco01.getCodigo() + " : " + resultAlfresco01.getDescripcion());
						logger.error("El documento nro: " + i 
									 + "Nombre Documeto" + this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(0).getFileAlfresco().getFileName());
						
					}
					
				}
			}
				
			if (!swAlfresco) {
				this.setMensajeError("Ocurri� un error al cargar el/los archivo(s) : " + nameFileAlfrescoError);
				return "";
			}
			
			try {
				this.getSolicitudMovimientoAdministrado().setUsuarioIdEmisor(this.getUsuario().getId());
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMovimientoAdministrado().setPerfilIdEmisor(6L); //6: Secretar�a OAJ
				this.getSolicitudMovimientoAdministrado().setT01UndOrgEmiId(unOrgEmisorId); //
				this.getSolicitudMovimientoAdministrado().setT01UndOrgRecId("8"); //Asistente a su jefe misma �rea 8.
				this.getSolicitudMovimientoAdministrado().setPerfilIdReceptor(3L); //3: Jefe OAJ
				this.getSolicitudMovimientoAdministrado().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
				this.getSolicitudMovimientoAdministrado().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMovimientoAdministrado().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				//Valida que tipo de respuesta debe enviar, si es una respuesta final o de observaci�n.s
				if (this.getTmpFormularioBndRep().getEstado_solicitud_id() == 1L) { // SI ES REGISTRADO
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("2"); // 2: Proyectado Respuesta OBS. a Jefe OAJ.
				} else if (this.getTmpFormularioBndRep().getEstado_solicitud_id() == 8L) { // Respuesta Asignado a Asistente OAJ
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("9"); // 9: Proyectado Respuesta FINAL a Jefe OAJ.
				}
				
				//this.parsearDocumento();
				SolicitudMovimiento tmpSolicMov = new SolicitudMovimiento();
				tmpSolicMov.setListaDocsRespuestas(this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas());
				this.getSolicitudMovimientoAdministrado().setListaDocsRespuestas(null);
				
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMovimientoAdministrado());
				
				if (sw.getId() != 0) {
					int cantDocsTraidos = this.getLstDetalleMovimiento().size();
					//AQUI INSERCION DE DOCUMENTOS.
					if(cantDocs > 0 || cantDocsTraidos > 0){ //Si tiene documentos adjuntos guarda primero O si tiene documentos que han sido traidos.
						
						ReturnObject sw2 = new ReturnObject();
						int nroExt = 0;
						int nroFal = 0;
						
						try {
							//Solo si trajo documentos que subi� el anterior usuario se a�ade a los que subi� el actual usuario.
							if(cantDocsTraidos > 0) {
								for (DetalleMovimiento tmpDetaMovi: this.getLstDetalleMovimiento()) {
									tmpSolicMov.getListaDocsRespuestas().add(tmpDetaMovi);
								}
							}
							
							int catn = tmpSolicMov.getListaDocsRespuestas().size();
							for (int i = 0; i  < catn; i++){
							
								DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
								
								tmpDetalleMovimiento.setSolicitudMovimientoId(sw.getId()); 
								tmpDetalleMovimiento.setTipoDocumentoId("17"); // TIPO DOCUMENTO: OTROS
								tmpDetalleMovimiento.setNombreDocumento(tmpSolicMov.getListaDocsRespuestas().get(i).getNombreDocumento());
								tmpDetalleMovimiento.setUuidDocumento(tmpSolicMov.getListaDocsRespuestas().get(i).getUuidDocumento());
								tmpDetalleMovimiento.setUsuarioIdRegistro(this.getUsuario().getId());
								sw2 = this.getMovimientoDetalleServiceRemote().insertar(tmpDetalleMovimiento);
								
								//Cantidad de Documento subidos a Alfresco
								if (sw2.getId() != 0) {
									nroExt ++; //Cantidad Exitosa.
								} else {
									nroFal ++; //Cantidad Fallida.
								}
							}
						} catch (Exception e) {
							super.setMensajeError("Error al referenciar los documentos con la solicitud.");
							logger.error(e);
						}
						
						if(nroFal > 0){
							super.setMensajeAlerta("Hubo "+ nroFal +" Error(es) en la insersi�n del registro DETALLE_MOVIMIENTO.");
							super.setMensajeAlerta("Solo se insertaron "+ nroExt +" registros en DETALLE_MOVIMIENTO.");
						}
						
						super.setMensajeAviso("�xito al enviar la Proyecci�n de Respuesta a Jefe.");

					} else {
						super.setMensajeAviso("�xito al enviar la Proyecci�n de Respuesta a Jefe.");
					}
				
					this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
					this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar.
				} else {
					super.setMensajeError("Error al enviar la Proyecci�n de Respuesta a Jefe.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al enviar la Proyecci�n de Respuesta a Jefe.");
				logger.error(e);
			}

		}
		return pagina;
	}
	
	//SUBIR
	private ResultAlfresco subirArchivoAlfresco(
		AlfrescoDocumentoAdjunto alfrescoAdjunto, FileAlfresco fileAlfresco) {
		
		//categoriaDoc.setTipoDocumento(super.getString("directorio.uno"));
		
		Date actual = new Date();
		
		SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
		String anio = formatAnio.format(actual);
		SimpleDateFormat formatMes = new SimpleDateFormat("MM");
		String mes = formatMes.format(actual);
		String proceso = "ADJUNTOS";
		
		ResultAlfresco resultAlfresco = new ResultAlfresco();
		resultAlfresco = alfrescoAdjunto.subirArchivo(fileAlfresco, proceso, anio, mes);
		
		return resultAlfresco;
	}

	public void buscarConsulta2(){
		
		try {
			this.getReporteBandeja().setUsuario_id_responsable(this.getUsuario().getId());
			//EJB Listar.
//			this.setLstReporteBandeja(this.getBandejaServiceRemote().lstReporteBandejaSecretariaOAJ(this.getReporteBandeja()));
						
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al buscar solicitud!");
		}
	}
	
	public void buscarRegistrosSISTRAWS(){
		if(validarBusquedaRegistroSistra()){
			this.setListaRegistrosSISTRA(new ArrayList<RegistroSISTRA>());
			ReporteBandeja reporteBandeja = new ReporteBandeja();
			
			if(this.getReporteBandeja()!=null){				
				if(VO.isEmpty(this.getReporteBandeja().getNumero_documento_identidad())){					
					reporteBandeja.setNumero_documento_identidad("0");					
				}								
				reporteBandeja.setFechaRegistroDesde(this.getReporteBandeja().getFechaRegistroDesde());
				reporteBandeja.setFechaRegistroHasta(this.getReporteBandeja().getFechaRegistroHasta());
				this.setListaRegistrosSISTRA(super.getListaRegistrosSISTRAWS(reporteBandeja));
			}
			
			this.setTmpFormularioBndRep(new ReporteBandeja());
			this.setSolicitudMovimientoArea(new SolicitudMovimiento());
			this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
			this.setVistaRegistroSelecc(true);
		}		
		
	}
	
	public boolean validarDerivPreArea(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivArea()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar1').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	public boolean validarDerivPreAdministrado(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivAdministrado()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}
		
		//super.setMensajeAlerta("OK 2");
		ReqContext.execute("PF('idGrpConfirmar2').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	public String verDetallexrpt(ReporteBandeja prmRptBan) {

		this.setTmpReporteBandeja(prmRptBan);
		
		return "detalleBandAsistenteOAJ";
	}

	// ALFRESCO
	public void handleFileUpload(FileUploadEvent event) {
		Archivo archivo = new Archivo();
		String fileName;
		FileAlfresco fileAlfresco = new FileAlfresco();

		try {

			UploadedFile fu = event.getFile();

			fileName = fu.getFileName();

			fileAlfresco.setFileName(fileName);
			fileAlfresco.setExtension(fileName.substring(fileName.lastIndexOf(".") + 1));

			File file = new File(super.getString("ruta.documentos.adjunto.email"),
					fileName);
			FileOutputStream salida = new FileOutputStream(file);
			salida.write(fu.getContents());
			salida.flush();
			salida.close();

			String pathFile = file.getPath();
			
			fileAlfresco.setRawFile(archivo.read(new File(pathFile)));
			
			fileAlfresco.setTipoDoc("Adjunto");

			file.delete();
			
			archivo = null;
				//Agregar a la lista cada uno de los documentos adjuntados.
				DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
				//tmpDetalleMovimiento.setSolicitudMovimientoId(Long); <-- A�n no se guard� SOLICITUD_MOVIMIENTO.
				tmpDetalleMovimiento.setFileAlfresco(fileAlfresco);
				tmpDetalleMovimiento.setNombreDocumento(fileAlfresco.getFileName());
				
				this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().add(tmpDetalleMovimiento);
				int cont = this.getContFilesUp();
				//Extrae el nombre y la muestra en el output.
				if(VO.isEmpty(this.getNombreAdjunto01())){
					this.setNombreAdjunto01(cont+". "+tmpDetalleMovimiento.getNombreDocumento());
				} else {
					this.setNombreAdjunto01(this.getNombreAdjunto01()+"; "+cont+". "+tmpDetalleMovimiento.getNombreDocumento());
				}
				
				cont++;
				this.setContFilesUp(cont);
				
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al cargar el archivo");
		}

	}
	
	public void borrarAdjuntos() {
		this.getSolicitudMovimientoAdministrado().setListaDocsRespuestas(new ArrayList<DetalleMovimiento>());
		this.setNombreAdjunto01("");
		this.setContFilesUp(1);
	}
	
	private boolean validarBusquedaRegistroSistra(){
		
		if(VO.isEmpty(this.getReporteBandeja().getNumero_documento_identidad())){
			if(VO.isEmpty(this.getReporteBandeja().getFechaRegistroDesde())){
				super.setMensajeAlerta("Ingrese Fecha de Inicio");
				return false;	
			}						
		}		
		
		return true;
		
	}
	

	private boolean validarDerivArea() {

		int contador = 0;
		String mensaje = "";
		
		if ( this.getSolicitudMovimientoArea().getSolicitudTransparenciaId() == 0L ) {
			mensaje = mensaje.equals("") ? "No se ha extra�do ninguna solicitud."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getTxtUnidOrgDerivar())) {
			mensaje = mensaje.equals("") ? "Seleccione unidad org�nica."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMovimientoArea().getDetalleRespuesta().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un motivo de derivaci�n."
					: mensaje;
			contador++;
		}
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		
		logger.debug("Validado con �xito:");
		return true;
	}
	
	private boolean validarDerivAdministrado() {

		int contador = 0;
		String mensaje = "";
		
		if ( this.getSolicitudMovimientoArea().getSolicitudTransparenciaId() == 0L ) {
			mensaje = mensaje.equals("") ? "No se ha extra�do ninguna solicitud."
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(this.getSolicitudMovimientoAdministrado().getCorreo())) {
			mensaje = mensaje.equals("") ? "No se ha extraido ning�n correo para esta solicitud."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMovimientoAdministrado().getAsunto().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un asunto v�lido."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMovimientoAdministrado().getDetalleRespuesta().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un contenido v�lido."
					: mensaje;
			contador++;
		}
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		
		logger.debug("Validado con �xito:");
		return true;
	}
	
	
	/**************** STD **********************/
	
	public  void getListaAIPfromSTD(){

		/*FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String remoteHost = request.getRemoteAddr();*/ // cliente ip
		
		List<ResponseDatosTramiteCEType> retorno;
		Collection<Solicitud> retornoListaSolicitud = new ArrayList<Solicitud>();
		
		//String rt = "";
		try {

			URL url = new URL(super.getString("std.ws.wsdl"));						
			STDWebService service1 = new STDWebService();
			STDServiceInterface port1 = service1.getSTDServiceInterfaceImplPort();

			((BindingProvider) port1).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, super.getString("std.ws.endpoint"));
			pe.gob.servir.std.service.ws.SeguridadType seguridad = new pe.gob.servir.std.service.ws.SeguridadType();
			pe.gob.servir.std.service.ws.AuditoriaType auditoria = new pe.gob.servir.std.service.ws.AuditoriaType();

			seguridad.setCodAplicativo(super.getString("std.ws.codigo"));
			IpAddressPc file = new IpAddressPc();
			String[] audit = file.obtenerAuditoria();
			//auditoria.setIpPc(remoteHost);
			auditoria.setIpPc("172.16.24.129");
			auditoria.setMacAddress("AB:EC:EC:BE:1A");
			
			retorno = port1.getDatosTramiteAccesoInformacion(seguridad, auditoria);
			
			if(retorno!=null && retorno.size()>0){
				// metodo para devolver lista final
				retornoListaSolicitud = setListaSTDACEX(retorno);
			}
			
			logger.info("APP"+retorno.toString()); 

		} catch (MalformedURLException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}		
	}
	

	public List<Solicitud> setListaSTDACEX(List<ResponseDatosTramiteCEType> listaEntrada){
		boolean sw = false;
		//List<String> lstNroRegSTD = listarNroRegSTD();
		
		List<Solicitud> listaSalida = new ArrayList<Solicitud>();
		Solicitud bsolicitudAIP;
		//SimpleDateFormat formatoDeTexto = new SimpleDateFormat("dd/MM/yyyy");
		
		Iterator itr = listaEntrada.iterator();
	      while(itr.hasNext()) {	    	  
	    	  ResponseDatosTramiteCEType element = (ResponseDatosTramiteCEType) itr.next();	      
	    	  if(sw){
	    		  sw = false;
	    		  continue;
	    	  }	    	  
	    	  bsolicitudAIP = new Solicitud();
	    	  bsolicitudAIP.setNumRegistro(element.getNumTramite());
	    	  bsolicitudAIP.setTipoDocumentoIdentidadId(element.getTipoDocumento());
	    	  bsolicitudAIP.setNumeroDocumentoIdentidad(element.getNumDocumento());
	    	  bsolicitudAIP.setRemitentePersona(element.getRemitentePersona());
	    	  bsolicitudAIP.setRemitenteEntidad(element.getRemitente());
	    	  bsolicitudAIP.setAsuntoStd(element.getAsunto());
	    	  bsolicitudAIP.setRutaAlfrescoSTD(element.getRutaCMS());	
	    	  
	    	  listaSalida.add(bsolicitudAIP);
	      }		
		return listaSalida;
	}
	
	
	// +UPDATE
		public void updateMediosEntrega(){
			
			String dataLetra = "";
			String dataCode  = "";
			
			List<T01Maestro> tmpLstMedEntrSlc = new ArrayList<T01Maestro>();
			int conSlc = this.getLstMedioEntrSlc().size();
			for (int cont = 0; cont < conSlc; cont++) {
				T01Maestro T01MedEntr = this.getObtenerMaestroUOXNombre(this.getLstMedioEntrega(), this.getLstMedioEntrSlc().get(cont));
				tmpLstMedEntrSlc.add(T01MedEntr);
			}
			
			for (T01Maestro medEntr : tmpLstMedEntrSlc) {
				if(VO.isEmpty(dataLetra.trim())){
					dataLetra = medEntr.getNombreCorto();
				} else {
					dataLetra = dataLetra + "; " + medEntr.getNombreCorto();
				}
				
				if (VO.isEmpty(dataCode.trim())) {
					dataCode = medEntr.getCodigoRegistro();
				} else {
					dataCode = dataCode + "; " + medEntr.getCodigoRegistro();
				}
			}
			
//			this.setTxtmediosEntrega(dataLetra);
			this.getTmpFormularioBndRep().setForma_de_entrega(dataLetra);
			this.setTxtmediosEntregaCode(dataCode);
			//ReqContext = RequestContext.getCurrentInstance();		
			//ReqContext.update("frmRegSolicitud:pnFormaEntregaPanel");
		}
	
		// Obtener el Maestro por el Nombre.
		private T01Maestro getObtenerMaestroUOXNombre(List<T01Maestro> prmList01Maestro, String codigo) {

			for (T01Maestro t01Maestro : prmList01Maestro) {
				if (t01Maestro.getNombreLargo().equalsIgnoreCase(codigo)) {
					return t01Maestro;
				}
			}
			return null;
		}
		
		public void updateProvincia(){
			
			UbigeoReniec tmpubigeo = new UbigeoReniec();
//			tmpubigeo.setCoddep(this.getSolicitud().getCoddep());
			tmpubigeo.setCoddep(this.getTmpFormularioBndRep().getCoddep());
			tmpubigeo.setCodpro("%");
			tmpubigeo.setCoddis("00");		
			try {
				this.lstProvincia = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
			} catch (ServicioException e) {			
				e.printStackTrace();
			}
		}
		
		public void updateDistrito(){
			
			UbigeoReniec tmpubigeo = new UbigeoReniec();
//			tmpubigeo.setCoddep(this.getSolicitud().getCoddep());
//			tmpubigeo.setCodpro(this.getSolicitud().getCodpro());
			tmpubigeo.setCoddep(this.getTmpFormularioBndRep().getCoddep());
			tmpubigeo.setCodpro(this.getTmpFormularioBndRep().getCodpro());
			tmpubigeo.setCoddis("%");		
			try {
				this.lstDistrito = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
			} catch (ServicioException e) {			
				e.printStackTrace();
			}
		}
		
		public boolean validarRegPreSolicitud(){

			ReqContext = RequestContext.getCurrentInstance();

			if (!this.validarRegSolicitud()) {
			//ReqContext.update(":frmRegSolicitud:mensaje");
			return false;
			}
			//super.setMensajeAlerta("OK 1");
			ReqContext.execute("PF('idGrpConfirmar4').show()");
			//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

			return true;
		}
		
		private boolean validarRegSolicitud() {
	
			if ( VO.isEmpty(this.getTmpFormularioBndRep().getRegistroSISTRA().getNumeroSistra()) ) {
				super.setMensajeAlerta("Seleccione una solicitud v�lida");
				return false;
			}
			
			if (VO.isEmpty(this.getTmpFormularioBndRep().getNombres())) {
				super.setMensajeAlerta("Ingrese un nombre");
				return false;
			}
			
			if (VO.isEmpty(this.getTmpFormularioBndRep().getApellido_paterno())) {
				super.setMensajeAlerta("Ingrese apellido paterno");
				return false;
			}
			
			if (VO.isEmpty(this.getTmpFormularioBndRep().getDetalle_solicitud())) {
				super.setMensajeAlerta("Ingrese el detalle");
				return false;
			}
			
			logger.debug("Validado con �xito:");
			return true;
		}
		
		// REGISTRAR
		public String grabar4() {

			String pagina = "bandejaSecretariaOAJ";

			ReturnObject sw = new ReturnObject();

			if (validarRegSolicitud()) {

				try {
					
					sw = this.getSolicitudServiceRemote().insertar(this.createBean());					

					if (sw.getId() != 0) {
						super.setMensajeAviso("�xito al registrar solicitud.");
						this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
						this.buscarRegistrosSISTRAWS(); // <--- Listar.
					} else {
						super.setMensajeError("Error al registrar solicitud.");
					}

				} catch (Exception e) {
					super.setMensajeError("Error al registrar solicitud.");
					logger.error(e);
				}

			}
			return pagina;
		}
	
		public Solicitud createBean(){
			
			Solicitud prmObj = new Solicitud();
						
			prmObj.setTipoDocumentoIdentidadId( (VO.isEmpty(this.getTmpFormularioBndRep().getTipo_documento_identidad_id())? "0L": String.valueOf(this.getTmpFormularioBndRep().getTipo_documento_identidad_id())));
			prmObj.setNumeroDocumentoIdentidad(this.getTmpFormularioBndRep().getNumero_documento_identidad());
			prmObj.setApellidoPaterno(this.getTmpFormularioBndRep().getApellido_paterno().trim().toUpperCase());
			prmObj.setApellidoMaterno(this.getTmpFormularioBndRep().getApellido_materno().trim().toUpperCase());
			prmObj.setNombres(this.getTmpFormularioBndRep().getNombres().trim().toUpperCase());
			prmObj.setDireccionDomicilio(this.getTmpFormularioBndRep().getDireccion_domicilio().trim().toUpperCase());
			prmObj.setCoddep(this.getTmpFormularioBndRep().getCoddep());
			prmObj.setCodpro(this.getTmpFormularioBndRep().getCodpro());
			prmObj.setCoddis(this.getTmpFormularioBndRep().getCoddis());
			prmObj.setTelefono(this.getTmpFormularioBndRep().getTelefono());
			prmObj.setCelular(this.getTmpFormularioBndRep().getCelular());
			prmObj.setCorreo_electronico(this.getTmpFormularioBndRep().getCorreo_electronico().trim().toLowerCase());					
			prmObj.setDetalleSolicitud(this.getTmpFormularioBndRep().getDetalle_solicitud());
			prmObj.setDependenciaDeInformacion(this.getTmpFormularioBndRep().getDependencia_de_informacion());
			prmObj.setFormaDeEntrega(this.getTxtmediosEntregaCode());
			prmObj.setFlag_autoriza_recojo(this.getTmpFormularioBndRep().getFlag_autoriza_recojo());
			prmObj.setPersAutonroDocumento(this.getTmpFormularioBndRep().getPers_auto_nro_documento());
			prmObj.setPersAutoNombreApellidos( (!VO.isEmpty(this.getTmpFormularioBndRep().getPers_auto_nombre_apellidos()))? this.getTmpFormularioBndRep().getPers_auto_nombre_apellidos().trim().toUpperCase():this.getTmpFormularioBndRep().getPers_auto_nombre_apellidos() );
			prmObj.setObservacion(this.getTmpFormularioBndRep().getObservacion().trim().toUpperCase());
			prmObj.setFlagDeclaracioJurada(this.getTmpFormularioBndRep().getFlag_declaracion_jurada());
			prmObj.setFlagNotifElectronica(this.getTmpFormularioBndRep().getFlag_notif_electronica());
			prmObj.setUsuarioIdRegistro(this.getUsuario().getId());
			prmObj.setNumRegistroSistra(this.getTmpFormularioBndRep().getRegistroSISTRA().getNumeroSistra());
			prmObj.setTipoSolicitud(Constantes.SOLICITUD_FISICAS);
			
			return prmObj;
		}
	/************************************************/
	// GETTERS AND SETTERS.
	//VARIABLES
	
	public String getSysSelected() {
		return sysSelected;
	}

	public ReporteBandeja getReporteBandeja() {
		return reporteBandeja;
	}

	public void setReporteBandeja(ReporteBandeja reporteBandeja) {
		this.reporteBandeja = reporteBandeja;
	}

	public void setSysSelected(String sysSelected) {
		this.sysSelected = sysSelected;
	}
	
	public List<ReporteBandeja> getLstReporteBandeja() {
		return lstReporteBandeja;
	}

	public void setLstReporteBandeja(List<ReporteBandeja> lstReporteBandeja) {
		this.lstReporteBandeja = lstReporteBandeja;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}

	public ReporteBandeja getTmpFormularioBndRep() {
		return tmpFormularioBndRep;
	}

	public void setTmpFormularioBndRep(ReporteBandeja tmpFormularioBndRep) {
		this.tmpFormularioBndRep = tmpFormularioBndRep;
	}

	public List<T01Maestro> getLstUnidadesOrganicas() {
		return lstUnidadesOrganicas;
	}

	public void setLstUnidadesOrganicas(List<T01Maestro> lstt02UnidadesOragnica) {
		this.lstUnidadesOrganicas = lstt02UnidadesOragnica;
	}

	public boolean isSwAutorizaPersona() {
		return swAutorizaPersona;
	}

	public void setSwAutorizaPersona(boolean swAutorizaPersona) {
		this.swAutorizaPersona = swAutorizaPersona;
	}

	public String getTxtUnidOrgDerivar() {
		return txtUnidOrgDerivar;
	}

	public void setTxtUnidOrgDerivar(String txtUnidOrgDerivar) {
		this.txtUnidOrgDerivar = txtUnidOrgDerivar;
	}

	public SolicitudMovimiento getSolicitudMovimientoArea() {
		return solicitudMovimientoArea;
	}

	public void setSolicitudMovimientoArea(
			SolicitudMovimiento solicitudMovimientoArea) {
		this.solicitudMovimientoArea = solicitudMovimientoArea;
	}

	public SolicitudMovimiento getSolicitudMovimientoAdministrado() {
		return solicitudMovimientoAdministrado;
	}

	public void setSolicitudMovimientoAdministrado(
			SolicitudMovimiento solicitudMovimientoAdministrado) {
		this.solicitudMovimientoAdministrado = solicitudMovimientoAdministrado;
	}

	public RequestContext getReqContext() {
		return ReqContext;
	}

	public void setReqContext(RequestContext reqContext) {
		ReqContext = reqContext;
	}
	
	public List<T01Maestro> getLstT01TipoDocumento() {
		return lstT01TipoDocumento;
	}

	public void setLstT01TipoDocumento(List<T01Maestro> lstT01TipoDocumento) {
		this.lstT01TipoDocumento = lstT01TipoDocumento;
	}
		
	public List<UbigeoReniec> getLstDepartamento() {
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep("%");
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");		
		try {
			lstDepartamento = super.getPersonaServiceRemote().listUbigeo(tmpubigeo);
		} catch (ServicioException e) {			
			e.printStackTrace();
		}

		return lstDepartamento;
	}

	public void setLstDepartamento(List<UbigeoReniec> lstDepartamento) {
		this.lstDepartamento = lstDepartamento;
	}
	
	public List<UbigeoReniec> getLstProvincia() {
		return lstProvincia;
	}

	public void setLstProvincia(List<UbigeoReniec> lstProvincia) {
		this.lstProvincia = lstProvincia;
	}

	public List<UbigeoReniec> getLstDistrito() {
		return lstDistrito;
	}

	public void setLstDistrito(List<UbigeoReniec> lstDistrito) {
		this.lstDistrito = lstDistrito;
	}

	public boolean isHabilitaRespuestaUsuario() {
		return habilitaRespuestaUsuario;
	}

	public void setHabilitaRespuestaUsuario(boolean habilitaRespuestaUsuario) {
		this.habilitaRespuestaUsuario = habilitaRespuestaUsuario;
	}

	public boolean isVistaTramiteOAJ() {
		return vistaTramiteOAJ;
	}

	public void setVistaTramiteOAJ(boolean vistaTramiteOAJ) {
		this.vistaTramiteOAJ = vistaTramiteOAJ;
	}

	public boolean isVistaRespuestaUsuario() {
		return vistaRespuestaUsuario;
	}

	public void setVistaRespuestaUsuario(boolean vistaRespuestaArea) {
		this.vistaRespuestaUsuario = vistaRespuestaArea;
	}

	public boolean isVistaRegistroSelecc() {
		return vistaRegistroSelecc;
	}

	public void setVistaRegistroSelecc(boolean vistaRegistroSelecc) {
		this.vistaRegistroSelecc = vistaRegistroSelecc;
	}
	
	
	public ReporteBandeja getTmpReporteBandeja() {
		return tmpReporteBandeja;
	}

	public void setTmpReporteBandeja(ReporteBandeja tmpReporteBandeja) {
		this.tmpReporteBandeja = tmpReporteBandeja;
	}
	
	public DetalleMovimiento getDetalleMovimiento() {
		return detalleMovimiento;
	}

	public void setDetalleMovimiento(DetalleMovimiento detalleMovimiento) {
		this.detalleMovimiento = detalleMovimiento;
	}

	public int getContFilesUp() {
		return contFilesUp;
	}

	public void setContFilesUp(int contFilesUp) {
		this.contFilesUp = contFilesUp;
	}

	public boolean isVistaRespuestaProyeccion() {
		return vistaRespuestaProyeccion;
	}

	public void setVistaRespuestaProyeccion(boolean vistaRespuestaProyeccion) {
		this.vistaRespuestaProyeccion = vistaRespuestaProyeccion;
	}

	public boolean isHabilitaRespuestaProyeccion() {
		return habilitaRespuestaProyeccion;
	}

	public void setHabilitaRespuestaProyeccion(boolean habilitaRespuestaProyeccion) {
		this.habilitaRespuestaProyeccion = habilitaRespuestaProyeccion;
	}

	public String getNombreAdjunto01() {
		return nombreAdjunto01;
	}

	public void setNombreAdjunto01(String nombreAdjunto01) {
		this.nombreAdjunto01 = nombreAdjunto01;
	}

	public List<String> getLstUndsOrgsStrg() {
		return lstUndsOrgsStrg;
	}

	public void setLstUndsOrgsStrg(List<String> lstUndsOrgsStrg) {
		this.lstUndsOrgsStrg = lstUndsOrgsStrg;
	}

	public List<String> getLstUndsOrgsSlc() {
		return lstUndsOrgsSlc;
	}

	public void setLstUndsOrgsSlc(List<String> lstUndsOrgsSlc) {
		this.lstUndsOrgsSlc = lstUndsOrgsSlc;
	}

	public List<T01Maestro> getLstPlantillaCorreo() {
		return lstPlantillaCorreo;
	}

	public void setLstPlantillaCorreo(List<T01Maestro> lstPlantillaCorreo) {
		this.lstPlantillaCorreo = lstPlantillaCorreo;
	}

	public boolean isHabilitaBtnDerivArea() {
		return habilitaBtnDerivArea;
	}

	public void setHabilitaBtnDerivArea(boolean habilitaBtnDerivArea) {
		this.habilitaBtnDerivArea = !habilitaBtnDerivArea;
	}
	
	public boolean isHabilitaBtnDerivUser() {
		return habilitaBtnDerivUser;
	}

	public void setHabilitaBtnDerivUser(boolean habilitaBtnDerivUser) {
		this.habilitaBtnDerivUser = !habilitaBtnDerivUser;
	}

	public boolean isHabilitaBtnCancelar() {
		return habilitaBtnCancelar;
	}

	public void setHabilitaBtnCancelar(boolean habilitaBtnCancelar) {
		this.habilitaBtnCancelar = !habilitaBtnCancelar;
	}

	public List<DetalleMovimiento> getLstDetalleMovimiento() {
		return lstDetalleMovimiento;
	}

	public void setLstDetalleMovimiento(List<DetalleMovimiento> lstDetalleMovimiento) {
		this.lstDetalleMovimiento = lstDetalleMovimiento;
	}

	public int getMaximoDocumentoAdj() {
		return maximoDocumentoAdj;
	}

	public void setMaximoDocumentoAdj(int maximoDocumentoAdj) {
		this.maximoDocumentoAdj = maximoDocumentoAdj;
	}

	public boolean isVistaAdjuntarDoc() {
		return vistaAdjuntarDoc;
	}

	public void setVistaAdjuntarDoc(boolean vistaAdjuntarDoc) {
		this.vistaAdjuntarDoc = vistaAdjuntarDoc;
	}

	//EJB
	public BandejaServiceRemote getBandejaServiceRemote() {
		return bandejaServiceRemote;
	}

	public void setBandejaServiceRemote(BandejaServiceRemote bandejaServiceRemote) {
		this.bandejaServiceRemote = bandejaServiceRemote;
	}

	public MovimientoSolicitudServiceRemote getMovimientoSolicitudServiceRemote() {
		return movimientoSolicitudServiceRemote;
	}

	public void setMovimientoSolicitudServiceRemote(
			MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote) {
		this.movimientoSolicitudServiceRemote = movimientoSolicitudServiceRemote;
	}

	public MovimientoDetalleServiceRemote getMovimientoDetalleServiceRemote() {
		return movimientoDetalleServiceRemote;
	}

	public void setMovimientoDetalleServiceRemote(
			MovimientoDetalleServiceRemote movimientoDetalleServiceRemote) {
		this.movimientoDetalleServiceRemote = movimientoDetalleServiceRemote;
	}

	public List<RegistroSISTRA> getListaRegistrosSISTRA() {
		return listaRegistrosSISTRA;
	}

	public void setListaRegistrosSISTRA(List<RegistroSISTRA> listaRegistrosSISTRA) {
		this.listaRegistrosSISTRA = listaRegistrosSISTRA;
	}

	public List<T01Maestro> getLstt02UnidadesOragnica() {
		return lstt02UnidadesOragnica;
	}

	public void setLstt02UnidadesOragnica(List<T01Maestro> lstt02UnidadesOragnica) {
		this.lstt02UnidadesOragnica = lstt02UnidadesOragnica;
	}

	public List<String> getLstMedioEntrStrg() {
		return lstMedioEntrStrg;
	}

	public void setLstMedioEntrStrg(List<String> lstMedioEntrStrg) {
		this.lstMedioEntrStrg = lstMedioEntrStrg;
	}

	public List<String> getLstMedioEntrSlc() {
		return lstMedioEntrSlc;
	}

	public void setLstMedioEntrSlc(List<String> lstMedioEntrSlc) {
		this.lstMedioEntrSlc = lstMedioEntrSlc;
	}

	public List<T01Maestro> getLstMedioEntrega() {
		return lstMedioEntrega;
	}

	public void setLstMedioEntrega(List<T01Maestro> lstMedioEntrega) {
		this.lstMedioEntrega = lstMedioEntrega;
	}

	public String getTxtmediosEntregaCode() {
		return txtmediosEntregaCode;
	}

	public void setTxtmediosEntregaCode(String txtmediosEntregaCode) {
		this.txtmediosEntregaCode = txtmediosEntregaCode;
	}

	public SolicitudServiceRemote getSolicitudServiceRemote() {
		return solicitudServiceRemote;
	}

	public void setSolicitudServiceRemote(
			SolicitudServiceRemote solicitudServiceRemote) {
		this.solicitudServiceRemote = solicitudServiceRemote;
	}

}

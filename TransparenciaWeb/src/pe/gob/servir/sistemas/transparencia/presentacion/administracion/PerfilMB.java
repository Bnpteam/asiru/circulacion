package pe.gob.servir.sistemas.transparencia.presentacion.administracion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "perfilMB")
@SessionScoped
public class PerfilMB extends BasicMB {

	private static final Logger logger = Logger.getLogger(PerfilMB.class);
	private static final long serialVersionUID = 1L;
	RequestContext ReqContext;

	private Perfil					perfilForm;
	private Perfil					perfilFormDelete;
	private List<Perfil>			lstPerfil;
	
	private boolean 				mostrarBotonGrabar;
	private boolean 				mostrarBotonEditar;
	private boolean 				mostrarBotonCancelar;

		
	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/PerfilServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.PerfilServiceRemote")	
	private PerfilServiceRemote perfilServiceRemote;

	@PostConstruct
	public void init(){}
	
	public void iniciarBandeja(){
		this.inicializarValores();
		this.cargarListaPerfiles();
	}
	
	public void inicializarValores(){
		this.setPerfilForm(new Perfil());
		this.setPerfilFormDelete(new Perfil());
		this.setMostrarBotonGrabar(true);
		this.setMostrarBotonEditar(false);
		this.setMostrarBotonCancelar(false);		
	}	
	
	public void cargarListaPerfiles(){
		this.setLstPerfil(this.getPerfilValues());
	}
	
	public List<Perfil> getPerfilValues(){
		List<Perfil> lstPerfil = new ArrayList<Perfil>();
		try{
			lstPerfil = this.getPerfilServiceRemote().listar();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		
		return lstPerfil;
	}
	
	public void seleccionarRegistro(Perfil perfil){

		this.setPerfilForm(perfil);
		this.cargarListaPerfiles();
		
		if(this.getPerfilForm().getId()==0){
			this.mostrarBotonGrabar=true;
			this.mostrarBotonEditar=false;
			this.mostrarBotonCancelar=false;
		}else{
			this.mostrarBotonGrabar=false;
			this.mostrarBotonEditar=true;
			this.mostrarBotonCancelar=true;
		}		
				
	}
	
	public void seleccionarRegistroConfirmacion(Perfil perfil){

		ReqContext = RequestContext.getCurrentInstance();
		
		this.setPerfilFormDelete(perfil);

		ReqContext.execute("PF('grpEliminarConfirmacion').show()");
		ReqContext.update("frmBandejaUltimoRevisor:idGrpEliminarConfirmacion");	
	}
	
	public boolean eliminar(){
		
		ReturnObject retObj = new ReturnObject();
		super.setUsuarioModificacion(this.getPerfilFormDelete());		
		try{
			retObj.setSw(this.getPerfilServiceRemote().eliminar(this.getPerfilFormDelete()));
			if(retObj.getSw()){
				super.setMensajeAviso("Perfil Eliminado ");	
				this.iniciarBandeja();
			} else {
				super.setMensajeError("Error al eliminar Perfil");
				return false;
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			super.setMensajeError("Error al eliminar Perfil");
			return false;
		}
		
		return true;
	}
	
	public boolean grabarPerfil(){
		
		ReturnObject retObj = new ReturnObject();

		if(this.getPerfilForm().getId()>0){			
				if (!this.validar()) {
					return false;
				}else{
					//edito
					super.setUsuarioModificacion(this.getPerfilForm());

					try{
						retObj.setSw(this.getPerfilServiceRemote().actualizar(this.getPerfilForm()));
						if(retObj.getSw()){
							super.setMensajeAviso("�xito al actualizar al Perfil ");	
							this.iniciarBandeja();
						} else {
							super.setMensajeError("Error al actualizar al Perfil");
							return false;
						}
					} catch (Exception e) {
						logger.error(e);
						e.printStackTrace();
						super.setMensajeError("Error al actualizar al Perfil");
						return false;
					}
				}
				
		}else{
			if (!this.validar()) {
				return false;
			}else{
				//inserto
				super.setUsuarioRegistro(this.getPerfilForm());
				try{
					retObj = this.getPerfilServiceRemote().insertar(this.getPerfilForm());
					if(retObj.getId() != 0){
						//correcto
						super.setMensajeAlerta("Grabaci�n con exito");
						this.iniciarBandeja();
					}else{
						//incorrecto
						super.setMensajeAlerta("Grabaci�n sin exito");
						return false;
					}
				} catch (Exception e) {
					logger.error(e);
					e.printStackTrace();
					super.setMensajeAlerta("Grabaci�n sin exito");
					return false;
				}
			}
		}
		return true;		
	}
	
	private boolean validar(){			
		
		if(VO.isEmpty(this.getPerfilForm().getNombre())){
			super.setMensajeAlerta("Ingrese nombre");
			return false;
		}		
		
		return true;
	}
	
	public void limpiarFormulario(){
		this.inicializarValores();
	}
	
	

	public boolean isMostrarBotonGrabar() {
		return mostrarBotonGrabar;
	}

	public void setMostrarBotonGrabar(boolean mostrarBotonGrabar) {
		this.mostrarBotonGrabar = mostrarBotonGrabar;
	}

	public boolean isMostrarBotonEditar() {
		return mostrarBotonEditar;
	}

	public void setMostrarBotonEditar(boolean mostrarBotonEditar) {
		this.mostrarBotonEditar = mostrarBotonEditar;
	}

	public boolean isMostrarBotonCancelar() {
		return mostrarBotonCancelar;
	}	

	public void setMostrarBotonCancelar(boolean mostrarBotonCancelar) {
		this.mostrarBotonCancelar = mostrarBotonCancelar;
	}
	
	public Perfil getPerfilForm() {
		return perfilForm;
	}

	public void setPerfilForm(Perfil perfilForm) {
		this.perfilForm = perfilForm;
	}

	public Perfil getPerfilFormDelete() {
		return perfilFormDelete;
	}

	public void setPerfilFormDelete(Perfil perfilFormDelete) {
		this.perfilFormDelete = perfilFormDelete;
	}

	public List<Perfil> getLstPerfil() {
		return lstPerfil;
	}

	public void setLstPerfil(List<Perfil> lstPerfil) {
		this.lstPerfil = lstPerfil;
	}

	public PerfilServiceRemote getPerfilServiceRemote() {
		return perfilServiceRemote;
	}

	public void setPerfilServiceRemote(PerfilServiceRemote perfilServiceRemote) {
		this.perfilServiceRemote = perfilServiceRemote;
	}
	
}

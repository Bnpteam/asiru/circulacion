package pe.gob.servir.sistemas.transparencia.presentacion.bandejas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.context.RequestContext;

import pe.gob.servir.sistemas.transparencia.constantes.Constantes;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.smtp.SmtpSendServiceImpl;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteDerivacion;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.SolicitudMovimiento;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.sistemas.transparencia.servicios.alfresco.AlfrescoBase;
import pe.gob.servir.sistemas.transparencia.servicios.alfresco.AlfrescoDocumentoAdjunto;
import pe.gob.servir.sistemas.transparencia.util.StringEncrypt;
import pe.gob.servir.systems.util.alfresco.Archivo;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.alfresco.ResultAlfresco;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "bandejaJefeOAJMB")
@SessionScoped
public class BandejaJefeOAJMB extends BasicMB {

	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(this.getClass());
	
	private ReporteBandeja       	reporteBandeja; //Para b�squeda.
	private List<ReporteBandeja> 	lstReporteBandeja;
	private ReporteBandeja		 	tmpFormularioBndRep; //Para Extraer a Formulario Bandeja.
	private List<T01Maestro> 	 	lstT01TipoDocumento;
	private List<T01Maestro> 	 	lstUndsOrgTodo; //Lista de Objetos T01Maestros
	private List<T01Maestro> 	 	lstUndsOrgCnSecrt; //Lista de Objetos T01Maestros
	private List<String> 		 	lstUndsOrgsStrg; //Lista de String T01Maestros --> Para CheckBoxMen�.
	private ReporteBandeja		 	rptBanjReenviar;
	private List<String>		 	lstUndsOrgsSlc;
	private boolean 			 	swAutorizaPersona;
	private String				 	txtUnidOrgDerivar;
	private String				 	txtUnidOrgDerivarCode;
	private List<ReporteDerivacion>	lstRptDerivacion; //Arreglo de objeto para mostrar la derivaci�n multiareas.
	private String				 	txtNumeroSolicitud;
	private List<T01Maestro>	 	lstPlantillaCorreo;
	RequestContext 				 	ReqContext;
	List<SolicitudMovimiento> 	 	lstSoliMoviXLineaTP;
	
	private List<UbigeoReniec>   	lstDepartamento;
	private List<UbigeoReniec>   	lstProvincia;
	private List<UbigeoReniec>   	lstDistrito;
	
	private boolean				 	vistaRegistroSelecc;
	
	private boolean				 	habilitaValidaInfoOAJ;//btn
	
	private boolean				 	vistaTramiteOAJ; //Panel
	
		private boolean				vistaRespuestaProyeccion;
		private boolean				habilitaRespuestaProyeccion;
	
		private boolean				vistaRespuestaUsuario;
		private boolean				habilitaRespuestaUsuario;
	
	private ReporteBandeja		 tmpReporteBandeja; //Ver detalle
	private SolicitudMovimiento	 solicitudMovimientoArea;
	private SolicitudMovimiento	 solicitudMovimientoAdministrado;
	private DetalleMovimiento	 detalleMovimiento;
	private String				 nombreAdjunto01;
	List<DetalleMovimiento> 	 lstDetalleMovimiento; //Listar documento adjuntados por el asistente.
	
	private int 				 cont = 1;
	private String				 mngAcordeon;
	private String				 nroSolicitudSelect;
	private boolean				 habilitaBtnAdjDoc;
	private int 				 cantMax;
	private int					 maxDocsAdjs;
	private boolean				 habilitaBtnDevolver;
	
	// Variables EJB.
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/BandejaServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote")
	private BandejaServiceRemote bandejaServiceRemote;
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoSolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote")
	private MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote;

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoDetalleServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote")
	private MovimientoDetalleServiceRemote movimientoDetalleServiceRemote;

	@PostConstruct
	public void init(){
		this.setHabilitaBtnDevolver(false);
		this.setCantMax(Integer.parseInt(super.getString("alfresco.cantidad.general")));
		this.setMaxDocsAdjs(this.getCantMax());
		this.setHabilitaBtnAdjDoc(false);
		this.setNroSolicitudSelect("");
		this.setLstSoliMoviXLineaTP(new ArrayList<SolicitudMovimiento>());
		this.setMngAcordeon(null);
		this.setSolicitudMovimientoArea(new SolicitudMovimiento());
		this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
		this.setReporteBandeja(new ReporteBandeja());
		this.getReporteBandeja().setEstado_solicitud_id(3L);
		Calendar cal= Calendar.getInstance();
		this.getReporteBandeja().setAnio_solicitud(String.valueOf(cal.get(Calendar.YEAR)));
		this.setTmpFormularioBndRep(new ReporteBandeja());
		this.setDetalleMovimiento(new DetalleMovimiento());
		this.setLstReporteBandeja(new ArrayList<ReporteBandeja>());
		this.setLstT01TipoDocumento(new ArrayList<T01Maestro>());
		this.setLstUndsOrgTodo(new ArrayList<T01Maestro>());
		this.setLstUndsOrgCnSecrt(new ArrayList<T01Maestro>());
		this.setLstDepartamento(new ArrayList<UbigeoReniec>());
		this.setLstProvincia(new ArrayList<UbigeoReniec>());
		this.setLstDistrito(new ArrayList<UbigeoReniec>());
		this.setVistaRegistroSelecc(true);
		this.setLstPlantillaCorreo(new ArrayList<T01Maestro>());
		this.setHabilitaValidaInfoOAJ(false);
		// ---->
		//CASO 1:
		this.setVistaTramiteOAJ(false); //Panel
			// A
			this.setVistaRespuestaProyeccion(false);
			this.setHabilitaRespuestaProyeccion(false);
			// B
			this.setVistaRespuestaUsuario(false);
			this.setHabilitaRespuestaUsuario(false);

		this.setLstUndsOrgsSlc(new ArrayList<String>());
		this.setLstUndsOrgsStrg(new ArrayList<String>());

		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		this.setLstRptDerivacion(new ArrayList<ReporteDerivacion>());
		this.setRptBanjReenviar(new ReporteBandeja());
	}
	
	public String regresarListado() {
		String pg = "bandejaJefeOAJ";
		this.buscarConsulta2();
		return pg;
	}
	
	public void cancelarSeleccionOAJ(){
		this.setTxtUnidOrgDerivarCode("");
		this.setTxtUnidOrgDerivar("");
		this.setHabilitaBtnDevolver(false);
		this.setMaxDocsAdjs(this.getCantMax());
		this.setNroSolicitudSelect("");
		this.setLstSoliMoviXLineaTP(new ArrayList<SolicitudMovimiento>());
		this.setMngAcordeon(null);
		this.setTmpFormularioBndRep(new ReporteBandeja());
		this.setSolicitudMovimientoArea(new SolicitudMovimiento());
		this.setSolicitudMovimientoAdministrado(new SolicitudMovimiento());
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		//this.habilitaPanelxArea();
		this.setVistaTramiteOAJ(false);//Panel
		this.setHabilitaValidaInfoOAJ(false);//BTn del panel
		this.setHabilitaRespuestaProyeccion(false);
		this.limpiarDerivacionArea();
		this.setHabilitaRespuestaUsuario(false);
		this.limpiarDerivacionAdministrado();
	}
	
	public void limpiarDerivacionArea(){
		this.setTxtUnidOrgDerivar("");
		this.setTxtUnidOrgDerivar(this.obtenerCadNombresCortsXCadCode(this.getLstUndsOrgCnSecrt(), this.getTmpFormularioBndRep().getListaUndOrgDeriv()));
		this.setLstUndsOrgsSlc(new ArrayList<String>());
		this.setLstUndsOrgsSlc(this.obtenerLstNombresLargsXCadCode(this.getLstUndsOrgCnSecrt(), this.getTmpFormularioBndRep().getListaUndOrgDeriv()));
		this.getSolicitudMovimientoArea().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
	}
	
	public void limpiarDerivacionAdministrado(){
		this.getSolicitudMovimientoAdministrado().setCorreo(this.getTmpFormularioBndRep().getCorreo_electronico());
		this.getSolicitudMovimientoAdministrado().setAsunto(this.getTmpFormularioBndRep().getAsunto());
		this.getSolicitudMovimientoAdministrado().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
		Long soliMoviId = this.getSolicitudMovimientoAdministrado().getId();
		this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));
		
		//Lista respuestas.
		int canDocsExtr = this.getLstDetalleMovimiento().size();
		if (this.getLstDetalleMovimiento().size() < this.getCantMax()) {
			this.setMaxDocsAdjs(this.getCantMax()-canDocsExtr);
			this.setHabilitaBtnAdjDoc(true);
		} else {
			this.setMaxDocsAdjs(0);
			this.setHabilitaBtnAdjDoc(false);
		}
		
		this.borrarAdjuntos();
	}

	public void llenarCombos() {
		this.setLstT01TipoDocumento(super.getT01MaestroValuesNL("T01_TIPO_DOCUMENTO_IDENTIDAD"));
		this.setLstUndsOrgTodo(new ArrayList<T01Maestro>());
		this.setLstUndsOrgTodo(super.getT01MaestroValuesNL("T01_UNIDAD_ORGANICA")); // Todas las unidades org para el formulario.
		this.setLstUndsOrgCnSecrt(new ArrayList<T01Maestro>());
		this.setLstUndsOrgCnSecrt(super.getUndOrg_CnSecretarias(4)); // 4: Perfil: Secreataria UO
		this.setLstUndsOrgsStrg(new  ArrayList<String>());
		for(T01Maestro prmT : this.getLstUndsOrgCnSecrt()){
			this.getLstUndsOrgsStrg().add(prmT.getNombreLargo());
		}
	}
	
	public void listarDepartamentos(String departamento){
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(departamento);
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");
		try {
			this.setLstDepartamento(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	public void cargarListaConsultas(){
		try {
			this.setVistaRegistroSelecc(true);
			this.setHabilitaRespuestaProyeccion(false);
			this.setHabilitaRespuestaUsuario(false);
			this.cancelarSeleccionOAJ();
			//EJB Listar.
			this.buscarConsulta2(); // <--- Listar.
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Ocurri� un error al cargar las solicitudes.");
		}
	
	//super.calcularAlturaTabla();
	}
	
	public void updateProvincia(String codDep, String codProv){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(codDep);
		tmpubigeo.setCodpro(codProv);
		tmpubigeo.setCoddis("00");		
		try {
			this.setLstProvincia(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}

	public void updateDistrito(String codDep, String codProv, String codDis){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(codDep);
		tmpubigeo.setCodpro(codProv);
		tmpubigeo.setCoddis(codDis);		
		try {
			this.setLstDistrito(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	//VER2BL
	public void verRespuestaProyeccion() {
		
		this.setHabilitaRespuestaProyeccion(true);
		this.setHabilitaRespuestaUsuario(false);
	}
	
	public void verRespuestaUsuario() {
		this.setHabilitaRespuestaProyeccion(false);
		this.setHabilitaRespuestaUsuario(true);
	}
	
	// Mostrar el icono de '+' Seleccionar y tmb sirve para el Icono Reenviar (Pero se interpreta al rev�s true/false)
	public boolean mostrarBtnXSituSoli(ReporteBandeja prmReporteBndj) {
		boolean result = false;
		Long situacionSolicitud = prmReporteBndj.getSituacion_solicitud_id();
		if( situacionSolicitud == 1L || situacionSolicitud == 2L){ // 1: Registrado o 2: EN TRAMITE
			result = true; //Mostrar
		} else {
			result = false; // No Mostrar
		}
		
		return result;
	}
	// REENVIAR MENSAJE
	public void reenviarCorreo(ReporteBandeja prmReporteBndj){
		try {
			ReporteBandeja tmpReporteBanj = new ReporteBandeja();
			//EJB Listar �ltimo envio de msj.
			tmpReporteBanj = this.getBandejaServiceRemote().lstUltimoEnvioMsj(prmReporteBndj);
			
			if(tmpReporteBanj != null){
				this.setRptBanjReenviar(tmpReporteBanj);
				ReqContext = RequestContext.getCurrentInstance();
				ReqContext.execute("PF('idGrpConfirmar3').show()");
			} else {
				super.setMensajeAlerta("No se pudo recuperar el �ltimo env�o de correo del reporte.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al recuperar el reporte para el reenv�o.!");
		}
	}
	
	public void reenviarMsj(){
		
		//super.setMensajeAviso("Reenviado �xito: " + this.getRptBanjReenviar().getSolicitud_transparencia_id());
		String asunto = this.getRptBanjReenviar().getAsunto();
		String correo = this.getRptBanjReenviar().getCorreo();
		String cuerpo = this.getRptBanjReenviar().getDetalle_asignacion();
		
		Long   solTranId = this.getRptBanjReenviar().getSolicitud_transparencia_id();
		
		Long soliMoviId = this.getRptBanjReenviar().getSolicitud_movimiento_id();
		List<DetalleMovimiento> tmpDetaMovi = new ArrayList<DetalleMovimiento>();
		tmpDetaMovi = this.listarDocumentosIngresados(soliMoviId);
		// Item para poder visualizar lo que se reenv�a.
		ArrayList<String> tmpLstUuidds = new ArrayList<String>();
		
		if (tmpDetaMovi.size() > 0) {
			for( DetalleMovimiento varDetaMovi : tmpDetaMovi ){	
				tmpLstUuidds.add(varDetaMovi.getUuidDocumento());
			}
		}
		//El cuerpo ya est� establecido.
		Boolean envioSmtp = enviarCorreoAdministrado(asunto, correo, cuerpo, tmpLstUuidds, solTranId);
		
		if(envioSmtp){
			super.setMensajeAviso("Se ha reenviado el correo al administrado.");
		}
		
		//Update movimiento que reenvio que se emiti�.
	}
	// ++
	public void seleccionarRegistro(ReporteBandeja prmRptBan){
		try {
			if (prmRptBan!=null) {
				this.cancelarSeleccionOAJ();
				this.setTmpFormularioBndRep(new ReporteBandeja());
				String codeModoEnv = prmRptBan.getForma_de_entrega();
				prmRptBan.setForma_de_entrega(codeModoEnv);
				this.setTmpFormularioBndRep(prmRptBan);
				this.updateAutoPersonaPanel();
				this.listarDepartamentos(this.getTmpFormularioBndRep().getCoddep());
				this.updateProvincia(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro());
				this.updateDistrito(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro(),this.getTmpFormularioBndRep().getCoddis());
				//Id de transparencia
				this.getSolicitudMovimientoArea().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
				this.getSolicitudMovimientoAdministrado().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
				
				this.setHabilitaValidaInfoOAJ(true); //Botones selectores.
				this.setVistaTramiteOAJ(true); // Panel de pesta�as. ---
				this.setNroSolicitudSelect("(Nro "+this.getTmpFormularioBndRep().getSolicitud_transparencia_id()+")");
				// Listar linea de tiempo.
				try {
					SolicitudMovimiento tmpSolicitudMovimiento = new SolicitudMovimiento();
					tmpSolicitudMovimiento.setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
					tmpSolicitudMovimiento.setT01UndOrgRecId("8"); //this.getTmpFormularioBndRep().getT01_und_org_receptor_id()
					
					this.setLstSoliMoviXLineaTP(this.getMovimientoSolicitudServiceRemote().lstLinTiempoXAreas1(tmpSolicitudMovimiento, "1,2,3,11,12"));
					
					/*if (this.getLstSoliMoviXLineaTP().size() > 0) {
						super.setMensajeAviso("�xito al cargar la l�nea de tiempo.");
					} else {
						super.setMensajeAlerta("No se han encontrado coincidencias para formar la l�nea de tiempo.");
					}*/
				} catch (Exception e) {
					e.printStackTrace();
					super.setMensajeAlerta("Error al cargar l�nea de tiempo.");
				}
				
				Long idEstadoSolicitud = this.getTmpFormularioBndRep().getEstado_solicitud_id();
				
				if (idEstadoSolicitud == 3L) { // 3:Derivaci�n �rea
					this.setHabilitaRespuestaProyeccion(true);
					this.setHabilitaRespuestaUsuario(false);
					this.getSolicitudMovimientoArea().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
					this.setTxtUnidOrgDerivar(this.getTmpFormularioBndRep().getListaUndOrgDeriv());
					//Preseleccionar lasunidades a derivar.
					this.setLstUndsOrgsSlc(this.obtenerLstNombresLargsXCadCode(this.getLstUndsOrgCnSecrt(), this.getTxtUnidOrgDerivar()));
					this.setTxtUnidOrgDerivar(this.obtenerCadNombresCortsXCadCode(this.getLstUndsOrgCnSecrt(), this.getTxtUnidOrgDerivar()));
					//this.obtenerLstNombresLargsXCode
					this.setHabilitaBtnDevolver(false);
					this.setMngAcordeon("0");
					super.setMensajeAviso("�xito al extraer la solicitud del tipo Proyectado Derivaci�n a Jefe OAJ.");
					
				} else if (idEstadoSolicitud == 2L || idEstadoSolicitud == 9L || idEstadoSolicitud == 16L) { // 2: Observaci�n, 9: Respuesta Final, 16: Respuesta Negaci�n.
						this.verRespuestaUsuario(); //Btn.
						//Le entrega el Id al objeto Solicitud Movimiento.
						this.getSolicitudMovimientoAdministrado().setId(this.getTmpFormularioBndRep().getSolicitud_movimiento_id());
						this.getSolicitudMovimientoAdministrado().setCorreo(this.getTmpFormularioBndRep().getCorreo());
						this.getSolicitudMovimientoAdministrado().setAsunto(this.getTmpFormularioBndRep().getAsunto());
						this.getSolicitudMovimientoAdministrado().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
						// El SolicitudMovimientoAdministrado debe tener ID;
						//Id entrega el SOLICITUD_MOVIMIENTO_ID del detalle derivaci�n para hacer las b�squedas.
						this.getSolicitudMovimientoAdministrado().setId(this.getTmpFormularioBndRep().getSolicitud_movimiento_id());
						//this.setMaximoDocumentoAdj(5); //Setear maximo documento adjuntados.
						Long soliMoviId = this.getSolicitudMovimientoAdministrado().getId();
						this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));
						
						int cantDocExtr = this.getLstDetalleMovimiento().size();
						if (cantDocExtr < this.getCantMax()) {
							this.setHabilitaBtnAdjDoc(true);
							
							this.setMaxDocsAdjs(this.getCantMax()-cantDocExtr); // 5 - X
						} else {
							this.setHabilitaBtnAdjDoc(false);
							this.setMaxDocsAdjs(0); // Si es bloqueado el bot�n entonces es 0. Msj.
						}
						
						this.setHabilitaBtnDevolver(true);
						this.setMngAcordeon("0");
						super.setMensajeAviso("�xito al extraer la solicitud del tipo Proyectado Respuesta a Jefe OAJ.");
				} else if (idEstadoSolicitud == 4L  || // 4: Notificado para Subsanaci�n.
						   idEstadoSolicitud == 10L || // 10: Respuesta Final
						   idEstadoSolicitud == 18L) { // 18: Respuesta Negaci�n
					
					this.verRespuestaUsuario(); //Btn.
					//Le entrega el Id al objeto Solicitud Movimiento.
					this.getSolicitudMovimientoAdministrado().setId(this.getTmpFormularioBndRep().getSolicitud_movimiento_id());
					this.getSolicitudMovimientoAdministrado().setCorreo(this.getTmpFormularioBndRep().getCorreo());
					this.getSolicitudMovimientoAdministrado().setAsunto(this.getTmpFormularioBndRep().getAsunto());
					this.getSolicitudMovimientoAdministrado().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
					// El SolicitudMovimientoAdministrado debe tener ID;
					//Id entrega el SOLICITUD_MOVIMIENTO_ID del detalle derivaci�n para hacer las b�squedas.
					this.getSolicitudMovimientoAdministrado().setId(this.getTmpFormularioBndRep().getSolicitud_movimiento_id());
					//this.setMaximoDocumentoAdj(5); //Setear maximo documento adjuntados.
					Long soliMoviId = this.getSolicitudMovimientoAdministrado().getId();
					this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));
					
					int cantDocExtr = this.getLstDetalleMovimiento().size();
					if (cantDocExtr < this.getCantMax()) {
						this.setHabilitaBtnAdjDoc(true);
						
						this.setMaxDocsAdjs(this.getCantMax()-cantDocExtr); // 5 - X
					} else {
						this.setHabilitaBtnAdjDoc(false);
						this.setMaxDocsAdjs(0); // Si es bloqueado el bot�n entonces es 0. Msj.
					}
					
					this.setHabilitaBtnDevolver(false);
					this.setMngAcordeon("0");
					super.setMensajeAviso("�xito al extraer la solicitud notificada.");
				} else {
						super.setMensajeAlerta("No se ha encontrado una acci�n extra para la presente categor�a.");
						this.setMngAcordeon(null);
				} //4 y el 10 Solo son reenvio de correo no necesita seleccionar. ni habilitar este bot�n.
				
				if (this.getLstDetalleMovimiento().size() < this.getCantMax()) {
					this.setHabilitaBtnAdjDoc(true);
				} else {
					this.setHabilitaBtnAdjDoc(false);
				}
				
			} else {
				super.setMensajeAviso("No se ha encontrado una solicitud.");
			}
		} catch (Exception e){
			logger.error("Seleccionar registro Jefe: " + e.getMessage());
			super.setMensajeAlerta("Ocurri� un error al seleccionar registro.");
		}
	}
	/*
	// Obtener un la lista nombres (LstSelect) a apartir una Cadena de nombres.
	public ArrayList<String> obtenerLstNombresXCadenaNombres(String prmK) {
		//Inicializa variable.
		ArrayList<String> resultado = new ArrayList<String>();
		if(!VO.isEmpty(prmK)){
			StringTokenizer tokens = new StringTokenizer(prmK,";");
			while(tokens.hasMoreTokens()){
				String var = tokens.nextToken().trim();
				
				if(!VO.isEmpty(var)){ resultado.add(var); }
		    }
		}
		return resultado;
	}*/
	// Obtener un la lista nombres largos (LstSelect) a apartir una Cadena de c�digos.
	public ArrayList<String> obtenerLstNombresLargsXCadCode(List<T01Maestro> prmT01Maestro, String prmK) {
		//Inicializa variable.
		ArrayList<String> resultado = new ArrayList<String>();
		if(!VO.isEmpty(prmK)){
			StringTokenizer tokens = new StringTokenizer(prmK,";");
			while(tokens.hasMoreTokens()){
				String var = tokens.nextToken().trim();
				
				if(!VO.isEmpty(var)){
					T01Maestro tmpMaestro = this.getObtenerMaestroUOXCode(prmT01Maestro, var);
					if(tmpMaestro != null){
						resultado.add(tmpMaestro.getNombreLargo());
					}
				}
		    }
		}
		return resultado;
	}
	
	// Obtener una cadena de nombres cortos (LstSelect) a apartir una Cadena de c�digos.
	public String obtenerCadNombresCortsXCadCode(List<T01Maestro> prmT01Maestro, String prmK) {
		//Inicializa variable.
		String resultado = "";
		if(!VO.isEmpty(prmK)){
			this.setTxtUnidOrgDerivarCode(prmK);
			StringTokenizer tokens = new StringTokenizer(prmK,";");
			while(tokens.hasMoreTokens()){
				String var = tokens.nextToken().trim();
				
				if(!VO.isEmpty(var)){
					T01Maestro tmpMaestro = this.getObtenerMaestroUOXCode(prmT01Maestro, var);
					if(tmpMaestro != null){
						if (VO.isEmpty(resultado)) {
							resultado = tmpMaestro.getNombreCorto();
						} else {
							resultado = resultado + "; " + tmpMaestro.getNombreCorto();
						}
					}
				}
		    }
		}
		
		return resultado;
	}

	//Necesita solo la Id de solicitudMovimientoId
	public List<DetalleMovimiento> listarDocumentosIngresados(Long SoliMoviId){
		List<DetalleMovimiento> tmpLstDetalleMov = new ArrayList<DetalleMovimiento>();
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());

		try {
			
			tmpLstDetalleMov = this.getMovimientoDetalleServiceRemote().lstDocumentosIngresados(SoliMoviId);
			
			/*int canDocIng = this.getLstDetalleMovimiento().size();
			this.setMaximoDocumentoAdj(this.getMaximoDocumentoAdj() - canDocIng);
			
			if(this.getMaximoDocumentoAdj() == 0){
				this.setVistaAdjuntarDoc(false);
			} else {
				this.setVistaAdjuntarDoc(true);
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al cargar los documentos Adjuntados Proyecci�n Respuesta.");
		}
		return tmpLstDetalleMov;
	}
	
	
	public void setRptDocumento(DetalleMovimiento prmDetalleMovimiento) {
		DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
		tmpDetalleMovimiento.setId(prmDetalleMovimiento.getId());
		
		this.setDetalleMovimiento(prmDetalleMovimiento);
		/*
		try {
			this.setDetalleMovimiento(this.getMovimientoDetalleServiceRemote().buscarXId(tmpDetalleMovimiento));
		} catch (Exception e) {
			super.setMensajeError("Error al recuperar documento ");
			logger.error(e);
		}*/
	}
	
	// ELIMINAR DOCUMENTO.
	public String eliminar() {
		boolean sw = false;
		/*try {*/
			//this.getDetalleMovimiento().setUsuarioIdModificacion(this.getUsuario().getId());
		int cantInicial = this.getLstDetalleMovimiento().size();
		this.getLstDetalleMovimiento().remove(this.getDetalleMovimiento());
		int cantFinal = this.getLstDetalleMovimiento().size();
			
		if (cantFinal != cantInicial) { //Si cantFinal es  menor que cantInicial significa que se ha eliminado de la lista.
			sw = true;
		}
		
		if (cantFinal < this.getCantMax()) {
			this.setMaxDocsAdjs(this.getCantMax()-cantFinal);
			this.setHabilitaBtnAdjDoc(true);
		} else {
			this.setMaxDocsAdjs(0);
			this.setHabilitaBtnAdjDoc(false);
		}
		
		this.borrarAdjuntos();
			
			/*
			sw = this.getMovimientoDetalleServiceRemote().eliminar(this.getDetalleMovimiento());*/
		if (sw) {
			super.setMensajeAviso("�xito al eliminar Documento.");
				//this.setMaximoDocumentoAdj(5); //Setear maximo documento adjuntados.
				/*Long soliMoviId = this.getSolicitudMovimientoAdministrado().getId();
				this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));*/
				
		} else {
			super.setMensajeError("Error al eliminar Documento");
		}
		/*} catch (Exception e) {
			super.setMensajeError("Error al eliminar Documento CHT");
			logger.error(e);
		}*/

		return "bandejaJefeOAJ";
	}

	public void limpiarBusqueda2(){
		this.setReporteBandeja(new ReporteBandeja());
		this.getReporteBandeja().setEstado_solicitud_id(3L);
		Calendar cal= Calendar.getInstance();
		this.getReporteBandeja().setAnio_solicitud(String.valueOf(cal.get(Calendar.YEAR)));
	}
	
	// Obtener el Maestro por el C�digo.
	private T01Maestro getObtenerMaestroUOXCode(List<T01Maestro> prmT01Maestro, String codigo) {
		
		for (T01Maestro t01Maestro : prmT01Maestro) {
			if (t01Maestro.getCodigoRegistro().equalsIgnoreCase(codigo)) {
				return t01Maestro;
			}
		}
			return null;
	}
	
	// Obtener el Maestro por el Nombre.
	private T01Maestro getObtenerMaestroUOXNombre(List<T01Maestro> prmT01Maestro, String codigo) {

		for (T01Maestro t01Maestro : prmT01Maestro) {
			if (t01Maestro.getNombreLargo().equalsIgnoreCase(codigo)) {
					return t01Maestro;
			}
		}
		return null;
	}
		
	// +UPDDATE
	public void updateUnidOrgDerivar(){
		//boolean selectIs = true;
		String dataLetra = "";
		String dataCode  = "";
		List<T01Maestro> tmpLstUniOrg = new ArrayList<T01Maestro>();
		for (int cont = 0; cont < this.getLstUndsOrgsSlc().size(); cont++) {
				T01Maestro T01UndOrg = this.getObtenerMaestroUOXNombre(this.getLstUndsOrgCnSecrt(), this.getLstUndsOrgsSlc().get(cont));
				tmpLstUniOrg.add(T01UndOrg);
		}
			
		for (T01Maestro undOrg : tmpLstUniOrg) {
			if (VO.isEmpty(dataLetra.trim())) {
					dataLetra = undOrg.getNombreCorto();
			} else {
					dataLetra = dataLetra + "; " + undOrg.getNombreCorto();
			}
				
			if (VO.isEmpty(dataCode.trim())) {
					dataCode = undOrg.getCodigoRegistro();
			} else {
					dataCode = dataCode + "; " + undOrg.getCodigoRegistro();
			}
		}
			
		this.setTxtUnidOrgDerivar(dataLetra);
		this.setTxtUnidOrgDerivarCode(dataCode);
	}

	public void updateAutoPersonaPanel(){
		if(this.getTmpFormularioBndRep().getFlag_autoriza_recojo().equals("SI")){
			this.setSwAutorizaPersona(true);
		} else {
			this.setSwAutorizaPersona(false);
		}
	}
	
	
	
	public boolean validarDerivPreArea(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivArea()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		
		return false;
		}
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar1').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}

	// DERIVADO SECRETARIA �REA.
	public String grabar1() {

		String pagina = "bandejaJefeOAJ";

		ReturnObject sw = new ReturnObject();

		if (validarDerivArea()) {

			try {
				this.setLstRptDerivacion(new ArrayList<ReporteDerivacion>());
				String unidOrgADerivar = this.getTxtUnidOrgDerivarCode(); // 1; 2; 3 Und Org.
				this.setTxtNumeroSolicitud(this.getSolicitudMovimientoArea().getSolicitudTransparenciaId().toString());
				StringTokenizer tokens = new StringTokenizer(unidOrgADerivar,";"); 
				
				while(tokens.hasMoreTokens()){
					String var = tokens.nextToken().trim();
					
					if(!VO.isEmpty(var)){
						this.getSolicitudMovimientoArea().setUsuarioIdEmisor(this.getUsuario().getId());
						this.getSolicitudMovimientoArea().setPerfilIdEmisor(3L); //3: Jefe OAJ
						String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
						this.getSolicitudMovimientoArea().setT01UndOrgEmiId(unOrgEmisorId); //
						this.getSolicitudMovimientoArea().setT01UndOrgRecId(var); //Multiples �rea pero enviado como texto SJTI no como c�digo 14.
						this.getSolicitudMovimientoArea().setPerfilIdReceptor(4L); //4: Secretaria Organo.
						this.getSolicitudMovimientoArea().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
						this.getSolicitudMovimientoArea().setUsuarioIdRegistro(this.getUsuario().getId());
						this.getSolicitudMovimientoArea().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
						this.getSolicitudMovimientoArea().setEstadoSolicitudId("5"); // 5: Asignado a �rgano responsable.

						sw = this.getMovimientoSolicitudServiceRemote().insertarMultiDesdeJefe(this.getSolicitudMovimientoArea());
						ReporteDerivacion tmpDetalleDerivado = new ReporteDerivacion();
						
						if (sw.getId() != 0) {
							//tmpDetalleDerivado.setNroSolicitudId(this.getSolicitudMovimientoArea().getSolicitudTransparenciaId().toString());
							tmpDetalleDerivado.setAreaDerivada(var);
							tmpDetalleDerivado.setPersonaDerivada(sw.getMsg());
							tmpDetalleDerivado.setEstadoDerivacion("Derivado con �xito.");
							
							this.getLstRptDerivacion().add(tmpDetalleDerivado);
							
						} else {
							tmpDetalleDerivado.setAreaDerivada(var);
							tmpDetalleDerivado.setPersonaDerivada("Ninguno");
							tmpDetalleDerivado.setEstadoDerivacion("Error de derivaci�n.");
							
							this.getLstRptDerivacion().add(tmpDetalleDerivado);
						}
					}
			    }
				
				ReqContext = RequestContext.getCurrentInstance();
				ReqContext.execute("PF('idGrpConfirmDeriv').show()");
				
				if(this.getLstRptDerivacion().size() > 0){ //Siempre estar� lleno bien de exitos o fallas.
					super.setMensajeAviso("�xito al asignar a Unidad Organica Responsable.");
				} else {
					super.setMensajeAviso("Error al asignar a Unidad Organica Responsable.");
				}
				
				this.cancelarSeleccionOAJ();
				this.buscarConsulta2();
				
			} catch (Exception e) {
				super.setMensajeError("Error al asignar a �rgano Responsable.");
				logger.error(e);
			}

		}
		return pagina;
	}
	
	// DERIVADO OBSERVACION AADMINISTRADO.
	public String grabar2() {

		String pagina = "bandejaJefeOAJ";

		ReturnObject sw = new ReturnObject();
		
		int cantDocs = this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().size();

		if (validarDerivAdministrado()) {
			
			boolean swAlfresco = true;
				
			String nameFileAlfrescoError = "";
			
			
			if ( cantDocs > 0) {
				
				for(DetalleMovimiento tmpDetalleMov : this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas()) {
					
					AlfrescoDocumentoAdjunto alfrescoAdjunto = new AlfrescoDocumentoAdjunto();
	
					ResultAlfresco resultAlfresco01 = this.subirArchivoAlfresco(alfrescoAdjunto, tmpDetalleMov.getFileAlfresco());
	
					if (resultAlfresco01.getCodigo().equals(Constantes.OPERACION_EXITOSA_ALFRESCO)) {
						tmpDetalleMov.getFileAlfresco().setUiid(resultAlfresco01.getUiid());
						tmpDetalleMov.getFileAlfresco().setRawFile(null);
						tmpDetalleMov.setUuidDocumento(resultAlfresco01.getUiid());
					} else {
						if(VO.isEmpty(nameFileAlfrescoError)){
							nameFileAlfrescoError = tmpDetalleMov.getFileAlfresco().getFileName();
						} else {
							nameFileAlfrescoError = nameFileAlfrescoError + "; " + tmpDetalleMov.getFileAlfresco().getFileName();
						}
						
						swAlfresco = false;
	
						logger.error(resultAlfresco01.getCodigo() + " : " + resultAlfresco01.getDescripcion());
						logger.error("Nombre Documeto" + tmpDetalleMov.getFileAlfresco().getFileName());
						
					}
					
				}
			}
				
			if (!swAlfresco) {
				this.setMensajeError("Ocurri� un error al cargar el/los archivo(s) : " + nameFileAlfrescoError);
				return "";
			}
			
			try {
				this.getSolicitudMovimientoAdministrado().setUsuarioIdEmisor(this.getUsuario().getId());
				this.getSolicitudMovimientoAdministrado().setPerfilIdEmisor(3L); //3: Jefe OAJ
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMovimientoAdministrado().setT01UndOrgEmiId(unOrgEmisorId); //Emisor: 8 Misma �rea
				this.getSolicitudMovimientoAdministrado().setT01UndOrgRecId("8"); //�rea del usuario OAJ.
				this.getSolicitudMovimientoAdministrado().setPerfilIdReceptor(3L); //Para el 3: Jefe OAJ Para �l mismo no perder la solicitud. cuando se le env�a al administrado
				this.getSolicitudMovimientoAdministrado().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
				this.getSolicitudMovimientoAdministrado().setUsuarioIdRegistro(this.getUsuario().getId());
				//Valida que tipo de respuesta debe enviar.
				Long estadoSolicitud = this.getTmpFormularioBndRep().getEstado_solicitud_id();
				if ( estadoSolicitud == 2L || estadoSolicitud == 4L) { // 2: RESPUESTA DE OBSERVACION, 18: Rpta. Observaci�n Notificada. REENVIO
					this.getSolicitudMovimientoAdministrado().setSituacionSolicitudId("3"); // 3: OBSERVACION NOTIFICADO.
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("4"); // 4: Notificado para Subsanaci�n.
				} else if (estadoSolicitud == 9L || estadoSolicitud == 10L) { // 9: RESPUESTA FINAL, 10: Rpta. Final Notificada. REENVIO
					this.getSolicitudMovimientoAdministrado().setSituacionSolicitudId("4"); // 4: RESPUESTA NOTIFICADO.
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("10"); // 10: Respuesta enviada al usuario.
				} else if (estadoSolicitud == 16L || estadoSolicitud == 18L) { // 16: RESPUESTA NEGACION FINAL, 18: Rpta. Negaci�n Notificada. REENVIO
					this.getSolicitudMovimientoAdministrado().setSituacionSolicitudId("4"); // 4: RESPUESTA NOTIFICADO.
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("18"); // 10: Respuesta enviada al usuario.
				}
					
				// Separando los documentos recien adjuntados antes de grabar en BD.
				SolicitudMovimiento tmpSolicMov = new SolicitudMovimiento();
				
				if (this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().size() > 0) {
					
					tmpSolicMov.setListaDocsRespuestas(this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas());
					this.getSolicitudMovimientoAdministrado().setListaDocsRespuestas(new ArrayList<DetalleMovimiento> ());
				}
				
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMovimientoAdministrado());
				
				int nroFal = 0;
				
				SolicitudMovimiento tmpListaDocumentoFinal = new SolicitudMovimiento();
				
				if (sw.getId() != 0) {
					int cantDocsTraidos = this.getLstDetalleMovimiento().size();
					//AQUI INSERCION DE DOCUMENTOS.
					if(cantDocs > 0 || cantDocsTraidos > 0){ //Si tiene documentos adjuntos guarda primero O si tiene documentos que han sido traidos.
						//VER MANERA DE USAR VARIABLES EFICIENTEMENTE.
						ReturnObject sw2 = new ReturnObject();
						try {
							
							//Solo si trajo documentos que subi� el anterior usuario se a�ade a los que subi� el actual usuario.
							if(this.getLstDetalleMovimiento().size() > 0) { //Documentos extraidos
								for (DetalleMovimiento tmpDetaMovi: this.getLstDetalleMovimiento()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							
							if(tmpSolicMov.getListaDocsRespuestas().size() > 0) { //Documentos recien subidos.
								for (DetalleMovimiento tmpDetaMovi: tmpSolicMov.getListaDocsRespuestas()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							
							for (DetalleMovimiento tmpDetalleMov3 : tmpListaDocumentoFinal.getListaDocsRespuestas()){
							
								DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
								
								tmpDetalleMovimiento.setSolicitudMovimientoId(sw.getId()); 
								tmpDetalleMovimiento.setTipoDocumentoId("17"); // TIPO DOCUMENTO: OTROS
								tmpDetalleMovimiento.setNombreDocumento(tmpDetalleMov3.getNombreDocumento());
								tmpDetalleMovimiento.setUuidDocumento(tmpDetalleMov3.getUuidDocumento());
								tmpDetalleMovimiento.setUsuarioIdRegistro(this.getUsuario().getId());
								sw2 = this.getMovimientoDetalleServiceRemote().insertar(tmpDetalleMovimiento);
								//Contador de fallas.
								if (sw2.getId() == 0) { nroFal ++; }
							}
							
							if (nroFal > 0 ) { //Se hizo el movimiento pero no adjunt�.
								super.setMensajeAlerta("Error al adjuntar documentos, en total: " + nroFal + "no han podido ser adjuntados.");
								this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
								this.buscarConsulta2(); // <--- Listar. 
								return pagina;
							}
						} catch (Exception e) {
							super.setMensajeAlerta("Error al adjuntar documentos."); //Se hizo el movimiento pero no adjunt�.
							logger.error(e);
							this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
							this.buscarConsulta2(); // <--- Listar. 
							return pagina;
						}
						
					}
					/****************************************/
					ArrayList<String> tmpLstUuidds = new ArrayList<String>();
					
					// Enviar todo los documentos que han sido subids a la base de datos y se encuentran en este temporal.
					if (tmpListaDocumentoFinal.getListaDocsRespuestas().size() > 0) {
						for( DetalleMovimiento tmpDetaMovi : tmpListaDocumentoFinal.getListaDocsRespuestas() ){	
							tmpLstUuidds.add(tmpDetaMovi.getUuidDocumento());
						}
					}
					
					// SMTP Si no hubo ninguna falla se le env�a correo Administrado. tmpLstUuidds si tmpLstUuidds est� vac�o el metodo lo interpreta solo.
					if ( nroFal == 0 ) {
						Boolean envioCorreo = 	this.enviarCorreoAdministrado(
									 			this.getSolicitudMovimientoAdministrado().getAsunto()
									 			,this.getSolicitudMovimientoAdministrado().getCorreo()
									 			,this.getSolicitudMovimientoAdministrado().getDetalleRespuesta()
									 			,tmpLstUuidds
									 			,this.getSolicitudMovimientoAdministrado().getSolicitudTransparenciaId());
							
						if (envioCorreo) {
							//super.setMensajeAviso("Se ha enviado un correo al administrado");
							super.setMensajeAviso("�xito al enviar la Notificaci�n al Administrado.");
						}
						
					} else {
						/*super.setMensajeAlerta("No se ha enviado un correo al administrado, "
								+ "por que algunos documentos han fallado en subir o registrarce en la base de datos.");*/
						super.setMensajeAlerta("Se ha hecho el movimiento pero no se ha enviado un correo de notificaci�n al administrado,"
								+ "				debido a que no se han adjuntado correctamente los documentos.");
					}
					
					this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
					this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar. 
					
				} else {
					super.setMensajeAlerta("Error al enviar la Notificaci�n al Administrado.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al enviar la Notificaci�n al Administrado.");
				logger.error(e);
				this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
				this.buscarConsulta2(); // <--- Listar. 
			}

		}
		return pagina;
	}
	
	public boolean validarDevolucionDervArea(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivArea()) {
			//ReqContext.update(":frmRegSolicitud:mensaje");
			return false;
		}
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar4').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	// DEVUELTO PROYECCION DERIVADO AREA
	public String grabar3() {

		String pagina = "bandejaJefeOAJ";

		ReturnObject sw = new ReturnObject();
		if (this.validarDerivArea() && !VO.isEmpty(this.getSolicitudMovimientoArea().getDetalleAsignacion())) {

			try {
				this.getSolicitudMovimientoArea().setUsuarioIdEmisor(this.getUsuario().getId());
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMovimientoArea().setPerfilIdEmisor(3L); //3: Jefe OAJ.
				this.getSolicitudMovimientoArea().setT01UndOrgEmiId(unOrgEmisorId); //Emisor: Misma �rea.
				this.getSolicitudMovimientoArea().setT01UndOrgRecId("8"); // Devolver Jefe a su asistente misma �rea 8.
				this.getSolicitudMovimientoArea().setPerfilIdReceptor(2L); //2: Asistente OAJ.
				this.getSolicitudMovimientoArea().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
				this.getSolicitudMovimientoArea().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMovimientoArea().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				this.getSolicitudMovimientoArea().setEstadoSolicitudId("11"); // 11: Devuelto por Jefe.
				this.getSolicitudMovimientoArea().setListaProyeccionUndOrg(this.getTxtUnidOrgDerivarCode());
					//this.parsearDocumento();

					sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMovimientoArea());

				if (sw.getId() != 0) {
					super.setMensajeAviso("�xito al devolver la solicitud Proyectada.");
					this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar.
				} else {
					super.setMensajeError("Error al devolver la solicitud Proyectada.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al devolver la solicitud Proyectada.");
				logger.error(e);
			}

		} else {
			super.setMensajeAlerta("Por favor, ingrese el motivo de su devoluci�n.");
		}
		return pagina;
	}
	
	// DEVUELTO PROYECCION OBSERVACION ADMINISTRADO/ PROYECCI�N RESPUESTA ADMINISTRADO
	public String grabar4() {

		String pagina = "bandejaJefeOAJ";

		ReturnObject sw = new ReturnObject();
		//Listade documentos subido con el FileUpload;
		int cantDocs = this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().size();

		if (validarDerivAdministrado() && !VO.isEmpty(this.getSolicitudMovimientoAdministrado().getDetalleAsignacion())) {
				
			boolean swAlfresco = true;
				
			String nameFileAlfrescoError = "";
			
			if ( cantDocs > 0) {
				
				for(int i = 0; i < cantDocs; i++) {
					
					AlfrescoDocumentoAdjunto alfrescoAdjunto = new AlfrescoDocumentoAdjunto();
	
					ResultAlfresco resultAlfresco01 = this.subirArchivoAlfresco(alfrescoAdjunto, 
							this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco());
	
					if (resultAlfresco01.getCodigo().equals(Constantes.OPERACION_EXITOSA_ALFRESCO)) {
						this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().setUiid(resultAlfresco01.getUiid());
						this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().setRawFile(null);
						this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).setUuidDocumento(resultAlfresco01.getUiid());
					} else {
						if(VO.isEmpty(nameFileAlfrescoError)){
							nameFileAlfrescoError = this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().getFileName();
						} else {
							nameFileAlfrescoError = nameFileAlfrescoError + "; " +this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(i).getFileAlfresco().getFileName();
						}
						
						swAlfresco = false;
	
						logger.error(resultAlfresco01.getCodigo() + " : " + resultAlfresco01.getDescripcion());
						logger.error("El documento nro: " + i 
									 + "Nombre Documeto" + this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().get(0).getFileAlfresco().getFileName());
						
					}
					
				}
			}
				
			if (!swAlfresco) {
				this.setMensajeError("Ocurri� un error al cargar el/los archivo(s) : " + nameFileAlfrescoError);
				return "";
			}
			
			try {
				this.getSolicitudMovimientoAdministrado().setUsuarioIdEmisor(this.getUsuario().getId());
				this.getSolicitudMovimientoAdministrado().setPerfilIdEmisor(3L); //3: Jefe OAJ.
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMovimientoAdministrado().setT01UndOrgEmiId(unOrgEmisorId); //Emisor: 8 Misma �rea (FALTA)
				this.getSolicitudMovimientoAdministrado().setT01UndOrgRecId("8"); //jefe a su Asistente misma �rea 8.
				this.getSolicitudMovimientoAdministrado().setPerfilIdReceptor(2L); //2: Asistente OAJ
				this.getSolicitudMovimientoAdministrado().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
				this.getSolicitudMovimientoAdministrado().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMovimientoAdministrado().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				Long estadoSolicitud = this.getTmpFormularioBndRep().getEstado_solicitud_id();
				if (estadoSolicitud == 2L) { //Si es enviado como proyeccion observaci�n
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("12"); // 12: Devuelto por jefe OAJ POA.
				} else if (estadoSolicitud == 9L) { //Si es enviado como proyeccion respuesta
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("15"); // 15: Devuelto por Jefe OAJ PRA.
				} else if (estadoSolicitud == 16L) { //Si es enviado como proyeccion respuesta
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("17"); // 17: Devuelto por Jefe OAJ PNA.
				} else { //Default.
					this.getSolicitudMovimientoAdministrado().setEstadoSolicitudId("12"); // 12: Devuelto por jefe OAJ POA.
				}
				
				//this.parsearDocumento();
				SolicitudMovimiento tmpSolicMov = new SolicitudMovimiento();
				tmpSolicMov.setListaDocsRespuestas(this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas());
				this.getSolicitudMovimientoAdministrado().setListaDocsRespuestas(null);
				
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMovimientoAdministrado());
				
				SolicitudMovimiento tmpListaDocumentoFinal = new SolicitudMovimiento();
				if (sw.getId() != 0) {
					int cantDocsTraidos = this.getLstDetalleMovimiento().size();
					//AQUI INSERCION DE DOCUMENTOS.
					if(cantDocs > 0 || cantDocsTraidos > 0){ //Si tiene documentos adjuntos guarda primero O si tiene documentos que han sido traidos.
						
						ReturnObject sw2 = new ReturnObject();
						int nroExt = 0;
						int nroFal = 0;
						
						try {
							//Solo si trajo documentos que subi� el anterior usuario se a�ade a los que subi� el actual usuario.
							if(this.getLstDetalleMovimiento().size() > 0) { //Documentos extraidos
								for (DetalleMovimiento tmpDetaMovi: this.getLstDetalleMovimiento()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							//Llevar el orden deguardar losdocumentos como han sido adjuntadocronologicamente.
							if(tmpSolicMov.getListaDocsRespuestas().size() > 0) { //Documentos recien subidos.
								for (DetalleMovimiento tmpDetaMovi: tmpSolicMov.getListaDocsRespuestas()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							
							int catn = tmpListaDocumentoFinal.getListaDocsRespuestas().size();
							for (int i = 0; i  < catn; i++){
							
								DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
								
								tmpDetalleMovimiento.setSolicitudMovimientoId(sw.getId()); 
								tmpDetalleMovimiento.setTipoDocumentoId("17"); // TIPO DOCUMENTO: OTROS
								tmpDetalleMovimiento.setNombreDocumento(tmpListaDocumentoFinal.getListaDocsRespuestas().get(i).getNombreDocumento());
								tmpDetalleMovimiento.setUuidDocumento(tmpListaDocumentoFinal.getListaDocsRespuestas().get(i).getUuidDocumento());
								tmpDetalleMovimiento.setUsuarioIdRegistro(this.getUsuario().getId());
								sw2 = this.getMovimientoDetalleServiceRemote().insertar(tmpDetalleMovimiento);
								
								//Cantidad de Documento subidos a Alfresco
								if (sw2.getId() != 0) {
									nroExt ++; //Cantidad Exitosa.
								} else {
									nroFal ++; //Cantidad Fallida.
								}
							}
						} catch (Exception e) {
							super.setMensajeError("Error al referenciar los documentos con la solicitud.");
							logger.error(e);
						}
						
						if(nroFal > 0){
							super.setMensajeAlerta("Hubo "+ nroFal +" Error(es) en la inserci�n del registro DETALLE_MOVIMIENTO.");
							super.setMensajeAlerta("Solo se insertaron "+ nroExt +" registros en DETALLE_MOVIMIENTO.");
						}
						
						super.setMensajeAviso("�xito al devolver la solicitud Proyectada.");

					} else {
						super.setMensajeAviso("�xito al devolver la solicitud Proyectada.");
					}
				
					this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
					this.cancelarSeleccionOAJ(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar.
				} else {
					super.setMensajeError("Error al devolver la solicitud Proyectada.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al devolver la solicitud Proyectada.");
				logger.error(e);
			}

		} else {
			super.setMensajeAviso("Por favor, ingrese el motivo de su devoluci�n.");
		}
		
		return pagina;
	}
	
	// SMTP
	public boolean enviarCorreoAdministrado(String prmAsunto, String prmCorreo, String prmCuerpo, ArrayList<String> prmLstUuidds, Long idSoliTran) {
		boolean envio = false;

		try {
			boolean swMailEnviado = false;
			SmtpSendServiceImpl smtp = new SmtpSendServiceImpl();
			try {
				
				String asunto = prmAsunto;
				String cuerpoCorreo = this.getRegistroPlantillaCorreo("4");
				String pmrCorreoUsuario = prmCorreo;

				cuerpoCorreo = cuerpoCorreo.replace("{t_detalle}", prmCuerpo);
				String idST = String.valueOf(idSoliTran);
				
				String UrlPreProp = super.getString("url.FomularioSolicitud");
				
				if (!VO.isEmpty(idST)) { //Si no env�a el c�digo no hay correo.
					String codigoEnc = VO.encodeURL(
											StringEncrypt.encrypt(super.getString("KEY_ENCRYPT"), 
													super.getString("VECTOR_ENCRYPT"), idST));
					
					UrlPreProp = UrlPreProp + "?user=" + codigoEnc;
					
				} else {
					
					return envio;
				}

				if(!VO.isEmpty(UrlPreProp)){
					if(cuerpoCorreo.contains("{t_urlNOCAMBIAR}")){
						cuerpoCorreo = cuerpoCorreo.replace("{t_urlNOCAMBIAR}", UrlPreProp);
					}
				}

				//PREPARANDO: Si existen documentos para adjuntar.
				if(prmLstUuidds.size() > 0){
					AlfrescoBase alfrescoBase = new AlfrescoBase();
					ArrayList<FileAlfresco> tmpLstFileAlfresco = new ArrayList<FileAlfresco>();
					String ruta = super.getString("ruta.documentos.adjunto.email");
					
					for (String uuid : prmLstUuidds) {
						FileAlfresco fileAlfresco = new FileAlfresco();
						//Extrayendo documentos en servidor.
						fileAlfresco = alfrescoBase.obtenerArchivoAlfresco(uuid);
						
						FileAlfresco tmpFileAlfresco = new FileAlfresco();
						tmpFileAlfresco.setFileName(fileAlfresco.getFileName());
						tmpFileAlfresco.setRutaServidorTemp(ruta + fileAlfresco.getFileName());
						
						tmpLstFileAlfresco.add(tmpFileAlfresco);
						//Poner en temporales el File.
						File file = new File(ruta, fileAlfresco.getFileName());
						FileOutputStream salida = new FileOutputStream(file);
						salida.write(fileAlfresco.getRawFile());
						salida.flush();
						salida.close();
					}
					
					// 2. Env�o de correo CON adjuntos.
					swMailEnviado = smtp.envioCorreoUsuario(asunto, cuerpoCorreo, pmrCorreoUsuario, tmpLstFileAlfresco);
				// Sin env�o Adjuntos.
				} else {
					// 1. Env�o de correo SIN adjuntos.
					swMailEnviado = smtp.envioCorreoUsuario(asunto, cuerpoCorreo, pmrCorreoUsuario);
				}
				
						
				if (!swMailEnviado) {
					super.setMensajeAlerta("No se pudo enviar la notificaci�n.");
				}

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
				
			if (swMailEnviado) {
				envio = true;
			}

			this.cargarplantillas();

		} catch (Exception e) {
			super.setMensajeError("Error al enviar notificaci�n.");
		}
			
		return envio;
	}
	/*
	public boolean enviarCorreoPendiente(EntUsuario prmEntUsuario, String prmCorreo) {
		boolean envio = false;
		try {
			boolean swMailEnviado = false;
			SmtpSendServiceImpl smtp = new SmtpSendServiceImpl();
			try {
				String asunto = this.getTituloPlantillaCorreo("1");
				String cuerpoCorreo = this.getRegistroPlantillaCorreo("1");
				String pmrCorreoUsuario = prmCorreo;

				//String pathModuloInxterno = super.getString("LINK_SISTEMA_CREDENCIAL");
				
				cuerpoCorreo = cuerpoCorreo.replace("{t_contacto}", prmEntUsuario.getS_Contacto());
				cuerpoCorreo = cuerpoCorreo.replace("{t_entidad}", prmEntUsuario.getS_Nombre_Entidad());
				if (VO.isEmpty(prmEntUsuario.getS_fechaRegistro())) {
					cuerpoCorreo = cuerpoCorreo.replace("{t_registro}", " registrado el " + prmEntUsuario.getS_fechaRegistro());
				} else { // Si por alg�n extra�o caso no dispone de una fecha de registro.
					cuerpoCorreo = cuerpoCorreo.replace("{t_registro}", "");
				}
				
				cuerpoCorreo = cuerpoCorreo.replace("{t_usuario}", prmEntUsuario.getV_Usuario());
				
				cuerpoCorreo = cuerpoCorreo.replace("{t_password}", prmEntUsuario.getV_Clave());
				
				//if (cuerpoCorreo.contains("{urlParam}")) {
				//	cuerpoCorreo = cuerpoCorreo.replace("{urlParam}",
				//			pathModuloInxterno);
				//} else {
				//	cuerpoCorreo = cuerpoCorreo.replace("{urlParam}",
				//			"#");
				//}

					swMailEnviado = smtp.envioCorreoUsuario(asunto, cuerpoCorreo, pmrCorreoUsuario);
					
					if (!swMailEnviado) {
						super.setMensajeError("No se pudo enviar la notificaci�n.");
					}

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			if (swMailEnviado) {
				envio = true;
			} else {
				super.setMensajeError("No se pudo enviar la notificaci�n.");
			}

			this.cargarplantillas();
			
			 // }else{ super.setMensajeError("Error al eliminar Usuario"); }
			 
		} catch (Exception e) {
			super.setMensajeError("Error al enviar notificaci�n.");
		}
		
		return envio;
	}
	*//*
	private String getTituloPlantillaCorreo(String codigo) {

		for (T01Maestro t01Maestro : lstPlantillaCorreo) {
			if (t01Maestro.getCodigoRegistro().equals(codigo)) {
				return t01Maestro.getValor3();
			}
		}
		return "";
	}
	*/
	private String getRegistroPlantillaCorreo(String codigo) {
		this.cargarplantillas();
		for (T01Maestro t01Maestro : this.getLstPlantillaCorreo()) {
			if (t01Maestro.getCodigoRegistro().equals(codigo)) {
				return t01Maestro.getNombreLargo();
			}
		}
		return "";
	}
	
	public void cargarplantillas() {
		this.setLstPlantillaCorreo(super.getT01MaestroValuesNL("PLANTILLA_CORREO"));
	}
	
	//SUBIR
	private ResultAlfresco subirArchivoAlfresco(
		AlfrescoDocumentoAdjunto alfrescoAdjunto, FileAlfresco fileAlfresco) {
		
		//categoriaDoc.setTipoDocumento(super.getString("directorio.uno"));
		
		Date actual = new Date();
		
		SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
		String anio = formatAnio.format(actual);
		SimpleDateFormat formatMes = new SimpleDateFormat("MM");
		String mes = formatMes.format(actual);
		String proceso = "ADJUNTOS";
		
		ResultAlfresco resultAlfresco = new ResultAlfresco();
		resultAlfresco = alfrescoAdjunto.subirArchivo(fileAlfresco, proceso, anio, mes);
		
		return resultAlfresco;
	}

	public void buscarConsulta2(){
		
		try {
			Long estadoSolicitudSelec = this.getReporteBandeja().getEstado_solicitud_id();
			if(estadoSolicitudSelec == 25L){
				this.getReporteBandeja().setUsuario_id_responsable(this.getUsuario().getId());
				this.getReporteBandeja().setEstado_solicitud_id(3L);
				//EJB Listar.
				this.setLstReporteBandeja(this.getBandejaServiceRemote().lstReporteBandejaJefeOAJ2(this.getReporteBandeja()));
				this.getReporteBandeja().setEstado_solicitud_id(25L);
			} else {
				this.getReporteBandeja().setUsuario_id_responsable(this.getUsuario().getId());
				//EJB Listar.
				this.setLstReporteBandeja(this.getBandejaServiceRemote().lstReporteBandejaJefeOAJ(this.getReporteBandeja()));
			}
				
			this.cancelarSeleccionOAJ();
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al buscar solicitud!");
		}
	}
	
	public boolean validarDevolucionDerAdmnistrado(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivAdministrado()) {
			//ReqContext.update(":frmRegSolicitud:mensaje");
			return false;
		}
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar5').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	public boolean validarDerivPreAdministrado(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivAdministrado()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}
		
		//super.setMensajeAlerta("OK 2");
		ReqContext.execute("PF('idGrpConfirmar2').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	public String verDetallexrpt(ReporteBandeja prmRptBan) {

		this.setTmpReporteBandeja(prmRptBan);
		
		return "detalleBandJefeOAJ";
	}

	// ALFRESCO
	public void handleFileUpload(FileUploadEvent event) {
		Archivo archivo = new Archivo();
		String fileName;
		FileAlfresco fileAlfresco = new FileAlfresco();

		try {

			UploadedFile fu = event.getFile();

			fileName = fu.getFileName();

			fileAlfresco.setFileName(fileName);
			fileAlfresco.setExtension(fileName.substring(fileName.lastIndexOf(".") + 1));

			File file = new File(super.getString("ruta.documentos.adjunto.email"),
					fileName);
			FileOutputStream salida = new FileOutputStream(file);
			salida.write(fu.getContents());
			salida.flush();
			salida.close();

			String pathFile = file.getPath();
			
			fileAlfresco.setRawFile(archivo.read(new File(pathFile)));
			
			fileAlfresco.setTipoDoc("Adjunto");

			file.delete();
			
			archivo = null;
				//Agregar a la lista cada uno de los documentos adjuntados.
				DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
				//tmpDetalleMovimiento.setSolicitudMovimientoId(Long); <-- A�n no se guard� SOLICITUD_MOVIMIENTO.
				tmpDetalleMovimiento.setFileAlfresco(fileAlfresco);
				tmpDetalleMovimiento.setNombreDocumento(fileAlfresco.getFileName());
				
				this.getSolicitudMovimientoAdministrado().getListaDocsRespuestas().add(tmpDetalleMovimiento);
				
				//Extrae el nombre y la muestra en el output.
				if(VO.isEmpty(this.getNombreAdjunto01())){
					this.setNombreAdjunto01(this.getCont()+". "+tmpDetalleMovimiento.getNombreDocumento());
				} else {
					this.setNombreAdjunto01(this.getNombreAdjunto01()+"; "+this.getCont()+". "+tmpDetalleMovimiento.getNombreDocumento());
				}
				
				this.setCont(this.getCont() + 1);
				
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al cargar el archivo");
		}

	}
	
	// DESCARGAR:
	public void descargarFile(String uuidDocumento) {
		AlfrescoBase alfrescoBase = new AlfrescoBase();
		FileAlfresco fileAlfresco = new FileAlfresco();

		fileAlfresco = alfrescoBase.obtenerArchivoAlfresco(uuidDocumento);
			// fileAlfresco.setFileName(this.getFileAlfrescoAdjUno().getFileName());

		try {
			StreamedContent streamedContent = new DefaultStreamedContent(
				new ByteArrayInputStream(fileAlfresco.getRawFile()),
					"application/" + fileAlfresco.getExtension(),
					fileAlfresco.getFileName());
			super.setStreamedContent(streamedContent);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}
	
	public void borrarAdjuntos() {
		this.getSolicitudMovimientoAdministrado().setListaDocsRespuestas(new ArrayList<DetalleMovimiento>());
		this.setNombreAdjunto01("");
		this.setCont(1);
	}

	private boolean validarDerivArea() {

		int contador = 0;
		String mensaje = "";
		
		if ( this.getSolicitudMovimientoArea().getSolicitudTransparenciaId() == 0L ) {
			mensaje = mensaje.equals("") ? "No se ha extra�do ninguna solicitud."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getTxtUnidOrgDerivarCode())) {
			mensaje = mensaje.equals("") ? "Seleccione Unidad Org�nica."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMovimientoArea().getDetalleRespuesta().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un Detalle v�lido."
					: mensaje;
			contador++;
		}
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		
		logger.debug("Validado con �xito:");
		return true;
	}
	
	private boolean validarDerivAdministrado() {

		int contador = 0;
		String mensaje = "";
		
		if ( this.getSolicitudMovimientoArea().getSolicitudTransparenciaId() == 0L ) {
			mensaje = mensaje.equals("") ? "No se ha extra�do ninguna solicitud."
					: mensaje;
			contador++;
		}

		if (VO.isEmpty(this.getSolicitudMovimientoAdministrado().getCorreo())) {
			mensaje = mensaje.equals("") ? "No se ha extraido ning�n correo para esta solicitud."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMovimientoAdministrado().getAsunto().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un asunto v�lido."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMovimientoAdministrado().getDetalleRespuesta().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un contenido v�lido."
					: mensaje;
			contador++;
		}
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		
		logger.debug("Validado con �xito:");
		return true;
	}
	
	/************************************************/
	// GETTERS AND SETTERS.
	//VARIABLES
	public ReporteBandeja getReporteBandeja() {
		return reporteBandeja;
	}

	public void setReporteBandeja(ReporteBandeja reporteBandeja) {
		this.reporteBandeja = reporteBandeja;
	}

	public List<ReporteBandeja> getLstReporteBandeja() {
		return lstReporteBandeja;
	}

	public void setLstReporteBandeja(List<ReporteBandeja> lstReporteBandeja) {
		this.lstReporteBandeja = lstReporteBandeja;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}

	public ReporteBandeja getTmpFormularioBndRep() {
		return tmpFormularioBndRep;
	}

	public void setTmpFormularioBndRep(ReporteBandeja tmpFormularioBndRep) {
		this.tmpFormularioBndRep = tmpFormularioBndRep;
	}

	public List<T01Maestro> getLstUndsOrgTodo() {
		return lstUndsOrgTodo;
	}

	public void setLstUndsOrgTodo(List<T01Maestro> lstUndsOrgTodo) {
		this.lstUndsOrgTodo = lstUndsOrgTodo;
	}

	public List<T01Maestro> getLstUndsOrgCnSecrt() {
		return lstUndsOrgCnSecrt;
	}

	public void setLstUndsOrgCnSecrt(List<T01Maestro> lstUndsOrgCnSecrt) {
		this.lstUndsOrgCnSecrt = lstUndsOrgCnSecrt;
	}

	public boolean isSwAutorizaPersona() {
		return swAutorizaPersona;
	}

	public void setSwAutorizaPersona(boolean swAutorizaPersona) {
		this.swAutorizaPersona = swAutorizaPersona;
	}

	public String getTxtUnidOrgDerivar() {
		return txtUnidOrgDerivar;
	}

	public void setTxtUnidOrgDerivar(String txtUnidOrgDerivar) {
		this.txtUnidOrgDerivar = txtUnidOrgDerivar;
	}

	public String getTxtUnidOrgDerivarCode() {
		return txtUnidOrgDerivarCode;
	}

	public void setTxtUnidOrgDerivarCode(String txtUnidOrgDerivarCode) {
		this.txtUnidOrgDerivarCode = txtUnidOrgDerivarCode;
	}

	public SolicitudMovimiento getSolicitudMovimientoArea() {
		return solicitudMovimientoArea;
	}

	public void setSolicitudMovimientoArea(
			SolicitudMovimiento solicitudMovimientoArea) {
		this.solicitudMovimientoArea = solicitudMovimientoArea;
	}

	public SolicitudMovimiento getSolicitudMovimientoAdministrado() {
		return solicitudMovimientoAdministrado;
	}

	public void setSolicitudMovimientoAdministrado(
			SolicitudMovimiento solicitudMovimientoAdministrado) {
		this.solicitudMovimientoAdministrado = solicitudMovimientoAdministrado;
	}

	public RequestContext getReqContext() {
		return ReqContext;
	}

	public void setReqContext(RequestContext reqContext) {
		ReqContext = reqContext;
	}
	
	public List<T01Maestro> getLstT01TipoDocumento() {
		return lstT01TipoDocumento;
	}

	public void setLstT01TipoDocumento(List<T01Maestro> lstT01TipoDocumento) {
		this.lstT01TipoDocumento = lstT01TipoDocumento;
	}

	public List<UbigeoReniec> getLstDepartamento() {
		return lstDepartamento;
	}

	public void setLstDepartamento(List<UbigeoReniec> lstDepartamento) {
		this.lstDepartamento = lstDepartamento;
	}
	
	public List<UbigeoReniec> getLstProvincia() {
		return lstProvincia;
	}

	public void setLstProvincia(List<UbigeoReniec> lstProvincia) {
		this.lstProvincia = lstProvincia;
	}

	public List<UbigeoReniec> getLstDistrito() {
		return lstDistrito;
	}

	public void setLstDistrito(List<UbigeoReniec> lstDistrito) {
		this.lstDistrito = lstDistrito;
	}

	public boolean isHabilitaRespuestaUsuario() {
		return habilitaRespuestaUsuario;
	}

	public void setHabilitaRespuestaUsuario(boolean habilitaRespuestaUsuario) {
		this.habilitaRespuestaUsuario = habilitaRespuestaUsuario;
	}

	public boolean isVistaTramiteOAJ() {
		return vistaTramiteOAJ;
	}

	public void setVistaTramiteOAJ(boolean vistaTramiteOAJ) {
		this.vistaTramiteOAJ = vistaTramiteOAJ;
	}

	public boolean isVistaRespuestaUsuario() {
		return vistaRespuestaUsuario;
	}

	public void setVistaRespuestaUsuario(boolean vistaRespuestaArea) {
		this.vistaRespuestaUsuario = vistaRespuestaArea;
	}

	public boolean isVistaRegistroSelecc() {
		return vistaRegistroSelecc;
	}

	public void setVistaRegistroSelecc(boolean vistaRegistroSelecc) {
		this.vistaRegistroSelecc = vistaRegistroSelecc;
	}
	
	public ReporteBandeja getTmpReporteBandeja() {
		return tmpReporteBandeja;
	}

	public void setTmpReporteBandeja(ReporteBandeja tmpReporteBandeja) {
		this.tmpReporteBandeja = tmpReporteBandeja;
	}
	
	public DetalleMovimiento getDetalleMovimiento() {
		return detalleMovimiento;
	}

	public void setDetalleMovimiento(DetalleMovimiento detalleMovimiento) {
		this.detalleMovimiento = detalleMovimiento;
	}

	public List<ReporteDerivacion> getLstRptDerivacion() {
		return lstRptDerivacion;
	}

	public void setLstRptDerivacion(List<ReporteDerivacion> lstRptDerivacion) {
		this.lstRptDerivacion = lstRptDerivacion;
	}

	public String getTxtNumeroSolicitud() {
		return txtNumeroSolicitud;
	}

	public void setTxtNumeroSolicitud(String txtNumeroSolicitud) {
		this.txtNumeroSolicitud = txtNumeroSolicitud;
	}

	public boolean isVistaRespuestaProyeccion() {
		return vistaRespuestaProyeccion;
	}

	public void setVistaRespuestaProyeccion(boolean vistaRespuestaProyeccion) {
		this.vistaRespuestaProyeccion = vistaRespuestaProyeccion;
	}

	public boolean isHabilitaRespuestaProyeccion() {
		return habilitaRespuestaProyeccion;
	}

	public void setHabilitaRespuestaProyeccion(boolean habilitaRespuestaProyeccion) {
		this.habilitaRespuestaProyeccion = habilitaRespuestaProyeccion;
	}

	public boolean isHabilitaValidaInfoOAJ() {
		return habilitaValidaInfoOAJ;
	}

	public void setHabilitaValidaInfoOAJ(boolean habilitaValidaInfoOAJ) {
		this.habilitaValidaInfoOAJ = !habilitaValidaInfoOAJ;
	}

	public String getNombreAdjunto01() {
		return nombreAdjunto01;
	}

	public void setNombreAdjunto01(String nombreAdjunto01) {
		this.nombreAdjunto01 = nombreAdjunto01;
	}

	public List<DetalleMovimiento> getLstDetalleMovimiento() {
		return lstDetalleMovimiento;
	}

	public void setLstDetalleMovimiento(List<DetalleMovimiento> lstDetalleMovimiento) {
		this.lstDetalleMovimiento = lstDetalleMovimiento;
	}

	public List<String> getLstUndsOrgsSlc() {
		return lstUndsOrgsSlc;
	}

	public void setLstUndsOrgsSlc(List<String> lstUndsOrgsSlc) {
		this.lstUndsOrgsSlc = lstUndsOrgsSlc;
	}

	public List<String> getLstUndsOrgsStrg() {
		return lstUndsOrgsStrg;
	}

	public void setLstUndsOrgsStrg(List<String> lstUndsOrgsStrg) {
		this.lstUndsOrgsStrg = lstUndsOrgsStrg;
	}

	public List<T01Maestro> getLstPlantillaCorreo() {
		return lstPlantillaCorreo;
	}

	public void setLstPlantillaCorreo(List<T01Maestro> lstPlantillaCorreo) {
		this.lstPlantillaCorreo = lstPlantillaCorreo;
	}
	
	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public ReporteBandeja getRptBanjReenviar() {
		return rptBanjReenviar;
	}

	public void setRptBanjReenviar(ReporteBandeja rptBanjReenviar) {
		this.rptBanjReenviar = rptBanjReenviar;
	}

	public String getMngAcordeon() {
		return mngAcordeon;
	}

	public void setMngAcordeon(String mngAcordeon) {
		this.mngAcordeon = mngAcordeon;
	}

	public List<SolicitudMovimiento> getLstSoliMoviXLineaTP() {
		return lstSoliMoviXLineaTP;
	}

	public void setLstSoliMoviXLineaTP(List<SolicitudMovimiento> lstSoliMoviXLineaTP) {
		this.lstSoliMoviXLineaTP = lstSoliMoviXLineaTP;
	}
	
	public String getNroSolicitudSelect() {
		return nroSolicitudSelect;
	}

	public void setNroSolicitudSelect(String nroSolicitudSelect) {
		this.nroSolicitudSelect = nroSolicitudSelect;
	}

	public boolean isHabilitaBtnAdjDoc() {
		return habilitaBtnAdjDoc;
	}

	public void setHabilitaBtnAdjDoc(boolean habilitaBtnAdjDoc) {
		this.habilitaBtnAdjDoc = !habilitaBtnAdjDoc;
	}
	
	public int getCantMax() {
		return cantMax;
	}

	public void setCantMax(int cantMax) {
		this.cantMax = cantMax;
	}

	public int getMaxDocsAdjs() {
		return maxDocsAdjs;
	}

	public void setMaxDocsAdjs(int maxDocsAdjs) {
		this.maxDocsAdjs = maxDocsAdjs;
	}

	public boolean isHabilitaBtnDevolver() {
		return habilitaBtnDevolver;
	}

	public void setHabilitaBtnDevolver(boolean habilitaBtnDevolver) {
		this.habilitaBtnDevolver = !habilitaBtnDevolver;
	}

	//EJB ***************************
	public BandejaServiceRemote getBandejaServiceRemote() {
		return bandejaServiceRemote;
	}

	public void setBandejaServiceRemote(BandejaServiceRemote bandejaServiceRemote) {
		this.bandejaServiceRemote = bandejaServiceRemote;
	}

	public MovimientoSolicitudServiceRemote getMovimientoSolicitudServiceRemote() {
		return movimientoSolicitudServiceRemote;
	}

	public void setMovimientoSolicitudServiceRemote(
			MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote) {
		this.movimientoSolicitudServiceRemote = movimientoSolicitudServiceRemote;
	}

	public MovimientoDetalleServiceRemote getMovimientoDetalleServiceRemote() {
		return movimientoDetalleServiceRemote;
	}

	public void setMovimientoDetalleServiceRemote(
			MovimientoDetalleServiceRemote movimientoDetalleServiceRemote) {
		this.movimientoDetalleServiceRemote = movimientoDetalleServiceRemote;
	}

}

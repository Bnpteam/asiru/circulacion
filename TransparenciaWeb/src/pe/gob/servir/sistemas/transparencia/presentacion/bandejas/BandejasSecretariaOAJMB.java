package pe.gob.servir.sistemas.transparencia.presentacion.bandejas;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;

import pe.gob.servir.sistemas.transparencia.model.negocio.Solicitud;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.std.service.ws.ResponseDatosTramiteCEType;
import pe.gob.servir.std.service.ws.STDServiceInterface;
import pe.gob.servir.std.service.ws.STDWebService;
import pe.gob.servir.systems.util.ipaddress.IpAddressPc;

import javax.xml.ws.BindingProvider;


@ManagedBean(name = "bandejasSecretariaOAJMB")
@SessionScoped
public class BandejasSecretariaOAJMB extends BasicMB {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());
	
	
	public  void getListaAIPfromSTD(){

		/*FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String remoteHost = request.getRemoteAddr();*/ // cliente ip
		
		List<ResponseDatosTramiteCEType> retorno;
		Collection<Solicitud> retornoListaSolicitud = new ArrayList<Solicitud>();
		
		//String rt = "";
		try {

			URL url = new URL(super.getString("std.ws.wsdl"));						
			STDWebService service1 = new STDWebService();
			STDServiceInterface port1 = service1.getSTDServiceInterfaceImplPort();

			((BindingProvider) port1).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, super.getString("std.ws.endpoint"));
			pe.gob.servir.std.service.ws.SeguridadType seguridad = new pe.gob.servir.std.service.ws.SeguridadType();
			pe.gob.servir.std.service.ws.AuditoriaType auditoria = new pe.gob.servir.std.service.ws.AuditoriaType();

			seguridad.setCodAplicativo(super.getString("std.ws.codigo"));
			IpAddressPc file = new IpAddressPc();
			String[] audit = file.obtenerAuditoria();
			//auditoria.setIpPc(remoteHost);
			auditoria.setIpPc("172.16.24.129");
			auditoria.setMacAddress("AB:EC:EC:BE:1A");
			
			retorno = port1.getDatosTramiteConsultasExternas(seguridad, auditoria);
			
			if(retorno!=null && retorno.size()>0){
				// metodo para devolver lista final
				retornoListaSolicitud = setListaSTDACEX(retorno);
			}
			
			logger.info("APP"+retorno.toString()); 

		} catch (MalformedURLException e) {
			logger.error(e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}		
	}
	

	public List<Solicitud> setListaSTDACEX(List<ResponseDatosTramiteCEType> listaEntrada){
		boolean sw = false;
		//List<String> lstNroRegSTD = listarNroRegSTD();
		
		List<Solicitud> listaSalida = new ArrayList<Solicitud>();
		Solicitud bsolicitudAIP;
		//SimpleDateFormat formatoDeTexto = new SimpleDateFormat("dd/MM/yyyy");
		
		Iterator itr = listaEntrada.iterator();
	      while(itr.hasNext()) {	    	  
	    	  ResponseDatosTramiteCEType element = (ResponseDatosTramiteCEType) itr.next();	      
	    	  if(sw){
	    		  sw = false;
	    		  continue;
	    	  }	    	  
	    	  bsolicitudAIP = new Solicitud();
	    	  bsolicitudAIP.setNumRegistro(element.getNumTramite());
	    	  bsolicitudAIP.setTipoDocumentoIdentidadId(element.getTipoDocumento());
	    	  bsolicitudAIP.setNumeroDocumentoIdentidad(element.getNumDocumento());
	    	  bsolicitudAIP.setRemitentePersona(element.getRemitentePersona());
	    	  bsolicitudAIP.setRemitenteEntidad(element.getRemitente());
	    	  bsolicitudAIP.setAsuntoStd(element.getAsunto());
	    	  bsolicitudAIP.setRutaAlfrescoSTD(element.getRutaCMS());	
	    	  
	    	  listaSalida.add(bsolicitudAIP);
	      }		
		return listaSalida;
	}

}

package pe.gob.servir.sistemas.transparencia.presentacion.bandejas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.ByteArrayInputStream;
//import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

import org.apache.log4j.Logger;
//import org.primefaces.model.DefaultStreamedContent;
//import org.primefaces.model.StreamedContent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import pe.gob.servir.sistemas.transparencia.constantes.Constantes;
import pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.exception.ServicioException;
import pe.gob.servir.sistemas.transparencia.ejb.service.smtp.SmtpSendServiceImpl;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.administracion.Perfil;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.sistemas.transparencia.model.maestra.T01Maestro;
import pe.gob.servir.sistemas.transparencia.model.maestra.UbigeoReniec;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteDerivacion;
import pe.gob.servir.sistemas.transparencia.model.negocio.DetalleMovimiento;
import pe.gob.servir.sistemas.transparencia.model.negocio.ReporteBandeja;
import pe.gob.servir.sistemas.transparencia.model.negocio.SolicitudMovimiento;
import pe.gob.servir.sistemas.transparencia.presentacion.base.BasicMB;
import pe.gob.servir.sistemas.transparencia.servicios.alfresco.AlfrescoBase;
import pe.gob.servir.sistemas.transparencia.servicios.alfresco.AlfrescoDocumentoAdjunto;
import pe.gob.servir.systems.util.alfresco.Archivo;
//import pe.gob.servir.sistemas.transparencia.servicios.alfresco.AlfrescoDocumentoAdjunto;
import pe.gob.servir.systems.util.alfresco.FileAlfresco;
import pe.gob.servir.systems.util.alfresco.ResultAlfresco;
import pe.gob.servir.systems.util.enums.Enums.PerfilInterno;
import pe.gob.servir.systems.util.retorno.ReturnObject;
import pe.gob.servir.systems.util.validator.VO;

@ManagedBean(name = "bandejaSecretariaUOMB")
@SessionScoped
public class BandejaSecretariaUOMB extends BasicMB {

	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(this.getClass());
	
	private ReporteBandeja       reporteBandeja; //Para b�squeda.
	private List<ReporteBandeja> lstReporteBandeja;
	private ReporteBandeja		 tmpFormularioBndRep; //Para Extraer a Formulario Bandeja.
	private List<T01Maestro> 	 lstT01TipoDocumento;
	private List<T01Maestro> 	 lstUnidadesOrganicas; //Lista de Objetos T01Maestros para formulario bloqueado.
	private boolean 			 swAutorizaPersona;
	private List<ReporteDerivacion>lstDetalleDerivado; //Arreglo de objeto para mostrar la derivaci�n multiareas.
	private List<T01Maestro>	 lstPlantillaCorreo;
	RequestContext 				 ReqContext;
	
	List<SolicitudMovimiento> 	 lstSoliMoviXLineaTP;
	
	private List<UbigeoReniec>   lstDepartamento;
	private List<UbigeoReniec>   lstProvincia;
	private List<UbigeoReniec>   lstDistrito;
	
	private boolean				 vistaRegistroSelecc;
	
	private boolean				 habilitaValidaInfoOAJ;//btn
	
	private boolean				 vistaTramiteOAJ; //Panel
	
	private boolean				 vistaRespuestaProyeccion;
	private boolean				 habilitaRespuestaProyeccion;

	private boolean				 vistaRespuestaUsuario;
	private boolean				 habilitaRespuestaUsuario;

	private ReporteBandeja		 tmpReporteBandeja; //Ver detalle
	private SolicitudMovimiento	 solicitudMoviUsuario;
	private SolicitudMovimiento	 solicitudMoviAsistOAJ;
	private DetalleMovimiento	 detalleMovimiento;
	private String				 nombreAdjunto01;
	List<DetalleMovimiento> 	 lstDetalleMovimiento; //Listar documento adjuntados por el asistente.
	
	private int 				 cont = 1;
	private List<Usuario> 		 lstUsuario;
	private String				 mngAcordeon;
	private String				 txtMotivoDevolucion1;
	private String				 txtMotivoDevolucion2;
	private String				 nroSolicitudSelect;
	private boolean				 habilitaBtnAdjDoc;
	private int 				 cantMax;
	private int					 maxDocsAdjs;
	
	private List<SelectItem> 	 listaGeneral;
	
	// Variables EJB.
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/BandejaServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.BandejaServiceRemote")
	private BandejaServiceRemote bandejaServiceRemote;
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoSolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoSolicitudServiceRemote")
	private MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote;

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/MovimientoDetalleServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.MovimientoDetalleServiceRemote")
	private MovimientoDetalleServiceRemote movimientoDetalleServiceRemote;
	
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/UsuarioServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.administracion.inf.remoto.UsuarioServiceRemote")
	private UsuarioServiceRemote usuarioServiceRemote;

	@PostConstruct
	public void init(){
		this.setCantMax(Integer.parseInt(super.getString("alfresco.cantidad.general")));
		this.setMaxDocsAdjs(this.getCantMax());
		this.setHabilitaBtnAdjDoc(false);
		this.setNroSolicitudSelect("");
		this.setTxtMotivoDevolucion1("");
		this.setTxtMotivoDevolucion2("");
		this.setMngAcordeon(null);
		this.setLstUnidadesOrganicas(new ArrayList<T01Maestro>());
		this.setReporteBandeja(new ReporteBandeja());
		this.getReporteBandeja().setEstado_solicitud_id(5L);
		Calendar cal= Calendar.getInstance();
		this.getReporteBandeja().setAnio_solicitud(String.valueOf(cal.get(Calendar.YEAR)));
		this.setLstSoliMoviXLineaTP(new ArrayList<SolicitudMovimiento>());
		this.setTmpFormularioBndRep(new ReporteBandeja());
		this.setDetalleMovimiento(new DetalleMovimiento());
		this.setLstReporteBandeja(new ArrayList<ReporteBandeja>());
		this.setLstT01TipoDocumento(new ArrayList<T01Maestro>());
		this.setLstDepartamento(new ArrayList<UbigeoReniec>());
		this.setLstProvincia(new ArrayList<UbigeoReniec>());
		this.setLstDistrito(new ArrayList<UbigeoReniec>());
		this.setVistaRegistroSelecc(true);
		this.setLstPlantillaCorreo(new ArrayList<T01Maestro>());
		this.setHabilitaValidaInfoOAJ(false);
		//CASO 1:
		this.setVistaTramiteOAJ(false); //Panel
			// A
			this.setVistaRespuestaProyeccion(false);
			this.setHabilitaRespuestaProyeccion(false);
			// B
			this.setVistaRespuestaUsuario(false);
			this.setHabilitaRespuestaUsuario(false);

		//this.llenarCombos();
		
		this.setSolicitudMoviUsuario(new SolicitudMovimiento());
		this.setSolicitudMoviAsistOAJ(new SolicitudMovimiento());
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		this.setLstDetalleDerivado(new ArrayList<ReporteDerivacion>());
		this.setListaGeneral(new ArrayList<SelectItem>());
		
	}
	
	
/***** METODOS *****************************************/
	
	public String regresarListado() {
		String pg = "bandejaSecretariaUO";
		this.cargarListaConsultas();
		return pg;
	}
	
	public void cancelarSeleccionUO(){
		this.setMaxDocsAdjs(this.getCantMax());
		this.setNroSolicitudSelect("");
		this.setLstSoliMoviXLineaTP(new ArrayList<SolicitudMovimiento>());
		this.setMngAcordeon(null);
		this.setTmpFormularioBndRep(new ReporteBandeja());
		this.setSolicitudMoviUsuario(new SolicitudMovimiento());
		this.setLstDetalleMovimiento(new ArrayList<DetalleMovimiento>());
		this.borrarAdjuntos();
		//this.habilitaPanelxArea();
		this.setVistaTramiteOAJ(false);//Panel
		this.setHabilitaValidaInfoOAJ(false);//BTn del panel
		this.setHabilitaRespuestaProyeccion(false);
		this.limpiarDerivacionUser();
		this.setHabilitaRespuestaUsuario(false);
	}
	
	public void limpiarDerivacionUser(){
		this.getSolicitudMoviUsuario().setUsuarioIdReceptor(0L);
		this.getSolicitudMoviUsuario().setDetalleAsignacion("");
	}
	
	public void limpiarDerivacionAsistOAJ(){
		this.getSolicitudMoviAsistOAJ().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
		Long soliMoviId = this.getSolicitudMoviAsistOAJ().getId();
		this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));
		//Relistar doc borrados.
		int cantDocExtr = this.getLstDetalleMovimiento().size();
		if (cantDocExtr < this.getCantMax()) {
			this.setMaxDocsAdjs(this.getCantMax()-cantDocExtr);
			this.setHabilitaBtnAdjDoc(true);
		} else {
			this.setMaxDocsAdjs(0);
			this.setHabilitaBtnAdjDoc(false);
		}
		
		this.borrarAdjuntos();
		this.setNombreAdjunto01("");
	}

	public void llenarCombos() {
		this.setLstT01TipoDocumento(super.getT01MaestroValuesNL("T01_TIPO_DOCUMENTO_IDENTIDAD"));
		this.setLstUnidadesOrganicas(super.getT01MaestroValuesNL("T01_UNIDAD_ORGANICA")); //Como objeto.
	}
	// USUARIOS
	public void listarUsuarios(String undOrganica){
		try {
			
			SelectItemGroup listaResponsables = new SelectItemGroup("Responsables");
			List<SelectItem> listaTemporalResponsables = new ArrayList<SelectItem>();
			
			List<SelectItem> listaTemporalOtros = new ArrayList<SelectItem>();
			List<Usuario> lstUsuarioTemporal;
			
			Usuario Temuser = new Usuario();
			Temuser.setIdUnidadOrganica(undOrganica);
			Temuser.setPerfil(this.getUsuario().getPerfil());
			Temuser.setId(this.getUsuario().getId());

			lstUsuario = this.getUsuarioServiceRemote().listarUsuarioXPerfilXArea(Temuser);
			
			if(lstUsuario!=null && lstUsuario.size()>0){				
				for(Usuario u: lstUsuario){
					listaTemporalResponsables.add(new SelectItem(u.getId(), u.getNombreCompleto()));
				}
				listaResponsables.setSelectItems(listaTemporalResponsables.toArray(new SelectItem[lstUsuario.size()]));
				listaGeneral.add(listaResponsables);
			}						
			
			Temuser.setPerfil(new Perfil(PerfilInterno.SECRETARIA_UO.getId()));			
			lstUsuarioTemporal = this.getUsuarioServiceRemote().listarUsuarioXPerfilXAreaAlternativo(Temuser);			
			if(lstUsuarioTemporal!=null && lstUsuarioTemporal.size()>0){	
				SelectItemGroup listaOtros = new SelectItemGroup("Otros");
				for(Usuario u: lstUsuarioTemporal){
					listaTemporalOtros.add(new SelectItem(u.getId(), u.getNombreCompleto()));
				}
				listaOtros.setSelectItems(listaTemporalOtros.toArray(new SelectItem[lstUsuarioTemporal.size()]));		
				listaGeneral.add(listaOtros);
			}
			
			System.out.println(listaGeneral);

		} catch (ServicioException e) {
			e.printStackTrace();
		}
	}
	
	public void listarDepartamentos(String departamento){
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(departamento);
		tmpubigeo.setCodpro("00");
		tmpubigeo.setCoddis("00");
		try {
			this.setLstDepartamento(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}
	
	public void updateProvincia(String codDep, String codProv){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(codDep);
		tmpubigeo.setCodpro(codProv);
		tmpubigeo.setCoddis("00");		
		try {
			this.setLstProvincia(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}

	public void updateDistrito(String codDep, String codProv, String codDis){
		
		UbigeoReniec tmpubigeo = new UbigeoReniec();
		tmpubigeo.setCoddep(codDep);
		tmpubigeo.setCodpro(codProv);
		tmpubigeo.setCoddis(codDis);		
		try {
			this.setLstDistrito(super.getPersonaServiceRemote().listUbigeo(tmpubigeo));
		} catch (ServicioException e) {			
			e.printStackTrace();
		}
	}

	//VER2BL
	public void verRespuestaProyeccion() {
		
		this.setHabilitaRespuestaProyeccion(true);
		this.setHabilitaRespuestaUsuario(false);
	}
	
	public void verRespuestaUsuario() {
		this.setHabilitaRespuestaProyeccion(false);
		this.setHabilitaRespuestaUsuario(true);
	}
	//Si tendr�ece 2 botones 2 estados diferentes.
	public boolean mostrarBtnXSituSoli(ReporteBandeja prmReporteBndj) {
		boolean result = false;
		if(prmReporteBndj.getSituacion_solicitud_id() == 2L){
			result = true;
		} else {
			result = false;
		}
		
		return result;
	}
	
	public void cargarListaConsultas(){
			try {
				this.setVistaRegistroSelecc(true);
				this.setHabilitaRespuestaProyeccion(false);
				this.setHabilitaRespuestaUsuario(false);
				this.cancelarSeleccionUO();
				//this.getReporteBandeja().setUsuario_id_responsable(this.getUsuario().getId());
				//EJB Listar.
				this.buscarConsulta2(); // <--- Listar.
			} catch (Exception e) {
				e.printStackTrace();
				super.setMensajeAlerta("Ocurri� un error al cargar las solicitudes.");
			}
		
		//super.calcularAlturaTabla();
	}
	
	// ++
	public void seleccionarRegistro(ReporteBandeja prmRptBan){
		if (prmRptBan!=null) {
			this.cancelarSeleccionUO(); //Reset todo;
			this.setTmpFormularioBndRep(new ReporteBandeja());
			this.setTmpFormularioBndRep(prmRptBan);
			this.updateAutoPersonaPanel();
			this.listarDepartamentos(this.getTmpFormularioBndRep().getCoddep());
			this.updateProvincia(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro());
			this.updateDistrito(this.getTmpFormularioBndRep().getCoddep(),this.getTmpFormularioBndRep().getCodpro(),this.getTmpFormularioBndRep().getCoddis());
			//Id de transparencia
			this.getSolicitudMoviUsuario().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
			this.getSolicitudMoviAsistOAJ().setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
			
			this.setHabilitaValidaInfoOAJ(true); //Botones selectores.
			this.setVistaTramiteOAJ(true); // Panel de pesta�as. ---
			this.setNroSolicitudSelect("(Nro "+this.getTmpFormularioBndRep().getSolicitud_transparencia_id()+")");
			
			// Listar linea de tiempo.
			try {
				SolicitudMovimiento tmpSolicitudMovimiento = new SolicitudMovimiento();
				tmpSolicitudMovimiento.setSolicitudTransparenciaId(this.getTmpFormularioBndRep().getSolicitud_transparencia_id());
				tmpSolicitudMovimiento.setT01UndOrgRecId(this.getTmpFormularioBndRep().getT01_und_org_receptor_id());
				
				this.setLstSoliMoviXLineaTP(this.getMovimientoSolicitudServiceRemote().lstLinTiempoXAreas2(tmpSolicitudMovimiento, "5,6,13,7,14"));
				
				/*if (this.getLstSoliMoviXLineaTP().size() > 0) {
					super.setMensajeAviso("�xito al cargar la l�nea de tiempo.");
				} else {
					super.setMensajeAlerta("No se han encontrado coincidencias para formar la l�nea de tiempo.");
				}*/
			} catch (Exception e) {
				e.printStackTrace();
				super.setMensajeAlerta("Error al cargar los documentos Adjuntados Proyecci�n Respuesta.");
			}
				
			if (this.getTmpFormularioBndRep().getEstado_solicitud_id() == 5L) { // 5:DERIVACION USUARIO RESPONSABLEUO
				this.verRespuestaProyeccion();
				/*this.setHabilitaRespuestaProyeccion(true);
				this.setHabilitaRespuestaUsuario(false);*/
				this.listarUsuarios(this.getTmpFormularioBndRep().getT01_und_org_receptor_id());
				this.setMngAcordeon("0");
				super.setMensajeAviso("�xito al extraer la solicitud del tipo Asignado a �rgano Responsable.");
				
			} else if (this.getTmpFormularioBndRep().getEstado_solicitud_id() == 7L) { // 7:DERIVACION RESPUESTAVALIDADO A ASISTENTE OAJ.
				this.verRespuestaUsuario();
				/*this.setHabilitaRespuestaProyeccion(false);
				this.setHabilitaRespuestaUsuario(true);*/
				this.getSolicitudMoviAsistOAJ().setDetalleRespuesta(this.getTmpFormularioBndRep().getDetalle_respuesta());
				
				//Id entrega el SOLICITUD_MOVIMIENTO_ID del detalle derivaci�n para hacer las b�squedas.
				this.getSolicitudMoviAsistOAJ().setId(this.getTmpFormularioBndRep().getSolicitud_movimiento_id());
				Long soliMoviId = this.getSolicitudMoviAsistOAJ().getId();
				this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));
				
				int cantDocExtr = this.getLstDetalleMovimiento().size();
				if (cantDocExtr < this.getCantMax()) {
					this.setHabilitaBtnAdjDoc(true);
					
					this.setMaxDocsAdjs(this.getCantMax()-cantDocExtr); // 5 - X
				} else {
					this.setHabilitaBtnAdjDoc(false);
					this.setMaxDocsAdjs(0); // Si es bloqueado el bot�n entonces es 0. Msj.
				}
				
				this.setMngAcordeon("0");
				super.setMensajeAviso("�xito al extraer la solicitud del tipo Respuesta Asignado a Secretaria UO.");
				
			} else if (this.getTmpFormularioBndRep().getEstado_solicitud_id() == 13L) { // 13: Devuelto por Responsable.
				this.verRespuestaProyeccion();
				/*this.setHabilitaRespuestaProyeccion(true);
				this.setHabilitaRespuestaUsuario(false);*/
				this.listarUsuarios(this.getTmpFormularioBndRep().getT01_und_org_receptor_id());
				
				if(this.getLstSoliMoviXLineaTP()!= null){
					int totalHisto = this.getLstSoliMoviXLineaTP().size();
					if(totalHisto > 2){
						SolicitudMovimiento tmpSoliMoviAnt = this.getLstSoliMoviXLineaTP().get(totalHisto-2);
						this.getSolicitudMoviUsuario().setUsuarioIdReceptor(tmpSoliMoviAnt.getUsuarioIdReceptor());
						this.getSolicitudMoviUsuario().setDetalleAsignacion(tmpSoliMoviAnt.getDetalleAsignacion());
					}
				}
				//Setear Usuario.
				// Falta setear el movimiento anterior.
				//this.getSolicitudMoviUsuario().setDetalleAsignacion(this.getTmpFormularioBndRep().getDetalle_asignacion());
				this.setMngAcordeon("0");
				super.setMensajeAviso("�xito al extraer la solicitud devuelto por el Responsable.");
				
			} else {
				super.setMensajeAviso("No se ha encontrado una acci�n extra para la presente categor�a.");
				/*this.setHabilitaRespuestaProyeccion(false);
				this.setHabilitaRespuestaUsuario(false);*/
				this.setMngAcordeon(null);
			}

		} else {
			super.setMensajeAviso("Error al formar la l�nea de tiempo.");
		}
	}
	/*
	private SolicitudMovimiento getMovimientoAnterior(String codigo) {
		
		for (SolicitudMovimiento tmpSoliMovi : lstSoliMoviXLineaTP) {
			if (tmpSoliMovi.getCodigoRegistro().equals(codigo)) {
				return tmpSoliMovi;
			} 		
		}
		return null;
	}*/

	//Necesita solo la Id de solicitudMovimientoId
	public List<DetalleMovimiento> listarDocumentosIngresados(Long SoliMoviId){
		List<DetalleMovimiento> tmpDetaSoli = new ArrayList<DetalleMovimiento>();
		
		try {
			
			tmpDetaSoli = this.getMovimientoDetalleServiceRemote().lstDocumentosIngresados(SoliMoviId);
			
			/*int canDocIng = this.getLstDetalleMovimiento().size();
			
			this.setMaximoDocumentoAdj(this.getMaximoDocumentoAdj() - canDocIng);
			// Bloqueo de adjuntar mas documento si se el maximo llega a 0.
			if (this.getMaximoDocumentoAdj() <= 1) {
				this.setHabilitaBtnAdjDoc(false);
			} else {
				this.setHabilitaBtnAdjDoc(true);
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al cargar los documentos Adjuntados Proyecci�n Respuesta.");
		}
		
		return tmpDetaSoli;
	}
	
	public void setRptDocumento(DetalleMovimiento prmDetalleMovimiento) {
		DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
		tmpDetalleMovimiento.setId(prmDetalleMovimiento.getId());
		
		this.setDetalleMovimiento(prmDetalleMovimiento);
		/*
		try {
			this.setDetalleMovimiento(this.getMovimientoDetalleServiceRemote().buscarXId(tmpDetalleMovimiento));
		} catch (Exception e) {
			super.setMensajeError("Error al recuperar documento ");
			logger.error(e);
		}*/
	}
	
	public String eliminar() {
		boolean sw = false;
		/*try {*/
			//this.getDetalleMovimiento().setUsuarioIdModificacion(this.getUsuario().getId());
		int cantInicial = this.getLstDetalleMovimiento().size();
		this.getLstDetalleMovimiento().remove(this.getDetalleMovimiento());
		int cantFinal = this.getLstDetalleMovimiento().size();
			
		if (cantFinal != cantInicial) { //Si cantFinal es  menor que cantInicial significa que se ha eliminado de la lista.
			sw = true;
		}
		
		if (cantFinal < this.getCantMax()) {
			this.setHabilitaBtnAdjDoc(true);
			this.setMaxDocsAdjs(this.getCantMax()-cantFinal);
		} else {
			this.setHabilitaBtnAdjDoc(false);
			this.setMaxDocsAdjs(0);
		}
		
		this.borrarAdjuntos();
			
			/*
			sw = this.getMovimientoDetalleServiceRemote().eliminar(this.getDetalleMovimiento());*/
		if (sw) {
			super.setMensajeAviso("�xito al eliminar Documento.");
				//this.setMaximoDocumentoAdj(5); //Setear maximo documento adjuntados.
				/*Long soliMoviId = this.getSolicitudMovimientoAdministrado().getId();
				this.setLstDetalleMovimiento(this.listarDocumentosIngresados(soliMoviId));*/
				
		} else {
			super.setMensajeError("Error al eliminar Documento.");
		}
		/*} catch (Exception e) {
			super.setMensajeError("Error al eliminar Documento CHT");
			logger.error(e);
		}*/

		return "bandejaSecretariaUO";
	}

	public void limpiarBusqueda2(){
		this.setReporteBandeja(new ReporteBandeja());
		this.getReporteBandeja().setEstado_solicitud_id(5L);
		Calendar cal= Calendar.getInstance();
		this.getReporteBandeja().setAnio_solicitud(String.valueOf(cal.get(Calendar.YEAR)));
	}
	
	public void updateAutoPersonaPanel(){
		if(this.getTmpFormularioBndRep().getFlag_autoriza_recojo().equals("SI")){
			this.setSwAutorizaPersona(true);
		} else {
			this.setSwAutorizaPersona(false);
		}
	}
	//Devoluci�n de la respuesta del Responsable que estaba dirigido para el asistente OAJ
	public boolean validarDevolucionPreAsisOAJr(){

		ReqContext = RequestContext.getCurrentInstance();
		/*
		if (!this.validarDerivPreUser()) {
			//ReqContext.update(":frmRegSolicitud:mensaje");
			return false;
		}*/
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar3').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	
	// Enviar a Responsable UO
	public String grabar1() {

		String pagina = "bandejaSecretariaUO";

		ReturnObject sw = new ReturnObject();

		if (validarDerivUsuario()) {

			try {
				this.getSolicitudMoviUsuario().setUsuarioIdEmisor(this.getUsuario().getId()); //
				//this.setLstDetalleDerivado(new ArrayList<DetalleDerivado>());
				this.getSolicitudMoviUsuario().setPerfilIdEmisor(4L); //4: Secreatar�a �rgano.
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviUsuario().setT01UndOrgEmiId(unOrgEmisorId); //
				String unOrgReceptorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviUsuario().setT01UndOrgRecId(unOrgReceptorId); // Se sabe que el secretario esta derivando a un responsable de su misma �rea.
				this.getSolicitudMoviUsuario().setPerfilIdReceptor(5L); //5: Responsable �rgano.
				//this.getSolicitudMoviUsuario().setUsuarioIdReceptor(0L); //El usuario se asigna desde el formulario.
				this.getSolicitudMoviUsuario().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMoviUsuario().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				this.getSolicitudMoviUsuario().setEstadoSolicitudId("6"); // 6: Asignado Usuario Responsable.
				
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMoviUsuario());
						
				if (sw.getId() != 0) {
					super.setMensajeAviso("�xito al asignar a Usuario Responsable.");
					this.setSolicitudMoviUsuario(new SolicitudMovimiento());
				} else {
					super.setMensajeAlerta("Error al asignar a Usuario Responsable.");
				}
				
				this.cancelarSeleccionUO();
				this.buscarConsulta2();
				
			} catch (Exception e) {
				super.setMensajeError("Error al asignar a Usuario Responsable.");
				logger.error(e);
			}

		}
		return pagina;
	}
	// Enviar a Asistente OAJ
	// Enviar a Responsable OAJ
	public String grabar2() {

		String pagina = "bandejaSecretariaUO";

		ReturnObject sw = new ReturnObject();
		
		int cantDocs = this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas().size();
		
		if (validarDerivAsistOAJ()) {
				
			boolean swAlfresco = true;
				
			String nameFileAlfrescoError = "";
			
			
			if ( cantDocs > 0) {
				
				for(DetalleMovimiento tmpDetalleMov : this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas()) {
					
					AlfrescoDocumentoAdjunto alfrescoAdjunto = new AlfrescoDocumentoAdjunto();
	
					ResultAlfresco resultAlfresco01 = this.subirArchivoAlfresco(alfrescoAdjunto, tmpDetalleMov.getFileAlfresco());
	
					if (resultAlfresco01.getCodigo().equals(Constantes.OPERACION_EXITOSA_ALFRESCO)) {
						tmpDetalleMov.getFileAlfresco().setUiid(resultAlfresco01.getUiid());
						tmpDetalleMov.getFileAlfresco().setRawFile(null);
						tmpDetalleMov.setUuidDocumento(resultAlfresco01.getUiid());
					} else {
						if(VO.isEmpty(nameFileAlfrescoError)){
							nameFileAlfrescoError = tmpDetalleMov.getFileAlfresco().getFileName();
						} else {
							nameFileAlfrescoError = nameFileAlfrescoError + "; " + tmpDetalleMov.getFileAlfresco().getFileName();
						}
						
						swAlfresco = false;
	
						logger.error(resultAlfresco01.getCodigo() + " : " + resultAlfresco01.getDescripcion());
						logger.error("Nombre Documeto" + tmpDetalleMov.getFileAlfresco().getFileName());
						
					}
					
				}
			}
				
			if (!swAlfresco) {
				this.setMensajeError("Ocurri� un error al cargar el/los archivo(s) : " + nameFileAlfrescoError);
				return "";
			}
			
			try {
				this.getSolicitudMoviAsistOAJ().setUsuarioIdEmisor(this.getUsuario().getId()); //
				this.getSolicitudMoviAsistOAJ().setPerfilIdEmisor(4L); //4: Secreatar�a �rgano.
				String unOrgId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviAsistOAJ().setT01UndOrgEmiId(unOrgId); //
				this.getSolicitudMoviAsistOAJ().setT01UndOrgRecId("8"); //Area de OAJ - Para el Asistente OAJ.
//				this.getSolicitudMoviAsistOAJ().setPerfilIdReceptor(2L); //Para el 2: Asistente OAJ
				this.getSolicitudMoviAsistOAJ().setPerfilIdReceptor(3L); //Para el 3: Responsable REIP
				this.getSolicitudMoviAsistOAJ().setUsuarioIdReceptor(0L); //Si ingresa 0L debe de ingresar s� o s� el perfil a quien se env�a LA SOLICITUD.
				this.getSolicitudMoviAsistOAJ().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMoviAsistOAJ().setSituacionSolicitudId("2"); // 2: EN TR�MITE.
//				this.getSolicitudMoviAsistOAJ().setEstadoSolicitudId("8"); // 8: Respuesta Asignado a Asistente OAJ.
				this.getSolicitudMoviAsistOAJ().setEstadoSolicitudId("9"); // 9: Proyectado Respuesta Final a Jefe REIP.
				this.getSolicitudMoviAsistOAJ().setDetalleAsignacion("(Contenido de Env�o)");
				//this.parsearDocumento();
				
				//Guarda en un temporal los documentos subido por el usuario actual.
				SolicitudMovimiento tmpSolicMov = new SolicitudMovimiento();
				// Si se tiene una lista de documentos junto con el reporte de respuestase setea.
				if (this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas().size() > 0) {
					tmpSolicMov.setListaDocsRespuestas(this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas());
					this.getSolicitudMoviAsistOAJ().setListaDocsRespuestas(new ArrayList<DetalleMovimiento> ());
				}
				
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMoviAsistOAJ());
				int nroFal = 0;
				
				//Setear la uuid del documento actual para poder subirlo a la BD detalle_movimiento.
				SolicitudMovimiento tmpListaDocumentoFinal = new SolicitudMovimiento();
				if (sw.getId() != 0) {
					int cantDocsTraidos = this.getLstDetalleMovimiento().size();
					//AQUI INSERCION DE DOCUMENTOS.
					if(cantDocs > 0 || cantDocsTraidos > 0){ //Si tiene documentos adjuntos guarda primero O si tiene documentos que han sido traidos.
						//VER MANERA DE USAR VARIABLES EFICIENTEMENTE.
						ReturnObject sw2 = new ReturnObject();
						try {
							
							//Solo si trajo documentos que subi� el anterior usuario se a�ade a los que subi� el actual usuario.
							if(this.getLstDetalleMovimiento().size() > 0) { //Documentos extraidos
								for (DetalleMovimiento tmpDetaMovi: this.getLstDetalleMovimiento()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							//Llevar el orden deguardar losdocumentos como han sido adjuntadocronologicamente.
							if(tmpSolicMov.getListaDocsRespuestas().size() > 0) { //Documentos recien subidos.
								for (DetalleMovimiento tmpDetaMovi: tmpSolicMov.getListaDocsRespuestas()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							
							for (DetalleMovimiento tmpDetalleMov3 : tmpListaDocumentoFinal.getListaDocsRespuestas()){
							
								DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
								
								tmpDetalleMovimiento.setSolicitudMovimientoId(sw.getId()); 
								tmpDetalleMovimiento.setTipoDocumentoId("17"); // TIPO DOCUMENTO: OTROS
								tmpDetalleMovimiento.setNombreDocumento(tmpDetalleMov3.getNombreDocumento());
								tmpDetalleMovimiento.setUuidDocumento(tmpDetalleMov3.getUuidDocumento());
								tmpDetalleMovimiento.setUsuarioIdRegistro(this.getUsuario().getId());
								sw2 = this.getMovimientoDetalleServiceRemote().insertar(tmpDetalleMovimiento);
								//Contador de fallas.
								if (sw2.getId() == 0) { nroFal ++; }
							}
							
							if (nroFal > 0 ) { //Se hizo el movimiento pero no adjunt�.
								super.setMensajeAviso("Se hizo el movimiento, pero hubo un error al referenciar los documentos adjuntados, total: " + nroFal);
								super.setMensajeAviso("Podr� visualizar la solicitud en la categoria - Situaci�n: OBSERVACI�N NOTIFICADO.");
								this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
								this.buscarConsulta2(); // <--- Listar. 
								return pagina;
							}
						} catch (Exception e) {
							super.setMensajeError("Error al enviar la Respuesta al Asistente OAJ."); //Se hizo el movimiento pero no adjunt�.
							logger.error(e);
							super.setMensajeAviso("Podr� visualizar la solicitud en la categoria - Situaci�n: OBSERVACI�N NOTIFICADO.");
							this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
							this.buscarConsulta2(); // <--- Listar. 
							return pagina;
						}
						
					}
					this.setSolicitudMoviAsistOAJ(new SolicitudMovimiento());
					super.setMensajeAviso("�xito al enviar la Respuesta al Asistente OAJ.");
					this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar. 
					
				} else {
					super.setMensajeAlerta("Error al enviar la Respuesta al Asistente OAJ.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al enviar la Respuesta al Asistente OAJ.");
				logger.error(e);
				this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
				this.buscarConsulta2(); // <--- Listar. 
			}

		}
		return pagina;
	}
	// Devoluci�n Respuesta de Responsable UO
	public String grabar3() {

		String pagina = "bandejaSecretariaUO";

		ReturnObject sw = new ReturnObject();
		
		int cantDocs = this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas().size();
		
		if (validarDerivAsistOAJ()) {
				
			boolean swAlfresco = true;
				
			String nameFileAlfrescoError = "";
			
			
			if ( cantDocs > 0) {
				
				for(DetalleMovimiento tmpDetalleMov : this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas()) {
					
					AlfrescoDocumentoAdjunto alfrescoAdjunto = new AlfrescoDocumentoAdjunto();
	
					ResultAlfresco resultAlfresco01 = this.subirArchivoAlfresco(alfrescoAdjunto, tmpDetalleMov.getFileAlfresco());
	
					if (resultAlfresco01.getCodigo().equals(Constantes.OPERACION_EXITOSA_ALFRESCO)) {
						tmpDetalleMov.getFileAlfresco().setUiid(resultAlfresco01.getUiid());
						tmpDetalleMov.getFileAlfresco().setRawFile(null);
						tmpDetalleMov.setUuidDocumento(resultAlfresco01.getUiid());
					} else {
						if(VO.isEmpty(nameFileAlfrescoError)){
							nameFileAlfrescoError = tmpDetalleMov.getFileAlfresco().getFileName();
						} else {
							nameFileAlfrescoError = nameFileAlfrescoError + "; " + tmpDetalleMov.getFileAlfresco().getFileName();
						}
						
						swAlfresco = false;
	
						logger.error(resultAlfresco01.getCodigo() + " : " + resultAlfresco01.getDescripcion());
						logger.error("Nombre Documeto" + tmpDetalleMov.getFileAlfresco().getFileName());
						
					}
					
				}
			}
				
			if (!swAlfresco) {
				this.setMensajeError("Ocurri� un error al cargar el/los archivo(s) : " + nameFileAlfrescoError);
				return "";
			}
			
			try {
				
				Long idUsuarioEmisor = 0L;
				if(this.getLstSoliMoviXLineaTP()!= null && this.getLstSoliMoviXLineaTP().size()>0){
					for(SolicitudMovimiento sol: this.getLstSoliMoviXLineaTP()){
						idUsuarioEmisor = sol.getUsuarioIdEmisor();
					}
				}			
				
				this.getSolicitudMoviAsistOAJ().setUsuarioIdEmisor(0L);
				this.getSolicitudMoviAsistOAJ().setPerfilIdEmisor(4L); //4: Secreatar�a �rgano.
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviAsistOAJ().setT01UndOrgEmiId(unOrgEmisorId); //
				String unOrgRecId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviAsistOAJ().setT01UndOrgRecId(unOrgRecId); // Se sabe que el secretario esta derivando a un responsable de su misma �rea.
				
				this.getSolicitudMoviAsistOAJ().setPerfilIdReceptor(5L); //5: Responsable �rgano.
				this.getSolicitudMoviAsistOAJ().setUsuarioIdReceptor(idUsuarioEmisor); // Si ingresa 0 el usuario lo saca por el perfil y el �rea.
				this.getSolicitudMoviAsistOAJ().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMoviAsistOAJ().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				this.getSolicitudMoviAsistOAJ().setEstadoSolicitudId("14"); // 14: Devuelto Respuesta a Responsable UO.
				this.getSolicitudMoviAsistOAJ().setDetalleAsignacion(this.getTxtMotivoDevolucion1());
				//this.parsearDocumento();
				
				//Guarda en un temporal los documentos subido por el usuario actual.
				SolicitudMovimiento tmpSolicMov = new SolicitudMovimiento();
				// Si se tiene una lista de documentos junto con el reporte de respuestase setea.
				if (this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas().size() > 0) {
					tmpSolicMov.setListaDocsRespuestas(this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas());
					this.getSolicitudMoviAsistOAJ().setListaDocsRespuestas(new ArrayList<DetalleMovimiento> ());
				}
				
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMoviAsistOAJ());
				int nroFal = 0;
				
				//Setear la uuid del documento actual para poder subirlo a la BD detalle_movimiento.
				SolicitudMovimiento tmpListaDocumentoFinal = new SolicitudMovimiento();
				if (sw.getId() != 0) {
					int cantDocsTraidos = this.getLstDetalleMovimiento().size();
					//AQUI INSERCION DE DOCUMENTOS.
					if(cantDocs > 0 || cantDocsTraidos > 0){ //Si tiene documentos adjuntos guarda primero O si tiene documentos que han sido traidos.
						//VER MANERA DE USAR VARIABLES EFICIENTEMENTE.
						ReturnObject sw2 = new ReturnObject();
						try {
							//Solo si trajo documentos que subi� el anterior usuario se a�ade a los que subi� el actual usuario.
							if(this.getLstDetalleMovimiento().size() > 0) { //Documentos extraidos
								for (DetalleMovimiento tmpDetaMovi: this.getLstDetalleMovimiento()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							//Llevar el orden deguardar losdocumentos como han sido adjuntadocronologicamente.
							if(tmpSolicMov.getListaDocsRespuestas().size() > 0) { //Documentos recien subidos.
								for (DetalleMovimiento tmpDetaMovi: tmpSolicMov.getListaDocsRespuestas()) {
									tmpListaDocumentoFinal.getListaDocsRespuestas().add(tmpDetaMovi);//Documento recien subidos.
								}
							}
							
							for (DetalleMovimiento tmpDetalleMov3 : tmpListaDocumentoFinal.getListaDocsRespuestas()){
							
								DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
								
								tmpDetalleMovimiento.setSolicitudMovimientoId(sw.getId()); 
								tmpDetalleMovimiento.setTipoDocumentoId("17"); // TIPO DOCUMENTO: OTROS
								tmpDetalleMovimiento.setNombreDocumento(tmpDetalleMov3.getNombreDocumento());
								tmpDetalleMovimiento.setUuidDocumento(tmpDetalleMov3.getUuidDocumento());
								tmpDetalleMovimiento.setUsuarioIdRegistro(this.getUsuario().getId());
								sw2 = this.getMovimientoDetalleServiceRemote().insertar(tmpDetalleMovimiento);
								//Contador de fallas.
								if (sw2.getId() == 0) { nroFal ++; }
							}
							
							if (nroFal > 0 ) { //Se hizo el movimiento pero no adjunt�.
								super.setMensajeAviso("Se hizo el movimiento, pero hubo un error al referenciar los documentos adjuntados, total: " + nroFal);
								super.setMensajeAviso("Podr� visualizar la solicitud en la categoria - Situaci�n: OBSERVACI�N NOTIFICADO.");
								this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
								this.buscarConsulta2(); // <--- Listar. 
								return pagina;
							}
						} catch (Exception e) {
							super.setMensajeError("Error al devolver la Respuesta al Responsable UO."); //Se hizo el movimiento pero no adjunt�.
							logger.error(e);
							super.setMensajeAviso("Podr� visualizar la solicitud en la categoria - Situaci�n: OBSERVACI�N NOTIFICADO.");
							this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
							this.buscarConsulta2(); // <--- Listar. 
							return pagina;
						}
						
					}

					super.setMensajeAviso("�xito al devolver la Respuesta al Responsable UO.");
					this.setTxtMotivoDevolucion1("");
					this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
					this.buscarConsulta2(); // <--- Listar. 
					
				} else {
					super.setMensajeAlerta("Error al devolver la Respuesta al Responsable UO.");
				}

			} catch (Exception e) {
				super.setMensajeError("Error al devolver la Respuesta al Responsable UO.");
				logger.error(e);
				this.cancelarSeleccionUO(); // <--- Limpieza e inhabilitaci�n del formulario.
				this.buscarConsulta2(); // <--- Listar. 
			}

		}
		return pagina;
	}
	
	public boolean validarDevolPreAsisOAJ(){

		ReqContext = RequestContext.getCurrentInstance();
		/*
		if (!this.validarDerivPreUser()) {
			//ReqContext.update(":frmRegSolicitud:mensaje");
			return false;
		}*/
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar4').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	// Devoluci�n a asistente  OAJ.
	public String grabar4() {

		String pagina = "bandejaSecretariaUO";

		ReturnObject sw = new ReturnObject();

		if (!VO.isEmpty(this.getTxtMotivoDevolucion2())) {

			try {
				this.getSolicitudMoviUsuario().setUsuarioIdEmisor(this.getUsuario().getId()); //
				//this.setLstDetalleDerivado(new ArrayList<DetalleDerivado>());
				this.getSolicitudMoviUsuario().setPerfilIdEmisor(4L); //4: Secreatar�a �rgano.
				String unOrgEmisorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviUsuario().setT01UndOrgEmiId(unOrgEmisorId); //
				//String unOrgReceptorId = this.getTmpFormularioBndRep().getT01_und_org_receptor_id();
				this.getSolicitudMoviUsuario().setT01UndOrgRecId("8"); // La debe derecibir OAJ.
				this.getSolicitudMoviUsuario().setPerfilIdReceptor(2L); //2: Asistente OAJ.
				this.getSolicitudMoviUsuario().setUsuarioIdReceptor(0L); //Si no se ingresa se le asigna por PKG.
				this.getSolicitudMoviUsuario().setUsuarioIdRegistro(this.getUsuario().getId());
				this.getSolicitudMoviUsuario().setSituacionSolicitudId("2"); // 2: EN TRAMITE.
				this.getSolicitudMoviUsuario().setEstadoSolicitudId("8"); // 8: Respuesta Asignado a Asistente OAJ.
				this.getSolicitudMoviUsuario().setDetalleRespuesta("");
				this.getSolicitudMoviUsuario().setDetalleRespuesta(this.getTxtMotivoDevolucion2());
				this.getSolicitudMoviUsuario().setDetalleAsignacion("");
				sw = this.getMovimientoSolicitudServiceRemote().insertar(this.getSolicitudMoviUsuario());
						
				if (sw.getId() != 0) {
					this.setSolicitudMoviUsuario(new SolicitudMovimiento());
					super.setMensajeAviso("�xito al devolver solicitud a Asistente OAJ.");
				} else {
					super.setMensajeAlerta("Error al devolver solicitud a Asistente OAJ.");
				}
				this.setTxtMotivoDevolucion2("");
				this.cancelarSeleccionUO();
				this.buscarConsulta2();
				
			} catch (Exception e) {
				super.setMensajeError("Error al devolver solicitud a Asistente OAJ.");
				logger.error(e);
			}

		} else {
			super.setMensajeAlerta("Debe de ingresar un detalle de devoluci�n v�lido.");
		}
		
		return pagina;
	}
	
	public boolean validarDerivPreAsistOAJ(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivAsistOAJ()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}
		
		//super.setMensajeAlerta("OK 2");
		ReqContext.execute("PF('idGrpConfirmar2').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	//PANEL2
	private boolean validarDerivAsistOAJ() {

		int contador = 0;
		String mensaje = "";
		
		if ( this.getSolicitudMoviAsistOAJ().getSolicitudTransparenciaId() == 0L ) {
			mensaje = mensaje.equals("") ? "No se ha extra�do ninguna solicitud."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMoviAsistOAJ().getDetalleRespuesta().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un contenido v�lido."
					: mensaje;
			contador++;
		}
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		
		logger.debug("Validado con �xito:");
		return true;
	}

	// SMTP
	public boolean enviarCorreoAdministrado(String prmAsunto, String prmCorreo, String prmCuerpo, ArrayList<String> prmLstUuidds) {
		boolean envio = false;

		try {
			boolean swMailEnviado = false;
			SmtpSendServiceImpl smtp = new SmtpSendServiceImpl();
			try {
					
				String asunto = prmAsunto;
				String cuerpoCorreo = this.getRegistroPlantillaCorreo("3");
				String pmrCorreoUsuario = prmCorreo;

				cuerpoCorreo = cuerpoCorreo.replace("{t_detalle}", prmCuerpo);
					
				String UrlPreProp = super.getString("url.FomularioSolicitud");
				
				if(!VO.isEmpty(UrlPreProp)){
					if(cuerpoCorreo.contains("{t_urlNOCAMBIAR}")){
						cuerpoCorreo = cuerpoCorreo.replace("{t_urlNOCAMBIAR}", UrlPreProp);
					}
				}
				
				//PREPARANDO: Si existen documentos para adjuntar.
				if(prmLstUuidds.size() > 0){
					AlfrescoBase alfrescoBase = new AlfrescoBase();
					ArrayList<FileAlfresco> tmpLstFileAlfresco = new ArrayList<FileAlfresco>();
					String ruta = super.getString("ruta.documentos.adjunto.email");
					
					for (String uuid : prmLstUuidds) {
						FileAlfresco fileAlfresco = new FileAlfresco();
						//Extrayendo documentos en servidor.
						fileAlfresco = alfrescoBase.obtenerArchivoAlfresco(uuid);
						
						FileAlfresco tmpFileAlfresco = new FileAlfresco();
						tmpFileAlfresco.setFileName(fileAlfresco.getFileName());
						tmpFileAlfresco.setRutaServidorTemp(ruta + fileAlfresco.getFileName());
						
						tmpLstFileAlfresco.add(tmpFileAlfresco);
						//Poner en temporales el File.
						File file = new File(ruta, fileAlfresco.getFileName());
						FileOutputStream salida = new FileOutputStream(file);
						salida.write(fileAlfresco.getRawFile());
						salida.flush();
						salida.close();
					}
					
					// 2. Env�o de correo CON adjuntos.
					swMailEnviado = smtp.envioCorreoUsuario(asunto, cuerpoCorreo, pmrCorreoUsuario, tmpLstFileAlfresco);
				// Sin env�o Adjuntos.
				} else {
					// 1. Env�o de correo SIN adjuntos.
					swMailEnviado = smtp.envioCorreoUsuario(asunto, cuerpoCorreo, pmrCorreoUsuario);
				}
				
						
				if (!swMailEnviado) {
					super.setMensajeError("No se pudo enviar la notificaci�n.");
				}

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
				
			if (swMailEnviado) {
				envio = true;
			} else {
				super.setMensajeError("No se pudo enviar la notificaci�n.");
			}

			this.cargarplantillas();

		} catch (Exception e) {
			super.setMensajeError("Error al enviar notificaci�n.");
		}
			
		return envio;
	}

	private String getRegistroPlantillaCorreo(String codigo) {
		this.cargarplantillas();
		for (T01Maestro t01Maestro : this.getLstPlantillaCorreo()) {
			if (t01Maestro.getCodigoRegistro().equals(codigo)) {
				return t01Maestro.getNombreLargo();
			}
		}
		return "";
	}
	
	public void cargarplantillas() {
		this.setLstPlantillaCorreo(super.getT01MaestroValuesNL("PLANTILLA_CORREO"));
	}

	//SUBIR
	private ResultAlfresco subirArchivoAlfresco(
		AlfrescoDocumentoAdjunto alfrescoAdjunto, FileAlfresco fileAlfresco) {
		
		//categoriaDoc.setTipoDocumento(super.getString("directorio.uno"));
		
		Date actual = new Date();
		
		SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
		String anio = formatAnio.format(actual);
		SimpleDateFormat formatMes = new SimpleDateFormat("MM");
		String mes = formatMes.format(actual);
		String proceso = "ADJUNTOS";
		
		ResultAlfresco resultAlfresco = new ResultAlfresco();
		resultAlfresco = alfrescoAdjunto.subirArchivo(fileAlfresco, proceso, anio, mes);
		
		return resultAlfresco;
	}
	// COMBO
	public void buscarConsulta2(){
		
		try {
			this.getReporteBandeja().setUsuario_id_responsable(this.getUsuario().getId());
			//EJB Listar.
			this.setLstReporteBandeja(this.getBandejaServiceRemote().lstReporteBandejaSecretariaUO(this.getReporteBandeja()));
			this.cancelarSeleccionUO();
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al buscar solicitud!");
		}
	}
	//Panel1
	public boolean validarDerivPreUser(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivUsuario()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}
		//super.setMensajeAlerta("OK 1");
		ReqContext.execute("PF('idGrpConfirmar1').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	/*
	public boolean validarDerivPreAdministrado(){

		ReqContext = RequestContext.getCurrentInstance();

		if (!this.validarDerivAdministrado()) {
		//ReqContext.update(":frmRegSolicitud:mensaje");
		return false;
		}
		
		//super.setMensajeAlerta("OK 2");
		ReqContext.execute("PF('idGrpConfirmar2').show()");
		//ReqContext.update("frmBandejaAtencion:idGrpProyectarConfirmacion");

		return true;
	}
	*/
	public String verDetallexrpt(ReporteBandeja prmRptBan) {

		this.setTmpReporteBandeja(prmRptBan);
		
		return "detalleBandSecretariaUO";
	}

	// ALFRESCO
	public void handleFileUpload(FileUploadEvent event) {
		Archivo archivo = new Archivo();
		String fileName;
		FileAlfresco fileAlfresco = new FileAlfresco();

		try {

			UploadedFile fu = event.getFile();

			fileName = fu.getFileName();

			fileAlfresco.setFileName(fileName);
			fileAlfresco.setExtension(fileName.substring(fileName.lastIndexOf(".") + 1));

			File file = new File(super.getString("ruta.documentos.adjunto.email"),
					fileName);
			FileOutputStream salida = new FileOutputStream(file);
			salida.write(fu.getContents());
			salida.flush();
			salida.close();

			String pathFile = file.getPath();
			
			fileAlfresco.setRawFile(archivo.read(new File(pathFile)));
			
			fileAlfresco.setTipoDoc("Adjunto");

			file.delete();
			
			archivo = null;
				//Agregar a la lista cada uno de los documentos adjuntados.
				DetalleMovimiento tmpDetalleMovimiento = new DetalleMovimiento();
				//tmpDetalleMovimiento.setSolicitudMovimientoId(Long); <-- A�n no se guard� SOLICITUD_MOVIMIENTO.
				tmpDetalleMovimiento.setFileAlfresco(fileAlfresco);
				tmpDetalleMovimiento.setNombreDocumento(fileAlfresco.getFileName());
				
				this.getSolicitudMoviAsistOAJ().getListaDocsRespuestas().add(tmpDetalleMovimiento);
				
				//Extrae el nombre y la muestra en el output.
				if(VO.isEmpty(this.getNombreAdjunto01())){
					this.setNombreAdjunto01(this.getCont()+". "+tmpDetalleMovimiento.getNombreDocumento());
				} else {
					this.setNombreAdjunto01(this.getNombreAdjunto01()+"; "+this.getCont()+". "+tmpDetalleMovimiento.getNombreDocumento());
				}
				
				this.setCont(this.getCont() + 1);
				
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			this.setMensajeError("Error al cargar el archivo");
		}

	}
	/**/
	// DESCARGAR:
	public void descargarFile(String uuidDocumento) {
		AlfrescoBase alfrescoBase = new AlfrescoBase();
		FileAlfresco fileAlfresco = new FileAlfresco();

		fileAlfresco = alfrescoBase.obtenerArchivoAlfresco(uuidDocumento);
			// fileAlfresco.setFileName(this.getFileAlfrescoAdjUno().getFileName());

		try {
			StreamedContent streamedContent = new DefaultStreamedContent(
				new ByteArrayInputStream(fileAlfresco.getRawFile()),
					"application/" + fileAlfresco.getExtension(),
					fileAlfresco.getFileName());
			super.setStreamedContent(streamedContent);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}
	
	public void borrarAdjuntos() {
		this.getSolicitudMoviAsistOAJ().setListaDocsRespuestas(new ArrayList<DetalleMovimiento>());
		this.setNombreAdjunto01("");
		this.setCont(1);
	}

	private boolean validarDerivUsuario() {

		int contador = 0;
		String mensaje = "";
		
		if ( this.getSolicitudMoviUsuario().getSolicitudTransparenciaId() == 0L ) {
			mensaje = mensaje.equals("") ? "No se ha extra�do ninguna solicitud."
					: mensaje;
			contador++;
		}
		
		if ( this.getSolicitudMoviUsuario().getUsuarioIdReceptor() == 0L ) {
			mensaje = mensaje.equals("") ? "Seleccione un usuario responsable."
					: mensaje;
			contador++;
		}
		
		if (VO.isEmpty(this.getSolicitudMoviUsuario().getDetalleAsignacion().trim())) {
			mensaje = mensaje.equals("") ? "Ingrese un Detalle de Asignaci�n. XD"
					: mensaje;
			contador++;
		}
		
		if (contador > 0) {
			super.setMensajeAlerta(mensaje);
			return false;
		}
		
		logger.debug("Validado con �xito:");
		return true;
	}
	
	/************************************************/
	// GETTERS AND SETTERS.
	//VARIABLES
	public ReporteBandeja getReporteBandeja() {
		return reporteBandeja;
	}

	public void setReporteBandeja(ReporteBandeja reporteBandeja) {
		this.reporteBandeja = reporteBandeja;
	}

	public List<ReporteBandeja> getLstReporteBandeja() {
		return lstReporteBandeja;
	}

	public void setLstReporteBandeja(List<ReporteBandeja> lstReporteBandeja) {
		this.lstReporteBandeja = lstReporteBandeja;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}

	public ReporteBandeja getTmpFormularioBndRep() {
		return tmpFormularioBndRep;
	}

	public void setTmpFormularioBndRep(ReporteBandeja tmpFormularioBndRep) {
		this.tmpFormularioBndRep = tmpFormularioBndRep;
	}


	public boolean isSwAutorizaPersona() {
		return swAutorizaPersona;
	}

	public void setSwAutorizaPersona(boolean swAutorizaPersona) {
		this.swAutorizaPersona = swAutorizaPersona;
	}

	public SolicitudMovimiento getSolicitudMoviUsuario() {
		return solicitudMoviUsuario;
	}

	public void setSolicitudMoviUsuario(SolicitudMovimiento solicitudMoviUsuario) {
		this.solicitudMoviUsuario = solicitudMoviUsuario;
	}

	public RequestContext getReqContext() {
		return ReqContext;
	}

	public void setReqContext(RequestContext reqContext) {
		ReqContext = reqContext;
	}
	
	public List<T01Maestro> getLstT01TipoDocumento() {
		return lstT01TipoDocumento;
	}

	public void setLstT01TipoDocumento(List<T01Maestro> lstT01TipoDocumento) {
		this.lstT01TipoDocumento = lstT01TipoDocumento;
	}

	public List<UbigeoReniec> getLstDepartamento() {
		return lstDepartamento;
	}

	public void setLstDepartamento(List<UbigeoReniec> lstDepartamento) {
		this.lstDepartamento = lstDepartamento;
	}
	
	public List<UbigeoReniec> getLstProvincia() {
		return lstProvincia;
	}

	public void setLstProvincia(List<UbigeoReniec> lstProvincia) {
		this.lstProvincia = lstProvincia;
	}

	public List<UbigeoReniec> getLstDistrito() {
		return lstDistrito;
	}

	public void setLstDistrito(List<UbigeoReniec> lstDistrito) {
		this.lstDistrito = lstDistrito;
	}

	public boolean isHabilitaRespuestaUsuario() {
		return habilitaRespuestaUsuario;
	}

	public void setHabilitaRespuestaUsuario(boolean habilitaRespuestaUsuario) {
		this.habilitaRespuestaUsuario = habilitaRespuestaUsuario;
	}

	public boolean isVistaTramiteOAJ() {
		return vistaTramiteOAJ;
	}

	public void setVistaTramiteOAJ(boolean vistaTramiteOAJ) {
		this.vistaTramiteOAJ = vistaTramiteOAJ;
	}

	public boolean isVistaRespuestaUsuario() {
		return vistaRespuestaUsuario;
	}

	public void setVistaRespuestaUsuario(boolean vistaRespuestaArea) {
		this.vistaRespuestaUsuario = vistaRespuestaArea;
	}

	public boolean isVistaRegistroSelecc() {
		return vistaRegistroSelecc;
	}

	public void setVistaRegistroSelecc(boolean vistaRegistroSelecc) {
		this.vistaRegistroSelecc = vistaRegistroSelecc;
	}
	
	
	public ReporteBandeja getTmpReporteBandeja() {
		return tmpReporteBandeja;
	}

	public void setTmpReporteBandeja(ReporteBandeja tmpReporteBandeja) {
		this.tmpReporteBandeja = tmpReporteBandeja;
	}

	public List<SolicitudMovimiento> getLstSoliMoviXLineaTP() {
		return lstSoliMoviXLineaTP;
	}

	public void setLstSoliMoviXLineaTP(List<SolicitudMovimiento> lstSoliMoviXLineaTP) {
		this.lstSoliMoviXLineaTP = lstSoliMoviXLineaTP;
	}

	public DetalleMovimiento getDetalleMovimiento() {
		return detalleMovimiento;
	}

	public void setDetalleMovimiento(DetalleMovimiento detalleMovimiento) {
		this.detalleMovimiento = detalleMovimiento;
	}

	public List<ReporteDerivacion> getLstDetalleDerivado() {
		return lstDetalleDerivado;
	}

	public void setLstDetalleDerivado(List<ReporteDerivacion> lstDetalleDerivado) {
		this.lstDetalleDerivado = lstDetalleDerivado;
	}

	public boolean isVistaRespuestaProyeccion() {
		return vistaRespuestaProyeccion;
	}

	public void setVistaRespuestaProyeccion(boolean vistaRespuestaProyeccion) {
		this.vistaRespuestaProyeccion = vistaRespuestaProyeccion;
	}

	public boolean isHabilitaRespuestaProyeccion() {
		return habilitaRespuestaProyeccion;
	}

	public void setHabilitaRespuestaProyeccion(boolean habilitaRespuestaProyeccion) {
		this.habilitaRespuestaProyeccion = habilitaRespuestaProyeccion;
	}

	public boolean isHabilitaValidaInfoOAJ() {
		return habilitaValidaInfoOAJ;
	}

	public void setHabilitaValidaInfoOAJ(boolean habilitaValidaInfoOAJ) {
		this.habilitaValidaInfoOAJ = !habilitaValidaInfoOAJ;
	}

	public String getNombreAdjunto01() {
		return nombreAdjunto01;
	}

	public void setNombreAdjunto01(String nombreAdjunto01) {
		this.nombreAdjunto01 = nombreAdjunto01;
	}

	public List<DetalleMovimiento> getLstDetalleMovimiento() {
		return lstDetalleMovimiento;
	}

	public void setLstDetalleMovimiento(List<DetalleMovimiento> lstDetalleMovimiento) {
		this.lstDetalleMovimiento = lstDetalleMovimiento;
	}

	public List<T01Maestro> getLstPlantillaCorreo() {
		return lstPlantillaCorreo;
	}

	public void setLstPlantillaCorreo(List<T01Maestro> lstPlantillaCorreo) {
		this.lstPlantillaCorreo = lstPlantillaCorreo;
	}
	
	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}

	public List<Usuario> getLstUsuario() {
		return lstUsuario;
	}


	public void setLstUsuario(List<Usuario> lstUsuario) {
		this.lstUsuario = lstUsuario;
	}

	public List<T01Maestro> getLstUnidadesOrganicas() {
		return lstUnidadesOrganicas;
	}

	public void setLstUnidadesOrganicas(List<T01Maestro> lstUnidadesOrganicas) {
		this.lstUnidadesOrganicas = lstUnidadesOrganicas;
	}

	public SolicitudMovimiento getSolicitudMoviAsistOAJ() {
		return solicitudMoviAsistOAJ;
	}

	public void setSolicitudMoviAsistOAJ(SolicitudMovimiento solicitudMoviAsistOAJ) {
		this.solicitudMoviAsistOAJ = solicitudMoviAsistOAJ;
	}

	public String getMngAcordeon() {
		return mngAcordeon;
	}

	public void setMngAcordeon(String mngAcordeon) {
		this.mngAcordeon = mngAcordeon;
	}

	public String getTxtMotivoDevolucion1() {
		return txtMotivoDevolucion1;
	}

	public void setTxtMotivoDevolucion1(String txtMotivoDevolucion1) {
		this.txtMotivoDevolucion1 = txtMotivoDevolucion1;
	}

	public String getTxtMotivoDevolucion2() {
		return txtMotivoDevolucion2;
	}

	public void setTxtMotivoDevolucion2(String txtMotivoDevolucion2) {
		this.txtMotivoDevolucion2 = txtMotivoDevolucion2;
	}
	
	public String getNroSolicitudSelect() {
		return nroSolicitudSelect;
	}

	public void setNroSolicitudSelect(String nroSolicitudSelect) {
		this.nroSolicitudSelect = nroSolicitudSelect;
	}
	
	public boolean isHabilitaBtnAdjDoc() {
		return habilitaBtnAdjDoc;
	}

	public void setHabilitaBtnAdjDoc(boolean habilitaBtnAdjDoc) {
		this.habilitaBtnAdjDoc = !habilitaBtnAdjDoc;
	}

	public int getMaxDocsAdjs() {
		return maxDocsAdjs;
	}

	public void setMaxDocsAdjs(int maxDocsAdjs) {
		this.maxDocsAdjs = maxDocsAdjs;
	}

	public int getCantMax() {
		return cantMax;
	}

	public void setCantMax(int cantMax) {
		this.cantMax = cantMax;
	}

	//EJB ***************************
	public BandejaServiceRemote getBandejaServiceRemote() {
		return bandejaServiceRemote;
	}

	public void setBandejaServiceRemote(BandejaServiceRemote bandejaServiceRemote) {
		this.bandejaServiceRemote = bandejaServiceRemote;
	}

	public MovimientoSolicitudServiceRemote getMovimientoSolicitudServiceRemote() {
		return movimientoSolicitudServiceRemote;
	}

	public void setMovimientoSolicitudServiceRemote(
			MovimientoSolicitudServiceRemote movimientoSolicitudServiceRemote) {
		this.movimientoSolicitudServiceRemote = movimientoSolicitudServiceRemote;
	}

	public MovimientoDetalleServiceRemote getMovimientoDetalleServiceRemote() {
		return movimientoDetalleServiceRemote;
	}

	public void setMovimientoDetalleServiceRemote(
			MovimientoDetalleServiceRemote movimientoDetalleServiceRemote) {
		this.movimientoDetalleServiceRemote = movimientoDetalleServiceRemote;
	}
	
	public UsuarioServiceRemote getUsuarioServiceRemote() {
		return usuarioServiceRemote;
	}

	public void setUsuarioServiceRemote(UsuarioServiceRemote usuarioServiceRemote) {
		this.usuarioServiceRemote = usuarioServiceRemote;
	}


	public List<SelectItem> getListaGeneral() {
		return listaGeneral;
	}


	public void setListaGeneral(List<SelectItem> listaGeneral) {
		this.listaGeneral = listaGeneral;
	}

}

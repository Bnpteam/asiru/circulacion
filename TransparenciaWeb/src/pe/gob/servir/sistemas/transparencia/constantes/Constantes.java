package pe.gob.servir.sistemas.transparencia.constantes;

public class Constantes {	
	
	/*public static final String OBS_ULT_REV								= "1";		// Con observacion de Ultimo Revisor
	public static final String SIN_OBS_UR								= "0";		// Sin observacion del Ultimo Revisor (Fue derivado por encargado de atencion)
	
	public static final String STD										= "STD";
	public static final String GPGSC									= "GPGSC";*/
	
	public static final String OPERACION_EXITOSA_ALFRESCO				= "00000";
	
	// ESTADO
	
	/*// SIGLAS TIPO DE DOCUMENTO SUBIDO A ALFRESCO
	public static final String SIGLA_INFORME_TECNICO					= "IT";
	public static final String SIGLA_INFORME_RESPUESTA					= "OR";
	
	// ECE : ESTADO CONSULTA ESCRITA
	public static final String ECE_REGISTRADO_DE_STD_POR_REC_RESP		= "REGS_STD_REC_RES_E";
	public static final String ECE_DERIVADO_A_PRI_REV_POR_REC_RES		= "DERV_PRI_REV_X_REC_RES_E";
	public static final String ECE_DERIVADO_A_ULT_REV					= "DERV_ULT_REV_E";
	public static final String ECE_DERIVADO_A_PRI_REV_POR_ULT_REV		= "DERV_PRI_REV_X_ULT_REV_E";
	public static final String ECE_DERIVADO_A_ATENCION  				= "DERV_ATE_E";
	public static final String ECE_PROYECTADO_A_PRI_REV					= "PROY_PRI_REV_E";
	public static final String ECE_OBSERVADO_PRI_REV					= "OBS_PRI_REV_E";
	public static final String ECE_PROYECTADO_A_ULT_REV					= "PROY_ULT_REV_E";
	public static final String ECE_OBSERVADO_ULT_REV					= "OBS_ULT_REV_E";
	public static final String ECE_PROYECTADO_A_PE 						= "PROY_PE_E";

	//public static final String ECF_OBSERVADO_PE 						= "OBS_PE_F";
	public static final String ECE_FINALIZADO_PRI_REV					= "FIN_PRI_REV_E";
	public static final String ECE_ENVIADO_A_CIUDADANO 					= "ENV_CIU_E";
	public static final String ECE_NOTIFICADO_CIUDADANO 				= "NOTI_CIU_E";
	public static final String ECE_DEVUELTO_POR_REC_RES					= "DEV_REC_RES_E";
	public static final String ECE_DEVUELTO_POR_ULT_REV					= "DEV_ULT_REV_E";
	public static final String ECE_DEVUELTO_POR_PRI_REV_A_ULT_REV		= "DEV_PRI_REV_A_ULT_REV_E";
	public static final String ECE_DEVUELTO_POR_PRI_REV_A_REC_RES		= "DEV_PRI_REV_A_REC_RES_E";
	public static final String ECE_DEVUELTO_POR_ATENCION				= "DEV_ATE_E";
	
	public static final String ECE_REGISTRADO_DE_GPGSC_POR_REC_RESP		= "REGS_GPGSC_REC_RES_E";
	
	public static final String ECE_VALIDADO_ULTIMO_REVISOR				= "VALID_ULT_REV_E";
	
	public static final String CONSULTA_EXT_ESCRITA = "E";
	public static final String CONSULTA_EXT_VIRTUAL = "V";
	
	public static final String BANDEJA_RECEPCION_RESPUESTA_ESCRITA = "SISINT_BANDEJA_RECEP_RESP_E";
	public static final	String BANDEJA_PRIMER_REVISOR_ESCRITA = "SISINT_BANDEJA_PRIM_REVIS_E";
	public static final String BANDEJA_ATENCION_ESCRITA = "SISINT_BANDEJA_ATENCION_E";
	public static final String BANDEJA_ULTIMO_REVISOR_ESCRITA = "SISINT_BANDEJA_ULT_REVIS_E";
	
	//PARAMETROS BANDEJAS APLICATIVO EXTERNO VIRTUAL
	public static final String BANDEJA_ATENCION_GENERAL_VIRTUAL 	= "SISINT_BANDEJA_ATENCION_GENERAL_V";	
	public static final	String BANDEJA_PRIMER_REVISOR_VIRTUAL 		= "SISINT_BANDEJA_PRIM_REVIS_V";

	//PARAMETROS ESTADOS APLICATIVO EXTERNO VIRTUAL
	public static final String ECV_RECEPCIONADO_POR_ATENCION_GENERAL		= "RECP_ATE_GEN_V";
	public static final String ECV_NOTIFICADO_CIUDADANO						= "NOTI_CIU_V";
	public static final String ECV_OBSERVADO_A_ATENCION_GENERAL				= "OBS_ATE_GEN_V";
	public static final String ECV_PROYECTADO_A_REVISOR_ATENCION			= "PROY_ULT_REV_ATE_V";

	//PARAMETROS TIPOS DE EXPORTACION
	public static final String ECV_IMPORTACION_HISTORICO				= "1";
	public static final String ECV_IMPORTACION_BANDEJA_SECRETARIA		= "2";
	public static final String ECV_IMPORTACION_BANDEJA_COLABORADOR		= "3";*/
	
	//PLANTILLA DE CORREOS


}


package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDatosTramiteAIP complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDatosTramiteAIP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestSeguridad" type="{ws.service.std.servir.gob.pe}SeguridadType" minOccurs="0"/>
 *         &lt;element name="requestAuditoria" type="{ws.service.std.servir.gob.pe}AuditoriaType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDatosTramiteAIP", propOrder = {
    "requestSeguridad",
    "requestAuditoria"
})
public class GetDatosTramiteAIP {

    protected SeguridadType requestSeguridad;
    protected AuditoriaType requestAuditoria;

    /**
     * Gets the value of the requestSeguridad property.
     * 
     * @return
     *     possible object is
     *     {@link SeguridadType }
     *     
     */
    public SeguridadType getRequestSeguridad() {
        return requestSeguridad;
    }

    /**
     * Sets the value of the requestSeguridad property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeguridadType }
     *     
     */
    public void setRequestSeguridad(SeguridadType value) {
        this.requestSeguridad = value;
    }

    /**
     * Gets the value of the requestAuditoria property.
     * 
     * @return
     *     possible object is
     *     {@link AuditoriaType }
     *     
     */
    public AuditoriaType getRequestAuditoria() {
        return requestAuditoria;
    }

    /**
     * Sets the value of the requestAuditoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditoriaType }
     *     
     */
    public void setRequestAuditoria(AuditoriaType value) {
        this.requestAuditoria = value;
    }

}

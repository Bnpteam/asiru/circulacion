
package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TramiteType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TramiteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numTramite" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="anioTramite" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TramiteType", propOrder = {
    "numTramite",
    "anioTramite"
})
public class TramiteType {

    @XmlElement(required = true)
    protected String numTramite;
    protected int anioTramite;

    /**
     * Gets the value of the numTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTramite() {
        return numTramite;
    }

    /**
     * Sets the value of the numTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTramite(String value) {
        this.numTramite = value;
    }

    /**
     * Gets the value of the anioTramite property.
     * 
     */
    public int getAnioTramite() {
        return anioTramite;
    }

    /**
     * Sets the value of the anioTramite property.
     * 
     */
    public void setAnioTramite(int value) {
        this.anioTramite = value;
    }

}

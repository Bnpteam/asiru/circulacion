
package pe.gob.servir.std.service.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.gob.servir.std.service.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetDatosTramiteAIP_QNAME = new QName("ws.service.std.servir.gob.pe", "getDatosTramiteAIP");
    private final static QName _GetDatosTramiteCE_QNAME = new QName("ws.service.std.servir.gob.pe", "getDatosTramiteCE");
    private final static QName _GetDatosTramiteAIPResponse_QNAME = new QName("ws.service.std.servir.gob.pe", "getDatosTramiteAIPResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.gob.servir.std.service.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetDatosTramiteAIPResponse }
     * 
     */
    public GetDatosTramiteAIPResponse createGetDatosTramiteAIPResponse() {
        return new GetDatosTramiteAIPResponse();
    }

    /**
     * Create an instance of {@link GetDatosTramite }
     * 
     */
    public GetDatosTramite createGetDatosTramite() {
        return new GetDatosTramite();
    }

    /**
     * Create an instance of {@link SeguridadType }
     * 
     */
    public SeguridadType createSeguridadType() {
        return new SeguridadType();
    }

    /**
     * Create an instance of {@link AuditoriaType }
     * 
     */
    public AuditoriaType createAuditoriaType() {
        return new AuditoriaType();
    }

    /**
     * Create an instance of {@link TramiteType }
     * 
     */
    public TramiteType createTramiteType() {
        return new TramiteType();
    }

    /**
     * Create an instance of {@link GetDatosTramiteCEResponse }
     * 
     */
    public GetDatosTramiteCEResponse createGetDatosTramiteCEResponse() {
        return new GetDatosTramiteCEResponse();
    }

    /**
     * Create an instance of {@link ResponseDatosTramiteCEType }
     * 
     */
    public ResponseDatosTramiteCEType createResponseDatosTramiteCEType() {
        return new ResponseDatosTramiteCEType();
    }

    /**
     * Create an instance of {@link GetDatosTramiteResponse }
     * 
     */
    public GetDatosTramiteResponse createGetDatosTramiteResponse() {
        return new GetDatosTramiteResponse();
    }

    /**
     * Create an instance of {@link ResponseDatosTramiteType }
     * 
     */
    public ResponseDatosTramiteType createResponseDatosTramiteType() {
        return new ResponseDatosTramiteType();
    }

    /**
     * Create an instance of {@link GetDatosTramiteCE }
     * 
     */
    public GetDatosTramiteCE createGetDatosTramiteCE() {
        return new GetDatosTramiteCE();
    }

    /**
     * Create an instance of {@link GetDatosTramiteAIP }
     * 
     */
    public GetDatosTramiteAIP createGetDatosTramiteAIP() {
        return new GetDatosTramiteAIP();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDatosTramiteAIP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ws.service.std.servir.gob.pe", name = "getDatosTramiteAIP")
    public JAXBElement<GetDatosTramiteAIP> createGetDatosTramiteAIP(GetDatosTramiteAIP value) {
        return new JAXBElement<GetDatosTramiteAIP>(_GetDatosTramiteAIP_QNAME, GetDatosTramiteAIP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDatosTramiteCE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ws.service.std.servir.gob.pe", name = "getDatosTramiteCE")
    public JAXBElement<GetDatosTramiteCE> createGetDatosTramiteCE(GetDatosTramiteCE value) {
        return new JAXBElement<GetDatosTramiteCE>(_GetDatosTramiteCE_QNAME, GetDatosTramiteCE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDatosTramiteAIPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "ws.service.std.servir.gob.pe", name = "getDatosTramiteAIPResponse")
    public JAXBElement<GetDatosTramiteAIPResponse> createGetDatosTramiteAIPResponse(GetDatosTramiteAIPResponse value) {
        return new JAXBElement<GetDatosTramiteAIPResponse>(_GetDatosTramiteAIPResponse_QNAME, GetDatosTramiteAIPResponse.class, null, value);
    }

}

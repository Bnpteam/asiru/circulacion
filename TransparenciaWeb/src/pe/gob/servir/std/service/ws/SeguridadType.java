
package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SeguridadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SeguridadType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codAplicativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codRol" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codRepositorio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeguridadType", propOrder = {
    "codCliente",
    "codAplicativo",
    "codRol",
    "codRepositorio"
})
public class SeguridadType {

    @XmlElement(required = true)
    protected String codCliente;
    @XmlElement(required = true)
    protected String codAplicativo;
    @XmlElement(required = true)
    protected String codRol;
    @XmlElement(required = true)
    protected String codRepositorio;

    /**
     * Gets the value of the codCliente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCliente() {
        return codCliente;
    }

    /**
     * Sets the value of the codCliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCliente(String value) {
        this.codCliente = value;
    }

    /**
     * Gets the value of the codAplicativo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAplicativo() {
        return codAplicativo;
    }

    /**
     * Sets the value of the codAplicativo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAplicativo(String value) {
        this.codAplicativo = value;
    }

    /**
     * Gets the value of the codRol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRol() {
        return codRol;
    }

    /**
     * Sets the value of the codRol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRol(String value) {
        this.codRol = value;
    }

    /**
     * Gets the value of the codRepositorio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRepositorio() {
        return codRepositorio;
    }

    /**
     * Sets the value of the codRepositorio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRepositorio(String value) {
        this.codRepositorio = value;
    }

}

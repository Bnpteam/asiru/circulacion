
package pe.gob.servir.std.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for responseDatosTramiteCEType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="responseDatosTramiteCEType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTramite" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numTramite" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="anioTramite" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FechaHoraTram" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DocumentoTram" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Remitente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Asunto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NumDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="folios" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Observacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaCMS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expedientePertenencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaIngresoTexto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaIngreso" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="remitentePersona" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRemitentePersona" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sumilla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="propietario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="historico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseDatosTramiteCEType", propOrder = {
    "idTramite",
    "numTramite",
    "anioTramite",
    "fechaHoraTram",
    "documentoTram",
    "remitente",
    "asunto",
    "numDocumento",
    "folios",
    "observacion",
    "rutaCMS",
    "expedientePertenencia",
    "fechaIngresoTexto",
    "fechaIngreso",
    "tipoDocumento",
    "idTipoDocumento",
    "remitentePersona",
    "idRemitentePersona",
    "sumilla",
    "propietario",
    "historico"
})
public class ResponseDatosTramiteCEType {

    protected int idTramite;
    @XmlElement(required = true)
    protected String numTramite;
    protected int anioTramite;
    @XmlElement(name = "FechaHoraTram", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaHoraTram;
    @XmlElement(name = "DocumentoTram", required = true)
    protected String documentoTram;
    @XmlElement(name = "Remitente", required = true)
    protected String remitente;
    @XmlElement(name = "Asunto", required = true)
    protected String asunto;
    @XmlElement(name = "NumDocumento", required = true)
    protected String numDocumento;
    protected int folios;
    @XmlElement(name = "Observacion", required = true)
    protected String observacion;
    @XmlElement(required = true)
    protected String rutaCMS;
    protected String expedientePertenencia;
    protected String fechaIngresoTexto;
    @XmlElement(name = "FechaIngreso")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaIngreso;
    protected String tipoDocumento;
    protected int idTipoDocumento;
    protected String remitentePersona;
    protected int idRemitentePersona;
    protected String sumilla;
    protected String propietario;
    protected String historico;

    /**
     * Gets the value of the idTramite property.
     * 
     */
    public int getIdTramite() {
        return idTramite;
    }

    /**
     * Sets the value of the idTramite property.
     * 
     */
    public void setIdTramite(int value) {
        this.idTramite = value;
    }

    /**
     * Gets the value of the numTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTramite() {
        return numTramite;
    }

    /**
     * Sets the value of the numTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTramite(String value) {
        this.numTramite = value;
    }

    /**
     * Gets the value of the anioTramite property.
     * 
     */
    public int getAnioTramite() {
        return anioTramite;
    }

    /**
     * Sets the value of the anioTramite property.
     * 
     */
    public void setAnioTramite(int value) {
        this.anioTramite = value;
    }

    /**
     * Gets the value of the fechaHoraTram property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaHoraTram() {
        return fechaHoraTram;
    }

    /**
     * Sets the value of the fechaHoraTram property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaHoraTram(XMLGregorianCalendar value) {
        this.fechaHoraTram = value;
    }

    /**
     * Gets the value of the documentoTram property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoTram() {
        return documentoTram;
    }

    /**
     * Sets the value of the documentoTram property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoTram(String value) {
        this.documentoTram = value;
    }

    /**
     * Gets the value of the remitente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemitente() {
        return remitente;
    }

    /**
     * Sets the value of the remitente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemitente(String value) {
        this.remitente = value;
    }

    /**
     * Gets the value of the asunto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsunto() {
        return asunto;
    }

    /**
     * Sets the value of the asunto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsunto(String value) {
        this.asunto = value;
    }

    /**
     * Gets the value of the numDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumDocumento() {
        return numDocumento;
    }

    /**
     * Sets the value of the numDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumDocumento(String value) {
        this.numDocumento = value;
    }

    /**
     * Gets the value of the folios property.
     * 
     */
    public int getFolios() {
        return folios;
    }

    /**
     * Sets the value of the folios property.
     * 
     */
    public void setFolios(int value) {
        this.folios = value;
    }

    /**
     * Gets the value of the observacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * Sets the value of the observacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacion(String value) {
        this.observacion = value;
    }

    /**
     * Gets the value of the rutaCMS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaCMS() {
        return rutaCMS;
    }

    /**
     * Sets the value of the rutaCMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaCMS(String value) {
        this.rutaCMS = value;
    }

    /**
     * Gets the value of the expedientePertenencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpedientePertenencia() {
        return expedientePertenencia;
    }

    /**
     * Sets the value of the expedientePertenencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpedientePertenencia(String value) {
        this.expedientePertenencia = value;
    }

    /**
     * Gets the value of the fechaIngresoTexto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaIngresoTexto() {
        return fechaIngresoTexto;
    }

    /**
     * Sets the value of the fechaIngresoTexto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaIngresoTexto(String value) {
        this.fechaIngresoTexto = value;
    }

    /**
     * Gets the value of the fechaIngreso property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * Sets the value of the fechaIngreso property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaIngreso(XMLGregorianCalendar value) {
        this.fechaIngreso = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     */
    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     */
    public void setIdTipoDocumento(int value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the remitentePersona property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemitentePersona() {
        return remitentePersona;
    }

    /**
     * Sets the value of the remitentePersona property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemitentePersona(String value) {
        this.remitentePersona = value;
    }

    /**
     * Gets the value of the idRemitentePersona property.
     * 
     */
    public int getIdRemitentePersona() {
        return idRemitentePersona;
    }

    /**
     * Sets the value of the idRemitentePersona property.
     * 
     */
    public void setIdRemitentePersona(int value) {
        this.idRemitentePersona = value;
    }

    /**
     * Gets the value of the sumilla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSumilla() {
        return sumilla;
    }

    /**
     * Sets the value of the sumilla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSumilla(String value) {
        this.sumilla = value;
    }

    /**
     * Gets the value of the propietario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropietario() {
        return propietario;
    }

    /**
     * Sets the value of the propietario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropietario(String value) {
        this.propietario = value;
    }

    /**
     * Gets the value of the historico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistorico() {
        return historico;
    }

    /**
     * Sets the value of the historico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistorico(String value) {
        this.historico = value;
    }

}

package pe.gob.bnp.dapi.registrousuario.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.model.StreamedContent;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.master.entity.Pais;
import pe.gob.bnp.dapi.domain.master.entity.ParameterTable;
import pe.gob.bnp.dapi.ejb.service.remote.ServiceParameterTableRemote;
import pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;

@ManagedBean(name = "baseControllerMB")
public class BaseControllerExtern implements Serializable {
	private static final long serialVersionUID = 1L;
	private Logger LOGGER = Logger.getLogger(BaseControllerExtern.class);
	private StreamedContent streamedContent;
	protected String nombreSistema;
	protected String versionSistema;

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/ServiceParameterTable!pe.gob.bnp.dapi.ejb.service.remote.ServiceParameterTableRemote")
	private ServiceParameterTableRemote serviceParameterTableRemote;

	@EJB(lookup ="java:global/CirculacionEAR/TransparenciaEJB/PersonaServiceImpl!pe.gob.servir.sistemas.maestros.ejb.service.maestro.inf.remoto.PersonaServiceRemote")				  
	private PersonaServiceRemote personaServiceRemote;
	
	
	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream(Constants.PROPERTIES_FILE));
			return properties.getProperty(key);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Constants.EMPTY_STRING;
	}

	
	public List<ParameterTable> getListParameterTableIdentificationDocumentType(String tableCode, String filter) {
		List<ParameterTable> parametersTable = new ArrayList<ParameterTable>();
		try {
			parametersTable = serviceParameterTableRemote.listParameterTableIdentificationDocumentType(tableCode,filter);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
			parametersTable = new ArrayList<ParameterTable>();
			this.setErrorMessage("Error serviceParameterTableRemote.listParameterTableIdentificationDocumentType: Tabla TBL_PARAMETROS ");
		}
		return parametersTable;
	}
	public List<ParameterTable> getListParameterTableByCode(String tableCode) {
		List<ParameterTable> parametersTable = new ArrayList<ParameterTable>();
		try {
			parametersTable = serviceParameterTableRemote.listParameterTableByCode(tableCode);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
			parametersTable = new ArrayList<ParameterTable>();
			this.setErrorMessage("Error serviceParameterTableRemote.listParameterTableByCode: Tabla TBL_PARAMETROS ");
		}
		return parametersTable;
	}		

	public HttpServletResponse getHttpServletResponse() {			
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();		
		return httpServletResponse;	
	}

	public HttpServletRequest getHttpServletRequest() {			
		HttpServletRequest httpServletRequest= (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		return httpServletRequest;	
	}
		
	public HttpSession getHttpSession() {			
		return this.getHttpServletRequest().getSession(true);	
	}
	
	public Usuario getUsuario() {		
		Usuario usuario= (Usuario)this.getHttpSession().getAttribute("usuario");
//		if (usuario==null) {
//			usuario=new Usuario();
//		}
		return usuario;
	}	
	
	public String validarSesion() {

		if (this.getUsuario()==null) {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			try {
				response.sendRedirect("../../faces/seguridad/login.xhtml");
			} catch (IOException e) {
				LOGGER.error(e);
				e.printStackTrace();
			}	
		}
			
		return "";
	}
	
	public List<Pais> getListPaises() {
		List<Pais> listaPais;
		try {
			listaPais = serviceParameterTableRemote.listCountries();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
			listaPais = new ArrayList<Pais>();
			this.setErrorMessage("Error serviceParameterTableRemote.getListPaises: Tabla Paises ");
		}
		return listaPais;
	}	

	public void setMensaje(String titulo, String mensaje) {
		FacesMessage msg = new FacesMessage(titulo, mensaje);  
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void  setMensajeAviso(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso",mensaje) );               
	}
	
	public void  setMensajeAlerta(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso",mensaje) );               
	}

	public void  setMensajeError(String mensaje) {		
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Aviso",mensaje) );               
	}

	public void setMessage(String title, String message) {
		FacesMessage msg = new FacesMessage(title, message);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void setNotificationMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", message));
	}
	
	public void setAlertMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso",   message));
	}

	public void setErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Aviso", message));
	}

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public void setStreamedContent(StreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}

	public PersonaServiceRemote getPersonaServiceRemote() {
		return personaServiceRemote;
	}

	public void setPersonaServiceRemote(PersonaServiceRemote personaServiceRemote) {
		this.personaServiceRemote = personaServiceRemote;
	}
	public String getNombreSistema() {
		return nombreSistema;
	}
	public void setNombreSistema(String nombreSistema) {
		this.nombreSistema = nombreSistema;
	}
	public String getVersionSistema() {
		return versionSistema;
	}
	public void setVersionSistema(String versionSistema) {
		this.versionSistema = versionSistema;
	}

	
}

package pe.gob.bnp.dapi.registrousuario.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;

import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.common.util.response.ResponseObject;
import pe.gob.bnp.dapi.domain.entity.RegistroVisitaASala;
import pe.gob.bnp.dapi.ejb.service.exception.ServiceException;
import pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote;
import pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote;
import pe.gob.servir.sistemas.transparencia.model.dto.FiltroRegistroVisitaDto;
import pe.gob.servir.systems.util.validator.VO;


@ManagedBean(name = "bandejaRegistroVisitasSalaMB")
@SessionScoped
public class BandejaRegistroVisitasSalaMB extends BaseControllerExtern { // extends
	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(BandejaRegistroVisitasSalaMB.class);

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/SolicitudServiceImpl!pe.gob.servir.sistemas.transparencia.ejb.service.solicitud.inf.remoto.SolicitudServiceRemote")
	private SolicitudServiceRemote solicitudServiceRemote;

	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/RegistroServiceImpl!pe.gob.bnp.dapi.ejb.service.remote.RegistroServiceRemote")
	private RegistroServiceRemote registroServiceRemote;
	

	private String filtroNumeroDocumentoIdentidad;
	private String filtroNombre;
	private String filtroApellidoPaterno;
	private String filtroApellidoMaterno;
	private Date filtroFechaVisitaHasta;
	private Date filtroFechaVisitaDesde;
	
	private List<RegistroVisitaASala> listaRegistroVisitas;
	private Date fechaActual;
	  
	private RegistroVisitaASala usuarioVisitante;
	
	@PostConstruct
	public void init() {
		this.nombreSistema = this.getString(Constants.NOMBRE_SISTEMA_PROPERTIES);
		this.versionSistema = this.getString(Constants.VERSION_SISTEMA_PROPERTIES);

		this.filtroNumeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.filtroNombre = Constants.EMPTY_STRING;
		this.filtroApellidoPaterno = Constants.EMPTY_STRING;
		this.filtroApellidoMaterno = Constants.EMPTY_STRING;
		this.filtroFechaVisitaHasta = new Date();
		this.filtroFechaVisitaDesde = new Date();
		this.fechaActual = new Date();
		this.listaRegistroVisitas = new  ArrayList<RegistroVisitaASala>();
		this.usuarioVisitante = new RegistroVisitaASala();
		//this.loadComboLists();
	}

	
	public void limpiarBandejaConsulta() {
		this.listaRegistroVisitas = null;
		this.filtroNumeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.filtroNombre = Constants.EMPTY_STRING;
		this.filtroApellidoPaterno = Constants.EMPTY_STRING;
		this.filtroApellidoMaterno = Constants.EMPTY_STRING;
		this.filtroFechaVisitaHasta = null;
		this.filtroFechaVisitaDesde = null;
	}
	
	public void realizarConsulta() {
		try {
			int contador = 0;
			String mensaje = Constants.EMPTY_STRING;
			
			if ( this.filtroNumeroDocumentoIdentidad.equals(Constants.ZERO_VALUE_STRING) &&  this.filtroFechaVisitaDesde==null && this.filtroFechaVisitaHasta==null && VO.isEmpty(this.filtroNombre) && VO.isEmpty(this.filtroApellidoPaterno) && VO.isEmpty(this.filtroApellidoMaterno) &&  VO.isEmpty(this.filtroNumeroDocumentoIdentidad) ){
				mensaje = mensaje.equals(Constants.EMPTY_STRING)? "Debe agregar al menos un filtro de b�squeda.": mensaje;
				contador++;
			}
			if (contador > 0) {
				super.setAlertMessage(mensaje);
				return;
			}			
			FiltroRegistroVisitaDto filtro = new FiltroRegistroVisitaDto();
			filtro.setNombre(this.filtroNombre.trim());
			filtro.setApellidoPaterno(this.filtroApellidoPaterno.trim());
			filtro.setApellidoMaterno(this.filtroApellidoMaterno.trim());
			filtro.setNumeroDocumentoIdentidad(this.filtroNumeroDocumentoIdentidad.trim());
			filtro.setFechaVisitaDesde(this.filtroFechaVisitaDesde);
			filtro.setFechaVisitaHasta(this.filtroFechaVisitaHasta);
			this.listaRegistroVisitas = this.registroServiceRemote.listarBandejaRegistroVisitasSala(filtro);
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al realizar consulta de usuarios!");
		}
	}

	public void limpiarBusqueda() { 
		this.filtroNumeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.filtroNombre = Constants.EMPTY_STRING;
		this.filtroApellidoPaterno = Constants.EMPTY_STRING;
		this.filtroApellidoMaterno = Constants.EMPTY_STRING;
		this.filtroFechaVisitaHasta = null;
		this.filtroFechaVisitaDesde = null;
	}
	
	public String registrarVisitaASala() {//nuevos usuarios
		String pagina = "bandejaRegistroVisitaSala";
		try{
			ResponseObject resultado= registroServiceRemote.registrarVisitaASala(this.usuarioVisitante);
			super.setMensajeAlerta(resultado.getMensaje());
			this.realizarConsulta();
			} catch (ServiceException e) {
				super.setMensajeAlerta("Error al guardar registro de usuario!");
				e.printStackTrace();
			}
		return pagina;
	}
	

	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream(Constants.PROPERTIES_FILE));
			return properties.getProperty(key);
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}
		return Constants.EMPTY_STRING;
	}

	public String iniciarRegistroVisitaSala() {
		return "registrarVisitaASala";
	}

	public String goToHomePage() {
		return Constants.PAGE_HOME_INTERN;
	}

	public String goToBandejaSalaUsuarioPage() {
		this.realizarConsulta();
		return Constants.PAGE_BANDEJA_SALA_USUARIO;
	}

	public String getFiltroNumeroDocumentoIdentidad() {
		return filtroNumeroDocumentoIdentidad;
	}

	public void setFiltroNumeroDocumentoIdentidad(
			String filtroNumeroDocumentoIdentidad) {
		this.filtroNumeroDocumentoIdentidad = filtroNumeroDocumentoIdentidad;
	}

	public String getFiltroNombre() {
		return filtroNombre;
	}

	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}

	public String getFiltroApellidoPaterno() {
		return filtroApellidoPaterno;
	}

	public void setFiltroApellidoPaterno(String filtroApellidoPaterno) {
		this.filtroApellidoPaterno = filtroApellidoPaterno;
	}

	public String getFiltroApellidoMaterno() {
		return filtroApellidoMaterno;
	}

	public void setFiltroApellidoMaterno(String filtroApellidoMaterno) {
		this.filtroApellidoMaterno = filtroApellidoMaterno;
	}

	public Date getFiltroFechaVisitaHasta() {
		return filtroFechaVisitaHasta;
	}

	public void setFiltroFechaVisitaHasta(Date filtroFechaVisitaHasta) {
		this.filtroFechaVisitaHasta = filtroFechaVisitaHasta;
	}

	public Date getFiltroFechaVisitaDesde() {
		return filtroFechaVisitaDesde;
	}

	public void setFiltroFechaVisitaDesde(Date filtroFechaVisitaDesde) {
		this.filtroFechaVisitaDesde = filtroFechaVisitaDesde;
	}

	public List<RegistroVisitaASala> getListaRegistroVisitas() {
		return listaRegistroVisitas;
	}

	public void setListaRegistroVisitas(
			List<RegistroVisitaASala> listaRegistroVisitas) {
		this.listaRegistroVisitas = listaRegistroVisitas;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public RegistroVisitaASala getUsuarioVisitante() {
		return usuarioVisitante;
	}

	public void setUsuarioVisitante(RegistroVisitaASala usuarioVisitante) {
		this.usuarioVisitante = usuarioVisitante;
	}
}

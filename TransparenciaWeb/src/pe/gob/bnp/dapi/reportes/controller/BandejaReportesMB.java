package pe.gob.bnp.dapi.reportes.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import pe.gob.bnp.absysnet.domain.entity.dto.FiltroBusquedaReporte;
import pe.gob.bnp.absysnet.domain.entity.dto.ResumenAtencionDto;
import pe.gob.bnp.absysnet.ejb.reportes.remote.ReporteServiceRemote;
import pe.gob.bnp.dapi.common.util.constants.Constants;
import pe.gob.bnp.dapi.domain.entity.HistorialVigencia;
import pe.gob.bnp.dapi.registrousuario.controller.BaseControllerExtern;
import pe.gob.bnp.dapi.salalectura.dto.SalaBiblioteca;
import pe.gob.bnp.dapi.salalectura.dto.UsuarioEnSala;
import pe.gob.servir.sistemas.transparencia.model.administracion.Usuario;
import pe.gob.servir.sistemas.transparencia.util.ReporteUtil;

@ManagedBean(name = "bandejaReportesMB")
@SessionScoped
public class BandejaReportesMB extends BaseControllerExtern {
	@EJB(lookup = "java:global/CirculacionEAR/TransparenciaEJB/ReporteSIRUService!pe.gob.bnp.absysnet.ejb.reportes.remote.ReporteServiceRemote")
	private ReporteServiceRemote reporteService;
	
	private FiltroBusquedaReporte filtroBase;
    private Date fechaActual;
    
    //Reporte registro de usuarios
    private List<HistorialVigencia> listaReporteRegistroUsuarios;
    
    //Reporte resumen de atencion en sala de usuarios
    private List<ResumenAtencionDto> listaReporteResumenAtencionSalaUsuario;
    
    //Reporte detalle de accesos de usuarios a las salas de lectura
    private List<UsuarioEnSala> listaReporteDetalleAccesoaSala;
    
    //Reporte resumen de accesoa a salas de lectura
    private List<ResumenAtencionDto> listaReporteResumenAccesoASala;
    
	private List<SalaBiblioteca> listaSalas;    
	private List<Usuario> listaPersonalDAPI;
    
	@PostConstruct
	public void init() {
		this.filtroBase = new FiltroBusquedaReporte();
		this.versionSistema = this.getString(Constants.VERSION_SISTEMA_PROPERTIES);
		this.nombreSistema = this.getString(Constants.NOMBRE_SISTEMA_PROPERTIES);
		this.fechaActual = new Date();
		this.listaReporteRegistroUsuarios = new ArrayList<>();
		this.listaReporteResumenAtencionSalaUsuario = new ArrayList<>();
		this.listaReporteDetalleAccesoaSala = new ArrayList<>();
		this.listaReporteResumenAccesoASala = new ArrayList<>();
		this.obtenerListasBase();		
	}
	
	public void obtenerListasBase() {
		try {
			this.listaSalas = this.reporteService.listarSalasDisponibles(Constants.ABSYSNET_LECTOR_LECOBI_BNP);
			this.listaPersonalDAPI = this.reporteService.listarPersonalDAPI();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	/**API reporte registro de usuarios - INICIO**/
	
	public void obtenerReporteRegistroUsuario() {
		try {
			if (validarFiltroBusqueReporteRegistroUsuario()){
				this.listaReporteRegistroUsuarios = this.reporteService.obtenerListaReporteRegistroUsuarios(this.filtroBase);
			}
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al realizar la b�squeda");
		}
	}	

	public boolean validarFiltroBusqueReporteRegistroUsuario(){
		String mensaje = "";
		int contador=0;
		if (this.filtroBase.getRangoFechaDesde()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la fecha inicio (Desde).": mensaje;
			contador++;
		}
		if (this.filtroBase.getRangoFechaHasta()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la fecha fin (Hasta).": mensaje;
			contador++;
		}
		if (contador > 0) {
			super.setAlertMessage(mensaje);
			return false;
		}
		return true;
		
	}	
	
	public void limpiarFiltroReporteRegistroUsuario(){
		this.filtroBase.reset();
	}
	
	public void iniciarReporteRegistroUsuario(){
		this.filtroBase.reset();
		this.listaReporteRegistroUsuarios = null;
	}	
	
	public void exportarReporteRegistroUsuario() {
		if (this.listaReporteRegistroUsuarios == null || this.listaReporteRegistroUsuarios.size() <= 0) {
			super.setMensajeAlerta("No hay registros.");
		} else {
			Map<String, Object> beans = new HashMap<>();
			beans.put("reporte", this.listaReporteRegistroUsuarios);

			try {
				ReporteUtil.reporteXLS("/resources/xlsPlantillas/TemplateReporteRegistroUsuario.xls", beans, "ReporteRegistroUsuarios.xls");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}	
	
	/**API reporte registro de usuarios - FIN**/

	/**API reporte resumen de atencion en sala del usuario- INICIO**/
	public boolean validarFiltroBusqueReporteResumenAtencionSalaUsuario(){
		String mensaje = "";
		int contador=0;
		if (this.filtroBase.getRangoFechaDesde()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar el periodo inicio.": mensaje;
			contador++;
		}
		if (this.filtroBase.getRangoFechaHasta()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar el periodo fin.": mensaje;
			contador++;
		}
		if (contador > 0) {
			super.setAlertMessage(mensaje);
			return false;
		}
		return true;
		
	}	
	
	public void obtenerReporteResumenAtencionSalaUsuario() {
		try {
			if (validarFiltroBusqueReporteResumenAtencionSalaUsuario()){
				this.listaReporteResumenAtencionSalaUsuario = this.reporteService.obtenerReporteResumenAtencionSalaUsuario(this.filtroBase);
			}
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al realizar la b�squeda");
		}
	}	
	
	public void limpiarFiltroReporteResumenAtencionSalaUsuario(){
		this.filtroBase.reset();
	}
	
	public void iniciarReporteResumenAtencionSalaUsuario(){
		this.filtroBase.reset();
		this.listaReporteResumenAtencionSalaUsuario = null;
	}		
	
	public void exportarReporteResumenAtencionSalaUsuario() {
		if (this.listaReporteResumenAtencionSalaUsuario == null || this.listaReporteResumenAtencionSalaUsuario.size() <= 0) {
			super.setMensajeAlerta("No hay registros.");
		} else {
			Map<String, Object> beans = new HashMap<>();
			beans.put("reporte", this.listaReporteResumenAtencionSalaUsuario);
			beans.put("periodoInicio", this.filtroBase.getRangoFechaDesde());
			beans.put("periodoFin", this.filtroBase.getRangoFechaHasta());
			try {
				ReporteUtil.reporteXLS("/resources/xlsPlantillas/TemplateReporteResumenAtencionSalaUsuario.xls", beans, "ReporteResumenAtencionSalaUsuario.xls");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}		
	
	/**API reporte resumen de atencion en sala del usuario - FIN**/
	
	/**API reporte detalle de acceso a salas de lectura- INICIO**/

	public void obtenerReporteDetalleAccesosASala() {
		try {
			if (validarFiltroBusqueReporteDetalleAccesosASala()){
				this.listaReporteDetalleAccesoaSala = this.reporteService.obtenerReporteDetalleAccesosASala(this.filtroBase);
			}
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al realizar la b�squeda");
		}
	}	

	public boolean validarFiltroBusqueReporteDetalleAccesosASala(){
		String mensaje = "";
		int contador=0;
		if (this.filtroBase.getRangoFechaDesde()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la fecha inicio (Desde).": mensaje;
			contador++;
		}
		if (this.filtroBase.getRangoFechaHasta()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la fecha fin (Hasta).": mensaje;
			contador++;
		}
//		if (this.filtroBase.getIdSala()==null || this.filtroBase.getIdSala().equals(Constants.ZERO_VALUE_STRING) ){
//			mensaje = mensaje.equals("") ? "Debe seleccionar la Sala.": mensaje;
//			contador++;
//		}		
		if (contador > 0) {
			super.setAlertMessage(mensaje);
			return false;
		}
		return true;
		
	}	
	
	public void limpiarFiltroReporteDetalleAccesosASala(){
		this.filtroBase.reset();
	}
	
	public void iniciarReporteDetalleAccesosASala(){
		this.filtroBase.reset();
		this.listaReporteDetalleAccesoaSala = null;
	}	
	
	public void exportarReporteDetalleAccesosASala() {
		if (this.listaReporteDetalleAccesoaSala == null || this.listaReporteDetalleAccesoaSala.size() <= 0) {
			super.setMensajeAlerta("No hay registros.");
		} else {
			Map<String, Object> beans = new HashMap<>();
			beans.put("reporte", this.listaReporteDetalleAccesoaSala);

			try {
				ReporteUtil.reporteXLS("/resources/xlsPlantillas/TemplateReporteDetalleAccesosASala.xls", beans, "ReporteDetalleAccesosASala.xls");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}	
	
	/**API reporte detalle de acceso a salas de lectura- FIN**/
	
	/**API reporte resumen de atenci�n de usuarios (Control de acceso a salas) - INICIO**/

	public void obtenerReporteResumenAccesosASala() {
		try {
			if (validarFiltroBusqueReporteResumenAccesosASala()){
				this.listaReporteResumenAccesoASala = this.reporteService.obtenerReporteResumenAccesoASala(this.filtroBase);
			}
		} catch (Exception e) {
			e.printStackTrace();
			super.setMensajeAlerta("Error al realizar la b�squeda");
		}
	}	

	public boolean validarFiltroBusqueReporteResumenAccesosASala(){
		String mensaje = "";
		int contador=0;
		if (this.filtroBase.getRangoFechaDesde()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la fecha inicio (Desde).": mensaje;
			contador++;
		}
		if (this.filtroBase.getRangoFechaHasta()==null ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la fecha fin (Hasta).": mensaje;
			contador++;
		}
		if (this.filtroBase.getIdSala()==null || this.filtroBase.getIdSala().equals(Constants.ZERO_VALUE_STRING) ){
			mensaje = mensaje.equals("") ? "Debe seleccionar la Sala.": mensaje;
			contador++;
		}		
		if (contador > 0) {
			super.setAlertMessage(mensaje);
			return false;
		}
		return true;
		
	}	
	
	public void limpiarFiltroReporteResumenAccesosASala(){
		this.filtroBase.reset();
	}
	
	public void iniciarReporteResumenAccesosASala(){
		this.filtroBase.reset();
		this.listaReporteResumenAccesoASala = null;
	}	
	
	public void exportarReporteResumenAccesosASala() {
		if (this.listaReporteResumenAccesoASala == null || this.listaReporteResumenAccesoASala.size() <= 0) {
			super.setMensajeAlerta("No hay registros.");
		} else {
			Map<String, Object> beans = new HashMap<>();
			beans.put("reporte", this.listaReporteResumenAccesoASala);
			beans.put("nombreSala", this.obtenerNombreDeSala(Long.parseLong(this.filtroBase.getIdSala())).toUpperCase());
			beans.put("periodoInicio", this.filtroBase.getRangoFechaDesde());
			beans.put("periodoFin", this.filtroBase.getRangoFechaHasta());

			try {
				ReporteUtil.reporteXLS("/resources/xlsPlantillas/TemplateReporteResumenAtencionSalaLectura.xls", beans, "ReporteResumenAtencionSalaLectura.xls");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}	
	
	public String obtenerNombreDeSala(Long idSala){
		String resultado = Constants.EMPTY_STRING;
		for (SalaBiblioteca sala : listaSalas) {
			if (idSala == sala.getIdSala()){
				return sala.getNombreSala();
			}
		}
		return resultado;
	}
	/**API reporte resumen de atenci�n de usuarios (Control de acceso a salas) - FIN **/	
	
	
	/////////////////////////////////
	public FiltroBusquedaReporte getFiltroBase() {
		return filtroBase;
	}

	public void setFiltroBase(FiltroBusquedaReporte filtroBase) {
		this.filtroBase = filtroBase;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}	

	public List<HistorialVigencia> getListaReporteRegistroUsuarios() {
		return listaReporteRegistroUsuarios;
	}

	public void setListaReporteRegistroUsuarios(
			List<HistorialVigencia> listaReporteRegistroUsuarios) {
		this.listaReporteRegistroUsuarios = listaReporteRegistroUsuarios;
	}

	public List<ResumenAtencionDto> getListaReporteResumenAtencionSalaUsuario() {
		return listaReporteResumenAtencionSalaUsuario;
	}

	public void setListaReporteResumenAtencionSalaUsuario(
			List<ResumenAtencionDto> listaReporteResumenAtencionSalaUsuario) {
		this.listaReporteResumenAtencionSalaUsuario = listaReporteResumenAtencionSalaUsuario;
	}

	public List<UsuarioEnSala> getListaReporteDetalleAccesoaSala() {
		return listaReporteDetalleAccesoaSala;
	}

	public void setListaReporteDetalleAccesoaSala(
			List<UsuarioEnSala> listaReporteDetalleAccesoaSala) {
		this.listaReporteDetalleAccesoaSala = listaReporteDetalleAccesoaSala;
	}

	public List<SalaBiblioteca> getListaSalas() {
		return listaSalas;
	}

	public void setListaSalas(List<SalaBiblioteca> listaSalas) {
		this.listaSalas = listaSalas;
	}

	public List<Usuario> getListaPersonalDAPI() {
		return listaPersonalDAPI;
	}

	public void setListaPersonalDAPI(List<Usuario> listaPersonalDAPI) {
		this.listaPersonalDAPI = listaPersonalDAPI;
	}

	public List<ResumenAtencionDto> getListaReporteResumenAccesoASala() {
		return listaReporteResumenAccesoASala;
	}

	public void setListaReporteResumenAccesoASala(
			List<ResumenAtencionDto> listaReporteResumenAccesoASala) {
		this.listaReporteResumenAccesoASala = listaReporteResumenAccesoASala;
	}

	
}
